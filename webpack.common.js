const path = require('path');

module.exports = {
  name: 'lib',
  entry: ['./src/index'],
  externals: {
    'pdfjs-dist': 'pdfjs-dist',
    // 'socket.io-client': 'socket.io-client',
    // 'bluebird': 'bluebird',
    // 'underscore': 'underscore',
    // 'hammerjs': 'hammerjs',
    // 'ua-parser-js': 'ua-parser-js',
    // 'xhr': 'xhr',
    // 'deepmerge': 'deepmerge',
    // 'uuid': 'uuid',
    // 'node-forge': 'node-forge',
  },
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.ts'],
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './build'),
    library: 'ipushpull',
    libraryTarget: 'umd',
  },
};
