"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getBrowserVersion() {
    var regex = /Chrome\/[0-9.]+/gm;
    var str = navigator ? navigator.userAgent : "";
    var m;
    var version = 0;
    if (!str)
        return version;
    while ((m = regex.exec(str)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
        var result = m[0].split("/");
        version = parseInt(result[1]);
    }
    return version;
}
var LocalStorage = /** @class */ (function () {
    function LocalStorage(config) {
        this.config = config;
        this.prefix = "ipp";
        this.suffix = "";
    }
    LocalStorage.prototype.create = function (key, value) {
        localStorage.setItem(this.makeKey(key), value);
    };
    // Alias
    LocalStorage.prototype.save = function (key, value) {
        return this.create(key, value);
    };
    LocalStorage.prototype.get = function (key, defaultValue) {
        if (defaultValue === void 0) { defaultValue = null; }
        var val = localStorage.getItem(this.makeKey(key));
        if (!val) {
            return defaultValue;
        }
        if (this.isValidJSON(val)) {
            return JSON.parse(val);
        }
        else {
            return val;
        }
    };
    LocalStorage.prototype.remove = function (key) {
        localStorage.removeItem(this.makeKey(key));
    };
    LocalStorage.prototype.makeKey = function (key) {
        if (this.config.storage_prefix) {
            this.prefix = this.config.storage_prefix;
        }
        if (this.prefix && key.indexOf(this.prefix) !== 0) {
            key = this.prefix + "_" + key;
        }
        if (this.suffix) {
            key = key + "_" + this.suffix;
        }
        return key;
    };
    LocalStorage.prototype.isValidJSON = function (val) {
        try {
            var json = JSON.parse(val);
            return true;
        }
        catch (e) {
            return false;
        }
    };
    return LocalStorage;
}());
var CookieStorage = /** @class */ (function () {
    function CookieStorage(config) {
        this.config = config;
        // public config: IConfig;
        this.prefix = "ipp";
        this.suffix = "";
        var domain = document.domain;
        if (document.domain.indexOf("ipushpull") > 0) {
            var domainParts = document.domain.split(".");
            domainParts.splice(0, 1);
            domain = domainParts.join(".");
        }
        this._domain = domain;
    }
    CookieStorage.prototype.create = function (key, value, expireDays, ignorePrefix) {
        var expires = "";
        if (expireDays) {
            var date = new Date();
            date.setTime(date.getTime() + expireDays * 24 * 60 * 60 * 1000);
            expires = "; expires=" + date.toUTCString();
        }
        var path = "path=/;";
        var newKey = ignorePrefix ? key : this.makeKey(key);
        var version = getBrowserVersion();
        var secure = this.isSecure() ? "Secure;" : "";
        var sameSite = (version && version < 80) || !secure ? "" : "SameSite=None; ";
        document.cookie = newKey + "=" + value + expires + "; " + path + " domain=" + this._domain + "; " + sameSite + secure;
    };
    CookieStorage.prototype.save = function (key, value, expireDays, ignorePrefix) {
        this.create(key, value, expireDays, ignorePrefix);
    };
    CookieStorage.prototype.get = function (key, defaultValue, ignorePrefix) {
        key = ignorePrefix ? key : this.makeKey(key);
        var nameEQ = key + "=";
        var ca = document.cookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === " ") {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) === 0) {
                var val = c.substring(nameEQ.length, c.length);
                if (this.isValidJSON(val)) {
                    return JSON.parse(val);
                }
                else {
                    return val;
                }
            }
        }
        return defaultValue;
    };
    CookieStorage.prototype.remove = function (key) {
        this.create(this.makeKey(key), "", -1);
    };
    CookieStorage.prototype.isSecure = function () {
        return window.location.protocol === "https:";
    };
    CookieStorage.prototype.makeKey = function (key) {
        if (this.config.storage_prefix) {
            this.prefix = this.config.storage_prefix;
        }
        if (this.prefix && key.indexOf(this.prefix) !== 0) {
            key = this.prefix + "_" + key;
        }
        if (this.suffix) {
            key = key + "_" + this.suffix;
        }
        return key;
    };
    CookieStorage.prototype.isValidJSON = function (val) {
        try {
            var json = JSON.parse(val);
            return true;
        }
        catch (e) {
            return false;
        }
    };
    return CookieStorage;
}());
var StorageService = /** @class */ (function () {
    function StorageService(ippConfig) {
        // User Storage
        var userStorage = new LocalStorage(ippConfig);
        userStorage.suffix = "GUEST";
        // Global storage
        var globalStorage = new LocalStorage(ippConfig);
        // Persistent storage
        // @todo Should log some warning at least
        var persistentStorage = typeof navigator !== "undefined" && navigator.cookieEnabled
            ? new CookieStorage(ippConfig)
            : new LocalStorage(ippConfig);
        if (ippConfig.storage_prefix) {
            userStorage.prefix = ippConfig.storage_prefix;
            globalStorage.prefix = ippConfig.storage_prefix;
            persistentStorage.prefix = ippConfig.storage_prefix;
        }
        return {
            user: userStorage,
            global: globalStorage,
            persistent: persistentStorage
        };
    }
    StorageService.$inject = ["ippConfig"];
    return StorageService;
}());
exports.StorageService = StorageService;
//# sourceMappingURL=Storage.js.map