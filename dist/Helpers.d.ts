import { IGridContentCellPosition } from "./Grid4/GridCell";
import { IPageContentCellIndex } from "./Page/Content";
export interface IButtonTask {
    actions: any;
    conditions: string[];
}
export interface ICellRange {
    from: IGridContentCellPosition;
    to: IGridContentCellPosition;
}
export interface IHelpers {
    isTouch(): boolean;
    isTouchDevice(): boolean;
    getRefIndex(str: string, obj?: boolean): any;
    cellRange(str: string, rows: number, cols: number): ICellRange;
    normalizeCellSelection(from: IPageContentCellIndex, to: IPageContentCellIndex): any;
    getCellsReference(from: IPageContentCellIndex, to: IPageContentCellIndex, headingSelected?: string): string;
    toColumnName(num: number): string;
    expandCellsReference(str: string, direction: string, rowLen: number, colLen: number): string;
    getUuid(): string;
    getNextValueInArray(value: string | number, arr: string[]): string;
    isNumber(value: any): boolean;
    convertToNumber(value: string): number | null;
    validHex(hex: string): boolean;
    rgbToHex(rgb: string): string;
    componentToHex(c: any): string;
    addEvent(element: HTMLElement | Document | Window, eventName: string, func: any): void;
    removeEvent(element: HTMLElement | Document | Window, eventName: string, func: any): void;
    capitalizeFirstLetter(str: string): string;
    createSlug(str: string): string;
    getContrastYIQ(hexcolor: string): string;
    parseExcelDate(excelTimestamp: number): any;
    parseDateExcel(excelTimestamp: number): any;
    toExcelDate(timestamp: number): any;
    isValidEmail(str: string): boolean;
    convertTime(timestamp: any): string;
    serializeObject(obj: any): string;
    safeTitle(str: string, replaceWith?: string): string;
    safeXmlChars(str: string): string;
    isTarget(el: any, target: any): any;
    formatNumber(n: number, precision?: number): string;
    sentenceCase(str: string): string;
    tableFromArray(arr: any): string;
    getURLParameter(name: string, url: string): string | null;
    arrayDiff(a1: any, a2: any): any;
    getSymphonyStyles(): string[];
    factorsOf(num: number): number[];
    getObjectKeys(obj: any): string[];
    isDarkModeEnabled(): boolean;
    parseURL(str: string): any;
    arrayMove(arr: any, old_index: number, new_index: number): any;
    generatePassword(length: number): string;
    chunkArray(arr: any, n: any): any;
    getNested(obj: any, path: string): any;
    safeSymphonyUrl(str: string): string;
    getScrollbarWidth(parent?: any): number;
    toNumberSuffix(n: number): string;
}
declare class Helpers implements IHelpers {
    letters: string;
    constructor();
    getUuid(): string;
    getNextValueInArray(value: string, arr: string[]): string;
    isNumber(value: any): boolean;
    convertToNumber(value: any): number | null;
    getRefIndex(str: string, obj?: boolean): any;
    toColumnName(num: number): string;
    cellRange(str: string, rows: number, cols: number): ICellRange;
    validHex(hex: string): boolean;
    rgbToHex(rgb: string): string;
    componentToHex(c: any): string;
    addEvent(element: HTMLElement | Document | Window, eventName: string, func: any): void;
    capitalizeFirstLetter(str: string): string;
    /**
     * Remove event from element
     *
     * @param element
     * @param eventName
     * @param func
     * @returns {any}
     */
    removeEvent(element: HTMLElement | Document | Window, eventName: string, func: any): void;
    isTouch(): boolean;
    isTouchDevice(): boolean;
    clickEvent(): string;
    openWindow(link: string, target?: string, params?: {
        [s: string]: string;
    }): Window | null;
    createSlug(str: string): string;
    getScrollbarWidth(parent?: any): number;
    parseExcelDate(excelTimestamp: number): any;
    parseDateExcel(excelTimestamp: number): any;
    toExcelDate(timestamp: number): any;
    getContrastYIQ(hexcolor: string): string;
    convertTime(timestamp: any): string;
    isValidEmail(str: string): boolean;
    serializeObject(obj: any): string;
    getCellsReference(from: IPageContentCellIndex, to: IPageContentCellIndex, headingSelected?: string): string;
    safeTitle(str: string, replaceWith?: string): string;
    safeXmlChars(str: string): string;
    isTarget(el: any, target: any): any;
    formatNumber(n: number, precision?: number): string;
    sentenceCase(str: string): string;
    tableFromArray(arr?: any): string;
    getURLParameter(name: string, url: string): string | null;
    arrayDiff(a1: any, a2: any): any;
    expandCellsReference(str: string, direction: string, rowLen: number, colLen: number): string;
    downloadFile(contentType: string, name: string, data: any): void;
    factorsOf(num: number): number[];
    getSymphonyStyles(): string[];
    /**
     * Returns array of keys in an object
     *
     * @param obj
     * @returns {Array}
     */
    getObjectKeys(obj: any): string[];
    /**
     * Deep copy of object
     *
     * @todo Should be using angular clone..
     *
     * @param o
     * @returns {any}
     */
    cloneObject(o: any): any;
    isDarkModeEnabled(): boolean;
    parseURL(str: string): false | {
        protocol: string;
        hostname: string;
        port: string;
        pathname: string;
        search: string;
        hash: string;
        host: string;
        url: string;
    };
    arrayMove(arr: any, old_index: number, new_index: number): any;
    generatePassword(len: number): string;
    chunkArray(arr: any, n: any): any;
    getNested(obj: any, path: string): any;
    safeSymphonyUrl(str: string): string;
    toNumberSuffix(n: number): string;
    normalizeCellSelection(from: IPageContentCellIndex, to: IPageContentCellIndex): any;
}
export default Helpers;
