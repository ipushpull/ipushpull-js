import { IPageContent, IPageCellStyle, IPageContent2 } from "./Page/Content";
import { IPageColumnStyles } from "./Page/Page";
export interface IUtils {
    parseApiError: (err: any, def?: string) => string;
    clonePageContent(content: IPageContent, removeStyles?: boolean): IPageContent;
    comparePageContent(currentContent: any, newContent: any, ignore_styles?: boolean): IUtilsDiff | false;
    mergePageContent(_original: any, _deltas?: any, style?: any): any;
    mergePageContent2(original: IPageContent, deltas: IPageContent): any;
    getDefaultCellStyle(): IPageCellStyle;
    transformStyle(style: any, l2s?: boolean): IPageContent;
    transformContent(content: any): IPageContent;
}
export interface IUtilsDiff {
    content: IPageContent;
    content_diff: IPageContent;
    colDiff: number;
    rowDiff: number;
    sizeDiff: boolean;
}
declare class Utils implements IUtils {
    constructor();
    static readonly DEFAULT_CELL_STYLE: IPageCellStyle;
    parseApiError(err: any, def?: string): string;
    clonePageContent(content: IPageContent, removeStyles?: boolean): IPageContent;
    comparePageContent(currentContent: any, newContent: any, ignore_styles?: boolean): IUtilsDiff | false;
    mergePageContent(_original: any, _deltas?: any, cellStyle?: IPageCellStyle | IPageColumnStyles): any;
    mergePageContent2(original: IPageContent, deltas: IPageContent): any;
    getDefaultCellStyle(): IPageCellStyle;
    cellStyleMap(): any;
    cellStyleValueMap(): any;
    transformStyle(style: any, l2s?: boolean): any;
    transformContent(rawContent: IPageContent2): IPageContent;
}
export default Utils;
