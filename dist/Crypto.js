"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var forge = __importStar(require("node-forge"));
var Crypto = /** @class */ (function () {
    function Crypto() {
    }
    Crypto._instance = function () {
        return new Crypto();
    };
    /**
     * Decrypt page content using encryption key and encrypted string
     *
     * @param key
     * @param data
     * @returns {any}
     */
    Crypto.prototype.decryptContent = function (key, data) {
        if (!this.libCheck()) {
            return null;
        }
        if (!data)
            return null;
        var rawData = forge.util.decode64(data);
        var iv = rawData.substring(0, 16);
        var cleanData = rawData.substring(16);
        // cleanData = forge.util.createBuffer(cleanData, 'utf8');
        // iv = forge.util.createBuffer(iv, 'utf8');
        var decipher = forge.cipher.createDecipher('AES-CBC', this.hashPassphrase(key.passphrase));
        decipher.start({ iv: forge.util.createBuffer(iv) });
        decipher.update(forge.util.createBuffer(cleanData));
        decipher.finish();
        var decrypted;
        try {
            decrypted = JSON.parse(decipher.output.toString());
        }
        catch (e) {
            decrypted = null;
        }
        return decrypted;
    };
    /**
     * Encrypt raw page content with supplied encryption key
     *
     * @param key
     * @param data
     * @returns {any}
     */
    Crypto.prototype.encryptContent = function (key, data) {
        if (!this.libCheck()) {
            return null;
        }
        var readyData = JSON.stringify(data); // Stringify JS object data
        var hash = this.hashPassphrase(key.passphrase);
        var iv = forge.random.getBytesSync(16);
        var cipher = forge.cipher.createCipher('AES-CBC', hash);
        cipher.start({ iv: iv });
        cipher.update(forge.util.createBuffer(readyData));
        cipher.finish();
        var encrypted = cipher.output;
        var buffer = forge.util.createBuffer();
        buffer.putBytes(iv);
        buffer.putBytes(encrypted.bytes());
        var output = buffer.getBytes();
        return forge.util.encode64(output);
    };
    /**
     * Use forge library"s util to hash passphrase
     *
     * @param passphrase
     * @returns {any}
     */
    Crypto.prototype.hashPassphrase = function (passphrase) {
        var md = forge.md.sha256.create();
        md.update(passphrase);
        return md.digest().bytes();
    };
    Crypto.prototype.libCheck = function () {
        // Check if forge library is loaded. We do this because the library is huge and we dont want to package it together
        if (typeof forge === 'undefined') {
            console.error('[iPushPull]', 'If you want to use encryption make sure you include forge library in your header or use ng-ipushpull-standalone.min.js');
        }
        return typeof forge !== 'undefined';
    };
    return Crypto;
}());
exports.Crypto = Crypto;
//# sourceMappingURL=Crypto.js.map