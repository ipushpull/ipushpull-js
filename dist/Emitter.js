"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter = /** @class */ (function () {
    function Emitter() {
        var _this = this;
        this.listeners = [];
        this.onListeners = {};
        this.on = function (name, callback) {
            if (!_this.onListeners[name]) {
                _this.onListeners[name] = [];
            }
            _this.onListeners[name].push(callback);
        };
        this.off = function (name, callback) {
            if (!_this.onListeners[name]) {
                return;
            }
            var i = _this.onListeners[name].indexOf(callback);
            if (i > -1) {
                _this.onListeners[name].splice(i, 1);
            }
        };
        this.register = function (callback) {
            _this.listeners.push(callback);
        };
        return;
    }
    Emitter.prototype.unRegister = function (callback) {
        var i = this.listeners.indexOf(callback);
        if (i > -1) {
            this.listeners.splice(i, 1);
        }
    };
    Emitter.prototype.emit = function (name, args) {
        this.listeners.forEach(function (cb) {
            cb(name, args);
        });
        if (this.onListeners[name]) {
            var length_1 = this.onListeners[name].length * 1;
            for (var i = 0; i < length_1; i++) {
                if (this.onListeners[name] && this.onListeners[name][i]) {
                    this.onListeners[name][i](args);
                }
            }
        }
    };
    Emitter.prototype.removeEvent = function () {
        this.listeners = [];
        this.onListeners = {};
    };
    Emitter.prototype.removeEvents = function () {
        this.removeEvent();
    };
    return Emitter;
}());
exports.default = Emitter;
//# sourceMappingURL=Emitter.js.map