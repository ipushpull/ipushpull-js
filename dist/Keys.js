"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var deepmerge_1 = __importDefault(require("deepmerge"));
var KeysService = /** @class */ (function () {
    function KeysService(storage) {
        this.storage = storage;
        this.data = {};
        this.cookieName = 'encryption_keys';
        // this.data = storage.global.get(this.cookieName, {});
        this.load();
    }
    KeysService.prototype.saveKey = function (domainName, key) {
        if (!this.data[domainName]) {
            this.data[domainName] = {};
        }
        this.data[domainName][key.name] = key.passphrase;
        this.updateKeys();
    };
    KeysService.prototype.getKey = function (domainName, name) {
        // console.log('getKey', this.data);
        this.load();
        if (!this.data[domainName] || !this.data[domainName][name]) {
            return {
                name: '',
                passphrase: ''
            };
        }
        return {
            name: name,
            passphrase: this.data[domainName][name]
        };
    };
    KeysService.prototype.updateKeys = function () {
        var data = this.storage.global.get(this.cookieName, {});
        data = Object.assign(data, this.data);
        this.storage.global.create(this.cookieName, JSON.stringify(data));
        console.log('updateKeys', data);
    };
    KeysService.prototype.getKeys = function (domainName) {
        if (!this.data) {
            return {};
        }
        if (typeof domainName !== 'undefined') {
            return this.data[domainName] || {};
        }
        return this.data;
    };
    KeysService.prototype.load = function () {
        this.data = this.storage.global.get(this.cookieName, {});
    };
    KeysService.prototype.removeKey = function (domain, keyName) {
        if (!this.data[domain] || !this.data[domain][keyName]) {
            return;
        }
        delete this.data[domain][keyName];
        this.updateKeys();
    };
    KeysService.prototype.mergeData = function (data) {
        var _this = this;
        this.load();
        if (!Object.keys(data).length)
            return false;
        Object.keys(data).forEach(function (domain) {
            _this.data[domain] = deepmerge_1.default(_this.data[domain], data[domain]);
        });
        this.updateKeys();
        return true;
    };
    KeysService.prototype.reset = function () {
        this.storage.global.remove(this.cookieName);
        this.load();
    };
    return KeysService;
}());
exports.default = KeysService;
//# sourceMappingURL=Keys.js.map