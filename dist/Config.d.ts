export interface IIPPConfig {
    api_url?: string;
    api_version?: string;
    ws_url?: string;
    web_url?: string;
    app_url?: string;
    embed_url?: string;
    docs_url?: string;
    billing_url?: string;
    api_key: string;
    api_secret: string;
    transport?: string;
    storage_prefix?: string;
    cookie?: any;
    client_version?: any;
    uuid?: any;
    hsts?: boolean;
    maximum_pasted_cells?: number;
    pdfjs?: IConfigPDFJS;
    settings?: any;
    [key: string]: any;
}
export interface IConfigPDFJS {
    worker: string;
    load: string[];
}
export interface IConfig {
    set(config: IIPPConfig): void;
    api_url: string;
    api_version: string;
    ws_url: string;
    web_url: string;
    app_url: string;
    embed_url: string;
    docs_url: string;
    chat_url: string;
    help_url: string;
    auth_url: string;
    billing_url: string;
    api_key: string;
    api_secret: string;
    transport: string;
    storage_prefix: string;
    cookie: any;
    client_version: any;
    uuid: any;
    hsts: boolean;
    maximum_pasted_cells: number;
    pdfjs: IConfigPDFJS;
    settings: any;
}
export declare class Config implements IConfig {
    private _config;
    pdfjs: IConfigPDFJS;
    set(config: IIPPConfig): void;
    readonly api_url: any;
    readonly api_version: any;
    readonly ws_url: any;
    readonly web_url: any;
    readonly app_url: any;
    readonly embed_url: any;
    readonly docs_url: any;
    readonly chat_url: any;
    readonly help_url: any;
    readonly billing_url: any;
    readonly auth_url: any;
    readonly api_key: any;
    readonly api_secret: any;
    readonly transport: any;
    readonly storage_prefix: any;
    readonly cookie: any;
    readonly uuid: any;
    readonly client_version: any;
    readonly hsts: any;
    readonly maximum_pasted_cells: any;
    readonly settings: any;
    $get(): IIPPConfig;
}
