import { IAuthService } from './Auth';
import { IApiService } from './Api';
export declare class UsageWrap {
    constructor(ippApi: IApiService, ippAuth: IAuthService);
}
export interface IUsage {
    load(folderId?: number): any;
}
export default class Usage implements IUsage {
    private api;
    private auth;
    static $inject: string[];
    folders: any[];
    filteredFolders: any[];
    folderFilter: any;
    pages: any[];
    filteredPages: any[];
    pageFilter: any;
    users: any[];
    filteredUsers: any[];
    userFilter: any;
    stats: any;
    orgStats: any;
    indicator: string;
    type: string;
    output: string;
    threshold: any;
    sortBy: any;
    date: any;
    datePicker: any;
    statTotals: any;
    statTotalsByFolder: any;
    private folderId;
    private statDefault;
    private q;
    constructor(api: IApiService, auth: IAuthService);
    load(folderId?: number): any;
    setSortBy(which: string, type: string, value: string | number): void;
    queryFolders(): void;
    queryPages(): void;
    queryUsers(): void;
    convertNumber(indicator: string, value: any): string;
    pageFolderPercentage(type: string, indicator: string, value: number): string;
    cellClass(stat: number, folderId?: number): string;
    cellValue(pushes: any, folderId?: number): string;
    private emptyAsync;
}
