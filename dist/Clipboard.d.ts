import Emitter from './Emitter';
export interface IClipboard {
    ON_DATA: string;
    ON_PASTED: string;
    init(element: any, doc: boolean): boolean;
    copyTextToClipboard(text: string, el?: string): boolean;
    destroy(): void;
}
export declare class Clipboard extends Emitter implements IClipboard {
    readonly ON_DATA: string;
    readonly ON_PASTED: string;
    editableAreaId: string;
    editableAreaEl: any;
    focus: boolean;
    clipboardTextPlain: string;
    private key;
    private doc;
    private validStyles;
    /**
     * Linking names of excel/json styles to css styles
     */
    private excelStyles;
    /**
     * Map excel border styles to css border styles (with some compromise)
     */
    private excelBorderStyles;
    /**
     * Map excel border weights to css border weights (with some compromise)
     */
    private excelBorderWeights;
    private borderSides;
    private borderSyles;
    private helpers;
    private utils;
    private canvas;
    constructor(element?: any, doc?: boolean);
    init(element?: any, doc?: boolean): boolean;
    destroy(): void;
    copyTextToClipboard(text: string, el?: string): boolean;
    private selectElContents;
    private onEditFocus;
    private onEditBlur;
    private onPasteDocument;
    private getClipboardText;
    private onPaste;
    private createPastedElement;
    private getHtml;
    private onKeyDown;
    private parseText;
    private parseTable;
    private collectClipData;
    private tableColWidths;
    private getRawValue;
    private cssToStyles;
    private mapExcelBorder;
    private getTextWidth;
}
