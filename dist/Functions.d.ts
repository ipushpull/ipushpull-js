import Emitter from './Emitter';
import { IPageContentCell, IPageContent } from './Page/Content';
import { ITask, ITaskActionSet, ITaskActionSetAction } from './Actions/Tasks';
import { IActionsButton } from './Actions/Buttons';
import { IActionsStyle } from './Actions/Styles';
import { IGridCell } from './Grid4/GridCell';
import { IPageColumnDefs, IPage, IUserPageAccess } from './Page/Page';
import { IUserSelf } from './Auth';
interface IFunctionsCellUpdate {
    row: number;
    col: number;
    data: IPageContentCell;
}
export interface IFunctionsVar {
    id?: any;
    username?: string;
    lastname?: string;
    firstname?: string;
    email?: string;
    pageid?: string;
    pagename?: string;
    pagedescription?: string;
    folderid?: string;
    foldername?: string;
    folderdescription?: string;
    [key: string]: any;
}
export interface IFunctionsTable {
    values: any;
    headers: string[];
}
export interface IFunctions {
    cell: IPageContentCell;
    content: IPageContent;
    vars: IFunctionsVar;
    selectedRows: number[];
    columnDefs: IPageColumnDefs[];
    setVars(user: IUserSelf, page: IPage, access?: IUserPageAccess): void;
    updateVars(vars: IFunctionsVar): void;
    updateContentDelta(content: IPageContent): void;
    parse(str: string, joinWith?: string): string;
    getCellReference(str: string): IPageContentCell;
    setCellReference(cell: IPageContentCell): void;
    isButtonWithinTaskRange(buttonCell: IGridCell, taskGroup: ITask): boolean;
    isTaskValid(task: ITaskActionSet): boolean;
    getCellUpdate(action: ITaskActionSetAction): IFunctionsCellUpdate | null;
    updateCell(action: ITaskActionSetAction): IFunctionsCellUpdate | null;
    getCellFormatting(styles: IActionsStyle[], buttons?: IActionsButton[], contentDiff?: IPageContent): IActionsStyle[];
    setCellFormatting(styles: IActionsStyle[], buttons?: IActionsButton[], contentDiff?: IPageContent): IActionsStyle[];
    toUppercaseFunctions(expression: string): string;
    getTable(config: IFunctionsTable): string;
}
export declare class Functions extends Emitter implements IFunctions {
    content: IPageContent;
    vars: IFunctionsVar;
    selectedRows: number[];
    columnDefs: IPageColumnDefs[];
    cell: IPageContentCell;
    private helpers;
    private parser;
    constructor(content?: IPageContent, vars?: IFunctionsVar);
    setVars(user: IUserSelf, page: IPage, access?: IUserPageAccess): void;
    updateVars(vars: IFunctionsVar): void;
    updateContentDelta(content?: IPageContent): void;
    parse(str: string, joinWith?: string): string;
    getCellReference(str: string): IPageContentCell;
    setCellReference(cell: IPageContentCell): void;
    isButtonWithinTaskRange(buttonCell: IGridCell, taskGroup: ITask): boolean;
    isTaskValid(task: ITaskActionSet): boolean;
    getCellUpdate(action: ITaskActionSetAction): IFunctionsCellUpdate | null;
    updateCell(action: ITaskActionSetAction): IFunctionsCellUpdate | null;
    getCellFormatting(styles: IActionsStyle[], buttons?: IActionsButton[], contentDiff?: IPageContent): IActionsStyle[];
    setCellFormatting(styles: IActionsStyle[], buttons?: IActionsButton[], contentDiff?: IPageContent): IActionsStyle[];
    getTable(config: IFunctionsTable): string;
    toUppercaseFunctions(expression: string): string;
    private getCellValue;
    private getColCell;
    private getMinMax;
}
export {};
