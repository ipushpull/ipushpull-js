import { IApiService } from './Api';
import { IConfig } from './Config';
import { IStorageService } from './Storage';
export interface IBilling {
    setCustomerId(id: string): void;
    self(): any;
    getCard(): any;
    createCard(data: any): any;
    getCustomerInfo(): any;
    updateCustomerInfo(data: any): any;
    createCustomer(data: any): any;
    getCustomerSubscription(): any;
    deleteCustomerSubscription(data: any): any;
    getInvoices(): any;
    getUpcomingInvoices(): any;
    downloadInvoiceUrl(invoiceId: string): any;
    getPlan(): any;
    getUsage(): any;
}
declare class Billing implements IBilling {
    private config;
    private api;
    private storage;
    customerInfo: any;
    cardInfo: any;
    customerCard: any;
    invoices: any;
    upcomingInvoices: any;
    planInfo: any;
    constructor(config: IConfig, api: IApiService, storage: IStorageService);
    setCustomerId(id: string): void;
    self(): any;
    getCard(): any;
    createCard(data: any): any;
    getCustomerInfo(): any;
    updateCustomerInfo(data: any): any;
    createCustomer(data: any): any;
    getCustomerSubscription(): any;
    deleteCustomerSubscription(data: any): any;
    getInvoices(): any;
    getInvoice(id: string): any;
    getUpcomingInvoices(): any;
    downloadInvoiceUrl(invoiceId: string): any;
    getPlan(): any;
    getUsage(): any;
    createIntent(data: any): any;
    private fetch;
}
export default Billing;
