import Emitter, { IEmitter } from '../Emitter';
import { IPageContentCell } from '../Page/Content';
import { IGridSelection, IGridCells } from './Grid';
import { IGridCell, IGridCellDetails } from './GridCell';
export interface IGridEvents extends IEmitter {
    CELL: string;
    BLUR: string;
    FOCUS: string;
    NO_CELL: string;
    CELL_LINK_CLICKED: string;
    CELL_DOUBLE_CLICKED: string;
    CELL_CLICKED: string;
    CELL_VALUE: string;
    CELL_SELECTED: string;
    CELL_RESET: string;
    CELL_CHECKED: string;
    CELL_OPTION: string;
    COPY: string;
    EDITING: string;
    RESIZE: string;
    RESIZING: string;
    RESIZED: string;
    COLUMNS_MOVED: string;
    DELETE: string;
    SELECTED_ALL: string;
    ERROR: string;
    COLUMN_FILTER: string;
    COLUMN_FILTER_FOCUS: string;
    emitCellSelected(data: IGridEventCellSelected): void;
}
declare class GridEvents extends Emitter implements IGridEvents {
    constructor();
    readonly CELL_CLICKED: string;
    readonly BLUR: string;
    readonly FOCUS: string;
    readonly NO_CELL: string;
    readonly CELL_HISTORY_CLICKED: string;
    readonly CELL_LINK_CLICKED: string;
    readonly CELL_DOUBLE_CLICKED: string;
    readonly CELL_VALUE: string;
    readonly CELL_SELECTED: string;
    readonly CELL_RESET: string;
    readonly CELL_CHECKED: string;
    readonly CELL_OPTION: string;
    readonly EDITING: string;
    readonly RESIZE: string;
    readonly RESIZING: string;
    readonly RESIZED: string;
    readonly COLUMNS_MOVED: string;
    readonly COLUMN_FILTER: string;
    readonly COLUMN_FILTER_FOCUS: string;
    readonly DELETE: string;
    readonly SELECTED_ALL: string;
    readonly CELL: string;
    readonly COPY: string;
    readonly INTERNAL_CELL_SELECTED: string;
    readonly ERROR: string;
    emitCellSelected(data: IGridEventCellSelected): void;
}
export interface IGridEventEditing {
    content: IPageContentCell;
    cell: IGridCell;
    editing: boolean;
    dirty: boolean;
}
export interface IGridEventCellClicked {
    cell: IGridCellDetails;
    content: IPageContentCell;
    event: MouseEvent;
    rightClick: boolean;
    historyClick: boolean;
    heading: boolean;
}
export interface IGridEventCellChecked {
    cell: IGridCellDetails;
    content?: IPageContentCell;
    checked: boolean;
    value: any;
}
export interface IGridEventCellSelection {
    from: IGridCellDetails;
    to: IGridCellDetails;
    heading: boolean;
    inside: boolean;
    reference: string;
    keyboard: boolean;
    type: string;
}
export interface IGridEventCellSelected {
    reference: string;
    selection: IGridEventCellSelection;
    rightClick: boolean;
    event: MouseEvent | KeyboardEvent | TouchEvent;
    which: string;
    count: number;
    multiple: boolean;
    colFrom?: number;
    colTo?: number;
    rowFrom?: number;
    rowTo?: number;
}
export interface IGridEventCopy {
}
export interface IGridEventDelete {
    cells: IGridCells;
    selection: IGridSelection;
    event: KeyboardEvent;
}
export interface IGridEventCellValue {
    cell: IGridCell;
    content: IPageContentCell;
    value: string | number | boolean;
}
export interface IGridEventResized {
    index: number;
    which: string;
    size: number;
}
export interface IGridEventCellChecked {
    cell: IGridCellDetails;
    content?: IPageContentCell;
    checked: boolean;
    value: any;
}
export interface IGridEventCellOption {
    cell: IGridCellDetails;
    content?: IPageContentCell;
    option: string;
}
export interface IGridEventError {
    message: string;
}
export default GridEvents;
