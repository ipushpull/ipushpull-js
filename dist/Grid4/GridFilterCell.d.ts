import Emitter, { IEmitter } from '../Emitter';
import { IPageContentLink, IPageContentCell, IPageContentCellIndex } from '../Page/Content';
import { IActionsButton } from '../Actions/Buttons';
import { IGridPage } from './GridPage';
import { IActionsStyleConditionStyle } from '../Actions/Styles';
import { IGridEvents } from './GridEvents';
import { IGridSelection, IGridFilter } from './Grid';
export interface IGridCellDataset {
    x: number;
    y: number;
    width: number;
    height: number;
    overflow: number;
    row: number;
    col: number;
    [key: string]: any;
}
export interface IGridContentCellPosition {
    row: number;
    col: number;
    [key: string]: number;
}
export interface IGridCellEvent {
    evt: any;
    cell: IGridFilterCell;
    value: string;
    done: boolean;
}
export interface IGridFilterCellDetails {
    content: IPageContentCell;
    row: number;
    col: number;
    position: IGridContentCellPosition;
    index: IGridContentCellPosition;
    dataset: IGridCellDataset;
    reference: string;
    sticky: boolean;
}
export interface IGridFilterCell extends IEmitter {
    Page: IGridPage;
    Events: IGridEvents;
    row: number;
    rowIndex: number;
    col: number;
    init: boolean;
    cell: HTMLDivElement;
    dataset: IGridCellDataset;
    reference: string;
    content: IPageContentCell;
    hidden: boolean;
    position: IPageContentCellIndex;
    index: IGridContentCellPosition;
    type: string;
    create(): void;
    update(row: number, col: number, force?: boolean): void;
    refresh(): void;
    clear(): void;
    setFreeze(n: boolean): void;
    setSelected(selection: IGridSelection): void;
    setSorting(direction: string): void;
    setValue(filter: IGridFilter): void;
}
export declare class GridFilterCell extends Emitter implements IGridFilterCell {
    Page: IGridPage;
    Events: IGridEvents;
    row: number;
    col: number;
    type: string;
    init: boolean;
    sticky: boolean;
    selected: boolean;
    dataset: IGridCellDataset;
    button: IActionsButton | null;
    link: IPageContentLink | null;
    style: IActionsStyleConditionStyle | null;
    reference: string;
    hidden: boolean;
    position: IPageContentCellIndex;
    index: IGridContentCellPosition;
    cell: HTMLDivElement;
    handle: HTMLDivElement;
    cellFilterInput: HTMLInputElement;
    cellFilterDropdown: HTMLDivElement;
    editing: boolean;
    content: IPageContentCell;
    private borders;
    private highlightTimer;
    private oldValue;
    private _rowIndex;
    private buttonPopper;
    private selectedOption;
    private done;
    private editValue;
    private moving;
    private sorting;
    private canSort;
    startX: number;
    offsetX: number;
    startY: number;
    offsetY: number;
    zIndex: string;
    private width;
    private height;
    constructor(Page: IGridPage, Events: IGridEvents, row: number, col: number, type: string);
    rowIndex: number;
    create(): void;
    update(row: number, col: number): void;
    refresh(): void;
    getDetails(): IGridFilterCellDetails;
    setFreeze(n: boolean): void;
    setSelected(selection: IGridSelection): void;
    setSorting(direction: string): void;
    clear(): void;
    setValue(filter: IGridFilter): void;
    private onMouse;
    private onMouseUpSorting;
    private onMouseMoveSorting;
    private onMouseDown;
    private onMouseUp;
    private onMouseMove;
}
