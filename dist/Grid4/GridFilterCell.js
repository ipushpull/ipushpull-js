"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Helpers_1 = __importDefault(require("../Helpers"));
var Emitter_1 = __importDefault(require("../Emitter"));
var helpers = new Helpers_1.default();
var GridFilterCell = /** @class */ (function (_super) {
    __extends(GridFilterCell, _super);
    function GridFilterCell(Page, Events, row, col, type) {
        var _this = _super.call(this) || this;
        _this.Page = Page;
        _this.Events = Events;
        _this.row = row;
        _this.col = col;
        _this.type = type;
        // public Page!: IGridPage;
        // public Events!: IGridEvents;
        // public type: string = '';
        _this.init = false;
        _this.sticky = false;
        _this.selected = false;
        _this.dataset = {
            x: 0,
            y: 0,
            width: 0,
            height: 0,
            overflow: 0,
            row: 0,
            col: 0
        };
        _this.button = null;
        _this.link = null;
        _this.style = null;
        _this.reference = '';
        _this.hidden = false;
        _this.position = {
            row: -1,
            col: -1
        };
        _this.index = {
            row: -1,
            col: -1
        };
        _this.editing = false;
        _this.borders = {
            widths: {
                none: 0,
                thin: 1,
                medium: 2,
                thick: 3
            },
            styles: {
                none: 'none',
                solid: 'solid',
                double: 'double',
                dash: 'dashed'
            },
            names: {
                t: 'top',
                r: 'right',
                b: 'bottom',
                l: 'left'
            }
        };
        _this.highlightTimer = null;
        _this.oldValue = '';
        _this._rowIndex = 0;
        _this.selectedOption = 0;
        _this.done = false;
        _this.moving = false;
        _this.sorting = false;
        _this.canSort = false;
        _this.startX = 0;
        _this.offsetX = 0;
        _this.startY = 0;
        _this.offsetY = 0;
        _this.zIndex = '';
        _this.width = 0;
        _this.height = 0;
        _this.onMouse = function (evt) {
            if (_this.moving)
                return;
            if (_this.selected) {
                if (_this.canSort && evt.type == 'mousedown' && !_this.Page.isRightClick(evt)) {
                    _this.startX = evt.x;
                    _this.startY = evt.y;
                    _this.sorting = true;
                    window.addEventListener('mouseup', _this.onMouseUpSorting);
                    window.addEventListener('mousemove', _this.onMouseMoveSorting);
                    evt.stopPropagation();
                    _this.Events.emit(_this.Events.CELL, {
                        evt: { type: 'sort' },
                        cell: _this,
                        data: {
                            startX: _this.startX,
                            offsetX: _this.offsetX,
                            startY: _this.startY,
                            offsetY: _this.offsetY,
                        }
                    });
                    return;
                }
            }
            else {
                // if (evt.type == 'mousedown' && !this.Page.structured) {
                //   this.canSort = true;
                // }
            }
            _this.Events.emit(_this.Events.CELL, { evt: evt, cell: _this });
            evt.stopPropagation();
        };
        _this.onMouseUpSorting = function (evt) {
            console.log('onMouseUpSorting');
            _this.sorting = false;
            window.removeEventListener('mouseup', _this.onMouseUpSorting);
            window.removeEventListener('mousemove', _this.onMouseMoveSorting);
            evt.stopPropagation();
            _this.Events.emit(_this.Events.CELL, {
                evt: { type: 'sorted' },
                cell: _this
            });
        };
        _this.onMouseMoveSorting = function (evt) {
            _this.offsetX = evt.x - _this.startX;
            _this.offsetY = evt.y - _this.startY;
            _this.Events.emit(_this.Events.CELL, {
                evt: { type: 'sorting' },
                cell: _this,
                data: {
                    startX: _this.startX,
                    offsetX: _this.offsetX,
                    startY: _this.startY,
                    offsetY: _this.offsetY,
                }
            });
            evt.stopPropagation();
        };
        _this.onMouseDown = function (evt) {
            if (evt.target !== _this.handle) {
                return;
            }
            window.addEventListener('mouseup', _this.onMouseUp);
            window.addEventListener('mousemove', _this.onMouseMove);
            _this.moving = true;
            _this.width = _this.dataset.width = _this.Page.colWidths[_this.col]; // this.dataset.width;
            _this.height = _this.dataset.height = _this.Page.rowHeights[_this.row]; // this.dataset.height;
            _this.startX = evt.x;
            _this.startY = evt.y;
            evt.stopPropagation();
            _this.Events.emit(_this.Events.CELL, {
                evt: { type: 'resize' },
                cell: _this,
                data: {
                    width: _this.dataset.width,
                    height: _this.dataset.height
                }
            });
            // this.zIndex = this.element.style.getPropertyValue('z-index');
            // this.element.style.setProperty('z-index', `${parseFloat(this.zIndex) * 1000}`);
            // this.element.classList.add('active');
        };
        _this.onMouseUp = function (evt) {
            if (!_this.moving) {
                return;
            }
            window.removeEventListener('mouseup', _this.onMouseUp);
            window.removeEventListener('mousemove', _this.onMouseMove);
            _this.moving = false;
            // this.element.style.setProperty('z-index', `${parseFloat(this.zIndex) * 1000}`);
            // this.element.classList.remove('active');
            // this.emitSize();
            // this.emit(this.RESIZED);
            _this.Events.emit(_this.Events.CELL, {
                evt: { type: 'resized' },
                cell: _this,
                data: {
                    width: _this.dataset.width,
                    height: _this.dataset.height
                }
            });
            evt.stopPropagation();
        };
        _this.onMouseMove = function (evt) {
            if (!_this.moving) {
                return;
            }
            _this.offsetX = evt.x - _this.startX;
            _this.offsetY = evt.y - _this.startY;
            var width = _this.width + _this.offsetX;
            if (width < 20)
                width = 20;
            var height = _this.height + _this.offsetY;
            if (height < 20)
                height = 20;
            // this.emit(this.RESIZING, {
            //   type: this.type,
            //   width,
            //   height,
            //   offsetX: width - this.width,
            //   offsetY: height - this.height
            // });
            _this.Events.emit(_this.Events.CELL, {
                evt: { type: 'resizing' },
                cell: _this,
                data: {
                    type: _this.type,
                    width: width,
                    height: height,
                    offsetX: width - _this.width,
                    offsetY: height - _this.height
                }
            });
            if (_this.type === 'col') {
                _this.dataset.width = width;
            }
            if (_this.type === 'row') {
                _this.dataset.height = height;
            }
            evt.stopPropagation();
            // console.log(this.onListeners);
            // if (this.axis === 'col') {
            //   this.Grid.Content.setColSize(this.cell, width);
            // } else {
            //   this.Grid.Content.setRowSize(this.cell, height);
            // }
            // console.log(this.width, width, this.Grid.Content.cells[0][0].style.width);
        };
        _this.cell = document.createElement('div');
        // this.cell.classList.add('grid-cell-heading');
        // this.cell.classList.add(`grid-cell-heading-${type}`);
        _this.cell.classList.add("grid-col-" + col);
        _this.cell.classList.add('grid-cell');
        _this.cell.classList.add('filter');
        _this.rowIndex = row;
        if (_this.type === 'col') {
            _this.reference = "" + helpers.toColumnName(col + 1);
        }
        else if (_this.type === 'row') {
            _this.reference = "" + (row + 1);
        }
        // input and button
        _this.cellFilterInput = document.createElement('input');
        _this.cellFilterInput.classList.add('grid-filter-input');
        _this.cellFilterInput.setAttribute('tabindex', "" + (_this.col + 100));
        _this.cellFilterInput.addEventListener('focus', function (evt) {
            _this.Events.emit(_this.Events.CELL, {
                cell: _this,
                evt: {
                    type: 'filter_focus'
                },
            });
        });
        _this.cellFilterInput.addEventListener('keyup', function (evt) {
            _this.Events.emit(_this.Events.CELL, {
                cell: _this,
                evt: {
                    type: 'filter'
                },
                value: _this.cellFilterInput.value
            });
        });
        _this.cellFilterDropdown = document.createElement('div');
        _this.cellFilterDropdown.classList.add('filters');
        _this.cell.appendChild(_this.cellFilterInput);
        return _this;
    }
    Object.defineProperty(GridFilterCell.prototype, "rowIndex", {
        get: function () {
            return this._rowIndex;
        },
        set: function (n) {
            this._rowIndex = n;
            this.index.row = n;
        },
        enumerable: true,
        configurable: true
    });
    GridFilterCell.prototype.create = function () {
        var _this = this;
        this.init = true;
        // events
        // let rightClick = false;
        // let d = new Date();
        // let clickedAt = 0;
        // let doubleClicked = false;
        var emit = function (evt) {
            _this.Events.emit(_this.Events.CELL, { evt: evt, cell: _this });
        };
        if (this.Page.touch) {
            this.cell.ontouchstart = emit;
            // this.cell.ontouchmove = emit;
            this.cell.ontouchend = emit;
        }
        else {
            this.cell.onmousedown = this.onMouse;
            this.cell.onmouseover = this.onMouse;
            this.cell.onmouseup = this.onMouse;
        }
    };
    GridFilterCell.prototype.update = function (row, col) {
        this.content = __assign({}, this.Page.originalContent[row][col]);
        // if (
        //   this.Page.deltaContent[this.content.index!.row] &&
        //   this.Page.deltaContent[this.content.index!.row][this.content.index!.col]
        // ) {
        //   force = true;
        //   this.content = { ...this.Page.deltaContent[this.content.index!.row][this.content.index!.col] };
        // }
        // if (this.row === row && this.col === col && !force) return;
        this.index.row = this.row;
        this.index.col = this.col;
        this.position = this.index;
        this.row = row;
        this.col = col;
        if (!this.init)
            this.create();
        // this.cell.setAttribute('id', `cell-${row}-${col}`);
        var pos = this.Page.cellPos(row, col);
        this.dataset.x = pos.x;
        this.dataset.y = pos.y;
        this.dataset.width = parseFloat(this.content.style.width);
        this.dataset.height = parseFloat(this.content.style.height);
        // this.cell.style.setProperty('left', `${pos.x}px`);
        // if (this.type === 'col') this.cell.style.setProperty('width', this.content.style.width!);
        // this.cell.style.setProperty('height', this.content.style.height!);
        var offset = this.type === 'corner' ? 2 : 1;
        this.cell.style.setProperty('z-index', "" + (this.Page.cols - this.col + offset));
        // this.cell.style.cssText = `width:${this.content.style.width!};height:${this.content.style.height!};z-index:${this.Page.cols - this.col}`;
        // if (
        //   this.Page.originalContent[this.row][this.col - 1] &&
        //   this.content.style['background-color'] != this.Page.originalContent[this.row][this.col - 1].style['background-color']
        // ) {
        //   if (!this.cellBackground.parentNode) this.cell.appendChild(this.cellBackground);
        // }
        // this.cellBackground.style.cssText = `background-color:${this.content.style['background-color']}`;
        // this.refresh();
    };
    GridFilterCell.prototype.refresh = function () { };
    GridFilterCell.prototype.getDetails = function () {
        return {
            content: this.content,
            row: this.row,
            col: this.col,
            position: {
                row: this.row,
                col: this.col
            },
            index: {
                row: this.content.index.row,
                col: this.content.index.col
            },
            dataset: __assign({}, this.dataset),
            reference: this.reference,
            sticky: this.sticky
        };
    };
    GridFilterCell.prototype.setFreeze = function (n) {
        // if (n === this.sticky) return;
        this.sticky = n;
        if (n) {
            // let left = this.Page.colWidths.slice(0, this.col).reduce((a: any, b: any) => a + b, 0);
            this.cell.classList.add('sticky');
            var offset = this.Page.headings && this.type != 'corner' ? 40 : 0;
            this.cell.style.setProperty('left', this.dataset.x + offset + "px");
        }
        else {
            this.cell.classList.remove('sticky');
            this.cell.style.removeProperty('left');
        }
    };
    GridFilterCell.prototype.setSelected = function (selection) {
        this.selected =
            (this.type === 'row' && this.Page.selectedRows.includes(this.row)) ||
                (this.type == 'col' && this.Page.selectedCols.includes(this.col));
        // this.col >= selection.colFrom &&
        // this.row >= selection.rowFrom &&
        // this.col <= selection.colTo &&
        // this.row <= selection.rowTo;
        // if (n === this.selected) return;
        // console.log(this.selected, this.row, this.col);
        if (this.selected) {
            this.canSort = !this.Page.structured && this.Page.selection.type === 'col';
            this.cell.classList.add('selected');
        }
        else {
            this.cell.classList.remove('selected');
        }
    };
    GridFilterCell.prototype.setSorting = function (direction) {
        if (!direction) {
            this.cell.classList.remove('sorting-asc', 'sorting-desc');
        }
        else {
            this.cell.classList.add("sorting-" + direction);
        }
    };
    GridFilterCell.prototype.clear = function () {
        this.cellFilterInput.value = '';
    };
    GridFilterCell.prototype.setValue = function (filter) {
        // matches, ==, !=, contains, starts_with
        var prefix = '';
        if (filter.exp == 'starts_with')
            prefix = '^';
        if (filter.exp == '==')
            prefix = '=';
        if (filter.exp == '!=')
            prefix = '!=';
        if (filter.exp == '>')
            prefix = '>';
        if (filter.exp == '>=')
            prefix = '>=';
        if (filter.exp == '<')
            prefix = '<';
        if (filter.exp == '<=')
            prefix = '<=';
        this.cellFilterInput.value = prefix + filter.value;
    };
    return GridFilterCell;
}(Emitter_1.default));
exports.GridFilterCell = GridFilterCell;
//# sourceMappingURL=GridFilterCell.js.map