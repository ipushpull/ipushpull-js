"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var moment_timezone_1 = __importDefault(require("moment-timezone"));
var popper_js_1 = __importDefault(require("popper.js"));
var ua_parser_js_1 = require("ua-parser-js");
var GridCanvas_1 = __importDefault(require("./GridCanvas"));
var GridCell_1 = require("./GridCell");
var GridPage_1 = __importDefault(require("./GridPage"));
var GridRow_1 = require("./GridRow");
var GridEvents_1 = __importDefault(require("./GridEvents"));
var Helpers_1 = __importDefault(require("../Helpers"));
var __1 = require("..");
var GridHeadingCell_1 = require("./GridHeadingCell");
var Schema_1 = require("../Page/Schema");
var GridFilterCell_1 = require("./GridFilterCell");
// import { IGridEvents } from '../GridEvents';
var helpers = new Helpers_1.default();
var IGridFieldType;
(function (IGridFieldType) {
    IGridFieldType["STRING"] = "string";
    IGridFieldType["NUMBER"] = "number";
    IGridFieldType["DATE"] = "date";
})(IGridFieldType = exports.IGridFieldType || (exports.IGridFieldType = {}));
var IGridSortDirection;
(function (IGridSortDirection) {
    IGridSortDirection["ASC"] = "asc";
    IGridSortDirection["DESC"] = "desc";
})(IGridSortDirection = exports.IGridSortDirection || (exports.IGridSortDirection = {}));
var Grid = /** @class */ (function () {
    function Grid(ref, options) {
        var _this = this;
        this.ref = ref;
        // public structured: boolean;
        this.contrast = "light";
        this.freezeRow = 0;
        this.freezeRowTop = 0;
        this.freezeColLeft = 0;
        this.freezeCol = 0;
        this.minVisibleRows = 100;
        this.highlights = false;
        this.isFocus = false;
        this.disallowSelection = false;
        this.canEdit = true;
        this.userId = 0;
        this.height = 0;
        this.width = 0;
        this.content = [];
        this.rowsToHide = [];
        this.hoverType = "row";
        this._selectorColor = "green";
        this.state = {
            filters: [],
            hiddenColumns: [],
            sorting: {
                col: -1,
                field: "",
                direction: IGridSortDirection.ASC,
                enabled: false,
                type: IGridFieldType.STRING,
            },
        };
        this.initialized = false;
        this.rendering = false;
        this.scrollTop = 0;
        this.visibleRows = [];
        // private hiddenColumns: number[] = [];
        // private scrollbarWidth = 0;
        this._fit = "scroll";
        this._editing = false;
        this._hasFocus = false;
        this._gridlines = false;
        this._headings = false;
        this._hoverHighlights = false;
        this._underlineLinks = true;
        this.doubleClicked = false;
        this.clickedAt = 0;
        this.historyClick = false;
        this.parameterClick = false;
        this.filterClick = false;
        this.rightClick = false;
        this._optimisedScrolling = false;
        this.uuid = "";
        this.forceUpdate = false;
        this.dirty = false;
        this.rowChecked = false;
        this.scale = 0;
        this.pinchDistance = 0;
        this.pinchScale = 0;
        // tracking
        this.tracking = false;
        this.trackingUsers = [];
        this.rowFragments = [];
        this.colStyles = [];
        // cells
        this.gridCells = [[]];
        this.gridFilterCells = [];
        this.gridRowHeadingCells = [];
        this.gridColumnHeadingCells = [];
        this.rows = [];
        this.colRows = [];
        this.cornerRows = [];
        this.selection = {
            rowFrom: -1,
            rowTo: -1,
            colFrom: -1,
            colTo: -1,
            heading: false,
            inside: false,
            selected: false,
            keyboard: false,
            type: "cell",
        };
        this.buttons = [];
        this.styles = [];
        this.accessRanges = [];
        this.found = [];
        // keyboard tracking
        this.ctrlDown = false;
        this.shiftDown = false;
        this.arrowKeyDown = false;
        this.editDone = false;
        // column sorting tracking
        this.sortColLeft = 0;
        this.sortColWidth = 0;
        this.sortColScroll = false;
        this.sortColMoveTo = -1;
        this.onScrollFreezeCol = function (evt) {
            _this.mainEl.scrollTop += evt.deltaY;
            _this.mainEl.scrollLeft += evt.deltaX;
            console.log(evt);
        };
        this.onScroll = function (evt) {
            _this.freezeColEl.scrollTop = _this.mainEl.scrollTop;
            _this.freezeRowEl.scrollLeft = _this.mainEl.scrollLeft;
            // if (this.mainEl.scrollTop < 0 || this.mainEl.scrollLeft < 0) {
            //   return;
            // }
            // if (this.scrollTimer) {
            //   clearTimeout(this.scrollTimer);
            //   this.scrollTimer = null;
            // }
            // setTimeout(()=>{
            // console.log('scroll', this.mainEl.scrollTop);
            _this.destroyPopper();
            _this.scrollSelector();
            _this.scrollElements();
            // this.render();
            requestAnimationFrame(_this.render);
            // }, 100)
            _this.Page.scrollTop = _this.mainEl.scrollTop;
            _this.Page.scrollLeft = _this.mainEl.scrollLeft;
            _this.applyMergeWidths();
        };
        this.onCell = function (data) {
            console.log("onCell", data);
            // if (this.disallowSelection) {
            //   return;
            // }
            // soft merge
            if (data.evt.type == "merge") {
                // console.log('merge', data);
                _this.gridCells[data.cell.row].forEach(function (gridCell) {
                    gridCell.updateSoftMerge(data.data.left, "left", data.data.zIndex);
                    gridCell.updateSoftMerge(data.data.right);
                });
            }
            // cell value has changed
            if (data.evt.type == "value") {
                var event_1 = {
                    cell: data.cell,
                    content: data.cell.content,
                    value: data.value || "",
                };
                _this.Events.emit(_this.Events.CELL_VALUE, event_1);
                _this.dirty = true;
                return;
            }
            if (data.evt.type === "check") {
                var event_2 = {
                    cell: data.cell.getDetails(),
                    content: data.cell.content,
                    value: data.value,
                    checked: data.checked,
                };
                _this.Events.emit(_this.Events.CELL_CHECKED, event_2);
                return;
            }
            if (data.evt.type === "filter_focus") {
                _this.Events.emit(_this.Events.COLUMN_FILTER_FOCUS, data);
            }
            if (data.evt.type === "filter") {
                console.log(data);
                // let name = `grid-${data.cell.col}`;
                var exp = "contains";
                var type = IGridFieldType.STRING;
                if (data.value.indexOf(">") === 0) {
                    exp = data.value.indexOf("=") === 1 ? ">=" : ">";
                    data.value = data.value.replace(">", "").replace("=", "");
                    type = IGridFieldType.NUMBER;
                }
                if (data.value.indexOf("<") === 0) {
                    exp = data.value.indexOf("=") === 1 ? "<=" : "<";
                    data.value = data.value.replace("<", "").replace("=", "");
                    type = IGridFieldType.NUMBER;
                }
                if (data.value.indexOf("^") === 0) {
                    exp = "starts_with";
                    data.value = data.value.replace("^", "");
                }
                if (data.value.indexOf("=") === 0) {
                    exp = "==";
                    data.value = data.value.replace("=", "");
                }
                if (data.value.indexOf("!=") === 0) {
                    exp = "!=";
                    data.value = data.value.replace("!=", "");
                }
                if (data.value.indexOf(",") > -1)
                    exp = "matches";
                var filter = {
                    type: type,
                    exp: exp,
                    col: data.cell.col,
                    value: data.value,
                };
                var index = _this.state.filters.findIndex(function (f) {
                    return f.col == data.cell.col;
                });
                if (!data.value && index > -1) {
                    _this.state.filters.splice(index, 1);
                }
                else if (data.value) {
                    if (index > -1) {
                        _this.state.filters[index] = filter;
                    }
                    else {
                        _this.state.filters.push(filter);
                    }
                }
                _this.Events.emit(_this.Events.COLUMN_FILTER, filter);
                _this.updateState(_this.state, true);
                return;
            }
            if (data.evt.type === "option") {
                var event_3 = {
                    cell: data.cell.getDetails(),
                    content: data.cell.content,
                    option: data.option || "",
                };
                _this.destroyPopper();
                _this.Events.emit(_this.Events.CELL_OPTION, event_3);
                return;
            }
            // cell editing has finished
            if (data.evt.type === "blur") {
                _this.dirty = data.done || false;
                _this.editing = false;
                _this.gridCells[data.cell.row].forEach(function (gridCell) {
                    gridCell.update(true);
                });
                return;
            }
            // sorting
            if (data.evt.type === "sort") {
                // this.sortColFromPos = this.Page.colPos(this.selection.colFrom);
                var width = 0;
                for (var i = _this.selection.colFrom; i <= _this.selection.colTo; i++) {
                    width += _this.Page.colWidths[i];
                }
                _this.sortColWidth = width;
                _this.sortColLeft = _this.Page.colPos(_this.selection.colFrom) + _this.Page.headingsWidth;
                if (_this.selection.colFrom >= _this.freezeCol) {
                    _this.sortColLeft -= _this.mainEl.scrollLeft;
                }
                _this.pageEl.classList.add("sorting-columns");
                _this.columnsEl.style.setProperty("width", _this.sortColWidth + "px");
                _this.columnsEl.style.setProperty("left", _this.sortColLeft + "px");
                _this.columnsEl.style.setProperty("height", _this.Page.height + _this.Page.headingsHeight + "px");
                _this.columnSplitEl.style.setProperty("left", _this.sortColLeft + "px");
                _this.columnSplitEl.style.setProperty("height", _this.Page.height + _this.Page.headingsHeight + "px");
            }
            if (data.evt.type === "sorting") {
                var left_1 = _this.sortColLeft + data.data.offsetX;
                _this.columnsEl.style.setProperty("left", left_1 + "px");
                if (_this.sortColScroll)
                    return;
                var width_1 = _this.Page.headingsWidth - _this.mainEl.scrollLeft;
                var colIndex_1 = -1;
                _this.Page.colWidths.forEach(function (w, i) {
                    if (colIndex_1 > -1)
                        return;
                    if (data.data.offsetX < 0 && width_1 > left_1) {
                        colIndex_1 = i;
                    }
                    width_1 += w;
                    if (data.data.offsetX > 0 && width_1 > left_1 + _this.sortColWidth) {
                        colIndex_1 = i;
                    }
                });
                // bump to end of page
                if (colIndex_1 < 0 && data.data.offsetX > 0) {
                    colIndex_1 = _this.Page.content[0].length;
                }
                // console.log(
                //   colIndex,
                //   helpers.toColumnName(colIndex + 1),
                //   this.Page.colPos(colIndex)
                // );
                // auto scroll
                if (_this.scrollTo(0, data.data.offsetX > 0 ? colIndex_1 + 1 : colIndex_1 - 1)) {
                    _this.sortColScroll = true;
                    setTimeout(function () {
                        _this.sortColScroll = false;
                    }, 100);
                }
                // check scroll pos
                var splitOffsetX = _this.Page.colPos(colIndex_1) + _this.Page.headingsWidth;
                if (colIndex_1 >= _this.freezeCol) {
                    splitOffsetX -= _this.mainEl.scrollLeft;
                }
                // this.columnsEl.textContent = `${left}px - ${left + this.colWidth}`;
                if ((colIndex_1 > -1 && colIndex_1 < _this.selection.colFrom) || colIndex_1 > _this.selection.colTo + 1) {
                    _this.columnSplitEl.style.setProperty("left", splitOffsetX + "px");
                    _this.columnSplitEl.style.setProperty("display", "block");
                }
                else {
                    _this.columnSplitEl.style.setProperty("display", "none");
                }
                _this.sortColMoveTo = colIndex_1;
            }
            if (data.evt.type === "sorted") {
                _this.pageEl.classList.remove("sorting-columns");
                _this.columnSplitEl.style.setProperty("display", "none");
                if (!_this.selection.from || _this.sortColMoveTo < 0)
                    return;
                _this.Events.emit(_this.Events.COLUMNS_MOVED, {
                    index: _this.sortColMoveTo,
                });
                // console.log(this.selection.from.col, this.sortColMoveTo);
            }
            // resizing
            if (data.evt.type === "resize") {
                console.log("resize", data);
                _this.clearSelector();
                requestAnimationFrame(_this.render);
                _this.Events.emit(_this.Events.RESIZE, {
                    which: data.cell.type,
                    index: data.cell.type === "col" ? data.cell.col : data.cell.row,
                    data: data.data,
                });
                return;
            }
            if (data.evt.type === "resizing") {
                var cellSize = 0;
                console.log("resizing", data);
                if (!data.data)
                    return;
                if (data.data.type === "col") {
                    cellSize = data.data.width;
                    _this.updateColWidth(data.cell.col, cellSize);
                }
                if (data.data.type === "row") {
                    cellSize = data.data.height;
                    _this.Page.rowHeights[data.cell.rowIndex] = cellSize;
                    _this.rows[data.cell.row].setHeight(cellSize);
                    _this.colRows[data.cell.row].setHeight(cellSize);
                    if (_this.cornerRows[data.cell.row]) {
                        _this.cornerRows[data.cell.row].setHeight(cellSize);
                    }
                    _this.Page.content.forEach(function (row, visibleIndex) {
                        var rowIndex = row[0].index.row;
                        if (visibleIndex <= data.cell.rowIndex)
                            return;
                        _this.rows[rowIndex].setTop();
                        _this.colRows[rowIndex].setTop();
                        if (_this.cornerRows[rowIndex]) {
                            _this.cornerRows[rowIndex].setTop();
                        }
                    });
                    // cellSize = data.data.width;
                    // this.applyColWidth(data.cell.col, cellSize);
                }
                _this.Events.emit(_this.Events.RESIZING, {
                    which: data.cell.type,
                    index: data.cell.type === "col" ? data.cell.col : data.cell.row,
                    data: data.data,
                });
                return;
            }
            if (data.evt.type === "resized") {
                console.log("resized", data);
                var cellSize = 0;
                if (data.cell.type === "col") {
                    cellSize = data.data.width;
                    _this.Page.colWidths[data.cell.col] = cellSize;
                }
                if (data.cell.type === "row") {
                    // this.Page.colWidths[data.cell.row] = cellSize;
                    cellSize = data.data.height;
                    _this.Page.rowHeights[data.cell.row] = cellSize;
                }
                if (data.data.offsetX || data.data.offsetY) {
                    _this.Events.emit(_this.Events.RESIZED, {
                        which: data.cell.type,
                        index: data.cell.type === "col" ? data.cell.col : data.cell.row,
                        size: cellSize,
                    });
                    _this.forceUpdate = true;
                    _this.update(true);
                }
                return;
            }
            if (data.evt.touches) {
                if (data.evt.touches.length == 2) {
                    return;
                }
            }
            // if (data.cell.editing) {
            //   return;
            // }
            if (["mousedown", "touchstart"].includes(data.evt.type)) {
                _this.destroyPopper();
                _this.rightClick = _this.isRightClick(data.evt);
                _this.isFocus = _this.hasFocus = true;
                var d = new Date();
                console.log("CELL", data.cell.reference);
                _this.doubleClicked = d.getTime() - _this.clickedAt <= 300;
                _this.clickedAt = d.getTime();
                _this.historyClick = data.evt.target.classList.contains("grid-history");
                _this.parameterClick = data.evt.target.classList.contains("grid-map");
                _this.filterClick = data.evt.target.classList.contains("filters");
                _this.rowChecked = data.evt.target.classList.contains("checkbox");
                var tag = "";
                if (_this.historyClick)
                    tag = "tracking";
                else if (_this.parameterClick)
                    tag = "parameter";
                else if (_this.filterClick)
                    tag = "filter";
                // else if (this.rowChecked) tag = 'row';
                // check if diff
                if (_this.selection.from) {
                    if (_this.editing) {
                        // if (data.cell.reference != this.selection.from.reference) {
                        if (_this.editDone) {
                            _this.input.blur();
                        }
                        else {
                            _this.editing = false;
                        }
                        // this.selection.from.cancel();
                    }
                }
                // check if inside current selection
                _this.selection.selected = true;
                // let heading = false;
                var gridCell = data.cell;
                if (_this.rightClick && _this.Page.selectedRows.includes(data.cell.row) && _this.Page.selectedCols.includes(data.cell.col)) {
                    _this.selection.inside = true;
                }
                else {
                    _this.selection.inside = false;
                    if (data.cell.type === "col") {
                        // let c = this.Page.content[0][data.cell.col];
                        gridCell = _this.gridCells[0][data.cell.col];
                        // heading = true;
                    }
                    else if (data.cell.type === "row") {
                        // let c = this.Page.content[data.cell.row][0];
                        gridCell = _this.gridCells[data.cell.row][0];
                        // heading = true;
                    }
                    else if (data.cell.type === "corner") {
                        var c = _this.Page.content[0][0];
                        gridCell = _this.gridCells[c.index.row][c.index.col];
                        // heading = true;
                    }
                    _this.selection.from = gridCell;
                    _this.selection.to = gridCell;
                    if (!_this.Page.touch && !_this.disallowSelection) {
                        _this.updateSelector();
                        requestAnimationFrame(_this.render);
                    }
                }
                _this.selection.heading = data.cell.type !== "cell";
                _this.selection.type = data.cell.type;
                // this.Page.setSelectedCell(data);
                _this.Events.emit(_this.Events.CELL_CLICKED, {
                    cell: gridCell,
                    event: data.evt,
                    content: _this.selection.from.content,
                    rightClick: _this.rightClick,
                    historyClick: _this.historyClick,
                    parameterClick: _this.parameterClick,
                    filterClick: _this.filterClick,
                    heading: _this.selection.heading,
                    tag: tag,
                });
                // data.evt.preventDefault();
            }
            if (["mouseover", "mousemove"].includes(data.evt.type)) {
                if (_this.selection.selected && !_this.selection.inside) {
                    // console.log(data.evt.target.dataset.row, data.evt.target.dataset.col);
                    // this.selection.to = data.cell;
                    _this.setToCell(data);
                    if (!_this.disallowSelection) {
                        _this.updateSelector();
                        if (!_this.Page.touch && !_this.keepSelectorInView(true)) {
                            requestAnimationFrame(_this.render);
                        }
                    }
                    // console.log(this.selection.to?.reference);
                }
            }
            if (["mouseup", "touchend"].includes(data.evt.type)) {
                console.log("mouseup", data);
                if (!_this.hasFocus || !_this.selection.from) {
                    return;
                }
                if (_this.selection.selected && !_this.selection.inside) {
                    _this.setToCell(data);
                }
                _this.selection.selected = false;
                if (!_this.disallowSelection) {
                    _this.updateSelector();
                    requestAnimationFrame(_this.render);
                }
                // edit cell
                if (_this.selection.from.reference == _this.selection.to.reference) {
                    _this.editCell(_this.selection.from.row, _this.selection.from.col, undefined, true);
                    if (_this.editing) {
                        if (!_this.Page.touch)
                            data.evt.preventDefault();
                        else
                            setTimeout(function () {
                                _this.keepSelectorInView();
                            }, 300);
                    }
                }
                _this.emitCellSelected(data.evt);
                _this.historyClick = false;
                _this.filterClick = false;
                _this.rightClick = false;
                _this.doubleClicked = false;
                _this.selection.heading = false;
                data.evt.stopPropagation();
            }
        };
        this.render = function () {
            if (!_this.gridCells.length) {
                _this.forceUpdate = false;
                _this.rendering = false;
                return;
            }
            var scrollTop = _this.mainEl.scrollTop;
            var scrollLeft = _this.mainEl.scrollLeft * _this.scale;
            _this.scrollTop = scrollTop;
            var visible = [];
            var visibleCols = [];
            var rowStart = -1;
            var rowEnd = -1;
            var colStart = -1;
            var colEnd = -1;
            var offsetHeight = _this.pageEl.offsetHeight;
            var offsetWidth = _this.pageEl.offsetWidth;
            // update selector
            // if (this.selection.from) {
            //   if (this.selection.from.col < this.freezeCol && this.selection.from.row >= this.freezeRow) {
            //     let top = this.rows[this.selection.from.row].top;
            //     // this.selectorEl.style.setProperty('top', `${this.selection.from.dataset.y - scrollTop}px`);
            //     // this.selectorEl.style.setProperty('top', `${top - scrollTop}px`);
            //   }
            // }
            // get visible rows
            _this.Page.rowHeights.forEach(function (height, index) {
                // let h = this.Page.rowHeights.slice(0, index + 1).reduce((a, b) => a + b, 0);
                var h = _this.Page.rowPos(index) * _this.scale;
                var show = true;
                var before = -offsetHeight / 4;
                var after = offsetHeight;
                // after *= this.scale;
                if (h - scrollTop * _this.scale < before)
                    show = false;
                if (h - scrollTop * _this.scale - height * _this.scale > after)
                    show = false;
                if (show && rowStart < 0)
                    rowStart = index;
                if (index < _this.freezeRow)
                    show = true;
                if (show)
                    visible.push(index);
                if (!show && rowStart > -1 && rowEnd < 0) {
                    rowEnd = index;
                }
            });
            // get visible cols
            _this.Page.colWidths.forEach(function (width, index) {
                // let h = this.Page.rowHeights.slice(0, index + 1).reduce((a, b) => a + b, 0);
                var w = _this.Page.colPos(index) * _this.scale;
                var show = true;
                var before = _this.freezeColLeft * _this.scale;
                var after = offsetWidth;
                // after *= this.scale;
                if (w - scrollLeft + width * _this.scale < before)
                    show = false;
                if (w - scrollLeft > after)
                    show = false;
                if (show && colStart < 0)
                    colStart = index;
                if (index < _this.freezeCol)
                    show = true;
                if (show)
                    visibleCols.push(index);
                if (!show && colStart > -1 && colEnd < 0) {
                    colEnd = index;
                }
            });
            // console.log('cols', visibleCols);
            _this.gridColumnHeadingCells.forEach(function (gridCell) {
                gridCell.setSelected(_this.selection);
            });
            _this.gridRowHeadingCells.forEach(function (gridCell) {
                gridCell.setSelected(_this.selection);
            });
            // remove invisble row
            // let difference  = visible.filter(x => !this.visibleRows.includes(x));
            // console.log('intersection', difference );
            // difference.forEach(rowIndex => {
            //   if (!this.rows[rowIndex] || !this.rows[rowIndex].rowEl.parentNode) return;
            //   this.rows[rowIndex].rowEl.parentNode!.removeChild(this.rows[rowIndex].rowEl);
            // })
            // create visible rows
            var mainFragment = document.createDocumentFragment();
            var freezeCornerFragment = document.createDocumentFragment();
            var freezeColumnFragment = document.createDocumentFragment();
            var freezeRowFragment = document.createDocumentFragment();
            var freezeCornerCellsFragment = document.createDocumentFragment();
            var appendRows = false;
            _this.Page.content.forEach(function (row, visibleIndex) {
                var rowIndex = row[0].index.row;
                var show = visible.includes(visibleIndex);
                var gridRow = _this.rows[rowIndex];
                var gridColRow = _this.colRows[rowIndex];
                var gridCornerRow = _this.cornerRows[rowIndex];
                if (!show) {
                    if (!gridRow.rowEl.parentElement)
                        return;
                    gridRow.rowEl.parentElement.removeChild(gridRow.rowEl);
                    gridColRow.rowEl.parentElement.removeChild(gridColRow.rowEl);
                    return;
                }
                // check if we need to append row
                if (gridRow.row < _this.freezeRow) {
                    if (!_this.freezeRowEl.contains(gridRow.rowEl)) {
                        freezeRowFragment.appendChild(gridRow.rowEl);
                        freezeCornerFragment.appendChild(gridCornerRow.rowEl);
                        appendRows = true;
                    }
                }
                else if (!_this.mainEl.contains(gridRow.rowEl)) {
                    mainFragment.appendChild(gridRow.rowEl);
                    appendRows = true;
                }
                if (!_this.freezeColEl.contains(gridColRow.rowEl)) {
                    freezeColumnFragment.appendChild(gridColRow.rowEl);
                    // if (gridCornerRow) this.freezeCornerEl.appendChild(gridCornerRow.rowEl);
                    appendRows = true;
                }
                // attach cells to row
                var cellsFragment = document.createDocumentFragment();
                var freezeColCellsFragment = document.createDocumentFragment();
                var freezeRowCellsFragment = document.createDocumentFragment();
                var appendCells = false;
                // row heading cell
                var gridRowCell = _this.gridRowHeadingCells[rowIndex];
                // append row heading cell;
                if (rowIndex < _this.freezeRow) {
                    if (gridRowCell.cell.parentElement != gridCornerRow.rowEl) {
                        gridCornerRow.rowEl.appendChild(gridRowCell.cell);
                        appendCells = true;
                    }
                }
                else {
                    if (gridRowCell.cell.parentElement != gridColRow.rowEl) {
                        freezeRowCellsFragment.appendChild(gridRowCell.cell);
                        appendCells = true;
                    }
                    // freezeCornerFragment.appendChild(gridRowCell.cell);
                }
                gridRowCell.rowIndex = visibleIndex;
                // create/update cells
                for (var colIndex = 0; colIndex < _this.Page.cols; colIndex++) {
                    var gridCell = _this.gridCells[rowIndex][colIndex];
                    if (!visibleCols.includes(colIndex)) {
                        gridCell.remove();
                        continue;
                    }
                    // corner heading cells
                    if (!visibleIndex && colIndex < _this.freezeCol) {
                        if (_this.gridColumnHeadingCells[colIndex].cell.parentElement != _this.gridCornerHeading.rowEl) {
                            _this.gridCornerHeading.rowEl.appendChild(_this.gridColumnHeadingCells[colIndex].cell);
                        }
                        // filter cells
                        if (_this.gridFilterCells[colIndex].cell.parentElement != _this.gridCornerRowFilters.rowEl) {
                            _this.gridCornerRowFilters.rowEl.appendChild(_this.gridFilterCells[colIndex].cell);
                        }
                    }
                    else if (!visibleIndex) {
                        if (_this.gridColumnHeadingCells[colIndex].cell.parentElement != _this.gridRowHeading.rowEl) {
                            freezeColCellsFragment.appendChild(_this.gridColumnHeadingCells[colIndex].cell);
                            appendCells = true;
                        }
                        // filter cells
                        if (_this.gridFilterCells[colIndex].cell.parentElement != _this.gridRowFilters.rowEl) {
                            _this.gridRowFilters.rowEl.appendChild(_this.gridFilterCells[colIndex].cell);
                            // appendCells = true;
                        }
                    }
                    gridCell.update(_this.forceUpdate, true);
                    // let appendTo: any;
                    // check if cell is in correct element
                    if (colIndex < _this.freezeCol && rowIndex < _this.freezeRow) {
                        // corner row
                        if (gridCell.cell.parentElement != gridCornerRow.rowEl) {
                            // appendTo = gridCornerRow.rowEl;
                            gridCornerRow.rowEl.appendChild(gridCell.cell);
                            gridCell.sticky = true;
                            appendCells = true;
                        }
                    }
                    else if (colIndex < _this.freezeCol) {
                        // column row
                        if (gridCell.cell.parentElement != gridColRow.rowEl) {
                            // appendTo = freezeRowCellsFragment;
                            freezeRowCellsFragment.appendChild(gridCell.cell);
                            gridCell.sticky = true;
                            appendCells = true;
                        }
                    }
                    else {
                        // main row
                        if (gridCell.cell.parentElement != gridRow.rowEl) {
                            // appendTo = cellsFragment;
                            cellsFragment.appendChild(gridCell.cell);
                            gridCell.sticky = false;
                            appendCells = true;
                        }
                    }
                    // gridCell.setFreeze(colIndex < this.freezeCol);
                    // gridCell.update(this.forceUpdate);
                    // if (appendTo) {
                    //   appendTo.appendChild(gridCell.cell);
                    // }
                    gridCell.setSelected(_this.selection);
                    gridCell.rowIndex = visibleIndex;
                }
                if (appendCells) {
                    gridRow.rowEl.insertBefore(cellsFragment, gridRow.rowEl.firstChild);
                    gridColRow.rowEl.appendChild(freezeRowCellsFragment);
                    _this.gridRowHeading.rowEl.insertBefore(freezeColCellsFragment, _this.gridRowHeading.rowEl.firstChild);
                    // gridColRow.rowEl.appendChild(stickyCornerCellsFragment);
                }
                // this.rows[rowIndex].setFreeze(this.rows[rowIndex].row < this.freezeRow);
            });
            if (appendRows) {
                if (freezeRowFragment.childNodes.length)
                    _this.freezeRowEl.appendChild(freezeRowFragment);
                if (freezeCornerFragment.childNodes.length)
                    _this.freezeCornerEl.appendChild(freezeCornerFragment);
                // if (freezeCornerCellsFragment.childNodes.length) this.freezeCornerEl.appendChild(freezeCornerCellsFragment);
                if (freezeColumnFragment.childNodes.length)
                    _this.freezeColEl.appendChild(freezeColumnFragment);
                if (mainFragment.childNodes.length)
                    _this.mainEl.appendChild(mainFragment);
            }
            // remove any hidden rows
            _this.Page.hiddenRows.forEach(function (rowIndex) {
                if (_this.rows[rowIndex] && _this.rows[rowIndex].rowEl.parentElement) {
                    _this.rows[rowIndex].rowEl.parentElement.removeChild(_this.rows[rowIndex].rowEl);
                }
                if (_this.colRows[rowIndex] && _this.colRows[rowIndex].rowEl.parentElement) {
                    _this.colRows[rowIndex].rowEl.parentElement.removeChild(_this.colRows[rowIndex].rowEl);
                }
            });
            // show/hide rows
            _this.visibleRows = visible;
            _this.forceUpdate = false;
            _this.rendering = false;
        };
        this.onResize = function () {
            if (_this.resizeTimer) {
                clearTimeout(_this.resizeTimer);
                _this.resizeTimer = null;
            }
            _this.resizeTimer = setTimeout(function () {
                _this.updateElSizes();
                _this.applyScale();
                _this.forceUpdate = true;
                requestAnimationFrame(_this.render);
            }, 300);
        };
        this.onKeydown = function (evt) {
            console.log(evt.key);
            _this.selection.keyboard = false;
            if (_this.disallowSelection) {
                return;
            }
            // esc
            if (evt.key === "Escape") {
                _this.blur();
                _this.Events.emit(_this.Events.BLUR);
                return;
            }
            // ctrl is down
            if (evt.key === "Control") {
                _this.ctrlDown = true;
                return;
            }
            // shift
            if (evt.key === "Shift") {
                _this.shiftDown = true;
            }
            // select all
            if (evt.ctrlKey && evt.key === "a" && _this.hasFocus) {
                _this.selection.from = _this.getGridCell(0, 0) || undefined;
                _this.selection.to = _this.getGridCell(_this.Page.content.length - 1, _this.Page.content[0].length - 1) || undefined;
                _this.updateSelector();
                if (!_this.keepSelectorInView())
                    requestAnimationFrame(_this.render);
                evt.preventDefault();
                return;
            }
            // copy
            if (evt.ctrlKey && evt.key === "c" && _this.hasFocus) {
                _this.Events.emit(_this.Events.COPY, {
                    html: _this.getContentHtml(),
                });
                return;
            }
            // cut
            if (evt.ctrlKey && evt.key === "x" && _this.isFocus && _this.canEdit) {
                var html = _this.getContentHtml();
                var success = _this.emitDelete(evt);
                if (success) {
                    _this.Events.emit(_this.Events.COPY, {
                        html: html,
                    });
                }
                return;
            }
            if (!_this.hasFocus) {
                if (document.activeElement !== document.querySelector("body")) {
                    // evt.preventDefault();
                    return;
                }
            }
            if (evt.key === "F2" && !evt.shiftKey && !_this.selection.from && !_this.Page.disableFocusOnF2) {
                evt.stopPropagation();
                _this.focus();
                _this.Events.emit(_this.Events.FOCUS);
                return;
            }
            // if (this.gotoNextCell(evt.key)) {
            //   evt.preventDefault();
            //   return;
            // }
            // ctrl navigation
            // const moved = this.onCtrlNavigate(evt.key);
            // if (moved) {
            //   evt.preventDefault();
            //   return;
            // }
            if (["Home", "End", "PageUp", "PageDown"].indexOf(evt.key) > -1) {
                // evt.preventDefault();
                evt.preventDefault();
                if (_this.editing)
                    return;
                _this.jumpToCell(evt.key);
                return;
            }
            if (["ArrowLeft", "ArrowUp", "ArrowRight", "ArrowDown", "Enter", "Tab"].indexOf(evt.key) > -1) {
                // let direction: string = evt.key.replace("Arrow", "").toLowerCase();
                if (_this.buttonPopper) {
                    _this.selection.from.selectOption(evt.key);
                    evt.preventDefault();
                }
                else if (!_this.editing) {
                    _this.gotoNextCell(evt.key);
                    evt.preventDefault();
                }
                return;
            }
            // if (["PageUp", "PageDown"].indexOf(evt.key) > -1) {
            //   if (this.editing) return;
            //   let direction = evt.key.toLocaleLowerCase();
            //   // find cell that is out of view
            //   // let row = this.selection.from.dataset.row;
            //   this.selectNextCell(direction);
            //   evt.preventDefault();
            // }
            // if (evt.key === "Enter") {
            //   if (this.buttonPopper) {
            //     this.selection.from!.selectOption("Enter");
            //     return;
            //   }
            //   this.selectNextCell(this.shiftDown ? "up" : "down", evt.key);
            //   evt.preventDefault();
            // }
            // if (evt.key === "Tab") {
            //   if (this.buttonPopper) {
            //     this.selection.from!.selectOption("Enter");
            //     return;
            //   }
            //   this.selectNextCell(this.shiftDown ? "left" : "right", evt.key);
            //   evt.preventDefault();
            // }
            if (!_this.editing && _this.selection.from) {
                if (evt.key === "Delete" && _this.canEdit) {
                    _this.emitDelete(evt);
                    return;
                }
                if (_this.selection.from !== _this.selection.to) {
                    return;
                }
                if (evt.code === "Space") {
                    if (_this.selection.from.button && _this.selection.from.button.type === "checkbox") {
                        _this.rowChecked = true;
                        _this.selection.keyboard = true;
                        // this.selection.from.toggleCheckbox();
                        _this.editCell(_this.selection.from.row, _this.selection.from.col);
                    }
                    evt.preventDefault();
                    return;
                }
                if (evt.key === "F2" && !evt.shiftKey) {
                    if (_this.selection.from === _this.selection.to) {
                        _this.editCell(_this.selection.from.row, _this.selection.from.col);
                    }
                    return;
                }
                if (!evt.ctrlKey && evt.key.length === 1) {
                    if (!_this.selection.from.hasButtonOptions)
                        _this.keyValue = evt.key;
                    _this.editCell(_this.selection.from.row, _this.selection.from.col);
                }
            }
        };
        this.onKeyup = function (evt) {
            if (evt.key === "Control") {
                _this.ctrlDown = false;
            }
            if (evt.key === "Shift") {
                _this.shiftDown = false;
            }
            if (_this.selection.keyboard) {
                if (evt.key === "Shift") {
                    _this.selection.from = _this.getGridCell(_this.selection.rowFrom, _this.selection.colFrom);
                    _this.selection.to = _this.getGridCell(_this.selection.rowTo, _this.selection.colTo);
                }
                // KEEP this.emitSelected(evt, this.selection.type);
                _this.emitCellSelected(evt);
                _this.selection.keyboard = false;
            }
            if (!_this.shiftDown) {
                _this.selection.keyboard = false;
            }
        };
        this.onMouseDown = function (evt) {
            // check if grid is the target
            var isPopper = _this.buttonPopper && helpers.isTarget(_this.buttonPopper.popper, evt.target) ? true : false;
            var pageEl = helpers.isTarget(_this.pageEl, evt.target);
            if (pageEl || isPopper) {
                _this.hasFocus = true;
            }
            if (!isPopper) {
                _this.destroyPopper();
            }
            // stop
            // if (!this.hasFocus) {
            //   this.destroyPopper();
            //   return;
            // }
            // check if filter input
            if (evt.target.classList && evt.target.classList.contains("grid-filter-input")) {
                _this.blur();
            }
            // touch
            if (evt.touches) {
                if (evt.touches.length == 2) {
                    _this.pinchDistance = _this.getTouchDistance(evt.touches);
                    _this.pinchScale = _this.scale;
                    _this.clearSelector();
                    return;
                }
                evt.clientX = evt.touches[0].pageX;
                evt.clientY = evt.touches[0].pageY;
            }
            // get cell
            if (!pageEl)
                return;
            var cell = _this.getGridCellTarget(evt.target);
            if (cell) {
                _this.Events.emit(_this.Events.CELL, {
                    cell: _this.gridCells[cell.dataset.row][cell.dataset.col],
                    evt: evt,
                });
            }
            evt.stopPropagation();
        };
        this.onMouseMove = function (evt) {
            if (evt.touches) {
                if (evt.touches.length >= 2) {
                    var distance = _this.getTouchDistance(evt.touches);
                    // console.log(distance / this.pinchDistance);
                    // evt.preventDefault();
                    // evt.stopPropagation();
                    _this.applyScale((_this.pinchScale * distance) / _this.pinchDistance);
                    // this.updateSelector();
                    // this.applySortIndicator();
                    // this.render();
                    requestAnimationFrame(_this.render);
                }
                return;
            }
            if (_this.selection.selected && !_this.selection.inside) {
                var cellDiv = _this.getGridCellTarget(evt.target);
                if (!cellDiv)
                    return;
                var cell = _this.getGridCell(cellDiv.dataset.row, cellDiv.dataset.col);
                if (cell)
                    _this.Events.emit(_this.Events.CELL, {
                        cell: cell,
                        evt: evt,
                    });
            }
        };
        this.onMouseUp = function (evt) {
            console.log(_this.hasFocus, document.activeElement);
            // check active element
            var isPopper = _this.buttonPopper && helpers.isTarget(_this.buttonPopper.popper, evt.target) ? true : false;
            var pageEl = helpers.isTarget(_this.pageEl, evt.target);
            if (!pageEl) {
                _this.hasFocus = document.activeElement === document.querySelector("body");
            }
            // if (!this.hasFocus) {
            //   this.blur();
            //   return;
            // }
            // check if edit input
            if (evt.target.classList && evt.target.classList.contains("grid-input")) {
                // this.blur();
                return;
            }
            // check if filter input
            if (evt.target.classList && evt.target.classList.contains("grid-filter-input")) {
                return;
            }
            // check if button dropdown
            if (isPopper) {
                return;
            }
            if (!pageEl && !_this.selection.selected) {
                // this.Events.emit(this.Events.NO_CELL);
                return;
            }
            var cellDiv = _this.getGridCellTarget(evt.target);
            if (!cellDiv && !_this.selection.selected) {
                _this.Events.emit(_this.Events.NO_CELL);
                // TODO emit focus event
                return;
            }
            var cell = cellDiv ? _this.getGridCell(cellDiv.dataset.row, cellDiv.dataset.col) : _this.selection.to;
            _this.Events.emit(_this.Events.CELL, {
                cell: cell,
                evt: evt,
            });
            // if (!helpers.isTarget(evt.target, this.pageEl)) this.hasFocus = false;
            // this.selection.selected = false;
        };
        var pageEl = typeof this.ref === "string" ? document.getElementById(this.ref) : this.ref;
        if (!pageEl)
            throw new Error("Page element not found");
        this.pageEl = pageEl;
        this.Events = new GridEvents_1.default();
        this.Page = new GridPage_1.default();
        var parser = new ua_parser_js_1.UAParser();
        var parseResult = parser.getResult();
        this.Page.touch = parseResult.device && (parseResult.device.type === "tablet" || parseResult.device.type === "mobile");
        if (this.Page.touch)
            this.pageEl.classList.add("touch");
        if (options) {
            Object.keys(options).forEach(function (key) {
                _this[key] = options[key];
            });
        }
        this.Page.uuid = "" + Math.round(Math.random() * new Date().getTime());
        this.pageEl.classList.add("grid-" + this.Page.uuid);
        this.selectorColor = this._selectorColor;
    }
    Grid.prototype.image = function (options) {
        this.Canvas.gridCells = this.gridCells;
        this.Canvas.gridWidth = this.width;
        this.Canvas.gridHeight = this.height;
        return this.Canvas.render(options);
    };
    Object.defineProperty(Grid.prototype, "selectorColor", {
        set: function (n) {
            this._selectorColor = n;
            this.pageEl.classList.remove('selector-green', 'selector-blue');
            this.pageEl.classList.add("selector-" + this._selectorColor);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "structured", {
        get: function () {
            return this.Page.structured;
        },
        set: function (n) {
            this.Page.structured = n;
            if (n) {
                this.pageEl.classList.add("structured");
            }
            else {
                this.pageEl.classList.remove("structured");
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "columnFilters", {
        get: function () {
            return this.Page.structured;
        },
        set: function (n) {
            this.Page.columnFilters = n;
            if (n) {
                this.pageEl.classList.add("filters");
            }
            else {
                this.pageEl.classList.remove("filters");
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "hoverHighlights", {
        get: function () {
            return this._hoverHighlights;
        },
        set: function (n) {
            this._hoverHighlights = n;
            if (n) {
                this.pageEl.classList.add("hover", "hover-" + this.hoverType);
            }
            else {
                this.pageEl.classList.remove("hover", "hover-row", "hover-cell");
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "underlineLinks", {
        get: function () {
            return this._underlineLinks;
        },
        set: function (n) {
            this._underlineLinks = n;
            if (n) {
                this.pageEl.classList.remove("hide-link-underlines");
            }
            else {
                this.pageEl.classList.add("hide-link-underlines");
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "gridlines", {
        get: function () {
            return this._gridlines;
        },
        set: function (n) {
            this._gridlines = n;
            if (this.gridlines) {
                this.pageEl.classList.add("gridlines");
            }
            else {
                this.pageEl.classList.remove("gridlines");
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "headings", {
        get: function () {
            return this._headings;
        },
        set: function (n) {
            this._headings = n;
            this.Page.headings = n;
            if (this.headings) {
                this.pageEl.classList.add("headings");
            }
            else {
                this.pageEl.classList.remove("headings");
                if (this.mainEl)
                    this.mainEl.style.removeProperty("top");
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "hasFocus", {
        get: function () {
            return this._hasFocus;
        },
        set: function (n) {
            this._hasFocus = n;
            if (n) {
                this.pageEl.classList.add("focus");
            }
            else {
                this.pageEl.classList.remove("focus");
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "fit", {
        get: function () {
            return this._fit;
        },
        set: function (n) {
            this._fit = ["scroll", "width", "height", "contain"].includes(n) ? n : "scroll";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "softMerges", {
        get: function () {
            return this.Page.softMerges;
        },
        set: function (n) {
            this.Page.softMerges = n;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "editing", {
        get: function () {
            return this._editing;
        },
        set: function (n) {
            // if (!this.canEdit) return; // is this ok to comment out?
            this._editing = n;
            if (this.dirty) {
                this.Events.emit(this.Events.EDITING, {
                    cell: this.selection.from,
                    editing: this._editing,
                    dirty: this.dirty,
                });
                this.dirty = false;
            }
            if (n) {
                this.pageEl.classList.add("editing");
            }
            else {
                this.pageEl.classList.remove("editing");
                this.keyValue = "";
                this.destroyPopper();
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "optimiseScrolling", {
        set: function (n) {
            this._optimisedScrolling = n;
            if (n) {
                this.pageEl.classList.add("optimised");
            }
            else {
                this.pageEl.classList.remove("optimised");
            }
        },
        enumerable: true,
        configurable: true
    });
    Grid.prototype.destroy = function () {
        if (!this.initialized)
            return;
        this.destroyListeners();
        this.reset();
    };
    Grid.prototype.setContent = function (content, state, columnDefs) {
        this.init();
        this.state = state || this.getDefaultState();
        this.Page.reset();
        this.Page.content = this.Page.originalContent = content;
        this.Page.columnsDefs = columnDefs || [];
        // column styles
        for (var colIndex = 0; colIndex < this.Page.originalContent[0].length; colIndex++) {
            this.colStyles[colIndex] = document.createTextNode("");
            this.styleSheet.appendChild(this.colStyles[colIndex]);
        }
        // corner
        this.gridCornerHeading = new GridRow_1.GridRow(this.Page, this.Events, 0, true);
        this.gridCornerHeading.setIndex(0);
        this.freezeCornerEl.appendChild(this.gridCornerHeading.rowEl);
        // corner cell
        this.gridCornerHeadingCell = new GridHeadingCell_1.GridHeadingCell(this.Page, this.Events, 0, 0, "corner");
        this.gridCornerHeading.rowEl.appendChild(this.gridCornerHeadingCell.cell);
        // heading row
        this.gridRowHeading = new GridRow_1.GridRow(this.Page, this.Events, 0, true);
        this.gridRowHeading.setIndex(0);
        this.freezeRowEl.appendChild(this.gridRowHeading.rowEl);
        // filter row
        this.gridRowFilters = new GridRow_1.GridRow(this.Page, this.Events, 0, false, true);
        this.gridRowFilters.setIndex(0);
        this.freezeRowEl.appendChild(this.gridRowFilters.rowEl);
        // filter corner row
        this.gridCornerRowFilters = new GridRow_1.GridRow(this.Page, this.Events, 0, false, true);
        this.gridRowFilters.setIndex(0);
        this.freezeCornerEl.appendChild(this.gridCornerRowFilters.rowEl);
        // column heading cells
        for (var colIndex = 0; colIndex < this.Page.originalContent[0].length; colIndex++) {
            // filter cells
            this.gridFilterCells[colIndex] = new GridFilterCell_1.GridFilterCell(this.Page, this.Events, 0, colIndex, "col");
            this.gridRowFilters.rowEl.appendChild(this.gridFilterCells[colIndex].cell);
            // heading cells
            this.gridColumnHeadingCells[colIndex] = new GridHeadingCell_1.GridHeadingCell(this.Page, this.Events, 0, colIndex, "col");
            this.gridRowHeading.rowEl.appendChild(this.gridColumnHeadingCells[colIndex].cell);
        }
        this.applyFilterValues();
        this.initialized = true;
    };
    Grid.prototype.setDeltaContent = function (content) {
        if (!content || !content.length) {
            return;
        }
        // update content
        for (var rowIndex = 0; rowIndex < content.length; rowIndex++) {
            var row = content[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                if (!row[colIndex])
                    continue;
                this.Page.originalContent[rowIndex][colIndex] = row[colIndex];
            }
        }
        // update grid cell
        this.Page.deltaContent = JSON.parse(JSON.stringify(content));
        // let gridCells: IGridCell[] = [];
        for (var rowIndex = 0; rowIndex < content.length; rowIndex++) {
            var row = content[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                if (!row[colIndex])
                    continue;
                var gridCell = this.getGridCell(rowIndex, colIndex);
                if (gridCell) {
                    gridCell.update(true);
                    if (this.highlights)
                        gridCell.highlight();
                    // gridCells.push(gridCell);
                    // gridCell.cellText.textContent = `${row[colIndex].value}`;
                }
            }
        }
        this.update(true);
        // this.applyState(true);
        // highlight changes
        // if (this.highlights) {
        //   gridCells.forEach((gridCell) => {
        //     gridCell.highlight();
        //   });
        // }
        // this.forceUpdate = true;
        // this.render();
        // requestAnimationFrame(this.render);
    };
    Grid.prototype.setFreeze = function (row, col) {
        this.freezeRow = row;
        // this.freezeRowTop = this.Page.rowPos(row);
        this.freezeCol = col;
        // this.gridRowFilters.setIndex(row);
        // this.freezeColLeft = this.Page.colPos(col);
    };
    Grid.prototype.setFit = function (fit) {
        this.fit = fit;
    };
    Grid.prototype.setContrast = function (which) {
        this.contrast = which;
        this.pageEl.classList.remove("light", "dark");
        this.pageEl.classList.add(this.contrast);
    };
    Grid.prototype.setHiddenColumns = function (columns) {
        this.Page.hiddenColumns = columns;
        this.Page.hiddenColumns.sort();
    };
    Grid.prototype.update = function (refresh) {
        if (this.applyState(refresh)) {
            this.render();
            return;
        }
        this.applyScale();
        this.updateSelector();
        if (!this.Page.content.length && !this.Page.columnFilters) {
            this.containerEl.style.setProperty("display", "none");
            this.noMatchesEl.style.setProperty("display", "block");
            return;
        }
        else {
            this.containerEl.style.removeProperty("display");
            this.noMatchesEl.style.removeProperty("display");
        }
        this.forceUpdate = true;
        // requestAnimationFrame(this.render);
        this.render();
        this.applySortIndicator();
        // this.applyMergeWidths();
        // if (this.rowRemovalInterval) {
        //   clearInterval(this.rowRemovalInterval);
        //   this.rowRemovalInterval = null;
        // }
        // this.rowRemovalInterval = setInterval(() => {
        //   for (let rowIndex = this.visibleRows.length; rowIndex < this.rows.length; rowIndex++) {
        //     // if (this.visibleRows.includes(this.rows[rowIndex].row)) continue;
        //     this.rows[rowIndex].rowEl.style.setProperty('display', 'none');
        //   }
        // }, 300);
    };
    Grid.prototype.updateState = function (state, internal) {
        this.state = state;
        if (!internal)
            this.applyFilterValues();
        if (!this.initialized)
            return;
        this.forceUpdate = true;
        this.clearSelector();
        this.update();
        // this.applyState();
        // requestAnimationFrame(this.render);
    };
    Grid.prototype.updateColumnDefs = function (columnDefs) {
        this.Page.columnsDefs = columnDefs;
        this.forceUpdate = true;
        this.clearSelector();
        this.update();
    };
    Grid.prototype.updateHeadings = function (headings) {
        this.headings = headings;
        this.forceUpdate = true;
        this.clearSelector();
        this.update();
    };
    Grid.prototype.updateColumnFilters = function (n) {
        this.columnFilters = n;
        this.forceUpdate = true;
        this.clearSelector();
        this.update();
    };
    Grid.prototype.updateFreeze = function (row, col) {
        this.mainEl.scrollTop = 0;
        this.mainEl.scrollLeft = 0;
        this.setFreeze(row, col);
        this.clearSelector();
        this.update();
        // requestAnimationFrame(this.render);
    };
    Grid.prototype.updateFit = function (fit) {
        this.fit = fit;
        // this.applyScale();
        this.update();
        this.updateElSizes();
    };
    Grid.prototype.updateButtons = function (buttons) {
        var _this = this;
        // clear any current buttons
        if (this.buttons) {
            for (var rowIndex = 0; rowIndex < this.buttons.length; rowIndex++) {
                var row = this.buttons[rowIndex];
                if (!row)
                    continue;
                for (var colIndex = 0; colIndex < row.length; colIndex++) {
                    var button = row[colIndex];
                    if (!button)
                        continue;
                    this.applyCellUpdate(rowIndex, colIndex, "button", null);
                }
            }
        }
        this.buttons = [];
        if (!buttons) {
            return;
        }
        buttons.forEach(function (button) {
            var range = helpers.cellRange(button.range, _this.Page.content.length, _this.Page.content[0].length);
            if (range.to.row < 0)
                range.to.row = _this.Page.content.length;
            if (range.to.col < 0)
                range.to.col = _this.Page.content[0].length;
            for (var rowIndex = range.from.row; rowIndex <= range.to.row; rowIndex++) {
                for (var colIndex = range.from.col; colIndex <= range.to.col; colIndex++) {
                    if (!_this.buttons[rowIndex])
                        _this.buttons[rowIndex] = [];
                    _this.buttons[rowIndex][colIndex] = button;
                    _this.applyCellUpdate(rowIndex, colIndex, "button", button);
                }
            }
        });
    };
    Grid.prototype.updateStyles = function (styles) {
        // clear any styles
        if (this.styles) {
            for (var rowIndex = 0; rowIndex < this.styles.length; rowIndex++) {
                var row = this.styles[rowIndex];
                if (!row)
                    continue;
                for (var colIndex = 0; colIndex < row.length; colIndex++) {
                    var style = row[colIndex];
                    if (!style)
                        continue;
                    this.applyCellUpdate(rowIndex, colIndex, "style", null);
                }
            }
        }
        this.styles = [];
        if (!styles || !styles.length) {
            return;
        }
        this.styles = styles;
        this.applyStyles();
    };
    Grid.prototype.updateElements = function (elements) {
        var _this = this;
        this.Page.elements = elements;
        if (!this.initialized)
            return;
        this.elementsEl.innerHTML = "";
        this.Page.elements.forEach(function (element) {
            var cellRange = helpers.cellRange(element.range, _this.Page.originalContent.length, _this.Page.originalContent[0].length);
            var left = _this.Page.colPos(cellRange.from.col);
            var top = _this.Page.rowPos(cellRange.from.row);
            var height = _this.Page.rowPos(cellRange.to.row) - _this.Page.rowPos(cellRange.from.row) + _this.Page.rowHeights[cellRange.to.row];
            var width = _this.Page.colPos(cellRange.to.col) - _this.Page.colPos(cellRange.from.col) + _this.Page.colWidths[cellRange.to.col];
            var div = document.createElement("div");
            div.setAttribute("data-index", "" + top);
            div.setAttribute("data-top", "" + top);
            div.setAttribute("data-left", "" + left);
            div.classList.add("element", element.type);
            if (element.className)
                div.classList.add(element.className);
            div.style.setProperty("top", top + "px");
            div.style.setProperty("left", left + "px");
            div.style.setProperty("width", width + "px");
            div.style.setProperty("height", height + "px");
            _this.elementsEl.appendChild(div);
            _this.scrollElements();
        });
    };
    Grid.prototype.updateColWidth = function (colIndex, cellSize) {
        var _this = this;
        this.Page.colWidths[colIndex] = cellSize;
        this.Page.colWidths.forEach(function (width, colIndex) {
            _this.applyColWidth(colIndex, width);
        });
    };
    Grid.prototype.updateRanges = function (ranges, userId) {
        if (userId === void 0) { userId = 0; }
        if (this.accessRanges) {
            for (var rowIndex = 0; rowIndex < this.accessRanges.length; rowIndex++) {
                var row = this.accessRanges[rowIndex];
                if (!row)
                    continue;
                for (var colIndex = 0; colIndex < row.length; colIndex++) {
                    var gridCell = this.getGridCell(rowIndex, colIndex, true);
                    if (!gridCell)
                        continue;
                    gridCell.setPermission("rw");
                }
            }
        }
        this.accessRanges = [];
        if (!ranges || !ranges.length)
            return;
        // let offset = this.Grid.Settings.headings ? 1 : 0;
        var rowsToUpdate = [];
        for (var i = 0; i < ranges.length; i++) {
            var range = ranges[i];
            var rowEnd = void 0;
            var colEnd = void 0;
            if (range.range) {
                rowEnd = range.range.to.row;
                colEnd = range.range.to.col;
                if (rowEnd === -1) {
                    rowEnd = this.Page.content.length - 1;
                }
                if (colEnd === -1) {
                    colEnd = this.Page.content[0].length - 1;
                }
            }
            if (range instanceof __1.PermissionRange && this.Page.content.length) {
                var userRight = range.getPermission(userId || 0) || (userId ? "rw" : "no");
                var rowEnd_1 = range.rowEnd;
                var colEnd_1 = range.colEnd;
                if (rowEnd_1 === -1) {
                    rowEnd_1 = this.Page.content.length - 1;
                }
                if (colEnd_1 === -1) {
                    colEnd_1 = this.Page.content[0].length - 1;
                }
                for (var i_1 = range.rowStart; i_1 <= rowEnd_1; i_1++) {
                    rowsToUpdate.push(i_1);
                    for (var k = range.colStart; k <= colEnd_1; k++) {
                        if (!this.accessRanges[i_1]) {
                            this.accessRanges[i_1] = [];
                        }
                        if (!this.accessRanges[i_1][k]) {
                            this.accessRanges[i_1][k] = [];
                        }
                        if (userRight) {
                            this.accessRanges[i_1][k] = userRight;
                            this.applyCellUpdate(i_1, k, "permission", userRight);
                        }
                    }
                }
            }
        }
        if (rowsToUpdate.length) {
            // this.updateSoftMerges(rowsToUpdate);
        }
    };
    Grid.prototype.getContentHtml = function (type) {
        var only = type === "symphony" ? helpers.getSymphonyStyles() : [];
        var html = "<table style=\"border-collapse: collapse;\">";
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            if (this.Page.hiddenRows.indexOf(row[0].index.row) > -1)
                continue;
            if (this.selection.rowFrom > -1 && this.selection.rowTo > -1) {
                if (rowIndex < this.selection.rowFrom || rowIndex > this.selection.rowTo)
                    continue;
            }
            html += "<tr>";
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                if (this.state.hiddenColumns.indexOf(colIndex) > -1)
                    continue;
                if (this.selection.colFrom > -1 && this.selection.colTo > -1) {
                    if (colIndex < this.selection.colFrom || colIndex > this.selection.colTo)
                        continue;
                }
                var cell = row[colIndex];
                var gridCell = this.getGridCell(cell.index.row, colIndex);
                var styles = gridCell ? gridCell.getFlattenStyles(only) : "";
                html += "<td style=\"" + styles + "\"><div>" + (cell.formatted_value || cell.value || "&nbsp;") + "</div></td>";
            }
            html += "</tr>";
        }
        html += "</table>";
        return html;
    };
    Grid.prototype.find = function (query) {
        this.clearFound();
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                var regex = new RegExp(query, "ig");
                if (!regex.test("" + col.value) && !regex.test("" + col.formatted_value)) {
                    continue;
                }
                if (!this.found[rowIndex]) {
                    this.found[rowIndex] = [];
                }
                this.found[col.index.row][colIndex] = col;
                this.applyCellUpdate(col.index.row, col.index.col, "found", true);
            }
        }
    };
    Grid.prototype.clearFound = function () {
        if (this.found && this.found.length) {
            for (var rowIndex = 0; rowIndex < this.found.length; rowIndex++) {
                var row = this.found[rowIndex];
                if (!row)
                    continue;
                for (var colIndex = 0; colIndex < row.length; colIndex++) {
                    var found = row[colIndex];
                    if (!found)
                        continue;
                    this.applyCellUpdate(rowIndex, colIndex, "found", null);
                }
            }
        }
        this.found = [];
    };
    Grid.prototype.createSchema = function (columnKeys, hiddenColumns) {
        var pageSchema = new Schema_1.PageSchema();
        var content = this.Page.content;
        pageSchema.importContent(content, columnKeys, 2, hiddenColumns);
        return pageSchema;
    };
    Grid.prototype.applyStyles = function () {
        for (var rowIndex = 0; rowIndex < this.styles.length; rowIndex++) {
            var row = this.styles[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var style = this.styles[rowIndex][colIndex];
                if (!style)
                    continue;
                this.styles[rowIndex][colIndex] = style;
                this.applyCellUpdate(rowIndex, colIndex, "style", style);
            }
        }
    };
    Grid.prototype.applyButtons = function () {
        for (var rowIndex = 0; rowIndex < this.buttons.length; rowIndex++) {
            var row = this.buttons[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var style = this.buttons[rowIndex][colIndex];
                if (!style)
                    continue;
                this.buttons[rowIndex][colIndex] = style;
                this.applyCellUpdate(rowIndex, colIndex, "button", style);
            }
        }
    };
    Grid.prototype.applyRanges = function () {
        for (var rowIndex = 0; rowIndex < this.accessRanges.length; rowIndex++) {
            var row = this.accessRanges[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; row++) {
                var userRight = this.accessRanges[rowIndex][colIndex];
                if (!userRight)
                    continue;
                this.applyCellUpdate(rowIndex, colIndex, "permission", userRight);
            }
        }
    };
    Grid.prototype.applyCellUpdate = function (rowIndex, colIndex, which, value) {
        var gridCell = this.getGridCell(rowIndex, colIndex);
        if (!gridCell)
            return;
        // if (!gridCell.init) return;
        if (which === "sorting")
            gridCell.setSorting(value);
        if (which === "permission")
            gridCell.setPermission(value);
        if (which === "style")
            gridCell.setStyle(value);
        if (which === "button")
            gridCell.setButton(value);
        if (which === "found")
            gridCell.setFound(value);
    };
    Grid.prototype.hideSelector = function () {
        this.clearSelector();
    };
    Grid.prototype.clearSelector = function () {
        if (this.editing) {
            this.input.blur();
            this.editing = false;
        }
        this.resetSelection();
        this.Page.selectedCols = [];
        this.Page.selectedRows = [];
        this.isFocus = this.hasFocus = false;
        this.updateSelector();
        requestAnimationFrame(this.render);
    };
    Grid.prototype.setSelectorByRef = function (str) {
        var range = helpers.cellRange(str, this.Page.rows, this.Page.cols);
        // let cellFrom = this.content[range.from.row][range.from.col];
        // let cellTo = this.content[range.to.row][range.to.col];
        var from = this.getGridCell(range.from.row, range.from.col);
        var to = this.getGridCell(range.to.row, range.to.col);
        if (!from || !to)
            return;
        this.selection.from = from;
        this.selection.to = to;
        this.updateSelector();
        if (!this.keepSelectorInView())
            requestAnimationFrame(this.render);
    };
    Grid.prototype.setSelectorByIndex = function (fromIndex, toIndex) {
        // let range = helpers.cellRange(str, this.Page.rows, this.Page.cols);
        // let cellFrom = this.content[range.from.row][range.from.col];
        // let cellTo = this.content[range.to.row][range.to.col];
        var from = this.getGridCell(fromIndex.row, fromIndex.col);
        var to = this.getGridCell(toIndex ? toIndex.row : fromIndex.row, toIndex ? toIndex.col : fromIndex.col);
        if (!from || !to)
            return;
        this.selection.from = from;
        this.selection.to = to;
        this.updateSelector();
        if (!this.keepSelectorInView())
            requestAnimationFrame(this.render);
    };
    Grid.prototype.getSelection = function (event) {
        if (!this.selection.from || !this.selection.to) {
            throw new Error("Selection not found");
        }
        var reference = helpers.getCellsReference(this.selection.from.position, this.selection.to.position, "");
        var count = 0;
        if (this.selection.heading) {
            var from = "";
            var to = "";
            if (this.selection.type === "col") {
                from = helpers.toColumnName(this.selection.from.col + 1);
                to = helpers.toColumnName(this.selection.to.col + 1);
                count = this.selection.colTo + 1 - this.selection.colFrom;
            }
            if (this.selection.type === "row" || this.selection.type === "all") {
                from = "" + (this.selection.from.row + 1);
                to = "" + (this.selection.to.row + 1);
                count = this.selection.rowTo + 1 - this.selection.rowFrom;
            }
            reference = from == to ? from : from + ":" + to;
        }
        return {
            reference: reference,
            selection: {
                from: this.selection.from.getDetails(),
                to: this.selection.to.getDetails(),
                heading: this.selection.heading,
                inside: this.selection.inside,
                reference: reference,
                keyboard: this.selection.keyboard,
                type: this.selection.type,
            },
            rightClick: this.rightClick,
            event: event,
            which: this.selection.type,
            count: count,
            multiple: this.selection.from.reference != this.selection.to.reference,
            colFrom: this.selection.colFrom,
            colTo: this.selection.colTo,
            rowFrom: this.selection.rowFrom,
            rowTo: this.selection.rowTo,
        };
    };
    Grid.prototype.editCell = function (row, col, value, click) {
        var gridCell = this.getGridCell(row, col);
        if (!gridCell || (!this.canEdit && !gridCell.cellMap) || this.rightClick)
            return;
        if (this.Page.columnsDefs.length && !gridCell.row)
            return;
        if (gridCell.button && ["checkbox", "select", "rotate", "event"].includes(gridCell.button.type)) {
            if (gridCell.button.type === "checkbox") {
                if ((this.rowChecked && !gridCell.button.checkboxToggle && gridCell.permission == "rw") || gridCell.button.checkboxToggle)
                    gridCell.onButton();
            }
            if (gridCell.permission != "rw")
                return;
            if (gridCell.button.type === "select" || (gridCell.button.type === "event" && gridCell.button.options.length > 1)) {
                gridCell.openOptions();
                // attach popper to parent
                if (this.popperParent)
                    this.popperParent.appendChild(gridCell.cellPopper);
                else
                    this.pageEl.appendChild(gridCell.cellPopper);
                // let timer: any = setInterval(()=>{
                //   if (!gridCell || !gridCell.cellPopper.parentElement) return;
                //   clearInterval(timer);
                //   timer = null;
                this.buttonPopper = new popper_js_1.default(gridCell.cell, gridCell.cellPopper, {
                    placement: "bottom-start",
                    modifiers: {
                        preventOverflow: {
                            boundariesElement: "window",
                        },
                    },
                });
                // }, 10);
                if (gridCell.button.type != "event")
                    this.editing = true;
            }
            if (gridCell.button.type === "rotate") {
                gridCell.onButton();
            }
        }
        else if (!click || (click && this.doubleClicked)) {
            if (gridCell.permission != "rw")
                return;
            // gridCell.edit(value || this.keyValue);
            this.editValue = value || this.keyValue;
            // this.input.value = '';
            this.input.style.cssText = gridCell.getInputStyles();
            if (!this.keyValue) {
                this.input.value = gridCell.getValue();
            }
            this.editDone = this.keyValue || this.Page.touch ? true : false;
            this.input.style.setProperty("visibility", "visible");
            // if (this.Page.touch) {
            this.input.focus();
            // } else {
            //   let interval = setInterval(() => {
            //     if (!this.input.offsetParent) return;
            //     clearInterval(interval);
            //     this.input.focus();
            //   }, 20);
            // }
            this.editing = true;
        }
    };
    Grid.prototype.getSelectedCells = function () {
        var cells = [];
        if (!this.selection.from || !this.selection.to)
            return cells;
        // this.grid.Page.selectedRows
        // this.grid.Page.selectedCols
        for (var rowIndex = 0; rowIndex < this.Page.content.length; rowIndex++) {
            // if (!this.Page.selectedRows.includes(rowIndex)) continue;
            var row = this.Page.content[rowIndex];
            if (!this.Page.selectedRows.includes(row[0].index.row))
                continue;
            // if (
            //   row[0].index!.row < this.selection.rowFrom ||
            //   row[0].index!.row > this.selection.rowTo
            // )
            //   continue;
            var cellRow = [];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                // if (!this.Page.selectedCols.includes(colIndex)) continue;
                if (!this.Page.selectedCols.includes(colIndex))
                    continue;
                // if (
                //   colIndex < this.selection.colFrom ||
                //   colIndex > this.selection.colTo
                // )
                //   continue;
                var cell = row[colIndex];
                var gridCell = this.getGridCell(cell.index.row, colIndex);
                if (gridCell)
                    cellRow.push(gridCell);
            }
            cells.push(cellRow);
        }
        return cells;
    };
    Grid.prototype.getGridCell = function (rowIndex, colIndex, ignore) {
        if (!this.Page.originalContent[rowIndex] || !this.Page.originalContent[rowIndex][colIndex])
            return null;
        if (!this.gridCells[rowIndex]) {
            // return null;
            if (ignore)
                return null;
            this.gridCells[rowIndex] = [];
        }
        if (!this.gridCells[rowIndex][colIndex]) {
            // return null;
            if (ignore)
                return null;
            this.gridCells[rowIndex][colIndex] = new GridCell_1.GridCell(this.Page, this.Events, rowIndex, colIndex);
            // this.gridCells[rowIndex][colIndex].update(rowIndex, colIndex, true);
            // if (this.rows[rowIndex])
            // this.gridCells[rowIndex][colIndex].rowIndex = this.rows[rowIndex].rowIndex;
        }
        return this.gridCells[rowIndex][colIndex];
    };
    Grid.prototype.init = function () {
        this.reset();
        // this.destroy();
        this.dom();
        if (!this.initialized)
            this.createListeners();
    };
    Grid.prototype.reset = function () {
        // this.clearSelector();
        this.destroyPopper();
        this.gridCells = [];
        this.rows = [];
        this.colRows = [];
        this.cornerRows = [];
        this.gridRowHeadingCells = [];
        this.gridColumnHeadingCells = [];
        this.colStyles = [];
        this.selection.from = undefined;
        this.selection.to = undefined;
        this.selection.rowFrom = -1;
        this.selection.rowTo = -1;
        this.selection.colFrom = -1;
        this.selection.colTo = -1;
        this.pageEl.innerHTML = "";
    };
    Grid.prototype.createListeners = function () {
        window.addEventListener("resize", this.onResize);
        window.addEventListener("keydown", this.onKeydown);
        window.addEventListener("keyup", this.onKeyup);
        if (this.Page.touch) {
            window.addEventListener("touchstart", this.onMouseDown);
            window.addEventListener("touchend", this.onMouseUp);
            window.addEventListener("touchmove", this.onMouseMove);
        }
        else {
            window.addEventListener("mousedown", this.onMouseDown);
            window.addEventListener("mousemove", this.onMouseMove);
            window.addEventListener("mouseup", this.onMouseUp);
        }
        this.Events.on(this.Events.CELL, this.onCell);
    };
    Grid.prototype.destroyListeners = function () {
        window.removeEventListener("resize", this.onResize);
        window.removeEventListener("keydown", this.onKeydown);
        window.removeEventListener("keyup", this.onKeyup);
        if (this.Page.touch) {
            window.removeEventListener("touchstart", this.onMouseDown);
            window.removeEventListener("touchend", this.onMouseUp);
            window.removeEventListener("touchmove", this.onMouseMove);
        }
        else {
            window.removeEventListener("mousedown", this.onMouseDown);
            window.removeEventListener("mousemove", this.onMouseMove);
            window.removeEventListener("mouseup", this.onMouseUp);
        }
        // this.Events.off(this.Events.CELL, this.onCell);
        this.Events.removeEvents();
    };
    Grid.prototype.dom = function () {
        var _this = this;
        // grid image
        this.Canvas = new GridCanvas_1.default(this.pageEl);
        // stylesheet
        this.styleSheet = document.createElement("style");
        this.styleSheet.type = "text/css";
        this.rowStyle = document.createTextNode(".grid-" + this.Page.uuid + " .grid-row {}");
        this.sortStyles = document.createTextNode("");
        this.styleSheet.appendChild(this.rowStyle);
        this.styleSheet.appendChild(this.sortStyles);
        this.pageEl.appendChild(this.styleSheet);
        this.getScrollbarWidth();
        this.pageEl.classList.add(this.contrast);
        this.pageEl.oncontextmenu = function () {
            return false;
        };
        this.pageElRect = this.pageEl.getBoundingClientRect();
        // scale
        // this.scaleEl = document.createElement('div');
        // this.scaleEl.className = 'grid-scale';
        // this.pageEl.appendChild(this.scaleEl);
        // edit el
        // this.editEl = document.createElement('div');
        // this.editEl.classList.add('grid-edit');
        // this.pageEl.appendChild(this.editEl);
        // scroller
        // this.scrollEl = document.createElement('div');
        // this.scrollEl.className = 'grid-scroller';
        // this.scrollEl.appendChild(this.boxEl);
        // this.pageEl.appendChild(this.scrollEl);
        // no matches
        this.noMatchesEl = document.createElement("div");
        this.noMatchesEl.className = "grid-no-matches";
        this.noMatchesEl.textContent = "No rows match filter";
        this.noMatchesEl.style.setProperty("display", "none");
        this.pageEl.appendChild(this.noMatchesEl);
        // container
        this.containerEl = document.createElement("div");
        this.containerEl.className = "grid-container";
        this.pageEl.appendChild(this.containerEl);
        this.mainEl = document.createElement("div");
        this.mainEl.className = "grid-main";
        this.freezeRowEl = document.createElement("div");
        this.freezeRowEl.className = "grid-freeze-rows";
        this.freezeColEl = document.createElement("div");
        this.freezeColEl.className = "grid-freeze-columns";
        this.freezeCornerEl = document.createElement("div");
        this.freezeCornerEl.className = "grid-freeze-corner";
        // this.headingRow = document.createElement('div');
        // this.headingRow.className = 'grid-heading-row';
        // this.stickyEl.appendChild(this.headingRow);
        this.containerEl.appendChild(this.mainEl);
        this.containerEl.appendChild(this.freezeRowEl);
        this.containerEl.appendChild(this.freezeColEl);
        this.containerEl.appendChild(this.freezeCornerEl);
        // this.containerEl.addEventListener('wheel', this.onScrollWheel);
        // scroll boxes
        this.boxEl = document.createElement("div");
        this.boxEl.className = "grid-box";
        this.mainEl.appendChild(this.boxEl);
        this.mainEl.addEventListener("scroll", this.onScroll);
        this.boxColEl = document.createElement("div");
        this.boxColEl.className = "grid-box";
        this.freezeColEl.appendChild(this.boxColEl);
        this.freezeColEl.addEventListener("wheel", this.onScrollFreezeCol);
        this.boxRowEl = document.createElement("div");
        this.boxRowEl.className = "grid-box";
        this.freezeRowEl.appendChild(this.boxRowEl);
        // selector
        // this.selectorWrapperEl = document.createElement('div');
        // this.selectorBoxEl = document.createElement('div');
        // this.selectorWrapperEl.className = 'grid-selector-wrapper';
        // this.selectorBoxEl.className = 'grid-box';
        this.selectorEl = document.createElement("div");
        this.selectorEl.className = "grid-selector";
        this.selectorEl.style.setProperty("display", "none");
        // input
        this.input = document.createElement("input");
        this.input.style.setProperty("visibility", "hidden");
        this.input.classList.add("grid-input");
        this.input.setAttribute("autocapitalize", "none");
        this.input.addEventListener("keydown", function (evt) {
            if ((["ArrowLeft", "ArrowUp", "ArrowRight", "ArrowDown"].indexOf(evt.key) > -1 && _this.editValue) || ["Enter", "Tab"].indexOf(evt.key) > -1) {
                _this.editDone = true;
                _this.input.blur();
                // this.emit(this.editDone, input.value);
                // this.emit(this.STOP, evt.key);
            }
            else if (evt.key === "Escape") {
                _this.editDone = false;
                _this.input.blur();
                _this.Events.emit(_this.Events.CELL_RESET, _this.selection.from);
                evt.stopPropagation();
            }
            else if (["Home", "End"].indexOf(evt.key) > -1) {
                evt.stopPropagation();
            }
            else {
                _this.editDone = true;
            }
            // console.log('keydown', this.editDone);
        });
        this.input.addEventListener("keyup", function () {
            // console.log('keyup', this.editDone);
            // this.emit(this.editValue, input.value);
            _this.Events.emit(_this.Events.CELL, {
                cell: _this.selection.from,
                value: _this.input.value,
                evt: {
                    type: "value",
                },
            });
        });
        this.input.addEventListener("blur", function (evt) {
            // console.log('keyblur', this.editDone, this.selection.from!.content.formatted_value);
            if (_this.editDone) {
                // this.content.formatted_value = this.content.value = this.oldValue;
                // this.content = { ...this.Page.originalContent[this.row][this.col]};
                // } else {
                if (_this.selection.from.cellMap) {
                    _this.selection.from.mapValue = _this.input.value;
                }
                else {
                    _this.selection.from.content.formatted_value = _this.selection.from.content.value = _this.input.value;
                }
            }
            // this.editing = false;
            _this.input.value = "";
            // this.input.parentElement!.removeChild(this.input);
            _this.input.style.setProperty("visibility", "hidden");
            // this.cell.style.setProperty('z-index', `${this.params.zIndexCol}`);
            // this.editDone = false;
            // this.selection.from!.update(true);
            // TODO softmerges
            _this.Events.emit(_this.Events.CELL, {
                cell: _this.selection.from,
                evt: {
                    type: "blur",
                },
                done: _this.editDone,
            });
        });
        this.input.addEventListener("focus", function (evt) {
            console.log("keyfocus");
            if (!_this.editValue) {
                _this.input.selectionStart = 0;
                _this.input.selectionEnd = 999;
            }
            else {
                _this.input.selectionEnd = 999;
            }
            console.log("input");
        });
        this.selectorEl.appendChild(this.input);
        this.containerEl.appendChild(this.selectorEl);
        // this.selectorWrapperEl.appendChild(this.selectorBoxEl);
        // this.pageEl.appendChild(this.selectorWrapperEl);
        // columns
        this.columnsEl = document.createElement("div");
        this.columnsEl.className = "grid-columns";
        this.columnSplitEl = document.createElement("div");
        this.columnSplitEl.className = "grid-column-split";
        this.containerEl.appendChild(this.columnsEl);
        this.containerEl.appendChild(this.columnSplitEl);
        // ranges
        this.elementsEl = document.createElement("div");
        this.elementsEl.className = "grid-elements";
        // this.elementsBoxEl = document.createElement("div");
        // this.elementsBoxEl.className = "grid-box";
        // this.elementsEl.appendChild(this.elementsBoxEl);
        this.containerEl.appendChild(this.elementsEl);
        // sort el
        this.sortWrapperEl = document.createElement("div");
        this.sortWrapperEl.classList.add("grid-sorting-wrapper");
        this.sortEl = document.createElement("div");
        this.sortEl.innerHTML = "<div></div>";
        this.sortEl.classList.add("grid-sorting");
        this.sortWrapperEl.appendChild(this.sortEl);
        // this.containerEl.appendChild(this.sortWrapperEl);
    };
    Grid.prototype.getScrollbarWidth = function () {
        // Creating invisible container
        var outer = document.createElement("div");
        outer.style.visibility = "hidden";
        outer.style.overflow = "scroll"; // forcing scrollbar to appear
        outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps
        this.pageEl.appendChild(outer);
        // Creating inner element and placing it in the container
        var inner = document.createElement("div");
        outer.appendChild(inner);
        // Calculating difference between container's full width and the child width
        var scrollbarWidth = outer.offsetWidth - inner.offsetWidth;
        // Removing temporary elements from the DOM
        outer.parentNode.removeChild(outer);
        this.Page.scrollbarSize = scrollbarWidth;
    };
    Grid.prototype.updateElSizes = function () {
        var offsetLeft = this.headings ? this.Page.headingsWidth : 0;
        var offsetTop = this.headings ? this.Page.headingsHeight : 0;
        this.Page.scrollbarWidth = this.Page.height + offsetTop > this.pageEl.offsetHeight ? this.Page.scrollbarSize : 0;
        this.Page.scrollbarHeight = this.Page.width + offsetLeft + this.Page.scrollbarWidth > this.pageEl.offsetWidth ? this.Page.scrollbarSize : 0;
        var width = this.headings ? this.Page.width - this.Page.headingsWidth - this.freezeColLeft : this.Page.width - this.freezeColLeft;
        var overflowX = this.Page.scrollbarHeight ? "bottom: " + this.Page.scrollbarHeight + "px" : "";
        var overflowY = this.Page.scrollbarWidth ? "right: " + this.Page.scrollbarWidth + "px" : "";
        this.rowStyle.textContent = "\n      .grid-" + this.Page.uuid + ".headings .grid-freeze-corner .grid-row.cells {  \n        transform: translate(0px, " + offsetTop + "px)  \n      }\n      .grid-" + this.Page.uuid + " .grid-freeze-columns { \n        top: " + this.Page.filtersOffset + "px;\n        " + overflowX + "\n      }\n      .grid-" + this.Page.uuid + ".headings .grid-freeze-columns { \n        top: " + (offsetTop + this.Page.filtersOffset) + "px; \n      }\n      .grid-" + this.Page.uuid + " .grid-freeze-corner { \n        \n      }\n      .grid-" + this.Page.uuid + " .grid-freeze-rows { \n        left: " + offsetLeft + "px; " + overflowY + " \n      } \n      .grid-" + this.Page.uuid + ".headings .grid-freeze-rows { \n        left: " + offsetLeft + "px; " + overflowY + " \n      } \n      .grid-" + this.Page.uuid + ".filters .grid-freeze-corner .grid-row.filter .grid-cell{ \n        transform: translate(" + offsetLeft + "px, 0px) \n      }\n      .grid-" + this.Page.uuid + ".headings .grid-freeze-rows .grid-row.cells { \n        transform: translate(0px, " + offsetTop + "px) \n      } \n      .grid-" + this.Page.uuid + " .grid-main { \n        left: " + offsetLeft + "px; \n        top: " + (this.freezeRowTop + offsetTop + this.Page.filtersOffset) + "px; \n      }\n      .grid-" + this.Page.uuid + " .grid-main .grid-row { \n        transform: translateY(-" + this.freezeRowTop + "px)  \n      }\n      .grid-" + this.Page.uuid + " .grid-cell-heading.grid-cell-heading-col,\n      .grid-" + this.Page.uuid + " .grid-cell-heading.grid-cell-heading-corner {\n        height: " + offsetTop + "px;\n      }\n      .grid-" + this.Page.uuid + " .grid-cell-heading.grid-cell-heading-row,\n      .grid-" + this.Page.uuid + " .grid-cell-heading.grid-cell-heading-corner {\n        width: " + this.Page.headingsWidth + "px;\n      }\n      .grid-" + this.Page.uuid + " .grid-elements { \n        left: " + offsetLeft + "px; \n        top: " + offsetTop + "px; \n      }      \n    ";
        this.boxEl.style.setProperty("width", this.Page.width - this.freezeColLeft + "px");
        this.boxEl.style.setProperty("left", this.freezeColLeft + "px");
        this.boxEl.style.setProperty("height", this.Page.height - this.freezeRowTop + "px");
        this.boxColEl.style.setProperty("left", offsetLeft + "px");
        // this.boxEl.style.setProperty("top", `${this.freezeRowTop}px`);
        this.boxColEl.style.setProperty("width", this.Page.width + "px");
        this.boxColEl.style.setProperty("height", this.Page.height + "px");
        this.boxRowEl.style.setProperty("width", this.Page.width - this.freezeColLeft + "px");
        this.boxRowEl.style.setProperty("left", this.freezeColLeft + "px");
        this.boxRowEl.style.setProperty("height", this.Page.height + "px");
        // this.selectorBoxEl.style.setProperty('width', `${this.Page.width + width}px`);
        // this.selectorBoxEl.style.setProperty('height', `${this.Page.height + height}px`);
        // this.wrapperEl.style.setProperty('width', `${this.Page.width}px`);
        // this.wrapperEl.style.setProperty('height', `${this.Page.height}px`);
        // this.containerEl.style.setProperty('right', `${height}px`);
        // this.containerEl.style.setProperty('bottom', `${width}px`);
        // this.elementsBoxEl.style.setProperty("width", `${this.Page.width}px`);
        // this.elementsBoxEl.style.setProperty("height", `${this.Page.height}px`);
    };
    Grid.prototype.updateSelector = function () {
        if (!this.selection.from || !this.selection.to || (this.selection.rowFrom > -1 && !this.Page.content[this.selection.rowFrom])) {
            this.selectorEl.style.setProperty("display", "none");
            this.resetSelection();
            // this.clearSelector();
            return;
        }
        // set selected rows and columns
        var fromRow = this.selection.from.rowIndex;
        var toRow = this.selection.to.rowIndex;
        var flipRow = toRow < fromRow;
        var flipCol = this.selection.to.col < this.selection.from.col;
        // console.log(flipRow, flipCol);
        this.selection.rowFrom = flipRow ? this.selection.to.rowIndex : this.selection.from.rowIndex;
        this.selection.rowTo = flipRow ? this.selection.from.rowIndex : this.selection.to.rowIndex;
        this.selection.colFrom = flipCol ? this.selection.to.col : this.selection.from.col;
        this.selection.colTo = flipCol ? this.selection.from.col : this.selection.to.col;
        this.Page.selectedRows = [];
        this.Page.selectedCols = [];
        for (var index = this.selection.rowFrom; index <= this.selection.rowTo; index++) {
            this.Page.selectedRows.push(this.Page.content[index][0].index.row);
        }
        for (var index = this.selection.colFrom; index <= this.selection.colTo; index++) {
            this.Page.selectedCols.push(index);
        }
        // let from = this.getGridCell(this.selection.rowFrom, this.selection.colFrom)!;
        // let to = this.getGridCell(this.selection.rowTo, this.selection.colTo)!;
        // let pos = this.Page.cellPos(this.selection.rowFrom, this.selection.colFrom);
        // let pos2 = this.Page.cellPos(this.selection.rowTo, this.selection.colTo);
        // let width = pos2.x + this.selection.to.dataset.width - pos.x;
        // let height = pos2.y + this.selection.to.dataset.height - pos.y;
        var top = this.rows[this.selection.from.row].top;
        if (this.headings)
            top += this.Page.headingsHeight;
        if (this.selection.rowFrom >= this.freezeRow)
            top += this.Page.filtersOffset;
        var left = this.headings ? this.selection.from.dataset.x + this.Page.headingsWidth : this.selection.from.dataset.x;
        this.selectorEl.style.setProperty("left", left + "px");
        this.selectorEl.style.setProperty("top", top + "px");
        // this.selectorEl.style.setProperty('width', `${this.selection.from.dataset.width}px`);
        this.selectorEl.className = "grid-selector grid-col-" + this.selection.from.col;
        var height = this.Page.rowHeights[this.selection.from.rowIndex];
        this.selectorEl.style.setProperty("height", height + "px");
        this.selectorEl.style.setProperty("display", "block");
        this.selectorEl.style.removeProperty("clip-path");
        // this.selectorEl.style.setProperty(
        //   'position',
        //   this.selection.from.col < this.freezeCol ? 'sticky' : 'absolute'
        // );
        if (this.selection.from.row < this.freezeRow && this.selection.from.col < this.freezeCol) {
            this.selectorEl.classList.add("corner");
        }
        else
            this.selectorEl.classList.remove("corner");
        if (this.selection.from.row < this.freezeRow && this.selection.from.col >= this.freezeCol)
            this.selectorEl.classList.add("row");
        else
            this.selectorEl.classList.remove("row");
        if (this.selection.from.row >= this.freezeRow && this.selection.from.col < this.freezeCol)
            this.selectorEl.classList.add("col");
        else
            this.selectorEl.classList.remove("col");
        this.scrollSelector();
        // let from = this.selection.from;
        // let to = this.selection.to || this.selection.from;
        // this.selectorEl.style.setProperty('display', 'block');
        // let pos = this.Page.cellPos(this.selection.from.row, this.selection.from.col);
        // this.selectorEl.style.setProperty('left', `${pos.x}px`);
        // this.selectorEl.style.setProperty('top', `${pos.y}px`);
        // let pos2 = this.Page.cellPos(to.row, to.col);
        // this.selectorEl.style.setProperty('width', `${to.dataset.width}px`);
        // this.selectorEl.style.setProperty('height', `${to.dataset.height}px`);
        this.Page.selection = this.selection;
    };
    Grid.prototype.scrollSelector = function () {
        // if (this.state.sorting.enabled && this.state.hiddenColumns.indexOf(this.state.sorting.col) < 0) {
        //   // this.sortEl.style.setProperty('top', `${this.mainEl.scrollTop + 4}px`);
        //   if (this.state.sorting.col < this.freezeCol) {
        //     let left =
        //       (this.Page.colPos(this.state.sorting.col) + this.Page.colWidths[this.state.sorting.col] - 4) * this.scale;
        //     // this.sortEl.style.setProperty('left', `${left}px`);
        //   }
        // }
        if (!this.selection.from)
            return;
        if (this.selection.from.row >= this.freezeRow) {
            var top_1 = this.rows[this.selection.from.row].top;
            if (this.headings)
                top_1 += this.Page.headingsHeight;
            top_1 += this.Page.filtersOffset;
            // top *= this.scale;
            top_1 -= this.mainEl.scrollTop;
            this.selectorEl.style.setProperty("top", top_1 + "px");
        }
        // left *= this.scale;
        if (this.selection.from.col >= this.freezeCol) {
            var left = this.headings ? this.selection.from.dataset.x + this.Page.headingsWidth : this.selection.from.dataset.x;
            left -= this.mainEl.scrollLeft;
            this.selectorEl.style.setProperty("left", left + "px");
        }
    };
    Grid.prototype.scrollElements = function () {
        var _this = this;
        // scroll elements
        var elements = document.querySelectorAll(".grid-elements .element");
        elements.forEach(function (el) {
            var div = el;
            if (!div.dataset)
                return;
            div.style.setProperty("top", parseFloat("" + div.dataset.top) - _this.mainEl.scrollTop + "px");
            div.style.setProperty("left", parseFloat("" + div.dataset.left) - _this.mainEl.scrollLeft + "px");
        });
    };
    Grid.prototype.keepSelectorInView = function (to) {
        if (!this.selection.from)
            return false;
        if (to && !this.selection.to)
            return false;
        var which = to ? "to" : "from";
        var row = this.selection[which].rowIndex;
        var col = this.selection[which].col;
        if (to) {
            if (!this.selection.heading || this.selection.type === "col") {
                if (this.selection.to.col < this.selection.from.col) {
                    col--;
                    if (col < 0)
                        col = 0;
                }
                else {
                    col++;
                    if (col >= this.Page.content[0].length)
                        col = this.Page.content[0].length - 1;
                }
            }
            if (!this.selection.heading || this.selection.type === "row") {
                if (this.selection.to.rowIndex < this.selection.from.rowIndex) {
                    row--;
                    if (row < 0)
                        row = 0;
                }
                else {
                    row++;
                    if (row >= this.Page.content.length)
                        row = this.Page.content.length - 1;
                }
            }
        }
        if (this.selection.type === "col") {
            row = this.selection.from.row;
        }
        if (this.selection.type === "row") {
            col = this.selection.from.col;
        }
        // let scrollLeft = this.getScrollLeft(col);
        // let scrollTop = this.getScrollTop(row);
        // console.log('keepSelectorInView', scrollLeft, scrollTop);
        // let scrolled = false;
        // if (scrollLeft >= 0 && scrollLeft != this.mainEl.scrollLeft) {
        //   this.mainEl.scrollLeft = scrollLeft;
        //   scrolled = true;
        // }
        // if (scrollTop >= 0 && scrollTop != this.mainEl.scrollTop) {
        //   this.mainEl.scrollTop = scrollTop;
        //   scrolled = true;
        // }
        return this.scrollTo(row, col);
    };
    Grid.prototype.scrollTo = function (row, col) {
        var scrollLeft = this.getScrollLeft(col);
        var scrollTop = this.getScrollTop(row);
        console.log("keepSelectorInView", scrollLeft, scrollTop);
        var scrolled = false;
        if ((!this.mainEl.scrollLeft && scrollLeft > 0) ||
            (scrollLeft >= 0 && scrollLeft != this.mainEl.scrollLeft && scrollLeft < this.mainEl.scrollWidth - this.mainEl.offsetWidth + this.Page.scrollbarWidth)) {
            this.mainEl.scrollLeft = scrollLeft;
            scrolled = true;
        }
        if ((!this.mainEl.scrollTop && scrollTop > 0) ||
            (scrollTop >= 0 && scrollTop != this.mainEl.scrollTop && scrollTop < this.mainEl.scrollHeight - this.mainEl.offsetHeight + this.Page.scrollbarHeight)) {
            this.mainEl.scrollTop = scrollTop;
            scrolled = true;
        }
        return scrolled;
    };
    Grid.prototype.applyState = function (refresh) {
        this.Page.content = this.Page.originalContent.concat();
        this.applyFilters();
        this.applySorting();
        var rowLen = this.Page.content.length;
        // this.Page.rows = rowLen;
        if (!this.Page.colWidths.length || rowLen != this.Page.rowHeights.length) {
            refresh = false;
        }
        if (!refresh) {
            this.Page.rowHeights = [];
            this.Page.colWidths = [];
        }
        this.Page.height = 0;
        this.Page.width = 0;
        // hidden columns
        var hiddenColumns = this.state.hiddenColumns || [];
        // this.Page.hiddenColumns = (this.state.hiddenColumns || []).sort();
        // check if column defs have hidden columns
        var hiddenColumnDefs = this.Page.columnsDefs
            .map(function (def, defIndex) {
            return def.hidden ? defIndex : -1;
        })
            .filter(function (def) {
            return def > -1;
        });
        hiddenColumns = hiddenColumns.concat(hiddenColumnDefs);
        hiddenColumns.sort();
        this.Page.hiddenColumns = hiddenColumns;
        var cssCol = [];
        for (var visibleIndex = 0; visibleIndex < rowLen; visibleIndex++) {
            var row_1 = this.Page.content[visibleIndex];
            var firstCell = row_1[0];
            var rowIndex = firstCell.index.row;
            var h = parseFloat("" + firstCell.style.height);
            if (!h && !this.Page.hiddenRows.includes(rowIndex)) {
                this.Page.hiddenRows.push(rowIndex);
            }
            if (!refresh) {
                this.Page.rowHeights.push(h);
            }
            this.Page.height += h;
            var colLen = row_1.length;
            if (!visibleIndex)
                this.Page.cols = colLen;
            // setup/update row
            var gridRow = void 0;
            var gridColRow = void 0;
            var gridCornerRow = void 0;
            // check if corner row needs to be created
            if (rowIndex < this.freezeRow && !this.cornerRows[rowIndex]) {
                gridCornerRow = new GridRow_1.GridRow(this.Page, this.Events, rowIndex);
                this.cornerRows[rowIndex] = gridCornerRow;
            }
            // create rows
            if (!this.rows[rowIndex]) {
                gridRow = new GridRow_1.GridRow(this.Page, this.Events, rowIndex);
                this.rows[rowIndex] = gridRow;
                gridColRow = new GridRow_1.GridRow(this.Page, this.Events, rowIndex);
                this.colRows[rowIndex] = gridColRow;
                if (!this.gridCells[rowIndex]) {
                    this.gridCells[rowIndex] = [];
                }
            }
            else {
                gridRow = this.rows[rowIndex];
                gridColRow = this.colRows[rowIndex];
                gridCornerRow = this.cornerRows[rowIndex];
            }
            // if (gridRow.row < this.freezeRow) {
            //   this.stickyEl.appendChild(gridRow.rowEl);
            // }
            gridRow.setIndex(visibleIndex);
            // gridRow.setFreeze(gridRow.row < this.freezeRow);
            gridColRow.setIndex(visibleIndex);
            if (gridCornerRow)
                gridCornerRow.setIndex(visibleIndex);
            // create heading row cells
            if (!this.gridRowHeadingCells[rowIndex]) {
                this.gridRowHeadingCells[rowIndex] = new GridHeadingCell_1.GridHeadingCell(this.Page, this.Events, rowIndex, 0, "row");
                this.gridRowHeadingCells[rowIndex].update(rowIndex, 0, true);
            }
            for (var colIndex = 0; colIndex < colLen; colIndex++) {
                var col = row_1[colIndex];
                if (!visibleIndex) {
                    if (this.gridColumnHeadingCells[colIndex]) {
                        this.gridColumnHeadingCells[colIndex].update(rowIndex, colIndex);
                        // this.gridColumnHeadingCells[colIndex].setFreeze(colIndex < this.freezeCol);
                    }
                }
                if (!this.gridCells[rowIndex][colIndex])
                    this.gridCells[rowIndex][colIndex] = new GridCell_1.GridCell(this.Page, this.Events, rowIndex, colIndex);
                // this.gridCells[rowIndex][colIndex].setData();
                this.gridCells[rowIndex][colIndex].rowIndex = visibleIndex;
                // get largest border
                if (colIndex == colLen - 1) {
                    var borderWidths = this.gridCells[rowIndex][colIndex].getBorderWidths();
                    if (borderWidths.r > this.Page.borderRightWidth) {
                        this.Page.borderRightWidth = borderWidths.r;
                    }
                    if (borderWidths.b > this.Page.borderBottomWidth) {
                        this.Page.borderBottomWidth = borderWidths.b;
                    }
                }
                // filter cells
                // if (!this.gridFilterCells[0][colIndex])
            }
        }
        var row = this.Page.originalContent[0];
        for (var colIndex = 0; colIndex < row.length; colIndex++) {
            var col = row[colIndex];
            var w = parseFloat("" + col.style.width);
            // hidden column from excel
            if (!w && !this.Page.hiddenColumns.includes(colIndex)) {
                this.Page.hiddenColumns.push(colIndex);
            }
            // check column defs
            var colDefW = this.Page.getColDefWidth(colIndex);
            if (colDefW)
                w = colDefW;
            if (!refresh) {
                if (this.Page.hiddenColumns.includes(colIndex)) {
                    w = 0;
                }
                this.Page.colWidths.push(w);
                // cssCol.push(`.grid-${this.Page.uuid}-col-${colIndex} { width: ${w}px }`);
                // this.colStyles[colIndex].textContent = `.grid-${this.Page.uuid} .grid-col-${colIndex} { width: ${w}px }`;
            }
            else {
                w = this.Page.colWidths[colIndex];
            }
            this.applyColWidth(colIndex, w);
            this.Page.width += w;
        }
        this.gridCornerHeadingCell.update(0, 0);
        // this.styleSheet.textContent = cssCol.join('');
        this.Page.rows = rowLen;
        this.width = this.Page.width;
        this.height = this.Page.height;
        this.content = this.Page.content;
        this.freezeRowTop = this.Page.rowPos(this.freezeRow);
        this.freezeColLeft = this.Page.colPos(this.freezeCol);
        this.gridRowFilters.setTop(this.freezeRowTop);
        this.gridCornerRowFilters.setTop(this.freezeRowTop);
        this.updateElSizes();
        // sorting indicator
        // if (this.state.sorting.enabled && this.state.hiddenColumns.indexOf(this.state.sorting.col) < 0) {
        //   this.pageEl.classList.add('sorting');
        //   this.sortEl.classList.add(`sorting-${this.state.sorting.direction}`);
        //   let cell = this.Page.content[0][this.state.sorting.col];
        //   let gridCell = this.getGridCell(cell.index.row, cell.index.col);
        //   if (gridCell) {
        //     // this.sortEl.style.setProperty('left', `${gridCell.dataset.x + this.Page.colWidths[cell.index.col] - 4}px`);
        //     gridCell.cell.appendChild(this.sortWrapperEl);
        //   }
        // } else {
        //   if (this.sortWrapperEl.parentElement) this.sortWrapperEl.parentElement.removeChild(this.sortWrapperEl);
        //   this.pageEl.classList.remove('sorting');
        //   this.sortEl.classList.remove('sorting-asc', 'sorting-desc');
        // }
        return refresh || false;
    };
    Grid.prototype.applyColWidth = function (colIndex, width) {
        var text = width ? "width: " + width + "px; left: " + this.Page.colPos(colIndex) + "px" : "display: none !important";
        this.colStyles[colIndex].textContent = ".grid-" + this.Page.uuid + " .grid-col-" + colIndex + " { " + text + " }";
    };
    Grid.prototype.applySortIndicator = function () {
        var _this = this;
        // sorting indicator
        if (this.state.sorting.enabled && this.state.hiddenColumns.indexOf(this.state.sorting.col) < 0) {
            this.pageEl.classList.add("sorting");
            this.sortEl.classList.add("sorting-" + this.state.sorting.direction);
            // check if cell exists
            var interval_1 = setInterval(function () {
                var row = _this.freezeRow ? _this.freezeRow - 1 : 0;
                var cell = _this.Page.content[row][_this.state.sorting.col];
                var gridCell = _this.getGridCell(cell.index.row, cell.index.col);
                if (!gridCell || !gridCell.cell)
                    return;
                gridCell.cell.appendChild(_this.sortWrapperEl);
                clearInterval(interval_1);
                interval_1 = null;
            }, 50);
        }
        else {
            if (this.sortWrapperEl.parentElement)
                this.sortWrapperEl.parentElement.removeChild(this.sortWrapperEl);
            this.pageEl.classList.remove("sorting");
            this.sortEl.classList.remove("sorting-asc", "sorting-desc");
        }
    };
    Grid.prototype.applyFilters = function () {
        this.Page.hiddenRows = [];
        // let originalData = JSON.parse(JSON.stringify(content));
        if (!this.state.filters.length) {
            return;
        }
        // let data = [];
        var rowsToRemove = [];
        // let rowOffset = this.Grid.Settings.headings ? 1 : 0;
        for (var rowIndex = 0; rowIndex < this.Page.originalContent.length; rowIndex++) {
            // let offset = this.Grid.Settings.headings ? -1 : 0;
            if (rowIndex < this.freezeRow) {
                // rowsToKeep.push(rowIndex);
                continue;
            }
            var _loop_1 = function (i) {
                var filter = this_1.state.filters[i];
                if (filter.enabled === false) {
                    return "continue";
                }
                var col = this_1.Page.originalContent[rowIndex][filter.col];
                // if (!col || rowsToRemove.indexOf(rowIndex) > -1) continue;
                if (!col)
                    return "continue";
                var colValue = filter.type === "number" ? helpers.convertToNumber("" + col.value) : "" + (col.formatted_value || col.value);
                var filterValue = filter.type === "number" ? helpers.convertToNumber(filter.value) : "" + filter.value;
                var validValue = false;
                switch (filter.type) {
                    case "number":
                        try {
                            validValue = eval(colValue + " " + filter.exp + " " + filterValue);
                        }
                        catch (e) {
                            validValue = false;
                        }
                        break;
                    case "date":
                        var dates = filterValue.split(",");
                        var fromDate = moment_timezone_1.default(dates[0], filter.dateFormat).toISOString();
                        var toDate = moment_timezone_1.default(dates[1] || dates[0], filter.dateFormat).toISOString();
                        // function
                        var regex = new RegExp("=DATETIME((.*))", "gmi");
                        if (regex.test(filterValue)) {
                            var regex_1 = /=DATETIME\((.*)\)/gim;
                            var m = void 0;
                            var params = "";
                            while ((m = regex_1.exec(filterValue)) !== null) {
                                if (m.index === regex_1.lastIndex) {
                                    regex_1.lastIndex++;
                                }
                                params = m[1];
                            }
                            var args = params.split(",");
                            var mo = moment_timezone_1.default.tz(eval(args[1]) || "UTC").add(eval(args[2]) || 0, eval(args[3]) || "minutes");
                            // .format(args[0]);
                            filter.dateFormat = args[0];
                            fromDate = toDate = mo.toISOString();
                        }
                        var dateString = colValue;
                        var date = new Date(dateString);
                        if (helpers.isNumber(colValue)) {
                            dateString = helpers.parseDateExcel(colValue);
                            date = new Date(dateString);
                        }
                        else if (filter.dateFormat) {
                            // eg: DD MMMM YYYY (01 January 2019)
                            date = moment_timezone_1.default(colValue, filter.dateFormat);
                        }
                        if (date.toString() === "Invalid Date")
                            break;
                        if (filter.exp === "between") {
                            validValue = date.toISOString() >= fromDate && date.toISOString() <= toDate;
                        }
                        else {
                            try {
                                validValue = eval("'" + date.toISOString() + "' " + filter.exp + " '" + fromDate + "'");
                            }
                            catch (e) {
                                validValue = false;
                            }
                        }
                        break;
                    default:
                        filterValue.split(",").forEach(function (value) {
                            if (validValue) {
                                return;
                            }
                            switch (filter.exp) {
                                case "contains":
                                    validValue = colValue.toLowerCase().indexOf(value.toLowerCase()) > -1;
                                    break;
                                case "starts_with":
                                    validValue = colValue.toLowerCase().substring(0, value.length) === value.toLowerCase();
                                    break;
                                case "!=":
                                    validValue = colValue != value; // eval(`"${colValue}" == "${value}"`);
                                    break;
                                default:
                                    validValue = colValue == value; // eval(`"${colValue}" == "${value}"`);
                                    break;
                            }
                        });
                        break;
                }
                if (!validValue && rowsToRemove.indexOf(rowIndex) < 0) {
                    rowsToRemove.push(col.index.row);
                }
            };
            var this_1 = this;
            for (var i = 0; i < this.state.filters.length; i++) {
                _loop_1(i);
            }
        }
        for (var i = rowsToRemove.length - 1; i >= 0; i--)
            this.Page.content.splice(rowsToRemove[i], 1);
        this.Page.hiddenRows = rowsToRemove;
        this.rowsToHide = rowsToRemove;
        // return content;
        // return data;
    };
    Grid.prototype.applySorting = function () {
        var _this = this;
        // if (!this.state.sorting || !this.state.sorting.enabled) return content;
        // let key = Object.keys(this.state.sorting)[0];
        var filter = this.state.sorting;
        var data = this.freezeRow ? this.Page.content.slice(this.freezeRow, this.Page.content.length) : this.Page.content.slice(0);
        var exp = filter.direction === "desc" ? ">" : "<";
        var opp = filter.direction === "desc" ? "<" : ">";
        data.sort(function (a, b) {
            if (!_this.state.sorting.enabled) {
                return a[0].index.row - b[0].index.row;
            }
            var col = filter.col;
            if (!a || !b || !a[col] || !b[col]) {
                return 1;
            }
            var cellValueA = a[col].formatted_value !== undefined ? a[col].formatted_value : a[col].value;
            var cellValueB = b[col].formatted_value !== undefined ? b[col].formatted_value : b[col].value;
            var aVal = filter.type === "number" ? parseFloat(cellValueA) || 0 : ("\"" + cellValueA + "\"").toLowerCase();
            var bVal = filter.type === "number" ? parseFloat(cellValueB) || 0 : ("\"" + cellValueB + "\"").toLowerCase();
            if (filter.type === "date") {
                aVal = "\"" + moment_timezone_1.default(cellValueA, filter.dateFormat).toISOString() + "\"";
                bVal = "\"" + moment_timezone_1.default(cellValueB, filter.dateFormat).toISOString() + "\"";
            }
            try {
                if (eval(aVal + " " + exp + " " + bVal)) {
                    return -1;
                }
                if (eval(aVal + " " + opp + " " + bVal)) {
                    return 1;
                }
            }
            catch (e) {
                return 0;
            }
            return 0;
        });
        this.Page.content = this.freezeRow ? this.Page.content.slice(0, this.freezeRow).concat(data) : data;
    };
    Grid.prototype.applyMergeWidths = function () {
        var _this = this;
        // merged cells
        this.visibleRows.forEach(function (rowIndex) {
            var gridCell = _this.getGridCell(rowIndex, _this.freezeCol - 1);
            if (!gridCell)
                return;
            gridCell.updateMergeWidth(_this.mainEl.scrollLeft, _this.freezeColLeft);
        });
    };
    Grid.prototype.setToCell = function (data) {
        var gridCell;
        if (data.cell.type === "col") {
            var c = this.Page.content[this.Page.content.length - 1][data.cell.col];
            gridCell = this.getGridCell(c.index.row, c.index.col);
            this.selection.type = "col";
        }
        else if (data.cell.type === "row") {
            // let c = this.Page.content[data.cell.row][this.Page.content[0].length - 1];
            gridCell = this.getGridCell(data.cell.row, this.Page.content[0].length - 1);
            this.selection.type = "row";
        }
        else if (data.cell.type === "corner") {
            var c = this.Page.content[this.Page.content.length - 1][this.Page.content[0].length - 1];
            gridCell = this.getGridCell(c.index.row, c.index.col);
            this.selection.type = "all";
        }
        else {
            gridCell = data.cell;
            this.selection.heading = false;
        }
        if (gridCell)
            this.selection.to = gridCell;
    };
    Grid.prototype.emitDelete = function (evt) {
        // check if there are buttons
        var gridCells = this.getSelectedCells();
        var isButtonValue = false;
        gridCells.forEach(function (row) {
            row.forEach(function (gridCell) {
                if (gridCell.hasButtonOptions)
                    isButtonValue = true;
            });
        });
        if (isButtonValue) {
            this.Events.emit(this.Events.ERROR, {
                message: "Cell range contains at least one button",
            });
            return false;
        }
        this.Events.emit(this.Events.DELETE, {
            selection: this.selection,
            event: evt,
        });
        return true;
    };
    Grid.prototype.blur = function () {
        this.clearSelector();
    };
    Grid.prototype.focus = function () {
        this.hasFocus = true;
        if (this.selection.from)
            return;
        if (this.selection.last) {
            this.selection.from = this.selection.last;
            this.selection.to = this.selection.last;
        }
        else {
            this.selection.from = this.gridCells[0][0];
            this.selection.to = this.gridCells[0][0];
        }
        this.updateSelector();
        if (!this.keepSelectorInView())
            requestAnimationFrame(this.render);
    };
    Grid.prototype.defocus = function (blur) {
        this.dirty = false;
        if (this.buttonPopper) {
            // this.cancelCellEdit(true);
            this.destroyPopper();
            this.hasFocus = false;
        }
        else if (this.editing) {
            this.editing = false;
            this.Events.emit(this.Events.CELL_RESET, this.selection.last);
        }
        else if (!blur) {
            this.hasFocus = false;
            this.selection.last = this.selection.from;
            if (this.selection.from) {
                this.Events.emit(this.Events.NO_CELL);
            }
            this.selection.from = undefined;
            this.selection.to = undefined;
            this.updateSelector();
            requestAnimationFrame(this.render);
        }
    };
    Grid.prototype.getGridCellTarget = function (evtTarget) {
        var clickedEl = evtTarget;
        while (clickedEl && clickedEl.className && !clickedEl.classList.contains("grid-cell")) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl.classList.contains("grid-cell") && !clickedEl.classList.contains("filter") ? clickedEl : null;
    };
    Grid.prototype.onCtrlNavigate = function (which) {
        if (this.editing ||
            ["Home", "End", "ArrowLeft", "ArrowRight", "ArrowDown", "ArrowUp"].indexOf(which) < 0 ||
            !this.ctrlDown ||
            !this.selection.from ||
            !this.selection.to ||
            this.disallowSelection) {
            return false;
        }
        var rowFrom = this.selection.from.row;
        var colFrom = this.selection.from.col;
        var rowTo = this.selection.to.row;
        var colTo = this.selection.to.col;
        if (which === "Home") {
            if (!this.shiftDown) {
                rowFrom = 0;
                colFrom = 0;
            }
            rowTo = 0;
            colTo = 0;
        }
        if (which === "End") {
            if (!this.shiftDown) {
                rowFrom = this.Page.content.length - 1;
                colFrom = this.Page.content[0].length - 1;
            }
            rowTo = this.Page.content.length - 1;
            colTo = this.Page.content[0].length - 1;
            // update = 'end';
        }
        var x = 0;
        var y = 0;
        // look for next empty cell value
        var lookForEmptyValue = true;
        var refCell = this.selection.to.dataset.col > this.selection.from.dataset.col ? this.selection.to : this.selection.from;
        if (which === "ArrowRight") {
            var refCell_1 = this.selection.to.col > this.selection.from.col ? this.selection.to : this.selection.from;
            var cell_1 = this.Page.content[refCell_1.rowIndex][refCell_1.col];
            lookForEmptyValue = cell_1.value ? true : false;
            for (var colIndex = refCell_1.col + 1; colIndex < this.Page.content[0].length; colIndex++) {
                var cell_2 = this.Page.content[refCell_1.rowIndex][colIndex];
                if (cell_2.map) {
                    x = colIndex;
                    break;
                }
                // let hidden = this.state.hiddenColumns.indexOf(cell.index.col) > -1;
                // if (hidden) continue;
                if (colIndex === refCell_1.col + 1 && !cell_2.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell_2.value && lookForEmptyValue) {
                    x = colIndex - 1;
                    break;
                }
                if (cell_2.value && !lookForEmptyValue) {
                    x = colIndex;
                    break;
                }
            }
            if (!x) {
                x = this.Page.content[0].length - 1;
            }
            colTo = x;
            if (!this.shiftDown) {
                colFrom = x;
            }
        }
        else if (which === "ArrowLeft") {
            var refCell_2 = this.selection.to.col > this.selection.from.col ? this.selection.from : this.selection.to;
            var cell_3 = this.Page.content[refCell_2.rowIndex][refCell_2.col];
            lookForEmptyValue = cell_3.value ? true : false;
            for (var colIndex = refCell_2.col - 1; colIndex >= 0; colIndex--) {
                var cell_4 = this.Page.content[refCell_2.rowIndex][colIndex];
                if (cell_4.map) {
                    x = colIndex;
                    break;
                }
                if (colIndex === refCell_2.col - 1 && !cell_4.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell_4.value && lookForEmptyValue) {
                    x = colIndex + 1;
                    break;
                }
                if (cell_4.value && !lookForEmptyValue) {
                    x = colIndex;
                    break;
                }
            }
            colTo = x;
            if (!this.shiftDown) {
                colFrom = x;
            }
        }
        else if (which === "ArrowDown") {
            var refCell_3 = this.selection.to.rowIndex > this.selection.from.rowIndex ? this.selection.to : this.selection.from;
            var cell_5 = this.Page.content[refCell_3.rowIndex][refCell_3.col];
            lookForEmptyValue = cell_5.value ? true : false;
            for (var rowIndex = refCell_3.rowIndex + 1; rowIndex < this.Page.content.length; rowIndex++) {
                var cell_6 = this.Page.content[rowIndex][refCell_3.col];
                if (cell_6.map) {
                    y = rowIndex;
                    break;
                }
                if (rowIndex === refCell_3.rowIndex + 1 && !cell_6.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell_6.value && lookForEmptyValue) {
                    y = rowIndex - 1;
                    break;
                }
                if (cell_6.value && !lookForEmptyValue) {
                    y = rowIndex;
                    break;
                }
            }
            if (!y) {
                y = this.Page.content.length - 1;
            }
            rowTo = y;
            if (!this.shiftDown) {
                rowFrom = y;
            }
        }
        else if (which === "ArrowUp") {
            var refCell_4 = this.selection.to.rowIndex > this.selection.from.rowIndex ? this.selection.from : this.selection.to;
            var cell_7 = this.Page.content[refCell_4.rowIndex][refCell_4.col];
            lookForEmptyValue = cell_7.value ? true : false;
            for (var rowIndex = refCell_4.rowIndex - 1; rowIndex >= 0; rowIndex--) {
                var cell_8 = this.Page.content[rowIndex][refCell_4.col];
                if (cell_8.map) {
                    y = rowIndex;
                    break;
                }
                if (rowIndex === refCell_4.rowIndex - 1 && !cell_8.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell_8.value && lookForEmptyValue) {
                    y = rowIndex + 1;
                    break;
                }
                if (cell_8.value && !lookForEmptyValue) {
                    y = rowIndex;
                    break;
                }
            }
            rowTo = y;
            if (!this.shiftDown) {
                rowFrom = y;
            }
        }
        var cell = this.Page.content[rowFrom][colFrom];
        this.selection.from = this.getGridCell(cell.index.row, cell.index.col);
        var cellTo = this.Page.content[rowTo][colTo];
        this.selection.to = this.getGridCell(cellTo.index.row, cellTo.index.col);
        this.selection.keyboard = true;
        this.updateSelector();
        if (!this.keepSelectorInView())
            requestAnimationFrame(this.render);
        return true;
    };
    Grid.prototype.selectNextCell = function (direction, key) {
        var _this = this;
        if (key === void 0) { key = ""; }
        if (!this.selection.from || !this.selection.to)
            return;
        var which = this.shiftDown && key !== "Enter" && key !== "Tab" ? "to" : "from";
        var row = this.selection[which].rowIndex;
        var col = this.selection[which].col;
        var set = "";
        var scrollTop = this.mainEl.scrollTop;
        var scrollLeft = this.mainEl.scrollLeft;
        var scrollBarWidth = this.Page.height > this.containerEl.offsetHeight ? this.Page.scrollbarWidth : 0;
        var scrollBarHeight = this.Page.width > this.containerEl.offsetWidth ? this.Page.scrollbarWidth : 0;
        if (direction === "pagedown") {
            var visibleRow_1 = -1;
            var yPos_1 = 0;
            this.visibleRows
                .concat()
                .reverse()
                .forEach(function (rowIndex) {
                if (visibleRow_1 > -1)
                    return;
                yPos_1 = _this.Page.rowPos(rowIndex);
                if (yPos_1 < _this.mainEl.scrollTop + _this.containerEl.offsetHeight + _this.Page.scrollbarWidth) {
                    visibleRow_1 = rowIndex;
                }
            });
            if (visibleRow_1 > -1) {
                scrollTop = yPos_1 - this.freezeRowTop;
                row = visibleRow_1;
            }
        }
        if (direction === "pageup") {
            var visibleRow_2 = -1;
            var yPos_2 = 0;
            this.visibleRows.forEach(function (rowIndex) {
                if (visibleRow_2 > -1)
                    return;
                yPos_2 = _this.Page.rowPos(rowIndex);
                if (yPos_2 >= _this.mainEl.scrollTop + _this.freezeRowTop) {
                    visibleRow_2 = rowIndex;
                }
            });
            if (visibleRow_2 > -1) {
                scrollTop = yPos_2 - this.containerEl.offsetHeight + scrollBarHeight + this.Page.rowHeights[row];
                row = visibleRow_2;
            }
        }
        if (direction === "home") {
            scrollLeft = 0;
            col = 0;
            set = "col";
        }
        if (direction === "end") {
            col = this.Page.content[0].length - 1;
            scrollLeft = this.Page.colPos(col);
            set = "col";
        }
        if (direction === "up") {
            row--;
            // this.Page.hiddenRows
            //   .concat()
            //   .reverse()
            //   .forEach((index) => {
            //     if (index === row) row--;
            //   });
            if (row < 0)
                row = 0;
            scrollTop = this.getScrollTop(row);
            // let yPos = this.Page.rowPos(row);
            // if (yPos - this.mainEl.scrollTop < this.freezeRowTop) {
            //   scrollTop = yPos - this.freezeRowTop;
            // }
        }
        if (direction === "down") {
            row++;
            if (!this.Page.content[row]) {
                row = this.Page.content.length - 1;
            }
            // this.Page.hiddenRows.forEach((index) => {
            //   if (index === row) row++;
            // });
            // let yPos = this.Page.rowPos(row);
            scrollTop = this.getScrollTop(row);
            // console.log(st);
            // if (st > -1) scrollTop = st;
            // if (
            //   yPos - this.mainEl.scrollTop + this.Page.rowHeights[row] + scrollBarHeight >
            //   this.containerEl.offsetHeight
            // ) {
            //   console.log('move');
            //   scrollTop = yPos - this.containerEl.offsetHeight + scrollBarHeight + this.Page.rowHeights[row];
            // }
        }
        if (direction === "right") {
            col++;
            if (!this.Page.content[row][col]) {
                if (key == "Tab" && this.Page.tabToLine) {
                    row++;
                    if (!this.Page.content[row]) {
                        row = this.Page.content.length - 1;
                        col = this.Page.content[0].length - 1;
                    }
                    else {
                        col = 0;
                    }
                }
                else {
                    col = this.Page.content[0].length - 1;
                }
            }
            this.Page.hiddenColumns.forEach(function (colIndex) {
                if (colIndex === col)
                    col++;
            });
            scrollLeft = this.getScrollLeft(col);
            // let xPos = this.Page.colPos(col);
            // if (
            //   xPos - this.mainEl.scrollLeft + this.Page.colWidths[col] + scrollBarWidth >
            //   this.containerEl.offsetWidth
            // ) {
            //   scrollLeft = xPos - this.containerEl.offsetWidth + scrollBarWidth + this.Page.colWidths[col];
            // }
        }
        if (direction === "left") {
            col--;
            this.Page.hiddenColumns
                .concat()
                .reverse()
                .forEach(function (colIndex) {
                if (colIndex === col)
                    col--;
            });
            if (col < 0) {
                if (key == "Tab" && this.Page.tabToLine) {
                    row--;
                    if (row < 0) {
                        row = 0;
                        col = 0;
                    }
                    else {
                        col = this.Page.content[0].length - 1;
                    }
                }
                else {
                    col = 0;
                }
            }
            scrollLeft = this.getScrollLeft(col);
            // let xPos = this.Page.colPos(col);
            // if (xPos - this.mainEl.scrollLeft < this.freezeColLeft) {
            //   scrollLeft = xPos - this.freezeColLeft;
            // }
        }
        var cell = this.Page.content[row][col];
        this.selection[which] = this.gridCells[cell.index.row][cell.index.col];
        if (!this.shiftDown || key === "Enter" || key === "Tab") {
            this.selection.to = this.selection.from;
        }
        this.selection.keyboard = true;
        this.updateSelector();
        var render = false;
        if (scrollLeft === this.mainEl.scrollLeft ||
            (scrollLeft <= 0 && !this.mainEl.scrollLeft) ||
            (scrollLeft > this.mainEl.scrollLeft && this.mainEl.scrollLeft >= this.mainEl.scrollWidth - this.mainEl.offsetWidth)) {
            render = true;
        }
        else {
            this.mainEl.scrollLeft = scrollLeft;
            console.log("scroll", "left", scrollLeft);
        }
        if (scrollTop === this.mainEl.scrollTop ||
            (scrollTop <= 0 && !this.mainEl.scrollTop) ||
            (scrollTop > this.mainEl.scrollTop && this.mainEl.scrollTop >= this.mainEl.scrollHeight - this.mainEl.offsetHeight)) {
            render = true;
        }
        else {
            this.mainEl.scrollTop = scrollTop;
            console.log("scroll", "top", scrollTop);
        }
        if (render)
            requestAnimationFrame(this.render);
        // this.updateSelector();
        // KEEP this.cancelCellEdit();
    };
    Grid.prototype.jumpToCell = function (key) {
        var direction = key.replace("Page", "").toLowerCase();
        var which = this.shiftDown && !["Enter", "Tab"].includes(key) ? "to" : "from";
        var row = this.selection[which].rowIndex;
        var col = this.selection[which].col;
        var newRow = row;
        var newCol = col;
        var scrollTop = this.mainEl.scrollTop;
        var scrollLeft = this.mainEl.scrollLeft;
        switch (direction) {
            case "home":
                scrollLeft = 0;
                newCol = 0;
                if (this.ctrlDown) {
                    newRow = 0;
                    scrollTop = 0;
                }
                break;
            case "end":
                scrollLeft = this.getScrollLeft(this.Page.content[0].length - 1);
                newCol = this.Page.content[0].length - 1;
                if (this.ctrlDown) {
                    newRow = this.Page.content.length - 1;
                    scrollTop = this.getScrollTop(this.Page.content.length - 1);
                }
                break;
            case "up":
                newRow = this.getNextPageIndex(this.Page.scrollbarHeight);
                scrollTop = this.getScrollTop(newRow) - this.mainEl.offsetHeight + this.Page.rowHeights[newRow];
                break;
            case "down":
                newRow = this.getNextPageIndex(this.Page.scrollbarHeight, true);
                scrollTop = this.Page.rowPos(newRow) - this.freezeRowTop;
                break;
        }
        var cell = this.Page.content[newRow][newCol];
        if (this.shiftDown)
            this.selection.to = this.gridCells[cell.index.row][cell.index.col];
        else
            this.selection.from = this.selection.to = this.gridCells[cell.index.row][cell.index.col];
        this.selection.keyboard = true;
        this.updateSelector();
        // let render = this.mainEl.scrollTop == scrollTop && this.mainEl.scrollLeft == scrollLeft;
        this.mainEl.scrollTop = scrollTop;
        this.mainEl.scrollLeft = scrollLeft;
        // requestAnimationFrame(this.render);
        this.render();
    };
    Grid.prototype.gotoNextCell = function (key) {
        if (!this.selection.from || !this.selection.to)
            return false;
        // let arrow = key.indexOf('Arrow') > -1 ? true : false;
        // let page = key.indexOf("Page") > -1 ? true : false;
        var which = this.shiftDown && !["Enter", "Tab"].includes(key) ? "to" : "from";
        var direction = key
            .replace("Arrow", "")
            // .replace("Page", "")
            .replace("Enter", this.shiftDown ? "up" : "down")
            .replace("Tab", this.shiftDown ? "left" : "right")
            .toLowerCase();
        var row = this.selection[which].rowIndex;
        var col = this.selection[which].col;
        var newRow = row;
        var newCol = col;
        // let scrollTop = this.mainEl.scrollTop;
        // let scrollLeft = this.mainEl.scrollLeft;
        // let scroll = false;
        // let scrollBarHeight =
        //   this.Page.width > this.containerEl.offsetWidth
        //     ? this.Page.scrollbarWidth
        //     : 0;
        switch (direction) {
            case "up":
                if (this.ctrlDown || this.Page.mappingNavigation) {
                    newRow = this.getNextIndex(row, this.getColValues(col, this.Page.mappingNavigation), true, this.Page.mappingNavigation);
                }
                else {
                    newRow--;
                }
                if (newRow < 0)
                    newRow = 0;
                break;
            case "down":
                if (this.ctrlDown || this.Page.mappingNavigation) {
                    newRow = this.getNextIndex(row, this.getColValues(col, this.Page.mappingNavigation), false, this.Page.mappingNavigation);
                }
                else {
                    newRow++;
                }
                if (!this.Page.content[newRow]) {
                    newRow = this.Page.content.length - 1;
                }
                break;
            case "left":
                if (key == "Tab" && this.Page.mappingNavigation) {
                    var index = this.getNextMappingIndex(row, col, "prev");
                    newRow = index.row;
                    newCol = index.col;
                }
                else if (this.ctrlDown || this.Page.mappingNavigation) {
                    newCol = this.getNextIndex(col, this.getRowValues(row, this.Page.mappingNavigation), true, this.Page.mappingNavigation);
                }
                else {
                    newCol--;
                    this.Page.hiddenColumns
                        .concat()
                        .reverse()
                        .forEach(function (colIndex) {
                        if (colIndex === newCol)
                            newCol--;
                    });
                }
                if (newCol < 0 || this.Page.hiddenColumns.indexOf(newCol) > -1) {
                    newCol = 0;
                    this.Page.hiddenColumns.forEach(function (cIndex) {
                        if (cIndex <= newCol)
                            newCol++;
                    });
                }
                break;
            case "right":
                if (key == "Tab" && this.Page.mappingNavigation) {
                    var index = this.getNextMappingIndex(row, col);
                    newRow = index.row;
                    newCol = index.col;
                }
                else if (this.ctrlDown || this.Page.mappingNavigation) {
                    newCol = this.getNextIndex(col, this.getRowValues(row, this.Page.mappingNavigation), false, this.Page.mappingNavigation, key == "Tab");
                }
                else {
                    newCol++;
                    this.Page.hiddenColumns.forEach(function (colIndex) {
                        if (colIndex === newCol)
                            newCol++;
                    });
                }
                if (!this.Page.content[row][newCol] || this.Page.hiddenColumns.indexOf(newCol) > -1) {
                    newCol = this.Page.content[0].length - 1;
                    this.Page.hiddenColumns.reverse().forEach(function (cIndex) {
                        if (cIndex >= newCol)
                            newCol--;
                    });
                }
                break;
        }
        var cell = this.Page.content[newRow][newCol];
        if (this.shiftDown && !["Enter", "Tab"].includes(key))
            this.selection.to = this.gridCells[cell.index.row][cell.index.col];
        else
            this.selection.from = this.selection.to = this.gridCells[cell.index.row][cell.index.col];
        this.selection.keyboard = true;
        this.updateSelector();
        if (!this.keepSelectorInView()) {
            this.render();
        }
        return true;
    };
    Grid.prototype.emitCellSelected = function (evt) {
        this.Events.emitCellSelected(this.getSelection(evt));
    };
    Grid.prototype.getScrollTop = function (row) {
        var scrollTop = this.mainEl.scrollTop;
        var yPos = this.Page.rowPos(row);
        var rowHeight = this.Page.rowHeights[row];
        var offset = this.headings ? this.Page.headingsHeight : 0;
        // if (this.headings) yPos += this.Page.headingsHeight;
        var freezeRowTop = this.freezeRowTop;
        if (yPos - scrollTop + rowHeight + this.Page.scrollbarHeight > this.mainEl.offsetHeight) {
            scrollTop = yPos - this.mainEl.offsetHeight + this.Page.scrollbarHeight + rowHeight;
        }
        else if (yPos - scrollTop < freezeRowTop) {
            scrollTop = yPos - freezeRowTop;
        }
        return scrollTop;
    };
    Grid.prototype.getScrollLeft = function (col) {
        var scrollLeft = this.mainEl.scrollLeft;
        var xPos = this.Page.colPos(col);
        if (this.headings) {
            // TODO strange issue when scrolling left
            // if (col > this.freezeCol) xPos += this.Page.headingsWidth;
        }
        // xPos *= this.scale;
        var freezeColLeft = this.freezeColLeft;
        var colWidth = this.Page.colWidths[col];
        var offset = this.headings ? this.Page.headingsHeight : 0;
        if (xPos - this.mainEl.scrollLeft + colWidth + this.Page.scrollbarWidth > this.mainEl.offsetWidth) {
            scrollLeft = xPos - this.mainEl.offsetWidth + this.Page.scrollbarWidth + colWidth;
        }
        else if (xPos - this.mainEl.scrollLeft < freezeColLeft) {
            scrollLeft = xPos - freezeColLeft;
        }
        return scrollLeft;
    };
    Grid.prototype.getTouchDistance = function (touches) {
        if (touches === void 0) { touches = []; }
        var x = touches[1].clientX - touches[0].clientX;
        var y = touches[1].clientY - touches[0].clientY;
        return Math.abs(Math.sqrt(x * x + y * y));
    };
    Grid.prototype.isRightClick = function (evt) {
        var isRightMB = false;
        if ("which" in evt)
            // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
            isRightMB = evt.which === 3;
        else if ("button" in evt)
            // IE, Opera
            isRightMB = evt.button === 2;
        return isRightMB;
    };
    Grid.prototype.destroyPopper = function () {
        if (this.buttonPopper) {
            if (this.popperParent)
                this.popperParent.removeChild(this.buttonPopper.popper);
            else
                this.pageEl.removeChild(this.buttonPopper.popper);
            this.buttonPopper.destroy();
            this.buttonPopper = null;
        }
    };
    // tracking
    Grid.prototype.clearTrackingData = function () {
        this.trackingData = [];
        for (var rowIndex = 0; rowIndex < this.gridCells.length; rowIndex++) {
            if (!this.gridCells[rowIndex])
                continue;
            for (var colIndex = 0; colIndex < this.gridCells[rowIndex].length; colIndex++) {
                var cell = this.getGridCell(rowIndex, colIndex, true);
                if (cell)
                    cell.history(false);
            }
        }
    };
    Grid.prototype.updateTrackingData = function (trackingData) {
        this.trackingData = trackingData;
        this.updateHistoryCells();
    };
    Grid.prototype.clearColumnFilters = function () {
        this.gridFilterCells.forEach(function (cell) {
            cell.clear();
        });
        var filters = [];
        this.state.filters.forEach(function (filter) {
            if (filter.name && filter.name.indexOf("grid-") > -1)
                return;
            filters.push(filter);
        });
        this.state.filters = filters;
        this.updateState(this.state);
    };
    Grid.prototype.updateHistoryCells = function () {
        if (!this.tracking || !this.trackingData || this.trackingData.length < 2)
            return;
        for (var i = 0; i < this.trackingData.length; i++) {
            if (!i)
                continue;
            for (var rowIndex = 0; rowIndex < this.trackingData[i].content_diff.length; rowIndex++) {
                if (!this.trackingData[i].content_diff[rowIndex])
                    continue;
                for (var colIndex = 0; colIndex < this.trackingData[i].content_diff[rowIndex].length; colIndex++) {
                    if (!this.trackingData[i].content_diff[rowIndex][colIndex])
                        continue;
                    var cell = this.getGridCell(rowIndex, colIndex);
                    if (!cell)
                        continue;
                    cell.history(true, this.getTrackingUser(this.trackingData[i].modified_by.id));
                }
            }
        }
    };
    Grid.prototype.getTrackingUser = function (userId) {
        var index = this.trackingUsers.indexOf(userId);
        if (index > -1) {
            return index;
        }
        else {
            this.trackingUsers.push(userId);
            return this.trackingUsers.length - 1;
        }
    };
    Grid.prototype.applyScale = function (n) {
        var _this = this;
        this.pageElRect = this.pageEl.getBoundingClientRect();
        // this.wrapperElRect = this.wrapperEl.getBoundingClientRect();
        this.mainEl.classList.remove("overflow-x", "overflow-y");
        var cellsOffsetX = this.headings ? this.Page.headingsWidth : 0;
        var cellsOffsetY = this.headings ? this.Page.headingsHeight : 0;
        var scale = 1;
        // const borderWidths: IGridCellBorderWidths = this.gridCells[0][this.gridCells.length-1].getBorderWidths();
        var fitWidth = function (contain) {
            // check for border
            var scale = _this.pageElRect.width / (_this.Page.width + _this.Page.borderRightWidth + cellsOffsetX);
            if (contain)
                return scale;
            var height = _this.Page.height * scale;
            if (height > _this.pageElRect.height) {
                scale = (_this.pageElRect.width - _this.Page.scrollbarSize * scale) / (_this.Page.width + _this.Page.borderRightWidth + cellsOffsetX);
            }
            return scale;
        };
        var fitHeight = function (contain) {
            var scale = _this.pageElRect.height / (_this.Page.height + _this.Page.borderBottomWidth + cellsOffsetY);
            if (contain)
                return scale;
            var width = _this.Page.width * scale;
            if (width > _this.pageElRect.width) {
                scale = (_this.pageElRect.height - _this.Page.scrollbarSize * scale) / (_this.Page.height + _this.Page.borderBottomWidth + cellsOffsetY);
            }
            return scale;
        };
        if (n) {
            if (n > 5)
                n = 5;
            if (n < 0.2)
                n = 0.2;
            this.scale = n;
            console.log("scale", n);
        }
        else if (this.fit === "width") {
            this.mainEl.classList.add("overflow-x");
            this.scale = fitWidth();
        }
        else if (this.fit === "height") {
            this.mainEl.classList.add("overflow-y");
            this.scale = fitHeight();
        }
        else if (this.fit === "contain") {
            var scaleWidth = fitWidth(true);
            var scaleHeight = fitHeight(true);
            this.mainEl.classList.add("overflow-y", "overflow-x");
            this.scale = scaleHeight < scaleWidth ? scaleHeight : scaleWidth;
        }
        else {
            this.scale = 1;
        }
        this.Page.scale = this.scale;
        if (this.scale == 1) {
            this.containerEl.style.removeProperty("transform");
            this.containerEl.style.removeProperty("width");
            this.containerEl.style.removeProperty("height");
            // this.mainEl.style.removeProperty('transform');
            // this.mainEl.style.removeProperty('top');
            // this.freezeCornerEl.style.removeProperty('transform');
            // this.freezeRowEl.style.removeProperty('transform');
            // this.freezeColEl.style.removeProperty('transform');
            // this.selectorEl.style.removeProperty('transform');
            // this.wrapperEl.style.removeProperty('width');
            // this.containerEl.style.removeProperty('height');
        }
        else {
            var width = this.headings ? this.Page.width + this.Page.headingsWidth : this.Page.width;
            var height = this.headings ? this.Page.height + this.Page.headingsHeight : this.Page.height;
            this.containerEl.style.setProperty("transform", "scale(" + this.scale + ")");
            this.containerEl.style.setProperty("width", this.pageElRect.width / this.scale + "px");
            this.containerEl.style.setProperty("height", this.pageElRect.height / this.scale + "px");
            // this.mainEl.style.setProperty('transform', `scale(${this.scale})`);
            // if (this.headings) {
            //   this.mainEl.style.setProperty('top', `${20 * this.scale}px`);
            // }
            // // this.wrapperEl.style.setProperty('width', `${this.pageElRect.width / this.scale}px`);
            // this.freezeCornerEl.style.setProperty('transform', `scale(${this.scale})`);
            // this.freezeRowEl.style.setProperty('transform', `scale(${this.scale})`);
            // this.freezeColEl.style.setProperty('transform', `scale(${this.scale})`);
            // this.selectorEl.style.setProperty('transform', `scale(${this.scale})`);
            // // this.stickyEl.style.setProperty('width', `${this.pageElRect.width / this.scale}px`);
            // // this.wrapperEl.style.setProperty('height', `${this.pageElRect.height / this.scale}px`);
            // width *= this.scale;
            // height *= this.scale;
            // this.boxEl.style.setProperty('width', `${width}px`);
            // this.boxEl.style.setProperty('height', `${height}px`);
        }
        // if (this.ps) this.ps.update(this.fit, this.scale);
    };
    Grid.prototype.applyFilterValues = function () {
        var _this = this;
        this.gridFilterCells.forEach(function (cell, colIndex) {
            cell.clear();
            var filter = _this.state.filters.find(function (f) {
                return f.col == colIndex;
            });
            if (!filter)
                return;
            cell.setValue(filter);
        });
    };
    Grid.prototype.getDefaultState = function () {
        return {
            hiddenColumns: [],
            filters: [],
            sorting: {
                enabled: false,
                type: IGridFieldType.STRING,
                direction: IGridSortDirection.ASC,
                col: 0,
                field: "",
            },
        };
    };
    Grid.prototype.getRowValues = function (rowIndex, map) {
        var _this = this;
        if (!this.Page.content[rowIndex]) {
            throw new Error("Row out of bounds");
        }
        return this.Page.content[rowIndex].map(function (c, cIndex) {
            if (_this.Page.hiddenColumns.indexOf(cIndex) > -1)
                return null;
            if (map)
                return c.map;
            return c.value;
        });
    };
    Grid.prototype.getColValues = function (columnIndex, map) {
        if (!this.Page.content[0][columnIndex]) {
            throw new Error("Column out of bounds");
        }
        var cells = [];
        for (var row = 0; row < this.Page.content.length; row++) {
            if (map)
                cells.push(this.Page.content[row][columnIndex].map);
            else
                cells.push(this.Page.content[row][columnIndex].value);
        }
        return cells;
    };
    Grid.prototype.getNextIndex = function (rowIndex, cellValues, reverse, map, tab) {
        var values = reverse ? cellValues.reverse() : cellValues.concat();
        var cellIndex = reverse ? cellValues.length - rowIndex - 1 : rowIndex;
        var value = values[cellIndex];
        var nextValue = values[cellIndex + 1];
        var index = values.findIndex(function (c, i) {
            if (i <= cellIndex || c === null)
                return false;
            if (!nextValue) {
                return c ? true : false;
            }
            if (value && !c)
                return true;
            if (!value && c)
                return true;
        });
        if (index < 0) {
            if (tab)
                return index;
            if (map) {
                return reverse ? rowIndex - 1 : rowIndex + 1;
            }
            index = cellValues.length;
        }
        else if (!values[index]) {
            if (!reverse)
                index--;
        }
        else {
            if (reverse)
                index++;
        }
        if (reverse)
            return cellValues.length - index;
        return index;
    };
    Grid.prototype.getNextPageIndex = function (scrollBarHeight, down) {
        var _this = this;
        var visibleRow = -1;
        var yPos = 0;
        var rows = down ? this.visibleRows.reverse() : this.visibleRows.concat();
        rows.forEach(function (rowIndex) {
            if (visibleRow > -1)
                return;
            yPos = _this.Page.rowPos(rowIndex);
            if (down) {
                if (yPos < _this.mainEl.scrollTop + _this.mainEl.offsetHeight + _this.freezeRowTop + scrollBarHeight) {
                    visibleRow = rowIndex;
                }
            }
            else {
                if (yPos > _this.mainEl.scrollTop + _this.freezeRowTop) {
                    visibleRow = rowIndex - 1;
                }
            }
        });
        // if (visibleRow > -1) {
        //   scrollTop = yPos - this.freezeRowTop;
        // }
        return visibleRow;
    };
    Grid.prototype.getNextMappingIndex = function (row, col, direction) {
        if (direction === void 0) { direction = "next"; }
        if (!this.Page.mapping || !this.Page.mapping.length) {
            return { row: row, col: col };
        }
        // sort mapping
        var mapping = this.Page.mapping.sort(function (a, b) {
            var i = 0;
            if (a.row > b.row)
                i = 1;
            if (a.row < b.row)
                i = -1;
            if (a.col > b.col)
                i = 1;
            if (a.col < b.col)
                i = -1;
            return i;
        });
        var index = mapping.findIndex(function (m) {
            return m.row == row && m.col == col;
        });
        if (index < 0) {
            // get the closest mapping cell
            if (direction == "next") {
                index = mapping.findIndex(function (m) {
                    if (m.row == row && m.col > col)
                        return true;
                    return m.row > row;
                });
                if (index > -1)
                    return mapping[index];
            }
            else {
                var rmapping = mapping.concat().reverse();
                index = rmapping.findIndex(function (m) {
                    if (m.row == row && m.col < col)
                        return true;
                    return m.row < row;
                });
                if (index > -1)
                    return rmapping[index];
            }
            return mapping[0];
        }
        // cycle to next mapping cell
        if (direction == "next") {
            index++;
            if (mapping.length == index)
                index = 0;
        }
        else {
            index--;
            if (index < 0)
                index = mapping.length - 1;
        }
        return mapping[index];
    };
    Grid.prototype.resetSelection = function () {
        this.selection.from = undefined;
        this.selection.to = undefined;
        this.selection.rowFrom = -1;
        this.selection.rowTo = -1;
        this.selection.colFrom = -1;
        this.selection.colTo = -1;
    };
    return Grid;
}());
exports.Grid = Grid;
//# sourceMappingURL=Grid.js.map