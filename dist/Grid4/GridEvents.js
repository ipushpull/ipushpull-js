"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = __importDefault(require("../Emitter"));
var GridEvents = /** @class */ (function (_super) {
    __extends(GridEvents, _super);
    function GridEvents() {
        return _super.call(this) || this;
    }
    Object.defineProperty(GridEvents.prototype, "CELL_CLICKED", {
        get: function () {
            return 'cell_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "BLUR", {
        get: function () {
            return 'blur';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "FOCUS", {
        get: function () {
            return 'focus';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "NO_CELL", {
        get: function () {
            return 'no_cell';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "CELL_HISTORY_CLICKED", {
        get: function () {
            return 'cell_history_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "CELL_LINK_CLICKED", {
        get: function () {
            return 'cell_link_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "CELL_DOUBLE_CLICKED", {
        get: function () {
            return 'cell_double_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "CELL_VALUE", {
        get: function () {
            return 'cell_value';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "CELL_SELECTED", {
        get: function () {
            return 'cell_selected';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "CELL_RESET", {
        get: function () {
            return 'cell_reset';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "CELL_CHECKED", {
        get: function () {
            return 'cell_checked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "CELL_OPTION", {
        get: function () {
            return 'cell_option';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "EDITING", {
        get: function () {
            return 'editing';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "RESIZE", {
        get: function () {
            return 'resize';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "RESIZING", {
        get: function () {
            return 'resizing';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "RESIZED", {
        get: function () {
            return 'resized';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "COLUMNS_MOVED", {
        get: function () {
            return 'columns_moved';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "COLUMN_FILTER", {
        get: function () {
            return 'column_filter';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "COLUMN_FILTER_FOCUS", {
        get: function () {
            return 'column_filter_focus';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "DELETE", {
        get: function () {
            return 'delete';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "SELECTED_ALL", {
        get: function () {
            return 'selected_all';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "CELL", {
        get: function () {
            return 'cell';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "COPY", {
        get: function () {
            return 'copy';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "INTERNAL_CELL_SELECTED", {
        get: function () {
            return 'internal_cell';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ERROR", {
        get: function () {
            return 'error';
        },
        enumerable: true,
        configurable: true
    });
    GridEvents.prototype.emitCellSelected = function (data) {
        this.emit(this.CELL_SELECTED, data);
    };
    return GridEvents;
}(Emitter_1.default));
exports.default = GridEvents;
//# sourceMappingURL=GridEvents.js.map