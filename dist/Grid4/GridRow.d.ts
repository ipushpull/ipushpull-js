import { IGridPage } from './GridPage';
import { IGridEvents } from './GridEvents';
export interface IGridRow {
    Page: IGridPage;
    Events: IGridEvents;
    rowEl: HTMLDivElement;
    row: number;
    rowIndex: number;
    top: number;
    sticky: boolean;
    height: number;
    setHeight(size: number): void;
    setTop(freezeRowTop?: number): void;
    setIndex(index: number): void;
    setFreeze(n: boolean): void;
}
export declare class GridRow implements IGridRow {
    Page: IGridPage;
    Events: IGridEvents;
    row: number;
    heading?: boolean | undefined;
    filters?: boolean | undefined;
    rowEl: HTMLDivElement;
    sticky: boolean;
    top: number;
    rowIndex: number;
    height: number;
    constructor(Page: IGridPage, Events: IGridEvents, row: number, heading?: boolean | undefined, filters?: boolean | undefined);
    setHeight(size: number): void;
    setTop(freezeRowTop?: number): void;
    setIndex(index: number): void;
    setFreeze(n: boolean): void;
}
