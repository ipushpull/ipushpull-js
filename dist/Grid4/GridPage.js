"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GridPage = /** @class */ (function () {
    function GridPage() {
        // public page!: IPageService;
        this.softMerges = true;
        this.structured = false;
        this.columnFilters = false;
        this.uuid = "";
        this.scale = 1;
        this.content = [];
        this.originalContent = [];
        this.deltaContent = [];
        this.rows = 0;
        this.cols = 0;
        this.hiddenColumns = [];
        this.hiddenRows = [];
        this.rowHeights = [];
        this.colWidths = [];
        this.width = 0;
        this.height = 0;
        this.borderRightWidth = 0;
        this.borderBottomWidth = 0;
        this.selectedCols = [];
        this.selectedRows = [];
        this.selectedCells = [];
        this.scrollTop = 0;
        this.scrollLeft = 0;
        this.scrollbarSize = 0;
        this.scrollbarWidth = 0;
        this.scrollbarHeight = 0;
        this.selection = {
            rowFrom: -1,
            rowTo: -1,
            colFrom: -1,
            colTo: -1,
            heading: false,
            inside: false,
            selected: false,
            keyboard: false,
            type: "cell",
        };
        this.headings = false;
        this.touch = false;
        this.columnsDefs = [];
        this.canSort = false;
        this.sorting = false;
        this.headingsHeight = 20;
        this.headingsWidth = 40;
        this.tabToLine = false;
        this.cellMapColor = '';
        this.cellMapErrorColor = '';
        this.disableFocusOnF2 = false;
        this.mappingNavigation = false;
        this.mapping = [];
        this.elements = [];
    }
    Object.defineProperty(GridPage.prototype, "filtersOffset", {
        get: function () {
            return this.columnFilters ? 30 : 0;
        },
        enumerable: true,
        configurable: true
    });
    GridPage.prototype.reset = function () {
        this.scale = 0;
        this.content = [];
        this.originalContent = [];
        this.deltaContent = [];
        this.rows = 0;
        this.cols = 0;
        this.hiddenColumns = [];
        this.hiddenRows = [];
        this.rowHeights = [];
        this.colWidths = [];
        this.width = 0;
        this.height = 0;
        this.selectedCells = [];
        this.selectedCols = [];
        this.selectedRows = [];
        this.scrollTop = 0;
        this.scrollLeft = 0;
        this.columnsDefs = [];
        this.elements = [];
        // this.scrollbarWidth = 0;
    };
    GridPage.prototype.resetSelection = function () {
        this.selectedCells = [];
        this.selectedCols = [];
        this.selectedRows = [];
        this.selection = {
            rowFrom: -1,
            rowTo: -1,
            colFrom: -1,
            colTo: -1,
            heading: false,
            inside: false,
            selected: false,
            keyboard: false,
            type: "cell",
        };
    };
    GridPage.prototype.rowPos = function (row) {
        return this.rowHeights.slice(0, row).reduce(function (a, b) { return a + b; }, 0);
    };
    GridPage.prototype.colPos = function (col) {
        return this.colWidths.slice(0, col).reduce(function (a, b) { return a + b; }, 0);
    };
    GridPage.prototype.cellPos = function (row, col) {
        return {
            x: this.colWidths.slice(0, col).reduce(function (a, b) { return a + b; }, 0),
            y: this.rowHeights.slice(0, row).reduce(function (a, b) { return a + b; }, 0),
        };
    };
    GridPage.prototype.setSelectedCell = function (cell) {
        this.selectedCells.push(cell);
    };
    GridPage.prototype.getColDefWidth = function (col) {
        if (this.columnsDefs[col] &&
            this.columnsDefs[col].style &&
            this.columnsDefs[col].style.width) {
            return parseFloat(this.columnsDefs[col].style.width);
        }
        return 0;
    };
    GridPage.prototype.isRightClick = function (evt) {
        var isRightMB = false;
        if ("which" in evt)
            // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
            isRightMB = evt.which === 3;
        else if ("button" in evt)
            // IE, Opera
            isRightMB = evt.button === 2;
        return isRightMB;
    };
    return GridPage;
}());
exports.default = GridPage;
//# sourceMappingURL=GridPage.js.map