"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GridRow = /** @class */ (function () {
    function GridRow(Page, Events, row, heading, filters) {
        this.Page = Page;
        this.Events = Events;
        this.row = row;
        this.heading = heading;
        this.filters = filters;
        this.sticky = false;
        this.top = 0;
        this.rowIndex = 0;
        this.height = 0;
        this.rowEl = document.createElement('div');
        this.rowEl.className = 'grid-row';
        if (heading)
            this.rowEl.classList.add('heading');
        else
            this.rowEl.classList.add('cells');
        if (filters)
            this.rowEl.classList.add('filter');
        // this.rowEl.setAttribute('id', `row-${row}`);
        //   this.update(row);
        // }
        // public update(row: number): void {
        this.row = row;
        this.rowIndex = row;
        // this.top = this.Page.rowPos(row);
        // this.rowEl.style.setProperty('top', `${this.top}px`);
        // this.rowEl.style.setProperty('transform', `translateY(${this.top}px)`);
        // this.rowEl.setAttribute('id', `row-${row}`);
        if (!heading && !filters) {
            var height = this.Page.originalContent[this.row][0].style.height;
            this.height = parseFloat("" + height);
            this.rowEl.style.setProperty('height', height);
        }
        else {
            this.height = this.filters ? 30 : this.Page.headingsHeight;
        }
        // this.rowEl.style.setProperty('background-color', `#${this.Page.originalContent[this.row][0].style['background-color']}`);
        // this.rowEl.style.removeProperty('display');
        // this.rowEl.textContent = `${row}`;
        this.setIndex(row);
    }
    GridRow.prototype.setHeight = function (size) {
        this.height = size;
        this.rowEl.style.setProperty('height', size + "px");
        this.Page.content[this.rowIndex][0].style.height = size + "px";
    };
    GridRow.prototype.setTop = function (freezeRowTop) {
        this.top = freezeRowTop || this.Page.rowPos(this.rowIndex);
        this.rowEl.style.setProperty('top', this.top + "px");
    };
    GridRow.prototype.setIndex = function (index) {
        this.rowIndex = index;
        // this.rowEl.textContent = `${index} - ${this.row}`;
        this.top = this.Page.rowPos(index);
        this.rowEl.style.setProperty('top', this.top + "px");
        var zIndex = this.Page.content.length - index;
        if (this.heading)
            zIndex++;
        this.rowEl.style.setProperty('z-index', "" + zIndex);
        if (!this.heading)
            this.rowEl.style.setProperty('height', this.height + "px");
    };
    GridRow.prototype.setFreeze = function (n) {
        if (this.sticky === n)
            return;
        // return;
        this.sticky = n;
        if (n) {
            // let top = this.Page.rowHeights.slice(0, this.row).reduce((a: any, b: any) => a + b, 0);
            // this.rowEl.classList.add('sticky');
            // this.rowEl.style.setProperty('top', `${this.top + this.Page.scrollTop}px`);
        }
        else {
            // this.rowEl.classList.remove('sticky');
            // this.rowEl.style.setProperty('top', `${this.top}px`);
        }
    };
    return GridRow;
}());
exports.GridRow = GridRow;
//# sourceMappingURL=GridRow.js.map