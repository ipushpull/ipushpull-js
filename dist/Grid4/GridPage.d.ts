import { IPageColumnDefs } from "../Page/Page";
import { IPageContentCell, IPageContent, IPageContentCellIndex } from "../Page/Content";
import { IGridCellDetails } from "./GridCell";
import { IGridSelection } from "./Grid";
export interface IGridPageContentCell extends IPageContentCell {
    index: IPageContentCellIndex;
}
export interface IGridPageContent extends IPageContent {
    [index: number]: IGridPageContentCell[];
}
export interface IGridPageCellPos {
    x: number;
    y: number;
}
export interface IGridPageMerges {
    [index: number]: number[];
}
export interface IGridPageElements {
    type: string;
    range: string;
    className?: string;
    style?: any;
}
export interface IGridPage {
    structured: boolean;
    uuid: string;
    scale: number;
    content: IGridPageContent;
    originalContent: IGridPageContent;
    deltaContent: IPageContent;
    rows: number;
    cols: number;
    hiddenColumns: number[];
    hiddenRows: number[];
    rowHeights: number[];
    colWidths: number[];
    width: number;
    height: number;
    borderRightWidth: number;
    borderBottomWidth: number;
    selectedCols: any[];
    selectedRows: any[];
    selectedCells: any[];
    scrollTop: number;
    scrollLeft: number;
    scrollbarSize: number;
    scrollbarWidth: number;
    scrollbarHeight: number;
    selection: IGridSelection;
    columnFilters: boolean;
    headings: boolean;
    touch: boolean;
    columnsDefs: IPageColumnDefs[];
    canSort: boolean;
    sorting: boolean;
    headingsHeight: number;
    filtersOffset: number;
    headingsWidth: number;
    softMerges: boolean;
    tabToLine: boolean;
    cellMapColor: string;
    cellMapErrorColor: string;
    disableFocusOnF2: boolean;
    mappingNavigation: boolean;
    mapping: any;
    elements: IGridPageElements[];
    reset(): void;
    rowPos(row: number): number;
    colPos(col: number): number;
    cellPos(row: number, col: number): IGridPageCellPos;
    resetSelection(): void;
    getColDefWidth(col: number): number;
    isRightClick(evt: any): boolean;
}
declare class GridPage implements IGridPage {
    softMerges: boolean;
    structured: boolean;
    columnFilters: boolean;
    uuid: string;
    scale: number;
    content: IGridPageContent;
    originalContent: IGridPageContent;
    deltaContent: IPageContent;
    rows: number;
    cols: number;
    hiddenColumns: number[];
    hiddenRows: number[];
    rowHeights: number[];
    colWidths: number[];
    width: number;
    height: number;
    borderRightWidth: number;
    borderBottomWidth: number;
    selectedCols: any[];
    selectedRows: any[];
    selectedCells: any[];
    scrollTop: number;
    scrollLeft: number;
    scrollbarSize: number;
    scrollbarWidth: number;
    scrollbarHeight: number;
    selection: IGridSelection;
    headings: boolean;
    touch: boolean;
    columnsDefs: IPageColumnDefs[];
    canSort: boolean;
    sorting: boolean;
    headingsHeight: number;
    headingsWidth: number;
    tabToLine: boolean;
    cellMapColor: string;
    cellMapErrorColor: string;
    disableFocusOnF2: boolean;
    mappingNavigation: boolean;
    mapping: any;
    elements: IGridPageElements[];
    readonly filtersOffset: number;
    reset(): void;
    resetSelection(): void;
    rowPos(row: number): number;
    colPos(col: number): number;
    cellPos(row: number, col: number): IGridPageCellPos;
    setSelectedCell(cell: IGridCellDetails): void;
    getColDefWidth(col: number): number;
    isRightClick(evt: any): boolean;
}
export default GridPage;
