import { IPageContent } from './Page/Content';
import Emitter, { IEmitter } from './Emitter';
interface ITrackingDataModifiedBy {
    id: number;
    first_name: string;
    last_name: string;
    screen_name: string;
}
export interface ITrackingData {
    content_diff: IPageContent;
    modified_by: ITrackingDataModifiedBy;
    modified_by_timestamp: Date;
}
interface ITrackingHistory {
    [key: string]: ITrackingData[];
}
export interface ITracking extends IEmitter {
    ERROR: string;
    createInstance(): ITracking;
    enabled: boolean;
    addHistory(pageId: number, data: ITrackingData, first?: boolean): void;
    getHistory(pageId: number): any;
    getHistoryForCell(pageId: number, cellRow: number, cellCol: number): any;
    restore(): any;
    reset(): any;
    cancel(): void;
}
export declare class Tracking extends Emitter implements ITracking {
    private storage;
    readonly ERROR: string;
    history: ITrackingHistory;
    enabledPages: number[];
    private _enabled;
    private _interval;
    private _length;
    private _utils;
    constructor(storage: any);
    createInstance(): ITracking;
    enabled: boolean;
    save(): void;
    enabledPage(pageId: number, enable: boolean): void;
    addHistory(pageId: number, data: ITrackingData, first?: boolean): void;
    getHistory(pageId: number): ITrackingData[];
    getHistoryForCell(pageId: number, cellRow: number, cellCol: number): any;
    restore(): any;
    reset(): any;
    cancel(): void;
    private totalChanges;
    private compressData;
}
export default Tracking;
