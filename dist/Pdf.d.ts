import Emitter, { IEmitter } from './Emitter';
import Promise from 'promise';
declare class PdfWrap {
    constructor(ippStorage: any, ippConf: any);
}
export default PdfWrap;
export interface IPdf extends IEmitter {
    ON_LOADING: string;
    ON_LOADED: string;
    fit: string;
    destroy(): void;
    render(data: any): Promise<void>;
    setFit(dimension: string): void;
    page(direction: any): void;
}
export declare class Pdf extends Emitter implements IPdf {
    container: string;
    readonly ON_LOADING: string;
    readonly ON_LOADED: string;
    readonly ON_FOCUS: string;
    data: any;
    rendering: boolean;
    annotationId: string;
    containerEl: any;
    canvasEl: any;
    annotationsEl: any;
    fileUrl: string;
    pdfObj: any;
    pdfLink: any;
    pdfPages: number;
    pdfPage: number;
    private _fit;
    private touch;
    private hammer;
    private scale;
    private ratio;
    private ratioScale;
    private containerBounds;
    private canvasBounds;
    scrollbars: any;
    constructor(container: string);
    fit: string;
    destroy(): void;
    render(data: any): Promise<void>;
    setFit(dimension: string): void;
    page(direction: any): void;
    private onResizeEvent;
    private onWheelEvent;
    private onMouseUp;
    private onMouseDown;
    private onKeydown;
    private onMouseMove;
    private resetScrollbars;
    private moveScrollbarTrack;
    private createScrollbars;
    private updateScrollbars;
    private setAnnotationsBounds;
    private setupTouch;
    private setPage;
    private renderPdf;
    private renderAnnotations;
    private handleURL;
}
