import EventEmitter, { IEmitter } from './Emitter';
import { IApiService } from './Api';
import { IAuthService } from './Auth';
export interface IOrganization extends IEmitter {
    getFolderUserAccess(): any;
    update(which: string, folder: any, data: any): any;
}
export default class Organization extends EventEmitter implements IOrganization {
    private api;
    private auth;
    folders: any[];
    filteredFolders: any[];
    disabledFolders: any[];
    users: any[];
    filteredUsers: any[];
    folderPages: any;
    folderFilter: any;
    userFilter: any;
    accessLevels: any;
    folderUsers: any;
    folderInvites: any;
    private folderCount;
    private folderQ;
    private folderPromiseResolve;
    private folderPromiseReject;
    constructor(api: IApiService, auth: IAuthService);
    load(): any;
    getUserFolderAcess(userId: number, folderId: number): any;
    updateFolderStatus(f: any): any;
    queryFolders(): void;
    queryUsers(): void;
    inviteUsers(folder: any, users: any): any;
    removeInvites(folder: any, invitations: any): any;
    setUserAccess(folder: any, users: any, level: any, active?: boolean): any;
    removeUserAccess(folder: any, users: any): any;
    updateUserPageAccess(folderId: number, pages: any, access: string, selected?: boolean): any;
    getTooltipStatus(user: any): string;
    getStatusClass(user: any): string;
    update(which: string, folder: any, data: any): any;
    getFolderUserAccess(): any;
    private getDomainUsersQ;
    private getDomainUsers;
}
