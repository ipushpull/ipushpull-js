"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Utils_1 = __importDefault(require("./Utils"));
var Emitter_1 = __importDefault(require("./Emitter"));
var Tracking = /** @class */ (function (_super) {
    __extends(Tracking, _super);
    function Tracking(storage) {
        var _this = _super.call(this) || this;
        _this.storage = storage;
        _this.history = {};
        _this.enabledPages = [];
        // public historyFirst: any = {};
        _this._enabled = false;
        _this._length = 0;
        _this._utils = new Utils_1.default();
        return _this;
        // setInterval(() => {
        //   let length: number = this.totalChanges();
        //   if (this._length !== length) {
        //     this.save();
        //   }
        //   this._length = length;
        // }, 10000);
    }
    Object.defineProperty(Tracking.prototype, "ERROR", {
        get: function () {
            return 'error';
        },
        enumerable: true,
        configurable: true
    });
    Tracking.prototype.createInstance = function () {
        return new Tracking(this.storage);
    };
    Object.defineProperty(Tracking.prototype, "enabled", {
        get: function () {
            return this._enabled;
        },
        set: function (value) {
            if (typeof value !== 'boolean') {
                return;
            }
            this._enabled = value;
            this.cancel();
            if (!value) {
                this.reset();
            }
            else {
                this.restore();
                // this._interval = setInterval(() => {
                //   let length: number = this.totalChanges();
                //   if (this._length !== length) {
                //     this.save();
                //   }
                //   this._length = length;
                // }, 10000);
            }
        },
        enumerable: true,
        configurable: true
    });
    Tracking.prototype.save = function () {
        try {
            this.storage.user.save('history', JSON.stringify(this.history));
        }
        catch (e) {
            console.error(e.message);
            this.emit(this.ERROR, e.messsage);
        }
    };
    Tracking.prototype.enabledPage = function (pageId, enable) {
        if (!enable) {
            if (this.history["page_" + pageId]) {
                delete this.history["page_" + pageId];
            }
            if (this.enabledPages.indexOf(pageId) > -1) {
                this.enabledPages.splice(this.enabledPages.indexOf(pageId), 1);
            }
        }
        else {
            if (this.enabledPages.indexOf(pageId) < 0) {
                this.enabledPages.push(pageId);
            }
        }
        this.save();
    };
    Tracking.prototype.addHistory = function (pageId, data, first) {
        if (first === void 0) { first = false; }
        if (!this.enabled || this.enabledPages.indexOf(pageId) < 0) {
            return;
        }
        if (first && (this.history["page_" + pageId] && this.history["page_" + pageId].length)) {
            return;
        }
        if (!this.history["page_" + pageId]) {
            this.history["page_" + pageId] = [];
        }
        this.history["page_" + pageId].push(this.compressData(data, first));
        this.save();
    };
    Tracking.prototype.getHistory = function (pageId) {
        if ((!this.enabled && this.enabledPages.indexOf(pageId) < 0) || !this.history["page_" + pageId]) {
            return [];
        }
        return this.history["page_" + pageId];
    };
    Tracking.prototype.getHistoryForCell = function (pageId, cellRow, cellCol) {
        var history = this.getHistory(pageId);
        if (!history.length) {
            return [];
        }
        var cellHistory = [];
        history.forEach(function (snapshot) {
            snapshot.content_diff.forEach(function (row, rowIndex) {
                if (!row) {
                    return;
                }
                row.forEach(function (col, colIndex) {
                    if (!col) {
                        return;
                    }
                    if (cellRow === rowIndex && cellCol === colIndex) {
                        cellHistory.push({
                            cell: JSON.parse(JSON.stringify(col)),
                            modified_by: snapshot.modified_by,
                            modified_by_timestamp: snapshot.modified_by_timestamp
                        });
                    }
                });
            });
        });
        return cellHistory;
    };
    Tracking.prototype.restore = function () {
        this.history = this.storage.user.get('history') || {};
        this._length = this.totalChanges();
        // if (history) {
        //     this.history = JSON.parse(history);
        // }
    };
    Tracking.prototype.reset = function () {
        this.history = {};
        this.storage.user.remove('history');
    };
    Tracking.prototype.cancel = function () {
        if (this._interval) {
            clearInterval(this._interval);
            this._interval = null;
        }
    };
    Tracking.prototype.totalChanges = function () {
        var length = 0;
        for (var id in this.history) {
            if (!this.history.hasOwnProperty(id)) {
                continue;
            }
            length += this.history[id].length;
        }
        return length;
    };
    Tracking.prototype.compressData = function (data, first) {
        if (first === void 0) { first = false; }
        if (first) {
            data.content_diff = this._utils.clonePageContent(data.content_diff, true);
        }
        return JSON.parse(JSON.stringify(data));
    };
    return Tracking;
}(Emitter_1.default));
exports.Tracking = Tracking;
exports.default = Tracking;
// export class TrackingWrap {
//     public static $inject: string[] = [];
//     constructor() {
//         return Tracking;
//     }
// }
//# sourceMappingURL=Tracking.js.map