import { IStorageService } from './Storage';
import { IEncryptionKey } from './Crypto';
export interface IKeysService {
    saveKey(domainName: string, key: any): void;
    getKey(domainName: string, name: string): IEncryptionKey;
    updateKeys(): void;
    load(): void;
    removeKey(domain: string, keyName: string): void;
    mergeData(data: any): boolean;
    reset(): void;
}
declare class KeysService {
    storage: IStorageService;
    private data;
    private cookieName;
    constructor(storage: IStorageService);
    saveKey(domainName: string, key: any): void;
    getKey(domainName: string, name: string): IEncryptionKey;
    updateKeys(): void;
    getKeys(domainName?: string): any;
    load(): void;
    removeKey(domain: string, keyName: string): void;
    mergeData(data: any): boolean;
    reset(): void;
}
export default KeysService;
