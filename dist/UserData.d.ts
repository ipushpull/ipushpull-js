import Promise from "bluebird";
import { IApiService } from "./Api";
import { IStorageService } from "./Storage";
import { IGridState } from "./Grid4/Grid";
export interface IUserDataService {
    META_HIDE_TUTORIAL: string;
    META_FAVS: string;
    META_BOOKMARKS: string;
    META_LOGO: string;
    META_RECENTS: string;
    META_WORKSPACES: string;
    META_ACTIVE_WORKSPACE: string;
    userId: number;
    get(key?: string, defaultValue?: any, parse?: boolean): any;
    set(key: string, value: any, stringify?: boolean): Promise<any>;
    parse: (data: any) => void;
    refresh: () => any;
    destroy(): void;
    getBookmarks(): Promise<any>;
    addBookmark(bookmark: IUserDataBookmark): Promise<any>;
    removeBookmark(index: number): Promise<any>;
    saveBookmarks(): Promise<any>;
    getData(key?: string): any;
    getFavs(): Promise<any>;
    getFav(folderId: number, pageId: number): IUserDataFav | undefined;
    updateFav(data: IUserDataFav): Promise<any>;
    addFav(data: IUserDataFav): Promise<any>;
    removeFav(data: IUserDataFav): Promise<any>;
    toggleFav(data: IUserDataFav): Promise<any>;
    getFavLink(folderId: number, pageId: number): string;
    isFav(folderId: number, pageId: number): boolean;
    saveFavs(): Promise<any>;
    getRecents(): IUserDataFav[];
    updateRecent(data: IUserDataFav): void;
    addRecent(data: IUserDataFav): void;
    getRecent(folderId: number, pageId: number): IUserDataFav | undefined;
    removeRecent(data: IUserDataFav): void;
    saveWorkspaces(workspaces: IUserDataWorkspace[]): Promise<any>;
    getWorkspaces(): Promise<any>;
    getNewWorkspace(): IUserDataWorkspace;
    addWorkspace(data: IUserDataWorkspace): Promise<any>;
    removeWorkspace(id: string): Promise<any>;
}
export interface IUserDataBookmark {
    page: string;
    title: string;
    folder: string;
    state?: IGridState | null;
    type: number;
}
export interface IUserDataFavStates {
    name: string;
    state?: IGridState;
}
export interface IUserDataFav {
    id: string;
    link: string;
    title: string;
    customTitle: string;
    pageId?: number;
    folderId?: number;
    folder?: string;
    states?: IUserDataFavStates[];
    pinned?: boolean;
    pinnedOrder?: number;
    pinnedColor?: string;
    view?: string;
    type: number;
    [key: string]: any;
}
export interface IUserDataWorkspaceTileCoords {
    x: number;
    y: number;
    w: number;
    h: number;
    z: number;
}
export interface IUserDataWorkspaceTile {
    title: string;
    type: string;
    domain: string;
    page: string;
    tile: IUserDataWorkspaceTileCoords;
    fit?: string;
    state?: string;
}
export interface IUserDataWorkspace {
    name: string;
    id: string;
    freeze: false;
    boards: IUserDataWorkspaceTile[];
    unit: "%";
}
export interface IUserDataStoragePageSettings {
    [key: string]: IUserDataStoragePageSettingSetting;
}
export interface IUserDataStoragePageSettingSetting {
    autosave?: boolean;
    fit?: string;
    formulaBar?: boolean;
    gridlines?: boolean;
    headings?: boolean;
    highlights?: boolean;
    hoverHighlights?: boolean;
    tracking?: boolean;
}
export interface IUserDataStorageGridState {
    [key: string]: IGridState;
}
export interface IUserDataStorage {
    recent_pages: IUserDataFav[];
    page_settings: IUserDataStoragePageSettings;
    domains_pages_selected: number[];
    domains_pages_pinned_selected: number[];
    color_picker_colors: any;
    grid_state: IUserDataStorageGridState;
    panel_cards: string[];
}
declare class UserData implements IUserDataService {
    api: IApiService;
    storage: IStorageService;
    userId: number;
    readonly META_HIDE_TUTORIAL: string;
    readonly META_FAVS: string;
    readonly META_BOOKMARKS: string;
    readonly META_LOGO: string;
    readonly META_RECENTS: string;
    readonly META_WORKSPACES: string;
    readonly META_ACTIVE_WORKSPACE: string;
    recentsLimit: number;
    private _data;
    private _favs;
    private _bookmarks;
    private _links;
    private _workspaces;
    private _storage;
    private _storagePrefix;
    constructor(api: IApiService, storage: IStorageService, userId: number);
    readonly favs: IUserDataFav[];
    readonly bookmarks: IUserDataBookmark[];
    parse(data: any): void;
    set(key: string, value: any, stringify?: boolean): Promise<any>;
    refresh(): any;
    get(key?: string, defaultValue?: any, parse?: boolean): any;
    destroy(): void;
    getRecents(): IUserDataFav[];
    updateRecent(data: IUserDataFav): void;
    addRecent(data: IUserDataFav): void;
    getRecent(folderId: number, pageId: number): IUserDataFav | undefined;
    removeRecent(data: IUserDataFav): void;
    getFavs(): Promise<any>;
    updateFav(data: IUserDataFav): Promise<any>;
    setFavName(folderId: number, pageId: number, title: string, folder?: string): boolean;
    saveFavs(): Promise<any>;
    addFav(data: IUserDataFav): Promise<any>;
    removeFav(data: IUserDataFav): Promise<any>;
    toggleFav(data: IUserDataFav): Promise<any>;
    getFav(folderId: number, pageId: number): IUserDataFav | undefined;
    getFavLink(folderId: number, pageId: number): string;
    isFav(folderId: number, pageId: number): boolean;
    saveWorkspaces(workspaces: IUserDataWorkspace[]): Promise<any>;
    getNewWorkspace(): IUserDataWorkspace;
    getWorkspaces(): Promise<any>;
    addWorkspace(data: IUserDataWorkspace): Promise<any>;
    removeWorkspace(id: string): Promise<any>;
    setWorkspaces(data: any): any;
    getBookmarks(): Promise<any>;
    addBookmark(bookmark: IUserDataBookmark): Promise<any>;
    removeBookmark(index: number): Promise<any>;
    saveBookmarks(): Promise<any>;
    getData(key?: string): any;
    private checkForFavDuplicate;
    private upgradeFavs;
    private saveRecents;
    private saveData;
}
export default UserData;
