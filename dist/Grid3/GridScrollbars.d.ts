import Emitter, { IEmitter } from '../Emitter';
export interface IGridScrollbars extends IEmitter {
    SCROLL: string;
    SCROLL_X: string;
    SCROLL_Y: string;
    scrollOffset: number;
    fit: string;
    pageEl: HTMLDivElement;
    mainEl: HTMLDivElement;
    scrollTrackX: HTMLDivElement;
    scrollTrackY: HTMLDivElement;
    update(fit?: string, scale?: number): void;
    destroy(): void;
}
export declare class GridScrollbars extends Emitter {
    pageEl: HTMLDivElement;
    mainEl: HTMLDivElement;
    get SCROLL(): string;
    get SCROLL_X(): string;
    get SCROLL_Y(): string;
    scrollOffset: number;
    scrollTrackX: HTMLDivElement;
    private scrollHandleX;
    scrollTrackY: HTMLDivElement;
    private scrollHandleY;
    fit: string;
    scale: number;
    private scrolling;
    private axis;
    private offset;
    private start;
    constructor(pageEl: HTMLDivElement, mainEl: HTMLDivElement);
    update(fit?: string, scale?: number): void;
    destroy(): void;
    private onScroll;
    private onResize;
    private updateHandles;
    private onMouseWheel;
    private onMouseDownTrack;
    private onMouseDown;
    private onMouseMove;
    private onMouseUp;
}
