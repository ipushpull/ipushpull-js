"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Helpers_1 = __importDefault(require("../Helpers"));
var Emitter_1 = __importDefault(require("../Emitter"));
var helpers = new Helpers_1.default();
var GridHeadingCell = /** @class */ (function (_super) {
    __extends(GridHeadingCell, _super);
    function GridHeadingCell(type, params) {
        var _this = _super.call(this) || this;
        _this.type = type;
        _this.params = params;
        _this.common = false;
        _this.reference = '';
        _this.hidden = false;
        _this.row = -1;
        _this.col = -1;
        _this.index = {
            row: -1,
            col: -1
        };
        _this.position = {
            row: -1,
            col: -1
        };
        _this.moving = false;
        _this.startX = 0;
        _this.offsetX = 0;
        _this.startY = 0;
        _this.offsetY = 0;
        _this.zIndex = '';
        _this.width = 0;
        _this.height = 0;
        _this.onMouseDown = function (evt) {
            if (evt.target !== _this.handle) {
                return;
            }
            window.addEventListener('mouseup', _this.onMouseUp);
            window.addEventListener('mousemove', _this.onMouseMove);
            _this.moving = true;
            _this.width = _this.dataset.width;
            _this.height = _this.dataset.height;
            _this.startX = evt.x;
            _this.startY = evt.y;
            // this.zIndex = this.element.style.getPropertyValue('z-index');
            // this.element.style.setProperty('z-index', `${parseFloat(this.zIndex) * 1000}`);
            // this.element.classList.add('active');
        };
        _this.onMouseUp = function (evt) {
            if (!_this.moving) {
                return;
            }
            window.removeEventListener('mouseup', _this.onMouseUp);
            window.removeEventListener('mousemove', _this.onMouseMove);
            _this.moving = false;
            // this.element.style.setProperty('z-index', `${parseFloat(this.zIndex) * 1000}`);
            // this.element.classList.remove('active');
            // this.emitSize();
            _this.emit(_this.RESIZED);
            console.log(_this);
        };
        _this.onMouseMove = function (evt) {
            if (!_this.moving) {
                return;
            }
            _this.offsetX = evt.x - _this.startX;
            _this.offsetY = evt.y - _this.startY;
            var width = _this.width + _this.offsetX;
            if (width < 20)
                width = 20;
            var height = _this.height + _this.offsetY;
            if (height < 20)
                height = 20;
            _this.emit(_this.RESIZING, {
                type: _this.type,
                width: width,
                height: height,
                offsetX: width - _this.width,
                offsetY: height - _this.height
            });
            // console.log(this.onListeners);
            // if (this.axis === 'col') {
            //   this.Grid.Content.setColSize(this.cell, width);
            // } else {
            //   this.Grid.Content.setRowSize(this.cell, height);
            // }
            // console.log(this.width, width, this.Grid.Content.cells[0][0].style.width);
        };
        // this.hidden = params.hidden;
        var cell = document.createElement('div');
        cell.className = "grid-cell-heading grid-cell-heading-" + type;
        cell.style.setProperty('transform', "translate(" + params.x + "px, " + params.y + "px)");
        // cell.style.setProperty('transform', `translateX(${params.x}px)`);
        if (type === 'row') {
            _this.reference = helpers.toColumnName(params.colIndex + 1);
            cell.innerText = helpers.toColumnName(params.colIndex + 1);
            cell.style.setProperty('width', params.content.style.width);
            cell.style.setProperty('height', '20px');
            cell.style.setProperty('z-index', "" + params.zIndexCol);
        }
        else if (type === 'col') {
            _this.reference = "" + (params.content.index.row + 1);
            cell.innerText = (params.content.index.row + 1).toString();
            cell.style.setProperty('width', '40px');
            cell.style.setProperty('height', params.content.style.height);
            cell.style.setProperty('z-index', "" + params.zIndexRow);
        }
        // cell.style.setProperty('z-index', `${params.zIndexRow * params.zIndexCol}`);
        // cell.dataset.width = parseFloat(params.col.style.width!).toString();
        // cell.dataset.height = parseFloat(params.col.style.height!).toString();
        // cell.dataset.overflow = cell.dataset.width;
        // cell.dataset.x = params.x.toString();
        // cell.dataset.y = params.y.toString();
        cell.dataset.row = params.rowIndex.toString();
        cell.dataset.col = params.colIndex.toString();
        cell.dataset.r = params.rowIndex.toString();
        cell.dataset.c = params.colIndex.toString();
        cell.dataset.type = type;
        _this.row = params.rowIndex;
        _this.col = params.colIndex;
        _this.position.row = params.rowIndex;
        _this.position.col = params.colIndex;
        _this.index.row = params.rowIndex;
        _this.index.col = params.colIndex;
        _this.handle = document.createElement('div');
        _this.handle.className = 'grid-resize-handle';
        _this.handle.addEventListener('mousedown', _this.onMouseDown);
        cell.appendChild(_this.handle);
        _this.cell = cell;
        var dataset = {
            width: Math.round(parseFloat(params.content.style.width)),
            height: Math.round(parseFloat(params.content.style.height)),
            overflow: Math.round(parseFloat(params.content.style.width)),
            x: params.x,
            y: params.y,
            row: params.rowIndex,
            col: params.colIndex,
            type: type
        };
        // Object.keys(cell.dataset).forEach((key: string) => {
        //   dataset[key] = key === 'type' ? cell.dataset[key] : parseFloat(cell.dataset[key] || '0');
        // });
        _this.dataset = dataset;
        return _this;
    }
    Object.defineProperty(GridHeadingCell.prototype, "VALUE", {
        get: function () {
            return 'value';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridHeadingCell.prototype, "RESIZING", {
        get: function () {
            return 'resizing';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridHeadingCell.prototype, "RESIZED", {
        get: function () {
            return 'resized';
        },
        enumerable: true,
        configurable: true
    });
    GridHeadingCell.prototype.selected = function (n) {
        if (!n) {
            this.cell.classList.remove('selected');
            return;
        }
        this.cell.classList.add('selected');
    };
    GridHeadingCell.prototype.setFreeze = function (common) {
        this.common = common;
        this.setTranslate(this.dataset.x, this.dataset.y);
    };
    GridHeadingCell.prototype.setSize = function (which, size) {
        if (this.type === 'corner')
            return;
        // if (this.type === 'col' && which === 'width') return;
        // if (this.type === 'row' && which === 'height') return;
        // if (which === 'width' && index !== this.dataset.col) return;
        // if (which === 'height' && index !== this.dataset.row) return;
        this.params.content.style[which] = size + "px";
        this.dataset[which] = size;
        if (which === 'width') {
            this.dataset.overflow = size;
        }
        this.cell.style.setProperty(which, size + "px");
    };
    GridHeadingCell.prototype.setOffset = function (which, offset) {
        if (this.type === 'corner')
            return;
        var xOffset = this.dataset.x;
        var yOffset = this.dataset.y;
        if (which === 'width') {
            // if (this.dataset.col <= index || this.type === 'col') return;
            xOffset = this.dataset.x + offset;
        }
        else {
            // if (this.dataset.row <= index || this.type === 'row') return;
            yOffset = this.dataset.y + offset;
        }
        // this.cell.style.setProperty('transform', `translate(${xOffset}px, ${yOffset}px)`);
        this.setTranslate(xOffset, yOffset);
    };
    GridHeadingCell.prototype.setPosition = function (which, offset, move) {
        if (this.type === 'corner')
            return;
        if (which === 'width') {
            // if (this.dataset.col <= index) return;
            // this.params.x += offset;
            this.dataset.x += offset;
        }
        else {
            // if (this.dataset.row <= index) return;
            // this.params.y += offset;
            this.dataset.y += offset;
        }
        if (move)
            this.setTranslate(this.dataset.x, this.dataset.y);
    };
    GridHeadingCell.prototype.setIndex = function (row, y, zIndex) {
        this.dataset.row = row;
        this.dataset.y = y;
        // this.cell.style.setProperty('transform', `translate(${this.dataset.x}px, ${this.dataset.y}px)`);
        this.setFreeze(this.common);
        // this.cell.style.setProperty('z-index', `${zIndex}`);
    };
    GridHeadingCell.prototype.setSorting = function (direction) {
        if (!direction) {
            this.cell.classList.remove('sorting-asc', 'sorting-desc');
        }
        else {
            this.cell.classList.add("sorting-" + direction);
        }
    };
    GridHeadingCell.prototype.setTranslate = function (x, y) {
        if (this.common) {
            this.hidden = false;
            if (this.type === 'row') {
                this.cell.style.setProperty('transform', "translate(" + (x + 40) + "px, " + y + "px)");
            }
            else {
                this.cell.style.setProperty('transform', "translate(" + x + "px, " + (y + 20) + "px)");
            }
        }
        else {
            this.cell.style.setProperty('transform', "translate(" + x + "px, " + y + "px)");
        }
    };
    return GridHeadingCell;
}(Emitter_1.default));
exports.GridHeadingCell = GridHeadingCell;
//# sourceMappingURL=GridHeadingCell.js.map