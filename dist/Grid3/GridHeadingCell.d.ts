import Emitter, { IEmitter } from '../Emitter';
import { IGridCellDataset, IGridCellParams, IGridContentCellPosition } from './GridCell';
export interface IGridCellHeadingDataset extends IGridCellDataset {
    type: string;
}
export interface IGridHeadingCellResizing extends IEmitter {
    type: string;
    width: number;
    height: number;
    offsetX: number;
    offsetY: number;
}
export interface IGridHeadingCell extends IEmitter {
    VALUE: string;
    RESIZING: string;
    RESIZED: string;
    type: string;
    cell: HTMLDivElement;
    handle: HTMLDivElement;
    dataset: IGridCellHeadingDataset;
    common: boolean;
    reference: string;
    hidden: boolean;
    params: IGridCellParams;
    row: number;
    col: number;
    position: IGridContentCellPosition;
    index: IGridContentCellPosition;
    selected(n: boolean): void;
    setFreeze(common: boolean): void;
    setSize(which: string, size: number): void;
    setOffset(which: string, size: number): void;
    setPosition(which: string, size: number, move?: boolean): void;
    setSorting(direction: string): void;
    setIndex(row: number, y: number, zIndex: number): void;
}
export declare class GridHeadingCell extends Emitter implements IGridHeadingCell {
    type: string;
    params: IGridCellParams;
    get VALUE(): string;
    get RESIZING(): string;
    get RESIZED(): string;
    cell: HTMLDivElement;
    handle: HTMLDivElement;
    dataset: IGridCellHeadingDataset;
    common: boolean;
    reference: string;
    hidden: boolean;
    row: number;
    col: number;
    index: IGridContentCellPosition;
    position: IGridContentCellPosition;
    private moving;
    startX: number;
    offsetX: number;
    startY: number;
    offsetY: number;
    zIndex: string;
    private width;
    private height;
    constructor(type: string, params: IGridCellParams);
    selected(n: boolean): void;
    setFreeze(common: boolean): void;
    setSize(which: string, size: number): void;
    setOffset(which: string, offset: number): void;
    setPosition(which: string, offset: number, move?: boolean): void;
    setIndex(row: number, y: number, zIndex: number): void;
    setSorting(direction: string): void;
    private setTranslate;
    private onMouseDown;
    private onMouseUp;
    private onMouseMove;
}
