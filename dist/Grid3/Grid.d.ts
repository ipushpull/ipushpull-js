import { IGridEvents } from './GridEvents';
import { IPageContentCell, IPageContent, IPageContentCellIndex } from '../Page/Content';
import { ITrackingData } from '../Tracking';
import { IGridCell, IGridContentCellPosition } from './GridCell';
import { IActionsButton } from '../Actions/Buttons';
import { IActionsStyleConditionStyle } from '../Actions/Styles';
import { IGridCanvas, IGridImageOptions } from './GridCanvas';
export interface IGridCells extends Array<any> {
    [index: number]: IGridCell[];
}
export interface IGridOptions {
    fit?: string;
    headings?: boolean;
    gridlines?: boolean;
    highlights?: boolean;
    tracking?: boolean;
    contrast?: string;
    disallowSelection?: boolean;
    canEdit?: boolean;
    [key: string]: any;
}
export interface IGridSelection {
    from?: IGridCell;
    to?: IGridCell;
    last?: IGridCell;
    selected: boolean;
    rowFrom: number;
    rowTo: number;
    colFrom: number;
    colTo: number;
    heading: boolean;
    inside: boolean;
    rowStart: number;
    rowEnd: number;
    rowLast: number;
    colLast: number;
    reference: string;
    keyboard: boolean;
    type: string;
    [key: string]: any;
}
export declare enum IGridFieldType {
    STRING = "string",
    NUMBER = "number",
    DATE = "date"
}
export declare enum IGridSortDirection {
    ASC = "asc",
    DESC = "desc"
}
export interface IGridSorting {
    enabled: boolean;
    field: string;
    direction: IGridSortDirection;
    col: number;
    type: IGridFieldType;
    [key: string]: any;
}
export interface IGridFilter {
    col: number;
    field: string;
    enabled: boolean;
    exp: string;
    type: string;
    value: string;
    dateFormat?: string;
}
export interface IGridState {
    filters: IGridFilter[];
    hiddenColumns: number[];
    sorting: IGridSorting;
}
export interface IGridContentStyles extends Array<any> {
    [index: number]: IActionsStyleConditionStyle[];
}
export interface IGrid {
    Canvas: IGridCanvas;
    ref: string | HTMLDivElement;
    fit: string;
    headings: boolean;
    gridlines: boolean;
    highlights: boolean;
    tracking: boolean;
    contrast: string;
    disallowSelection: boolean;
    hoverHighlights: boolean;
    selectionHighlights: boolean;
    canEdit: boolean;
    userId: number;
    Events: IGridEvents;
    state: IGridState;
    cellLimit: number;
    reset(): void;
    destroy(): void;
    setOption(key: string, value: any): void;
    setContent(content: IPageContent, state?: IGridState): void;
    setDeltaContent(content: any): void;
    setFreeze(row: number, col: number, headings?: boolean): void;
    setContrast(which: string): void;
    setHiddenColumns(columns: number[]): void;
    setSelector(from: IGridContentCellPosition, to: IGridContentCellPosition): void;
    setSelectorByRef(str: string): void;
    hideSelector(): void;
    clearSelector(): void;
    getGridCell(rowIndex: number, colIndex: number): IGridCell | null;
    getContentHtml(type?: string): string;
    getSelectedCells(): IGridCells;
    update(scale?: number): void;
    updateButtons(buttons?: IActionsButton[]): void;
    updateRanges(ranges: any, userId?: number): void;
    updateState(state: IGridState): void;
    updateSorting(sorting?: any): void;
    updateFreeze(row: number, col: number, headings?: boolean): void;
    updateScale(n: number): void;
    updateFit(fit: string): void;
    updateTrackingData(trackingData: ITrackingData[]): void;
    updateStyles(styles?: IGridContentStyles): void;
    image(options?: IGridImageOptions): string;
    find(query: string): void;
    [key: string]: any;
}
export interface IGridPageContentCell extends IPageContentCell {
    index: IPageContentCellIndex;
}
export interface IGridPageContent extends IPageContent {
    [index: number]: IGridPageContentCell[];
}
/**
 * Grid
 *
 * ```html
 * <style>
 * #page {
 *   position: absolute;
 *   z-index: 10;
 *   left: 0;
 *   top: 0;
 *   bottom: 0;
 *   right: 0;
 * }
 * </style>
 * <link rel="stylesheet" href="./grid.css" />
 * <div id="page" class="grid"></div>
 * ```
 *
 * ```javascript
 * let grid = new ipushpull.Grid('page');
 * ```
 */
export declare class Grid implements IGrid {
    ref: string | HTMLDivElement;
    Canvas: IGridCanvas;
    freezeCol: number;
    freezeRow: number;
    fit: string;
    highlights: boolean;
    hoverHighlights: boolean;
    selectionHighlights: boolean;
    tracking: boolean;
    trackingData: ITrackingData[] | undefined;
    contrast: string;
    disallowSelection: boolean;
    canEdit: boolean;
    userId: number;
    state: IGridState;
    cellLimit: number;
    Events: IGridEvents;
    private width;
    private height;
    private gridCells;
    private commonCells;
    private columnCells;
    private gridRowHeadingCells;
    private gridColumnHeadingCells;
    private rows;
    private rowsColumns;
    private rowsRows;
    private rowCells;
    private freezeHeight;
    private freezeWidth;
    private scale;
    private rowsToHide;
    private offsetX;
    private offsetY;
    private cellsOffsetX;
    private cellsOffsetY;
    touch: boolean;
    private pageEl;
    private sortWrapperEl;
    private sortEl;
    private pageElRect;
    private mergeEl;
    private scaleEl;
    private popperEl;
    private scrollEl;
    private boxEl;
    private editEl;
    private headingsEl;
    private headingsColumnsEl;
    private headingsColumnsCellsEl;
    private headingsRowsEl;
    private headingsRowsCellsEl;
    private headingsCommonEl;
    private headingsCommonCellsEl;
    private wrapperEl;
    private wrapperElRect;
    private mainEl;
    private mainElRect;
    private mainCellsEl;
    private mainBoxEl;
    private columnsEl;
    private columnsCellsEl;
    private rowsEl;
    private rowsCellsEl;
    private commonEl;
    private commonCellsEl;
    private selectorWrapper;
    private selectorMainContainer;
    private selectorMainContainerPosition;
    private selectorMain;
    private selectorColumnsContainer;
    private selectorColumnsContainerPosition;
    private selectorColumns;
    private selectorRowsContainer;
    private selectorRowsContainerPosition;
    private selectorRows;
    private selectorCommonContainer;
    private selectorCommonContainerPosition;
    private selectorCommon;
    private initialized;
    private rendering;
    private ps;
    private originalContent;
    private content;
    private resizing;
    private doubleClicked;
    private historyClick;
    private rightClick;
    private rowChecked;
    private clickedAt;
    private isFocus;
    private _editing;
    private _hasFocus;
    private _gridlines;
    private _headings;
    private dirty;
    private big;
    private trackingUsers;
    private hiddenColumns;
    private buttons;
    private styles;
    private found;
    private accessRanges;
    private headingsTimer;
    private pinchDistance;
    private pinchScale;
    private selection;
    private editingCell;
    private buttonPopper;
    private ctrlDown;
    private shiftDown;
    private arrowKeyDown;
    private unshift;
    private keyValue;
    private hammer;
    constructor(ref: string | HTMLDivElement, options?: IGridOptions);
    image(options: IGridImageOptions): string;
    init(): void;
    reset(): void;
    destroy(): void;
    setOption(key: string, value: any): void;
    setHiddenColumns(columns: number[]): void;
    updateSorting(sorting?: any): void;
    updateButtons(buttons?: IActionsButton[]): void;
    updateStyles(styles?: IGridContentStyles): void;
    updateRanges(ranges: any, userId?: number): void;
    private applyStyles;
    private applyButtons;
    private applyRanges;
    private applyCellUpdate;
    /**
     * Set grid content from Page.
     * Resets grid state and settings then create all new grid cell references. Use when first loading a Page or the Page size has changed.
     * @param {IGridPageContent} content - Page content.
     * @param {IGridState} state - Grid state. Sorting, Filters, Hidden Columns
     * @returns {void}
     */
    setContent(content: IGridPageContent, state?: IGridState): void;
    /**
     * Update grid cell values.
     * Use when there is new content from a Page.
     * @param content - Cells that have changed.
     */
    setDeltaContent(content: IPageContent): void;
    /**
     * Sort, Filter and/or show/hide Columns.
     * @param state
     */
    updateState(state: IGridState): void;
    updateTrackingData(trackingData: ITrackingData[]): void;
    /**
     * Freeze rows/columns.
     * Use when a Page is ready and only after setContent() has been applied.
     * @param row {number}
     * @param col {number}
     * @param headings {boolean}
     */
    setFreeze(row: number, col: number, headings?: boolean): void;
    updateHeadings(headings: boolean): void;
    updateFreeze(row: number, col: number, headings?: boolean): void;
    updateFit(fit: string): void;
    updateScale(n: number): void;
    setContrast(which: string): void;
    getGridCell(rowIndex: number, colIndex: number, original?: boolean): IGridCell | null;
    getContentHtml(type?: string): string;
    getSelectedCells(): IGridCells;
    find(query: string): void;
    updateSize(): void;
    /**
     * Display the grid.
     * Use after setContent().
     * @param scale
     */
    update(scale?: number): void;
    clearFound(): void;
    clearTrackingData(): void;
    hideSelector(): void;
    clearSelector(): void;
    setSelector(from: IGridContentCellPosition, to: IGridContentCellPosition): void;
    setSelectorByRef(str: string): void;
    private applyHeadingCellsFreeze;
    private setElementSizes;
    private setFreezeSizes;
    private updateSoftMerges;
    private setSize;
    private applyScale;
    private applyState;
    get hasFocus(): boolean;
    set hasFocus(n: boolean);
    get gridlines(): boolean;
    set gridlines(n: boolean);
    get headings(): boolean;
    set headings(n: boolean);
    private get editing();
    private set editing(value);
    /**
     * @description
     * After resizing set the final cell offets and update grid size
     *
     * @param {string} which row | col
     * @param {number} index
     * @param {number} offset
     */
    private setPositions;
    private onResize;
    private destroyPopper;
    private onCellUpdates;
    private cancelCellEdit;
    private getTouchDistance;
    private onMouseDown;
    private onResizeHandle;
    private onMouseMove;
    private onMouseUp;
    private onKeydown;
    private onKeyup;
    private onCtrlNavigate;
    private emitSelected;
    private isInsideSelection;
    private setCellSelection;
    private getGridCellByTarget;
    private selectNextCell;
    private scrollTimer;
    private updateSelector;
    private updateHistoryCells;
    private getTrackingUser;
    private dom;
    private domHeadings;
    private isFullyVisible;
    private isVisible;
    private scaleDimension;
    private render;
    private isTarget;
    private getCell;
    private isRightClick;
    private createListeners;
    private destroyListeners;
    private applySorting;
    private applyFilters;
}
