export interface IGridRowParams {
    y: number;
    rowIndex: number;
    zIndexRow: number;
    height: number;
}
export interface IGridRowDataset {
    y: number;
    row: number;
    zIndexRow: number;
    height: number;
}
export interface IGridRow {
    params: IGridRowParams;
    dataset: IGridRowDataset;
    rowEl: HTMLDivElement;
    hidden: boolean;
    row: number;
    setHeight(size: number): void;
    setOffet(offset: number): void;
    setPosition(offset: number, move?: boolean): void;
    setIndex(row: number, y: number, zIndex: number): void;
    setHover(n: boolean): void;
}
export declare class GridRow implements IGridRow {
    params: IGridRowParams;
    dataset: IGridRowDataset;
    rowEl: HTMLDivElement;
    hidden: boolean;
    row: number;
    constructor(params: IGridRowParams);
    setHeight(size: number): void;
    setOffet(offset: number): void;
    setPosition(offset: number, move?: boolean): void;
    setIndex(row: number, y: number, zIndex: number): void;
    setHover(n: boolean): void;
}
