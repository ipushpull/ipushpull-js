"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ua_parser_js_1 = require("ua-parser-js");
var GridEvents_1 = __importDefault(require("./GridEvents"));
var Helpers_1 = __importDefault(require("../Helpers"));
var GridCell_1 = require("./GridCell");
var GridHeadingCell_1 = require("./GridHeadingCell");
var GridRow_1 = require("./GridRow");
var GridScrollbars_1 = require("./GridScrollbars");
var __1 = require("..");
var popper_js_1 = __importDefault(require("popper.js"));
var moment_timezone_1 = __importDefault(require("moment-timezone"));
var GridCanvas_1 = __importDefault(require("./GridCanvas"));
var GridContent_1 = __importDefault(require("./GridContent"));
// import * as Hammer from 'hammerjs';
// import { eventNames } from 'cluster';
var helpers = new Helpers_1.default();
var IGridFieldType;
(function (IGridFieldType) {
    IGridFieldType["STRING"] = "string";
    IGridFieldType["NUMBER"] = "number";
    IGridFieldType["DATE"] = "date";
})(IGridFieldType = exports.IGridFieldType || (exports.IGridFieldType = {}));
var IGridSortDirection;
(function (IGridSortDirection) {
    IGridSortDirection["ASC"] = "asc";
    IGridSortDirection["DESC"] = "desc";
})(IGridSortDirection = exports.IGridSortDirection || (exports.IGridSortDirection = {}));
/**
 * Grid
 *
 * ```html
 * <style>
 * #page {
 *   position: absolute;
 *   z-index: 10;
 *   left: 0;
 *   top: 0;
 *   bottom: 0;
 *   right: 0;
 * }
 * </style>
 * <link rel="stylesheet" href="./grid.css" />
 * <div id="page" class="grid"></div>
 * ```
 *
 * ```javascript
 * let grid = new ipushpull.Grid('page');
 * ```
 */
var Grid = /** @class */ (function () {
    function Grid(ref, options) {
        var _this = this;
        this.ref = ref;
        this.freezeCol = 0;
        this.freezeRow = 0;
        this.fit = 'scroll';
        this.highlights = false;
        this.hoverHighlights = false;
        this.selectionHighlights = false;
        this.tracking = false;
        this.contrast = 'light';
        this.disallowSelection = false;
        this.canEdit = true;
        this.userId = 0;
        this.state = {
            filters: [],
            hiddenColumns: [],
            sorting: {
                col: -1,
                field: '',
                direction: IGridSortDirection.ASC,
                enabled: false,
                type: IGridFieldType.STRING
            }
        };
        this.cellLimit = 10000;
        this.width = 0;
        this.height = 0;
        this.gridCells = [[]];
        this.commonCells = [[]];
        this.columnCells = [[]];
        // private gridHeadingCells: IGridHeadingCell[] = [];
        this.gridRowHeadingCells = [];
        this.gridColumnHeadingCells = [];
        // private cells: IGridHtmlCells = [[]];
        this.rows = [];
        this.rowsColumns = [];
        this.rowsRows = [];
        // private columnCells: HTMLDivElement[] = [];
        this.rowCells = [];
        // private commonCells: HTMLDivElement[] = [];
        this.freezeHeight = 0;
        this.freezeWidth = 0;
        this.scale = 1;
        this.rowsToHide = [];
        this.offsetX = 0; // scroll offet
        this.offsetY = 0; // scroll offet
        this.cellsOffsetX = 0; // cell offset
        this.cellsOffsetY = 0;
        this.touch = false;
        this.initialized = false;
        this.rendering = false;
        this.originalContent = [];
        this.content = [];
        // private hasFocus: boolean = false;
        this.resizing = false;
        this.doubleClicked = false;
        this.historyClick = false;
        this.rightClick = false;
        this.rowChecked = false;
        this.clickedAt = 0;
        this.isFocus = false;
        this._editing = false;
        this._hasFocus = false;
        this._gridlines = false;
        this._headings = false;
        this.dirty = false;
        this.big = false;
        this.trackingUsers = [];
        this.hiddenColumns = [];
        this.buttons = [];
        this.styles = [];
        this.found = [];
        this.accessRanges = [];
        this.pinchDistance = 0;
        this.pinchScale = 0;
        this.selection = {
            selected: false,
            rowFrom: 0,
            rowTo: 0,
            rowStart: 0,
            rowEnd: 0,
            rowLast: 0,
            colTo: 0,
            colFrom: 0,
            colLast: 0,
            heading: false,
            inside: false,
            reference: '',
            keyboard: false,
            type: 'cell'
        };
        // keyboard tracking
        this.ctrlDown = false;
        this.shiftDown = false;
        this.arrowKeyDown = false;
        this.unshift = false;
        this.onResize = function () {
            // this.update();
            _this.setFreezeSizes(true);
            _this.applyScale();
            _this.render();
        };
        this.onMouseDown = function (evt) {
            if (evt.touches) {
                if (evt.touches.length == 2) {
                    _this.pinchDistance = _this.getTouchDistance(evt.touches);
                    _this.pinchScale = _this.scale;
                    return;
                }
                evt.clientX = evt.touches[0].pageX;
                evt.clientY = evt.touches[0].pageY;
            }
            // if (this.disallowSelection) {
            //   return;
            // }
            if (evt.target.nodeName === 'INPUT' && evt.target.classList.contains('grid-input')) {
                return;
            }
            var target = evt.target;
            _this.mainElRect = _this.mainEl.getBoundingClientRect();
            // this.Grid.Content.hasFocus = this.Grid.Container.hasFocus(evt);
            // this.Grid.Content.ctrlDown = false;
            // this.Grid.Content.shiftDown = false;
            // this.Grid.Container.containerRect = this.Grid.Container.container.getBoundingClientRect();
            // grid states
            _this.isFocus = _this.hasFocus = _this.isTarget(target, _this.pageEl);
            if (!_this.hasFocus) {
                _this.Events.emit(_this.Events.BLUR);
                return;
            }
            var d = new Date();
            _this.rightClick = _this.isRightClick(evt);
            _this.doubleClicked = d.getTime() - _this.clickedAt <= 300;
            _this.clickedAt = d.getTime();
            // check if cell
            var gridCell = _this.setCellSelection(target);
            _this.rowChecked = evt.target.nodeName === 'DIV' && evt.target.classList.contains('checkbox');
            // check if heading cell within current selection
            // event
            var type = '';
            if (gridCell) {
                _this.historyClick = target.classList.contains('grid-history');
                _this.selection.selected = true;
                // TODO fix grid heading cell types!
                if (gridCell.type === 'row')
                    type = 'col';
                else if (gridCell.type === 'col')
                    type = 'row';
                else
                    type = gridCell.type;
                _this.selection.type = type;
                var heading = gridCell instanceof GridHeadingCell_1.GridHeadingCell;
                _this.Events.emit(_this.Events.CELL_CLICKED, {
                    historyClick: _this.historyClick,
                    rightClick: _this.rightClick,
                    cell: _this.getGridCell(gridCell.row, gridCell.col),
                    event: evt,
                    content: gridCell.params.content,
                    heading: heading
                });
                // check if the resize has been clicked
                if (heading && target.classList.contains('grid-resize-handle')) {
                    _this.onResizeHandle(gridCell);
                    return;
                }
                if (_this.selection.inside) {
                    return;
                }
                if (gridCell === _this.selection.from) {
                    _this.editing = false;
                }
                // if (heading) type = gridCell.type;
            }
            else if (!_this.selection.heading && !_this.editingCell) {
                _this.hasFocus = false;
                _this.Events.emit(_this.Events.NO_CELL);
                _this.selection.from = undefined;
                _this.selection.to = undefined;
            }
            else if (_this.selection.heading) {
                _this.selection.selected = true;
                // this.GridEvents.emit(this.GridEvents.SELECTED_ALL, {
                //   selection: this.selection,
                //   reference: '1:-1',
                //   rightClick: this.rightClick
                // });
            }
            _this.updateSelector('', gridCell ? gridCell.type : '');
        };
        this.onMouseMove = function (evt) {
            if (evt.touches) {
                if (evt.touches.length >= 2) {
                    var distance = _this.getTouchDistance(evt.touches);
                    // console.log(distance / this.pinchDistance);
                    // evt.preventDefault();
                    // evt.stopPropagation();
                    _this.applyScale(_this.pinchScale * distance / _this.pinchDistance);
                    _this.render();
                }
                return;
            }
            var gridCell = null;
            // highlight row and column
            if (_this.hoverHighlights && _this.hasFocus) {
                gridCell = _this.getGridCellByTarget(evt.target);
                _this.rows.forEach(function (row) {
                    row.setHover(gridCell && gridCell.row === row.row ? true : false);
                });
                _this.rowsColumns.forEach(function (row) {
                    row.setHover(gridCell && gridCell.row === row.row ? true : false);
                });
                _this.rowsRows.forEach(function (row) {
                    row.setHover(gridCell && gridCell.row === row.row ? true : false);
                });
                if (!gridCell)
                    return;
            }
            if (!_this.selection.selected || _this.resizing || _this.selection.inside || !_this.hasFocus)
                return;
            // console.log(e.x, e.y);
            if (!gridCell)
                gridCell = _this.getGridCellByTarget(evt.target);
            if (!gridCell)
                return;
            var type = '';
            if (gridCell instanceof GridHeadingCell_1.GridHeadingCell && _this.selection.heading) {
                if (gridCell.type === 'row') {
                    _this.selection.to = _this.gridCells[_this.selection.rowEnd][gridCell.col];
                }
                else {
                    _this.selection.to = _this.gridCells[gridCell.row][_this.gridCells[0].length - 1];
                }
                type = gridCell.type;
            }
            else {
                _this.selection.to = gridCell;
            }
            _this.updateSelector('', type);
        };
        this.onMouseUp = function (evt) {
            if (!_this.hasFocus)
                return;
            if (!_this.selection.selected || _this.resizing) {
                _this.resizing = false;
                _this.pageEl.classList.remove('resizing');
                return;
            }
            var type = '';
            if (_this.selection.heading && _this.selection.from) {
                type = _this.selection.type;
            }
            _this.selection.inside = false;
            _this.selection.selected = false;
            _this.selection.from = _this.getGridCell(_this.selection.rowFrom, _this.selection.colFrom);
            _this.selection.to = _this.getGridCell(_this.selection.rowTo, _this.selection.colTo);
            // if (this.touch) {
            //   this.scrollEl.style.setProperty('pointer-events', 'none');
            //   this.boxEl.style.setProperty('pointer-events', 'none');
            //   if (evt.target === this.boxEl) {
            //     this.setCellSelection(document.elementFromPoint(evt.clientX, evt.clientY));
            //   }
            // }
            // this.scrollEl.style.setProperty('pointer-events', 'all');
            // this.boxEl.style.setProperty('pointer-events', 'all');
            // this.GridEvents.emit(this.GridEvents.CELL_SELECTED, {
            //   rightClick: this.rightClick,
            //   selection: { ...this.selection },
            //   event: evt,
            //   reference: helpers.getCellsReference(this.selection.from.position, this.selection.to.position, reference)
            // });
            _this.emitSelected(event, type);
            _this.destroyPopper();
            if (!_this.rightClick &&
                !_this.doubleClicked &&
                !_this.historyClick &&
                _this.selection.from === _this.selection.to &&
                _this.selection.from.hasButtonOptions &&
                _this.selection.from.permission === 'rw' &&
                _this.canEdit) {
                _this.onCellUpdates();
            }
            _this.historyClick = false;
            if (_this.doubleClicked) {
                _this.doubleClicked = false;
                if (!_this.disallowSelection &&
                    !_this.rightClick &&
                    (!_this.selection.from.button || (_this.selection.from.button && _this.selection.from.button.type !== 'checkbox')))
                    _this.editing = true;
            }
            _this.rightClick = false;
            _this.selection.heading = false;
        };
        this.onKeydown = function (evt) {
            console.log(evt.key);
            // esc
            if (evt.key === 'Escape') {
                _this.dirty = false;
                if (_this.editingCell) {
                    _this.cancelCellEdit(true);
                    _this.destroyPopper();
                }
                else if (_this.editing) {
                    // this.dirty = false;
                    _this.editing = false;
                    // this.destroyPopper();
                    _this.Events.emit(_this.Events.CELL_RESET, _this.selection.last);
                }
                else {
                    _this.hasFocus = false;
                    _this.selection.last = _this.selection.from;
                    if (_this.selection.from) {
                        _this.Events.emit(_this.Events.NO_CELL);
                    }
                    _this.selection.from = undefined;
                    _this.selection.to = undefined;
                    _this.updateSelector();
                }
                return;
            }
            if (!_this.hasFocus) {
                if (evt.key === 'F2' && !evt.shiftKey) {
                    if (!_this.selection.from) {
                        _this.hasFocus = true;
                        _this.mainElRect = _this.mainEl.getBoundingClientRect();
                        _this.selection.from = _this.gridCells[0][0];
                        _this.selection.to = _this.gridCells[0][0];
                        _this.updateSelector();
                    }
                }
                return;
            }
            // ctrl is down
            if (evt.key === 'Control') {
                _this.ctrlDown = true;
                return;
            }
            // shift
            if (evt.key === 'Shift') {
                _this.shiftDown = true;
            }
            // ctrl navigation
            var moved = _this.onCtrlNavigate(evt.key);
            if (moved) {
                evt.preventDefault();
                return;
            }
            if (['Home', 'End'].indexOf(evt.key) > -1) {
                if (_this.editing)
                    return;
                _this.selectNextCell(evt.key.toLowerCase());
            }
            if (['ArrowLeft', 'ArrowUp', 'ArrowRight', 'ArrowDown'].indexOf(evt.key) > -1) {
                var direction = evt.key.replace('Arrow', '').toLowerCase();
                if (_this.editingCell && _this.editingCell.hasButtonOptions) {
                    _this.editingCell.selectOption(evt.key);
                }
                else if (_this.editing) {
                    return;
                }
                else {
                    if (_this.arrowKeyDown)
                        return;
                    _this.arrowKeyDown = true;
                    evt.preventDefault();
                    _this.selectNextCell(direction);
                }
            }
            if (['PageUp', 'PageDown'].indexOf(evt.key) > -1) {
                if (_this.editing)
                    return;
                var direction = evt.key.toLocaleLowerCase();
                // find cell that is out of view
                // let row = this.selection.from.dataset.row;
                _this.selectNextCell(direction);
                evt.preventDefault();
            }
            if (evt.key === 'Enter') {
                evt.preventDefault();
                if (_this.editingCell) {
                    _this.editingCell.selectOption('Enter');
                    return;
                }
                _this.selectNextCell(_this.shiftDown ? 'up' : 'down', evt.key);
            }
            if (evt.key === 'Tab') {
                evt.preventDefault();
                if (_this.editingCell) {
                    _this.editingCell.selectOption('Enter');
                    return;
                }
                _this.selectNextCell(_this.shiftDown ? 'left' : 'right', evt.key);
            }
            if (!_this.editing && _this.selection.from) {
                if (evt.key === 'Delete') {
                    if (_this.canEdit) {
                        // check if there are buttons
                        var gridCells = _this.getSelectedCells();
                        var isButtonValue_1 = false;
                        gridCells.forEach(function (row) {
                            row.forEach(function (gridCell) {
                                if (gridCell.hasButtonOptions)
                                    isButtonValue_1 = true;
                            });
                        });
                        if (isButtonValue_1) {
                            _this.Events.emit(_this.Events.ERROR, {
                                message: 'Cell range contains at least one button'
                            });
                            return;
                        }
                        _this.Events.emit(_this.Events.DELETE, {
                            selection: _this.selection,
                            event: evt
                        });
                    }
                    return;
                }
                if (_this.selection.from !== _this.selection.to) {
                    return;
                }
                if (evt.code === 'Space' && _this.selection.from.button && _this.selection.from.button.type === 'checkbox') {
                    _this.rowChecked = true;
                    _this.selection.keyboard = true;
                    // this.selection.from.toggleCheckbox();
                    return;
                }
                if (evt.key === 'F2' && !evt.shiftKey) {
                    if (_this.selection.from === _this.selection.to &&
                        _this.selection.from.hasButtonOptions &&
                        _this.selection.from.permission === 'rw' &&
                        _this.canEdit) {
                        _this.onCellUpdates();
                    }
                    else {
                        _this.editing = true;
                    }
                    return;
                }
                if (!_this.ctrlDown && evt.key.length === 1) {
                    if (!_this.selection.from.hasButtonOptions)
                        _this.keyValue = evt.key;
                    _this.editing = true;
                }
            }
        };
        this.onKeyup = function (evt) {
            if (evt.key === 'Control') {
                _this.ctrlDown = false;
            }
            if (evt.key === 'Shift') {
                _this.shiftDown = false;
                _this.unshift = false;
            }
            if (_this.selection.keyboard) {
                if (evt.key === 'Shift') {
                    _this.selection.from = _this.getGridCell(_this.selection.rowFrom, _this.selection.colFrom);
                    _this.selection.to = _this.getGridCell(_this.selection.rowTo, _this.selection.colTo);
                }
                _this.emitSelected(evt, _this.selection.type);
            }
            if (!_this.shiftDown) {
                _this.selection.keyboard = false;
            }
            if (_this.editing)
                return;
        };
        this.scrollTimer = null;
        this.render = function () {
            _this.offsetY = _this.mainEl.scrollTop;
            _this.offsetX = _this.mainEl.scrollLeft;
            // main cells
            var rowFragment = document.createDocumentFragment();
            for (var rowIndex = 0; rowIndex < _this.gridCells.length; rowIndex++) {
                var row = _this.rows[rowIndex];
                if (row.hidden) {
                    if (_this.mainCellsEl.contains(row.rowEl)) {
                        _this.mainCellsEl.removeChild(row.rowEl);
                    }
                    continue;
                }
                if (!_this.isVisible(row, 'y')) {
                    if (_this.mainCellsEl.contains(row.rowEl)) {
                        _this.mainCellsEl.removeChild(row.rowEl);
                    }
                    continue;
                }
                for (var colIndex = 0; colIndex < _this.gridCells[rowIndex].length; colIndex++) {
                    var gridCell = _this.gridCells[rowIndex][colIndex];
                    // if (gridCell.hidden) continue;
                    if (gridCell.hidden) {
                        if (row.rowEl.contains(gridCell.cell)) {
                            row.rowEl.removeChild(gridCell.cell);
                        }
                        continue;
                    }
                    if (_this.isVisible(gridCell, 'x')) {
                        if (!row.rowEl.contains(gridCell.cell)) {
                            if (!gridCell.init)
                                gridCell.create();
                            row.rowEl.appendChild(gridCell.cell);
                        }
                    }
                    else {
                        if (row.rowEl.contains(gridCell.cell))
                            row.rowEl.removeChild(gridCell.cell);
                    }
                }
                if (!_this.mainCellsEl.contains(row.rowEl)) {
                    rowFragment.appendChild(row.rowEl);
                    // this.mainCellsEl.appendChild(row.rowEl);
                }
            }
            _this.mainCellsEl.appendChild(rowFragment);
            if (_this.headings) {
                var commonHeadingCell_1 = function (gridCell) {
                    if (!_this.headingsCommonCellsEl.contains(gridCell.cell))
                        _this.headingsCommonCellsEl.appendChild(gridCell.cell);
                };
                _this.gridColumnHeadingCells.forEach(function (gridCell) {
                    if (gridCell.common) {
                        commonHeadingCell_1(gridCell);
                        return;
                    }
                    if (gridCell.hidden) {
                        if (_this.headingsColumnsCellsEl.contains(gridCell.cell))
                            _this.headingsColumnsCellsEl.removeChild(gridCell.cell);
                        return;
                    }
                    if (_this.isVisible(gridCell, 'y')) {
                        if (!_this.headingsColumnsCellsEl.contains(gridCell.cell))
                            _this.headingsColumnsCellsEl.appendChild(gridCell.cell);
                    }
                    else {
                        if (_this.headingsColumnsCellsEl.contains(gridCell.cell))
                            _this.headingsColumnsCellsEl.removeChild(gridCell.cell);
                    }
                });
                _this.gridRowHeadingCells.forEach(function (gridCell) {
                    if (gridCell.common) {
                        commonHeadingCell_1(gridCell);
                        return;
                    }
                    if (gridCell.hidden) {
                        if (_this.headingsRowsCellsEl.contains(gridCell.cell))
                            _this.headingsRowsCellsEl.removeChild(gridCell.cell);
                        return;
                    }
                    if (_this.isVisible(gridCell, 'x')) {
                        if (!_this.headingsRowsCellsEl.contains(gridCell.cell))
                            _this.headingsRowsCellsEl.appendChild(gridCell.cell);
                    }
                    else {
                        if (_this.headingsRowsCellsEl.contains(gridCell.cell))
                            _this.headingsRowsCellsEl.removeChild(gridCell.cell);
                    }
                });
                // this.gridHeadingCells.forEach(gridCell => {
                //   if (gridCell.common) {
                //     if (!this.headingsCommonCellsEl.contains(gridCell.cell))
                //       this.headingsCommonCellsEl.appendChild(gridCell.cell);
                //   } else if (gridCell.type === 'row') {
                //   } else if (gridCell.type === 'col') {
                //   }
                // });
                // this.headingsColumnCells.forEach((cell) => {
                // });
                // this.headingsRowCells.forEach((cell) => {
                // });
                // this.headingsCommonCells.forEach((cell) => {
                //   if (!this.headingsCommonCellsEl.contains(cell)) this.headingsCommonCellsEl.appendChild(cell);
                // });
            }
            // frozen corner cells
            for (var rowIndex = 0; rowIndex < _this.freezeRow; rowIndex++) {
                for (var colIndex = 0; colIndex < _this.freezeCol; colIndex++) {
                    var gridCell = _this.commonCells[rowIndex][colIndex];
                    if (gridCell && !_this.commonCellsEl.contains(gridCell.cell)) {
                        if (!gridCell.init)
                            gridCell.create();
                        _this.commonCellsEl.appendChild(gridCell.cell);
                        // this.commonCellsEl.appendChild(gridCell.cellBorders);
                    }
                }
            }
            // frozen columns
            for (var rowIndex = _this.freezeRow; rowIndex < _this.gridCells.length; rowIndex++) {
                var row = _this.rowsColumns[rowIndex];
                if (!row)
                    continue;
                if (row.hidden || !_this.isVisible(row, 'y')) {
                    if (_this.columnsCellsEl.contains(row.rowEl))
                        _this.columnsCellsEl.removeChild(row.rowEl);
                    continue;
                }
                for (var colIndex = 0; colIndex < _this.freezeCol; colIndex++) {
                    var gridCell = _this.columnCells[rowIndex][colIndex];
                    if (_this.isVisible(gridCell, 'y')) {
                        if (!row.rowEl.contains(gridCell.cell)) {
                            if (!gridCell.init)
                                gridCell.create();
                            row.rowEl.appendChild(gridCell.cell);
                        }
                    }
                    else {
                        if (row.rowEl.contains(gridCell.cell)) {
                            row.rowEl.removeChild(gridCell.cell);
                        }
                    }
                }
                if (!_this.columnsCellsEl.contains(row.rowEl)) {
                    _this.columnsCellsEl.appendChild(row.rowEl);
                }
            }
            // frozen rows
            var frozeRowFragment = document.createDocumentFragment();
            for (var rowIndex = 0; rowIndex < _this.freezeRow; rowIndex++) {
                var row = _this.rowsRows[rowIndex];
                if (!_this.rowsCellsEl.contains(row.rowEl)) {
                    // this.rowsCellsEl.appendChild(row.rowEl);
                    frozeRowFragment.appendChild(row.rowEl);
                }
                for (var colIndex = 0; colIndex < _this.gridCells[rowIndex].length; colIndex++) {
                    var gridCell = _this.gridCells[rowIndex][colIndex];
                    if (gridCell.hidden) {
                        if (row.rowEl.contains(gridCell.cell)) {
                            if (!gridCell.init)
                                gridCell.create();
                            row.rowEl.removeChild(gridCell.cell);
                        }
                        continue;
                    }
                    if (_this.isVisible(gridCell, 'x')) {
                        if (!row.rowEl.contains(gridCell.cell)) {
                            if (!gridCell.init)
                                gridCell.create();
                            row.rowEl.appendChild(gridCell.cell);
                            // this.rowsCellsEl.appendChild(gridCell.cellBorders);
                        }
                    }
                    else {
                        if (row.rowEl.contains(gridCell.cell)) {
                            row.rowEl.removeChild(gridCell.cell);
                            // this.rowsCellsEl.removeChild(gridCell.cellBorders);
                        }
                    }
                }
            }
            _this.rowsCellsEl.appendChild(frozeRowFragment);
            _this.rendering = false;
        };
        var pageEl = typeof this.ref === 'string' ? document.getElementById(this.ref) : this.ref;
        if (!pageEl)
            throw new Error('Page element not found');
        this.pageEl = pageEl;
        var parser = new ua_parser_js_1.UAParser();
        var parseResult = parser.getResult();
        this.touch = parseResult.device && (parseResult.device.type === 'tablet' || parseResult.device.type === 'mobile');
        this.Events = new GridEvents_1.default();
        if (options) {
            Object.keys(options).forEach(function (key) {
                _this[key] = options[key];
            });
        }
        this.Canvas = new GridCanvas_1.default(this.pageEl);
        this.pageElRect = this.pageEl.getBoundingClientRect();
        if (this.touch) {
            this.pageEl.classList.add('touch');
            // this.hammer = new Hammer.default(this.pageEl);
            // let pinch: any = new Hammer.Pinch();
            // let scale: number = 1;
            // this.hammer.add([pinch]);
            // this.hammer.on('pinchstart', (evt: any) => {
            //   scale = this.scale;
            // });
            // this.hammer.on('pinchmove', (evt: any) => {
            //   console.log('hammer', evt.scale);
            //   // this.applyScale(scale * evt.scale);
            //   // this.render();
            //   // evt.stopPropgation();
            // });
            // this.hammer.on('pinchend', (evt: any) => {
            // });      
        }
        // this.init();
    }
    Grid.prototype.image = function (options) {
        this.Canvas.gridCells = this.gridCells;
        this.Canvas.gridWidth = this.width;
        this.Canvas.gridHeight = this.height;
        return this.Canvas.render(options);
    };
    Grid.prototype.init = function () {
        this.reset();
        this.dom();
        this.createListeners();
    };
    Grid.prototype.reset = function () {
        this.destroyPopper();
        this.big = false;
        this.width = 0;
        this.height = 0;
        this.rows = [];
        this.rowCells = [];
        this.gridCells = [];
        // this.gridHeadingCells = [];
        this.gridColumnHeadingCells = [];
        this.gridRowHeadingCells = [];
        this.freezeHeight = 0;
        this.freezeWidth = 0;
        this.scale = 1;
        this.offsetX = 0;
        this.offsetY = 0;
        this.selection.from = undefined;
        this.selection.to = undefined;
        this.content = [];
        if (this.ps)
            this.ps.destroy();
        if (this.initialized)
            this.pageEl.innerHTML = '';
        this.initialized = true;
    };
    Grid.prototype.destroy = function () {
        if (!this.initialized)
            return;
        // if (this.hammer) {
        //   this.hammer.destroy();
        // }
        this.destroyListeners();
        this.reset();
    };
    Grid.prototype.setOption = function (key, value) {
        if (this[key] === undefined)
            return;
        this[key] = value;
    };
    Grid.prototype.setHiddenColumns = function (columns) {
        this.hiddenColumns = columns;
    };
    Grid.prototype.updateSorting = function (sorting) {
        return;
    };
    // public setFilters(filters?: any): void {
    //   return;
    // }
    Grid.prototype.updateButtons = function (buttons) {
        var _this = this;
        // clear any current buttons
        if (this.buttons) {
            for (var rowIndex = 0; rowIndex < this.buttons.length; rowIndex++) {
                var row = this.buttons[rowIndex];
                if (!row)
                    continue;
                for (var colIndex = 0; colIndex < row.length; colIndex++) {
                    var button = row[colIndex];
                    if (!button)
                        continue;
                    this.applyCellUpdate(rowIndex, colIndex, 'button', null);
                }
            }
        }
        this.buttons = [];
        if (!buttons) {
            return;
        }
        buttons.forEach(function (button) {
            var range = helpers.cellRange(button.range, _this.content.length, _this.content[0].length);
            if (range.to.row < 0)
                range.to.row = _this.content.length;
            if (range.to.col < 0)
                range.to.col = _this.content[0].length;
            for (var rowIndex = range.from.row; rowIndex <= range.to.row; rowIndex++) {
                for (var colIndex = range.from.col; colIndex <= range.to.col; colIndex++) {
                    if (!_this.buttons[rowIndex])
                        _this.buttons[rowIndex] = [];
                    _this.buttons[rowIndex][colIndex] = button;
                    _this.applyCellUpdate(rowIndex, colIndex, 'button', button);
                }
            }
        });
    };
    Grid.prototype.updateStyles = function (styles) {
        // clear any styles
        if (this.styles) {
            for (var rowIndex = 0; rowIndex < this.styles.length; rowIndex++) {
                var row = this.styles[rowIndex];
                if (!row)
                    continue;
                for (var colIndex = 0; colIndex < row.length; colIndex++) {
                    var style = row[colIndex];
                    if (!style)
                        continue;
                    this.applyCellUpdate(rowIndex, colIndex, 'style', null);
                }
            }
        }
        this.styles = [];
        if (!styles || !styles.length) {
            return;
        }
        this.styles = styles;
        this.applyStyles();
    };
    Grid.prototype.updateRanges = function (ranges, userId) {
        if (userId === void 0) { userId = 0; }
        if (this.accessRanges) {
            for (var rowIndex = 0; rowIndex < this.accessRanges.length; rowIndex++) {
                var row = this.accessRanges[rowIndex];
                if (!row)
                    continue;
                for (var colIndex = 0; colIndex < row.length; colIndex++) {
                    var gridCell = this.getGridCell(rowIndex, colIndex, true);
                    if (!gridCell)
                        continue;
                    gridCell.setPermission('rw');
                    if (this.columnCells[gridCell.row] && this.columnCells[gridCell.row][gridCell.col]) {
                        this.columnCells[gridCell.row][gridCell.col].setPermission('rw');
                    }
                    if (this.commonCells[gridCell.row] && this.commonCells[gridCell.row][gridCell.col]) {
                        this.commonCells[gridCell.row][gridCell.col].setPermission('rw');
                    }
                }
            }
        }
        this.accessRanges = [];
        if (!ranges || !ranges.length)
            return;
        // let offset = this.Grid.Settings.headings ? 1 : 0;
        var rowsToUpdate = [];
        for (var i = 0; i < ranges.length; i++) {
            var range = ranges[i];
            var rowEnd = void 0;
            var colEnd = void 0;
            if (range.range) {
                rowEnd = range.range.to.row;
                colEnd = range.range.to.col;
                if (rowEnd === -1) {
                    rowEnd = this.content.length - 1;
                }
                if (colEnd === -1) {
                    colEnd = this.content[0].length - 1;
                }
            }
            if (range instanceof __1.PermissionRange && this.content.length) {
                var userRight = range.getPermission(userId || 0) || (userId ? 'rw' : 'no');
                var rowEnd_1 = range.rowEnd;
                var colEnd_1 = range.colEnd;
                if (rowEnd_1 === -1) {
                    rowEnd_1 = this.content.length - 1;
                }
                if (colEnd_1 === -1) {
                    colEnd_1 = this.content[0].length - 1;
                }
                for (var i_1 = range.rowStart; i_1 <= rowEnd_1; i_1++) {
                    rowsToUpdate.push(i_1);
                    for (var k = range.colStart; k <= colEnd_1; k++) {
                        if (!this.accessRanges[i_1]) {
                            this.accessRanges[i_1] = [];
                        }
                        if (!this.accessRanges[i_1][k]) {
                            this.accessRanges[i_1][k] = [];
                        }
                        if (userRight) {
                            this.accessRanges[i_1][k] = userRight;
                            this.applyCellUpdate(i_1, k, 'permission', userRight);
                        }
                    }
                }
            }
        }
        if (rowsToUpdate.length) {
            this.updateSoftMerges(rowsToUpdate);
        }
    };
    Grid.prototype.applyStyles = function () {
        for (var rowIndex = 0; rowIndex < this.styles.length; rowIndex++) {
            var row = this.styles[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var style = this.styles[rowIndex][colIndex];
                if (!style)
                    continue;
                this.styles[rowIndex][colIndex] = style;
                this.applyCellUpdate(rowIndex, colIndex, 'style', style);
            }
        }
    };
    Grid.prototype.applyButtons = function () {
        for (var rowIndex = 0; rowIndex < this.buttons.length; rowIndex++) {
            var row = this.buttons[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var style = this.buttons[rowIndex][colIndex];
                if (!style)
                    continue;
                this.buttons[rowIndex][colIndex] = style;
                this.applyCellUpdate(rowIndex, colIndex, 'button', style);
            }
        }
    };
    Grid.prototype.applyRanges = function () {
        for (var rowIndex = 0; rowIndex < this.accessRanges.length; rowIndex++) {
            var row = this.accessRanges[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; row++) {
                var userRight = this.accessRanges[rowIndex][colIndex];
                if (!userRight)
                    continue;
                this.applyCellUpdate(rowIndex, colIndex, 'permission', userRight);
            }
        }
    };
    Grid.prototype.applyCellUpdate = function (rowIndex, colIndex, which, value) {
        var gridCell = this.getGridCell(rowIndex, colIndex, true);
        if (!gridCell)
            return;
        // if (!gridCell.init) return;
        if (which === 'sorting')
            gridCell.setSorting(value);
        if (which === 'permission')
            gridCell.setPermission(value);
        if (which === 'style')
            gridCell.setStyle(value);
        if (which === 'button')
            gridCell.setButton(value);
        if (which === 'found')
            gridCell.setFound(value);
        if (this.columnCells[gridCell.row] && this.columnCells[gridCell.row][gridCell.col]) {
            if (which === 'sorting')
                this.columnCells[gridCell.row][gridCell.col].setSorting(value);
            if (which === 'permission')
                this.columnCells[gridCell.row][gridCell.col].setPermission(value);
            if (which === 'style')
                this.columnCells[gridCell.row][gridCell.col].setStyle(value);
            if (which === 'button')
                this.columnCells[gridCell.row][gridCell.col].setButton(value);
            if (which === 'found')
                this.columnCells[gridCell.row][gridCell.col].setFound(value);
        }
        if (this.commonCells[gridCell.row] && this.commonCells[gridCell.row][gridCell.col]) {
            if (which === 'sorting')
                this.commonCells[gridCell.row][gridCell.col].setSorting(value);
            if (which === 'permission')
                this.commonCells[gridCell.row][gridCell.col].setPermission(value);
            if (which === 'style')
                this.commonCells[gridCell.row][gridCell.col].setStyle(value);
            if (which === 'button')
                this.commonCells[gridCell.row][gridCell.col].setButton(value);
            if (which === 'found')
                this.commonCells[gridCell.row][gridCell.col].setFound(value);
        }
    };
    /**
     * Set grid content from Page.
     * Resets grid state and settings then create all new grid cell references. Use when first loading a Page or the Page size has changed.
     * @param {IGridPageContent} content - Page content.
     * @param {IGridState} state - Grid state. Sorting, Filters, Hidden Columns
     * @returns {void}
     */
    Grid.prototype.setContent = function (content, state) {
        this.init();
        // this.init();
        if (state) {
            this.state = state;
        }
        this.originalContent = content;
        this.content = GridContent_1.default.content = content;
        var zIndexRow = this.content.length;
        var zIndexCol = 0;
        var total = this.content.length * this.content[0].length;
        if (!total) {
            this.pageEl.innerText = 'No rows match filter';
            return;
        }
        if (total > this.cellLimit)
            this.big = true;
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            // this.content.forEach((row: any[], rowIndex: number) => {
            this.width = 0;
            // this.cells[rowIndex] = [];
            this.gridCells[rowIndex] = [];
            zIndexCol = row.length;
            var mergedCells = {
                cells: [],
                last: []
            };
            // let hiddenRow: boolean = this.rowsToHide.indexOf(rowIndex) > -1;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                // row.forEach((col: any, colIndex: number) => {
                // if (this.hiddenColumns.indexOf(colIndex) > -1) {
                //   return;
                // }
                var cellAbove = this.content[rowIndex - 1]
                    ? content[rowIndex - 1][colIndex]
                    : undefined;
                var cellBelow = this.content[rowIndex + 1]
                    ? content[rowIndex + 1][colIndex]
                    : undefined;
                if (cellAbove && cellAbove.style.bbs !== 'none') {
                    col.style.tbs = cellAbove.style.bbs;
                    col.style.tbc = cellAbove.style.bbc;
                    col.style.tbw = cellAbove.style.bbw;
                }
                if (cellBelow && cellBelow.style.tbs !== 'none') {
                    col.style.bbs = cellBelow.style.tbs;
                    col.style.bbc = cellBelow.style.tbc;
                    col.style.bbw = cellBelow.style.tbw;
                }
                // let hidden: boolean = this.state.hiddenColumns.indexOf(colIndex) > -1;
                var params = {
                    row: row,
                    content: col,
                    rowIndex: rowIndex,
                    colIndex: colIndex,
                    zIndexCol: zIndexCol,
                    zIndexRow: zIndexRow,
                    x: this.width,
                    y: this.height,
                    mergeEl: this.mergeEl,
                    total: total,
                    mergedCells: mergedCells
                };
                var gridCell = new GridCell_1.GridCell(params);
                if (this.isVisible(gridCell)) {
                    gridCell.create();
                }
                // gridCell.hidden = hidden;
                // this.cells[rowIndex].push(gridCell.cell);
                this.gridCells[rowIndex].push(gridCell);
                if (!rowIndex) {
                    // this.gridHeadingCells.push(new GridHeadingCell('row', params));
                    var gridHeadingCell = new GridHeadingCell_1.GridHeadingCell('row', params);
                    // gridHeadingCell.hidden = hidden;
                    this.gridRowHeadingCells.push(gridHeadingCell);
                }
                if (!colIndex) {
                    // params.hidden = hiddenRow;
                    // this.gridHeadingCells.push(new GridHeadingCell('col', params));
                    this.gridColumnHeadingCells.push(new GridHeadingCell_1.GridHeadingCell('col', params));
                }
                // if (!hidden) {
                this.width += gridCell.dataset.width;
                // }
                zIndexCol--;
                if (!rowIndex && !colIndex) {
                    // this.pageEl.style.setProperty('background-color', '#' + col.style['background-color']);
                }
            }
            // rows
            // let hiddenRow: boolean = this.rowsToHide.indexOf(rowIndex) > -1;
            var gridRow = new GridRow_1.GridRow({
                y: this.height,
                rowIndex: rowIndex,
                zIndexRow: zIndexRow,
                height: this.gridCells[rowIndex][0].dataset.height
            });
            this.rows.push(gridRow);
            // if (!hiddenRow) {
            this.height += Math.round(parseFloat(row[0].style.height)); // - this.borders.widths[row[0].style.bbw];
            // }
            zIndexRow--;
            // apply soft merges
            // this.gridCells[rowIndex].forEach(cell => {
            //   cell.updateSoftMerge();
            // });
        }
        // this.applyState();
        // this.width++;
        // this.height++;
        this.initialized = true;
    };
    /**
     * Update grid cell values.
     * Use when there is new content from a Page.
     * @param content - Cells that have changed.
     */
    Grid.prototype.setDeltaContent = function (content) {
        var _this = this;
        if (!content || !content.length) {
            return;
        }
        var gridCells = [];
        content.forEach(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach(function (col, colIndex) {
                if (!col)
                    return;
                var gridCell = _this.getGridCell(rowIndex, colIndex);
                if (!gridCell)
                    return;
                gridCell.update(col);
                if (_this.commonCells[gridCell.row] && _this.commonCells[gridCell.row][gridCell.col]) {
                    _this.commonCells[gridCell.row][gridCell.col].update(col);
                    gridCells.push(_this.commonCells[gridCell.row][gridCell.col]);
                }
                else if (_this.columnCells[gridCell.row] && _this.columnCells[gridCell.row][gridCell.col]) {
                    _this.columnCells[gridCell.row][gridCell.col].update(col);
                    gridCells.push(_this.columnCells[gridCell.row][gridCell.col]);
                }
                else {
                    gridCells.push(gridCell);
                }
                _this.originalContent[rowIndex][colIndex] = col;
            });
            // soft merges
            _this.gridCells[rowIndex].forEach(function (cell) {
                cell.updateSoftMerge();
            });
        });
        // update sorting and filters
        this.applyState();
        // highlight changes
        if (this.highlights) {
            gridCells.forEach(function (gridCell) {
                gridCell.highlight();
            });
        }
        this.render();
    };
    /**
     * Sort, Filter and/or show/hide Columns.
     * @param state
     */
    Grid.prototype.updateState = function (state) {
        this.state = state;
        if (!this.initialized)
            return;
        this.applyState(true);
        // this.updateSelector();
        this.setFreezeSizes();
        // this.applyScale();
        this.render();
        if (this.ps)
            this.ps.update(this.fit, this.scale);
    };
    Grid.prototype.updateTrackingData = function (trackingData) {
        this.trackingData = trackingData;
        this.updateHistoryCells();
    };
    /**
     * Freeze rows/columns.
     * Use when a Page is ready and only after setContent() has been applied.
     * @param row {number}
     * @param col {number}
     * @param headings {boolean}
     */
    Grid.prototype.setFreeze = function (row, col, headings) {
        var _this = this;
        this.freezeRow = row;
        this.freezeCol = col;
        this.commonCells = [];
        this.columnCells = [];
        // this.rowCells = [];
        this.rowsColumns = [];
        this.rowsRows = [];
        this.rowsCellsEl.innerHTML = '';
        this.columnsCellsEl.innerHTML = '';
        this.commonCellsEl.innerHTML = '';
        this.headingsColumnsCellsEl.innerHTML = '';
        this.headingsCommonCellsEl.innerHTML = '';
        this.headingsRowsCellsEl.innerHTML = '';
        this.freezeHeight = 0;
        this.freezeWidth = 0;
        // if (headings) {
        //   this.freezeRow += 1;
        //   this.freezeWidth += 1;
        // }
        if (typeof headings === 'boolean')
            this.headings = headings;
        headings = this.headings;
        if (!this.initialized ||
            !this.content.length ||
            !this.content[this.freezeRow] ||
            !this.content[this.freezeRow][this.freezeCol]) {
            this.freezeRow = 0;
            this.freezeCol = 0;
            return;
        }
        if (headings) {
            this.gridColumnHeadingCells.forEach(function (gridCell) {
                if (gridCell.dataset.row < _this.freezeRow) {
                    gridCell.setFreeze(true);
                }
                else {
                    gridCell.setFreeze(false);
                }
            });
            this.gridRowHeadingCells.forEach(function (gridCell) {
                if (gridCell.dataset.col < _this.freezeCol) {
                    gridCell.setFreeze(true);
                }
                else {
                    gridCell.setFreeze(false);
                }
            });
        }
        var zIndexRow = this.gridCells.length;
        var zIndexCol = this.gridCells[0].length;
        for (var rowIndex = 0; rowIndex < this.gridCells.length; rowIndex++) {
            var row_1 = this.rows[rowIndex];
            var gridRow = new GridRow_1.GridRow({
                y: row_1.dataset.y,
                zIndexRow: row_1.dataset.zIndexRow,
                rowIndex: rowIndex,
                height: this.gridCells[rowIndex][0].dataset.height
            });
            if (rowIndex >= this.freezeRow) {
                this.rowsColumns.push(gridRow);
            }
            else if (rowIndex < this.freezeRow) {
                this.rowsRows.push(gridRow);
                this.rowsColumns.push(row_1);
            }
            else {
                this.rowsColumns.push(row_1);
            }
            for (var colIndex = 0; colIndex < this.gridCells[rowIndex].length; colIndex++) {
                var gridCell = this.gridCells[rowIndex][colIndex];
                gridCell.setFreeze('');
                if (rowIndex < this.freezeRow && colIndex < this.freezeCol) {
                    // create new cell
                    var newGridCell = new GridCell_1.GridCell(Object.assign({}, gridCell.params, { zIndexCol: zIndexRow * zIndexCol }));
                    newGridCell.setFreeze('common', this.gridCells[rowIndex + 1][colIndex], this.gridCells[rowIndex][colIndex + 1]);
                    zIndexCol--;
                    if (!this.commonCells[rowIndex]) {
                        this.commonCells[rowIndex] = [];
                    }
                    this.commonCells[rowIndex][colIndex] = newGridCell;
                }
                else if (rowIndex === this.freezeRow - 1) {
                    gridCell.setFreeze('row', this.gridCells[rowIndex + 1][colIndex]);
                }
                else if (colIndex < this.freezeCol) {
                    var newGridCell = new GridCell_1.GridCell(gridCell.params);
                    newGridCell.setFreeze('col', this.gridCells[rowIndex][colIndex + 1]);
                    if (!this.columnCells[rowIndex]) {
                        this.columnCells[rowIndex] = [];
                    }
                    this.columnCells[rowIndex][colIndex] = newGridCell;
                }
                if (!rowIndex && colIndex < this.freezeCol) {
                    // let n: string = clone.dataset.width || '0';
                    this.freezeWidth += gridCell.dataset.width;
                }
            }
            zIndexRow--;
            if (rowIndex < this.freezeRow) {
                // let n: string = this.gridCells[rowIndex][0].dataset.height || '0';
                this.freezeHeight += this.gridCells[rowIndex][0].dataset.height;
            }
        }
    };
    Grid.prototype.updateHeadings = function (headings) {
        var _this = this;
        this.headings = headings;
        this.applyHeadingCellsFreeze();
        if (!headings) {
            if (this.headingsTimer) {
                clearTimeout(this.headingsTimer);
            }
            this.headingsTimer = setTimeout(function () {
                _this.headingsColumnsCellsEl.innerHTML = '';
                _this.headingsCommonCellsEl.innerHTML = '';
                _this.headingsRowsCellsEl.innerHTML = '';
                _this.setFreezeSizes();
                _this.applyScale();
            }, 300);
        }
        else {
            this.setFreezeSizes();
            this.applyScale();
            this.render();
        }
        // this.updateFreeze(this.freezeRow, this.freezeCol, headings);
    };
    Grid.prototype.updateFreeze = function (row, col, headings) {
        this.setFreeze(row, col, headings);
        this.setElementSizes('height', this.freezeHeight);
        this.setElementSizes('width', this.freezeWidth);
        this.applyScale();
        this.setFreezeSizes();
        this.applyRanges();
        this.applyStyles();
        this.applyButtons();
        this.updateSoftMerges();
        if (this.ps)
            this.ps.update(this.fit, this.scale);
        this.render();
    };
    // public setFit(fit: string): void {
    //   this.fit = fit;
    // }
    Grid.prototype.updateFit = function (fit) {
        this.fit = fit;
        this.applyScale();
        this.render();
    };
    Grid.prototype.updateScale = function (n) {
        this.applyScale(n);
        this.render();
    };
    Grid.prototype.setContrast = function (which) {
        this.contrast = which;
        this.pageEl.classList.remove('light', 'dark');
        this.pageEl.classList.add(this.contrast);
    };
    Grid.prototype.getGridCell = function (rowIndex, colIndex, original) {
        if (rowIndex < 0) {
            rowIndex = this.gridCells.length - 1;
        }
        if (colIndex < 0) {
            colIndex = this.gridCells[0].length - 1;
        }
        if (!this.gridCells[rowIndex] || !this.gridCells[rowIndex][colIndex])
            return null;
        if (original) {
            return this.gridCells[rowIndex][colIndex];
        }
        if (this.commonCells[rowIndex] && this.commonCells[rowIndex][colIndex]) {
            return this.commonCells[rowIndex][colIndex];
        }
        else if (this.columnCells[rowIndex] && this.columnCells[rowIndex][colIndex]) {
            return this.columnCells[rowIndex][colIndex];
        }
        return this.gridCells[rowIndex][colIndex];
    };
    Grid.prototype.getContentHtml = function (type) {
        var only = type === 'symphony' ? helpers.getSymphonyStyles() : [];
        var html = "<table style=\"border-collapse: collapse;\">";
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            if (this.rowsToHide.indexOf(row[0].index.row) > -1)
                continue;
            if (this.selection.from && this.selection.to) {
                if (row[0].index.row < this.selection.from.row || row[0].index.row > this.selection.to.row)
                    continue;
            }
            html += "<tr>";
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                if (this.state.hiddenColumns.indexOf(colIndex) > -1)
                    continue;
                if (this.selection.from && this.selection.to) {
                    if (colIndex < this.selection.from.col || colIndex > this.selection.to.col)
                        continue;
                }
                var cell = row[colIndex];
                var gridCell = this.getGridCell(cell.index.row, colIndex);
                var styles = gridCell ? gridCell.getFlattenStyles(only) : '';
                html += "<td style=\"" + styles + "\"><div>" + (cell.formatted_value || cell.value || '&nbsp;') + "</div></td>";
            }
            html += "</tr>";
        }
        html += "</table>";
        return html;
    };
    Grid.prototype.getSelectedCells = function () {
        var cells = [];
        if (!this.selection.from || !this.selection.to)
            return cells;
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            if (this.rowsToHide.indexOf(row[0].index.row) > -1)
                continue;
            if (row[0].index.row < this.selection.from.row || row[0].index.row > this.selection.to.row)
                continue;
            var cellRow = [];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                if (this.state.hiddenColumns.indexOf(colIndex) > -1)
                    continue;
                if (colIndex < this.selection.from.col || colIndex > this.selection.to.col)
                    continue;
                var cell = row[colIndex];
                var gridCell = this.getGridCell(cell.index.row, colIndex);
                if (gridCell)
                    cellRow.push(gridCell);
            }
            cells.push(cellRow);
        }
        return cells;
    };
    Grid.prototype.find = function (query) {
        this.clearFound();
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                var regex = new RegExp(query, 'ig');
                if (!regex.test("" + col.value) && !regex.test("" + col.formatted_value)) {
                    continue;
                }
                if (!this.found[rowIndex]) {
                    this.found[rowIndex] = [];
                }
                this.found[col.index.row][colIndex] = col;
                this.applyCellUpdate(col.index.row, col.index.col, 'found', true);
            }
        }
    };
    Grid.prototype.updateSize = function () {
        this.applyScale();
        this.render();
    };
    /**
     * Display the grid.
     * Use after setContent().
     * @param scale
     */
    Grid.prototype.update = function (scale) {
        this.domHeadings();
        console.log('domHeadings', performance.now());
        // this.updateSoftMerges();
        // console.log('softMerges', performance.now());
        // this.updateSize();
        this.applyState(true);
        console.log('state', performance.now());
        // this.applyScale();
        // resize elements
        this.setFreezeSizes();
        console.log('freeze', performance.now());
        // clear history
        this.clearTrackingData();
        console.log('tracking', performance.now());
        if (this.ps)
            this.ps.update(this.fit, this.scale);
        this.updateHistoryCells();
        console.log('history', performance.now());
        this.updateSelector();
        console.log('selector', performance.now());
        this.render();
        console.log('render', performance.now());
    };
    Grid.prototype.clearFound = function () {
        if (this.found && this.found.length) {
            for (var rowIndex = 0; rowIndex < this.found.length; rowIndex++) {
                var row = this.found[rowIndex];
                if (!row)
                    continue;
                for (var colIndex = 0; colIndex < row.length; colIndex++) {
                    var found = row[colIndex];
                    if (!found)
                        continue;
                    this.applyCellUpdate(rowIndex, colIndex, 'found', null);
                }
            }
        }
    };
    Grid.prototype.clearTrackingData = function () {
        this.trackingData = [];
        for (var rowIndex = 0; rowIndex < this.gridCells.length; rowIndex++) {
            for (var colIndex = 0; colIndex < this.gridCells[rowIndex].length; colIndex++) {
                var cell = this.gridCells[rowIndex][colIndex];
                cell.history(false);
                if (this.columnCells[rowIndex] && this.columnCells[rowIndex][colIndex]) {
                    this.columnCells[rowIndex][colIndex].history(false);
                }
                if (this.commonCells[rowIndex] && this.commonCells[rowIndex][colIndex]) {
                    this.commonCells[rowIndex][colIndex].history(false);
                }
            }
        }
    };
    Grid.prototype.hideSelector = function () {
        this.clearSelector();
    };
    Grid.prototype.clearSelector = function () {
        this.selection.from = undefined;
        this.selection.to = undefined;
        this.updateSelector();
    };
    Grid.prototype.setSelector = function (from, to) {
        var cellFrom = this.getGridCell(from.row, from.col);
        var cellTo = this.getGridCell(to.row, to.col);
        if (cellFrom && cellTo) {
            this.selection.from = cellFrom;
            this.selection.to = cellTo;
            this.updateSelector();
        }
    };
    Grid.prototype.setSelectorByRef = function (str) {
        var range = helpers.cellRange(str, this.gridCells.length, this.gridCells[0].length);
        // let cellFrom = this.content[range.from.row][range.from.col];
        // let cellTo = this.content[range.to.row][range.to.col];
        var from = this.getGridCell(range.from.row, range.from.col);
        var to = this.getGridCell(range.to.row, range.to.col);
        if (!from || !to)
            return;
        this.selection.from = from;
        this.selection.to = to;
        this.updateSelector();
    };
    Grid.prototype.applyHeadingCellsFreeze = function () {
        var _this = this;
        if (!this.headings)
            return;
        this.gridColumnHeadingCells.forEach(function (gridCell) {
            if (gridCell.dataset.row < _this.freezeRow) {
                gridCell.setFreeze(true);
            }
            else {
                gridCell.setFreeze(false);
            }
        });
        this.gridRowHeadingCells.forEach(function (gridCell) {
            if (gridCell.dataset.col < _this.freezeCol) {
                gridCell.setFreeze(true);
            }
            else {
                gridCell.setFreeze(false);
            }
        });
    };
    Grid.prototype.setElementSizes = function (which, size) {
        // top, height
        if (which === 'height') {
            this.headingsCommonEl.style.setProperty('height', 21 + size + "px");
            this.headingsColumnsEl.style.setProperty('top', 20 + "px");
            this.headingsRowsEl.style.setProperty('height', 20 + "px");
            this.mainEl.style.setProperty('top', size + "px");
            this.mainCellsEl.style.setProperty('top', "-" + size + "px");
            this.rowsEl.style.setProperty('height', size + 1 + "px");
            this.commonEl.style.setProperty('height', size + 1 + "px");
            this.selectorMainContainer.style.setProperty('top', size + "px");
            this.selectorMainContainerPosition.style.setProperty('top', "-" + size + "px");
            this.selectorColumnsContainer.style.setProperty('top', size + "px");
            this.selectorColumnsContainerPosition.style.setProperty('top', "-" + size + "px");
            this.selectorRowsContainer.style.setProperty('height', size + "px");
            this.selectorCommonContainer.style.setProperty('height', size + "px");
        }
        // left, width
        if (which === 'width') {
            this.headingsCommonEl.style.setProperty('width', 41 + size + "px");
            this.headingsColumnsEl.style.setProperty('width', 40 + "px");
            this.headingsRowsEl.style.setProperty('left', 40 + "px");
            this.mainEl.style.setProperty('left', size + "px");
            this.mainCellsEl.style.setProperty('left', "-" + size + "px");
            this.columnsEl.style.setProperty('width', size + 1 + "px");
            this.commonEl.style.setProperty('width', size + 1 + "px");
            this.selectorMainContainer.style.setProperty('left', size + "px");
            this.selectorMainContainerPosition.style.setProperty('left', "-" + size + "px");
            this.selectorRowsContainer.style.setProperty('left', size + "px");
            this.selectorRowsContainerPosition.style.setProperty('left', "-" + size + "px");
            this.selectorColumnsContainer.style.setProperty('width', size + "px");
            this.selectorCommonContainer.style.setProperty('width', size + "px");
        }
    };
    Grid.prototype.setFreezeSizes = function (update) {
        this.setElementSizes('height', this.freezeHeight);
        this.setElementSizes('width', this.freezeWidth);
        // this.wrapperEl.style.setProperty('left', `${this.cellsOffsetX}px`);
        // this.wrapperEl.style.setProperty('top', `${this.cellsOffsetY}px`);
        // this.selectorWrapper.style.setProperty('left', `${this.cellsOffsetX}px`);
        // this.selectorWrapper.style.setProperty('top', `${this.cellsOffsetY}px`);
        this.mainElRect = this.mainEl.getBoundingClientRect();
        var boxes = this.pageEl.getElementsByClassName('grid-box');
        for (var i = 0; i < boxes.length; i++) {
            var nX = this.width;
            var nY = this.height;
            if (boxes[i].classList.contains('grid-box-main')) {
                nX -= this.freezeWidth;
                nY -= this.freezeHeight;
            }
            if (this.fit !== 'contain') {
                if (this.pageEl.clientWidth < this.width && this.fit !== 'width') {
                    nY += 10;
                }
                if (this.pageEl.clientHeight < this.height && this.fit !== 'height') {
                    nX += 10;
                }
            }
            if (boxes[i].parentNode.classList.contains('grid-rows')) {
                nY = 20;
            }
            if (boxes[i].parentNode.classList.contains('grid-columns')) {
                nX = 40;
            }
            boxes[i].style.setProperty('width', nX + "px");
            boxes[i].style.setProperty('height', nY + "px");
        }
        if (!update) {
            this.rowsEl.scrollLeft = 0;
            this.columnsEl.scrollTop = 0;
            this.mainEl.scrollTop = 0;
            this.mainEl.scrollLeft = 0;
            this.headingsColumnsEl.scrollTop = 0;
            this.headingsRowsEl.scrollLeft = 0;
        }
    };
    Grid.prototype.updateSoftMerges = function (rows) {
        var _this = this;
        if (rows === void 0) { rows = []; }
        if (!rows.length) {
            rows = Object.keys(this.gridCells);
        }
        rows.forEach(function (rowIndex) {
            if (typeof rowIndex === 'string')
                rowIndex = parseFloat(rowIndex);
            // update main cells
            _this.gridCells[rowIndex].forEach(function (cell) {
                cell.updateSoftMerge(_this.state.hiddenColumns);
            });
            if (_this.columnCells[rowIndex]) {
                _this.columnCells[rowIndex].forEach(function (cell) {
                    cell.updateSoftMerge(_this.state.hiddenColumns);
                });
            }
            if (_this.commonCells[rowIndex]) {
                _this.commonCells[rowIndex].forEach(function (cell) {
                    cell.updateSoftMerge(_this.state.hiddenColumns);
                });
            }
        });
    };
    Grid.prototype.setSize = function (cellRef, data) {
        if (cellRef.type === 'col') {
            // set new cell height
            for (var colIndex = 0; colIndex < this.content[cellRef.dataset.row].length; colIndex++) {
                var cell = this.content[cellRef.dataset.row][colIndex];
                var gridCell = this.gridCells[cell.index.row][cell.index.col];
                if (!gridCell)
                    continue;
                gridCell.setSize('height', data.height);
                // set column cells height
                if (this.columnCells[gridCell.row] && this.columnCells[gridCell.row][gridCell.col]) {
                    this.columnCells[gridCell.row][gridCell.col].setSize('height', data.height);
                }
                if (this.commonCells[gridCell.row] && this.commonCells[gridCell.row][gridCell.col]) {
                    this.commonCells[gridCell.row][cell.index.col].setSize('height', data.height);
                }
            }
            // set row heights
            this.rows[cellRef.dataset.row].setHeight(data.height);
            this.rowsColumns[cellRef.index.row].setHeight(data.height);
            if (this.rowsRows[cellRef.index.row]) {
                this.rowsRows[cellRef.index.row].setHeight(data.height);
            }
            // set new heading cell height
            this.gridColumnHeadingCells[cellRef.row].setSize('height', data.height);
            // // move rows
            if (!this.big) {
                for (var rowIndex = cellRef.dataset.row + 1; rowIndex < this.content.length; rowIndex++) {
                    var cell = this.content[rowIndex][0];
                    // move common cells
                    // if (cell.index.row < this.freezeRow) {
                    //   for (let colIndex = 0; colIndex < this.freezeCol; colIndex++) {
                    //     this.gridCells[cell.index.row][colIndex].setOffset(data.offsetY, 'height');
                    //   }
                    // }
                    for (var colIndex = 0; colIndex < this.freezeCol; colIndex++) {
                        if (this.commonCells[cell.index.row] && this.commonCells[cell.index.row][colIndex]) {
                            this.commonCells[cell.index.row][colIndex].setOffset(data.offsetY, 'height');
                        }
                    }
                    // move rows
                    this.rows[cell.index.row].setOffet(data.offsetY);
                    // move frozen rows
                    this.rowsColumns[cell.index.row].setOffet(data.offsetY);
                    if (this.rowsRows[cell.index.row]) {
                        this.rowsRows[cell.index.row].setOffet(data.offsetY);
                    }
                    // move headings
                    this.gridColumnHeadingCells[cell.index.row].setOffset('height', data.offsetY);
                }
            }
            // resize freezeheight
            if (cellRef.dataset.row < this.freezeRow) {
                this.setElementSizes('height', this.freezeHeight + data.offsetY);
            }
        }
        else {
            // resize cells
            for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
                var row = this.content[0];
                for (var colIndex = cellRef.dataset.col; colIndex < row.length; colIndex++) {
                    var cell = this.content[rowIndex][colIndex];
                    var gridCell = this.gridCells[cell.index.row][cell.index.col];
                    if (!gridCell)
                        continue;
                    if (cell.index.col === cellRef.col) {
                        gridCell.setSize('width', data.width);
                        this.gridRowHeadingCells[cell.index.col].setSize('width', data.width);
                        if (this.columnCells[cell.index.row] && this.columnCells[cell.index.row][cell.index.col]) {
                            this.columnCells[cell.index.row][cell.index.col].setSize('width', data.width);
                        }
                        if (this.commonCells[cell.index.row] && this.commonCells[cell.index.row][cell.index.col]) {
                            this.commonCells[cell.index.row][cell.index.col].setSize('width', data.width);
                        }
                    }
                    else if (!this.big) {
                        gridCell.setOffset(data.offsetX, 'width');
                        this.gridRowHeadingCells[cell.index.col].setOffset('width', data.offsetX);
                        if (this.columnCells[cell.index.row] && this.columnCells[cell.index.row][cell.index.col]) {
                            this.columnCells[cell.index.row][cell.index.col].setOffset(data.offsetX, 'width');
                        }
                        if (this.commonCells[cell.index.row] && this.commonCells[cell.index.row][cell.index.col]) {
                            this.commonCells[cell.index.row][cell.index.col].setOffset(data.offsetX, 'width');
                        }
                    }
                }
                // this.gridCells[rowIndex][cellRef.col].updateSoftMerge();
            }
            // resize freezewidth
            if (cellRef.dataset.col < this.freezeCol) {
                this.setElementSizes('width', this.freezeWidth + data.offsetX);
            }
        }
    };
    Grid.prototype.applyScale = function (n) {
        this.pageElRect = this.pageEl.getBoundingClientRect();
        this.wrapperElRect = this.wrapperEl.getBoundingClientRect();
        // TODO. account for headings
        this.cellsOffsetX = this.headings ? 40 : 0;
        this.cellsOffsetY = this.headings ? 20 : 0;
        if (n) {
            if (n > 5)
                n = 5;
            if (n < .2)
                n = .2;
            this.scale = n;
        }
        else if (this.fit === 'width') {
            this.scale = this.pageElRect.width / (this.width + this.cellsOffsetX);
        }
        else if (this.fit === 'height') {
            this.scale = this.pageElRect.height / (this.height + this.cellsOffsetY);
        }
        else if (this.fit === 'contain') {
            var scaleWidth = this.pageElRect.width / (this.width + this.cellsOffsetX);
            var scaleHeight = this.pageElRect.height / (this.height + this.cellsOffsetY);
            this.scale = scaleHeight < scaleWidth ? scaleHeight : scaleWidth;
        }
        else {
            this.scale = 1;
        }
        this.scaleEl.style.setProperty('transform', "scale(" + this.scale + ")");
        this.scaleEl.style.setProperty('width', this.pageElRect.width / this.scale + "px");
        this.scaleEl.style.setProperty('height', this.pageElRect.height / this.scale + "px");
        if (this.ps)
            this.ps.update(this.fit, this.scale);
    };
    Grid.prototype.applyState = function (update) {
        if (update === void 0) { update = false; }
        // apply state
        var hiddenColumnsChange = helpers.arrayDiff(this.state.hiddenColumns, this.hiddenColumns).length ||
            this.hiddenColumns.length !== this.state.hiddenColumns.length;
        if (hiddenColumnsChange) {
            update = true;
        }
        this.applyFilters();
        this.applySorting();
        // update row index
        this.height = 0;
        var zIndexRow = this.content.length;
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            // set new row indexes
            var cell = this.content[rowIndex][0];
            if (!rowIndex) {
                this.selection.rowStart = cell.index.row;
            }
            var hiddenRow = this.rowsToHide.indexOf(cell.index.row) > -1;
            this.rows[cell.index.row].hidden = hiddenRow;
            this.rows[cell.index.row].setIndex(rowIndex, this.height, zIndexRow);
            if (this.rowsColumns[cell.index.row]) {
                this.rowsColumns[cell.index.row].hidden = hiddenRow;
                this.rowsColumns[cell.index.row].setIndex(rowIndex, this.height, zIndexRow);
            }
            this.gridColumnHeadingCells[cell.index.row].hidden = hiddenRow;
            this.gridColumnHeadingCells[cell.index.row].setIndex(rowIndex, this.height, zIndexRow);
            var row = this.gridCells[cell.index.row];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var gridCell = row[colIndex];
                gridCell.setRowIndex(rowIndex, this.height);
                if (this.columnCells[gridCell.row] && this.columnCells[gridCell.row][gridCell.col]) {
                    this.columnCells[gridCell.row][gridCell.col].setRowIndex(rowIndex, this.height);
                }
            }
            if (!hiddenRow) {
                this.height += this.rows[cell.index.row].dataset.height;
                this.selection.rowEnd = cell.index.row;
                this.selection.rowLast = rowIndex;
            }
            zIndexRow--;
            // set new cell x offset
            // this.selection.colLast = this.content[0].length - 1;
            if (update) {
                var row_2 = this.content[rowIndex];
                this.width = 0;
                for (var colIndex = 0; colIndex < row_2.length; colIndex++) {
                    var cell_1 = this.content[rowIndex][colIndex];
                    var gridCell = this.gridCells[cell_1.index.row][cell_1.index.col];
                    var hidden = cell_1.index.col >= this.freezeCol && this.state.hiddenColumns.indexOf(cell_1.index.col) > -1;
                    gridCell.hidden = hidden;
                    this.gridRowHeadingCells[cell_1.index.col].hidden = hidden;
                    if (!hidden) {
                        this.gridRowHeadingCells[cell_1.index.col].dataset.x = this.width;
                        this.gridRowHeadingCells[cell_1.index.col].setOffset('width', 0);
                        gridCell.dataset.x = this.width;
                        gridCell.setOffset(0);
                        this.width += gridCell.dataset.width;
                    }
                }
            }
        }
        if (!this.height) {
            this.mainCellsEl.innerText = 'No rows match filter';
        }
        // this.height++;
        if (update) {
            // this.width++;
            this.selection.from = undefined;
            this.selection.to = undefined;
            this.updateSelector();
            this.updateSoftMerges();
            this.applyScale(this.scale);
        }
        this.hiddenColumns = this.state.hiddenColumns;
        // clear sorting flag
        if (this.state.sorting.enabled && this.hiddenColumns.indexOf(this.state.sorting.col) < 0) {
            this.pageEl.classList.add('sorting');
            this.sortEl.classList.add("sorting-" + this.state.sorting.direction);
            var cell = this.content[0][this.state.sorting.col];
            var gridCell = this.getGridCell(cell.index.row, cell.index.col);
            if (gridCell) {
                this.sortEl.style.setProperty('left', gridCell.dataset.x + gridCell.dataset.width - 4 + "px");
            }
        }
        else {
            this.pageEl.classList.remove('sorting');
            this.sortEl.classList.remove('sorting-asc', 'sorting-desc');
        }
    };
    Object.defineProperty(Grid.prototype, "hasFocus", {
        get: function () {
            return this._hasFocus;
        },
        set: function (n) {
            this._hasFocus = n;
            if (n) {
                this.pageEl.classList.add('focus');
            }
            else {
                this.pageEl.classList.remove('focus');
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "gridlines", {
        get: function () {
            return this._gridlines;
        },
        set: function (n) {
            this._gridlines = n;
            if (this.gridlines) {
                this.pageEl.classList.add('gridlines');
            }
            else {
                this.pageEl.classList.remove('gridlines');
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "headings", {
        get: function () {
            return this._headings;
        },
        set: function (n) {
            this._headings = n;
            if (this.headings) {
                this.pageEl.classList.add('headings');
            }
            else {
                this.pageEl.classList.remove('headings');
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "editing", {
        get: function () {
            return this._editing;
        },
        set: function (n) {
            if (!this.canEdit) {
                return;
            }
            var restore = false;
            if (this.dirty) {
                this.Events.emit(this.Events.EDITING, {
                    cell: this.editingCell || this.selection.from,
                    editing: this._editing,
                    dirty: this.dirty
                });
                this.dirty = false;
            }
            else {
                restore = true;
            }
            if (n) {
                if (!this.selection.from || this.disallowSelection)
                    return;
                var valid = true;
                if (this.selection.from !== this.selection.to) {
                    valid = false;
                }
                if (this.selection.from.button && !this.selection.from.canEditButton) {
                    valid = false;
                }
                if (this.selection.from.permission !== 'rw' || this.selection.from.isImage) {
                    valid = false;
                }
                if (!valid)
                    return;
                // if (this.selection.from.button && this.selection.from.button.type === 'rotate') {
                //   return;
                // }
                this.pageEl.classList.add('editing');
                this.selection.last = this.selection.from;
                this.onCellUpdates();
            }
            else {
                this.pageEl.classList.remove('editing');
                this.cancelCellEdit(restore);
            }
            this._editing = n;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @description
     * After resizing set the final cell offets and update grid size
     *
     * @param {string} which row | col
     * @param {number} index
     * @param {number} offset
     */
    Grid.prototype.setPositions = function (cellRef, data) {
        if (cellRef.type === 'row') {
            var index = cellRef.col;
            var offset = data.offsetX;
            var rowsToUpdate = [];
            for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
                var row = this.content[rowIndex];
                // this.gridCells[row[0].index.row][cellRef.dataset.col].updateSoftMerge();
                rowsToUpdate.push(row[0].index.row);
                for (var colIndex = cellRef.dataset.col + 1; colIndex < row.length; colIndex++) {
                    var cell = this.content[rowIndex][colIndex];
                    // let gridCell = this.getGridCell(cell.index.row, cell.index.col);
                    var gridCell = this.gridCells[cell.index.row][cell.index.col];
                    if (!gridCell)
                        continue;
                    gridCell.setXPosition(offset, this.big);
                    if (this.columnCells[gridCell.row] && this.columnCells[gridCell.row][gridCell.col]) {
                        this.columnCells[gridCell.row][gridCell.col].setXPosition(offset, this.big);
                    }
                    if (this.commonCells[gridCell.row] && this.commonCells[gridCell.row][gridCell.col]) {
                        this.commonCells[gridCell.row][gridCell.col].setXPosition(offset, this.big);
                    }
                    if (!rowIndex)
                        this.gridRowHeadingCells[gridCell.col].setPosition('width', offset, this.big);
                    // this.gridCells[cell.index.row][colIndex].updateSoftMerge();
                }
            }
            // update size
            if (cellRef.col < this.freezeCol) {
                this.freezeWidth += offset;
            }
            this.updateSoftMerges(rowsToUpdate);
            this.width += offset;
        }
        else {
            var index = cellRef.row;
            var offset = data.offsetY;
            for (var rowIndex = cellRef.dataset.row + 1; rowIndex < this.content.length; rowIndex++) {
                var cell = this.content[rowIndex][0];
                this.rows[cell.index.row].setPosition(offset, this.big);
                if (this.rowsRows[cell.index.row])
                    this.rowsRows[cell.index.row].setPosition(offset, this.big);
                if (this.rowsColumns[cell.index.row])
                    this.rowsColumns[cell.index.row].setPosition(offset, this.big);
                for (var colIndex = 0; colIndex < this.content[0].length; colIndex++) {
                    var cell_2 = this.content[rowIndex][colIndex];
                    // let gridCell = this.getGridCell(cell.index.row, colIndex);
                    var gridCell = this.gridCells[cell_2.index.row][cell_2.index.col];
                    if (!gridCell)
                        continue;
                    gridCell.setYPosition(offset, this.big);
                    if (this.columnCells[gridCell.row] && this.columnCells[gridCell.row][gridCell.col]) {
                        this.columnCells[gridCell.row][gridCell.col].setYPosition(offset, this.big);
                    }
                    if (this.commonCells[gridCell.row] && this.commonCells[gridCell.row][gridCell.col]) {
                        this.commonCells[gridCell.row][gridCell.col].setYPosition(offset, this.big);
                    }
                }
                this.gridColumnHeadingCells[cell.index.row].setPosition('height', offset, this.big);
            }
            // update size
            if (cellRef.row < this.freezeRow) {
                this.freezeHeight += offset;
            }
        }
    };
    Grid.prototype.destroyPopper = function () {
        if (this.buttonPopper) {
            this.pageEl.removeChild(this.buttonPopper.popper);
            this.buttonPopper.destroy();
            this.buttonPopper = null;
        }
    };
    Grid.prototype.onCellUpdates = function () {
        var _this = this;
        if (!this.selection.from || this.disallowSelection)
            return;
        // listen for value changes
        var frozenCell = false;
        var gridCell = this.selection.from;
        // check if common cell
        if (gridCell.row < this.freezeRow && gridCell.col < this.freezeCol) {
            gridCell = this.commonCells[gridCell.row][gridCell.col];
            frozenCell = true;
        }
        else if (gridCell.col < this.freezeCol) {
            gridCell = this.columnCells[gridCell.row][gridCell.col];
            frozenCell = true;
        }
        this.editingCell = gridCell;
        gridCell.removeEvents();
        gridCell.on(gridCell.VALUE, function (value) {
            var event = {
                cell: gridCell,
                content: gridCell.params.content,
                value: value
            };
            _this.Events.emit(_this.Events.CELL_VALUE, event);
            _this.dirty = true;
        });
        gridCell.on(gridCell.DONE, function (value) {
            console.log(value);
            _this.dirty = true;
            if (frozenCell) {
                _this.gridCells[gridCell.row][gridCell.col].update(gridCell.params.content);
            }
            _this.updateSoftMerges([gridCell.row]);
        });
        gridCell.on(gridCell.CANCEL, function () {
            _this.dirty = false;
        });
        gridCell.on(gridCell.STOP, function (key) {
            // gridCell.removeEvents();
            console.log('gridCell.STOP', key);
            if (gridCell.button) {
                _this.dirty = false;
            }
            if (_this.editing) {
                _this.editing = false;
            }
            else {
                _this.cancelCellEdit();
            }
            _this.keyValue = undefined;
            // if (key) {
            //   let direction: string = key.replace('Arrow', '').toLowerCase();
            //   this.selectNextCell(direction);
            // }
            _this.destroyPopper();
        });
        gridCell.on(gridCell.CHECKED, function (data) {
            var event = {
                cell: gridCell.getDetails(),
                content: gridCell.params.content,
                value: gridCell.params.content.value,
                checked: data
            };
            _this.Events.emit(_this.Events.CELL_CHECKED, event);
        });
        // wait for editing to end. should we listen for an instead?
        if (gridCell.hasButtonOptions) {
            if (!gridCell.onButton()) {
                gridCell.openOptions();
                this.pageEl.appendChild(gridCell.cellPopper);
                this.buttonPopper = new popper_js_1.default(gridCell.cell, gridCell.cellPopper, {
                    placement: 'bottom-start'
                });
            }
        }
        else {
            // if touch attach to page
            gridCell.edit(this.keyValue, this.touch);
            if (this.touch) {
                this.editEl.appendChild(gridCell.input);
                setTimeout(function () {
                    _this.updateSelector('row');
                }, 300);
            }
        }
    };
    Grid.prototype.cancelCellEdit = function (restore) {
        if (this.editingCell) {
            this.editingCell.cancel(restore);
            this.editingCell = undefined;
            // this.destroyPopper();
        }
    };
    Grid.prototype.getTouchDistance = function (touches) {
        if (touches === void 0) { touches = []; }
        var x = touches[1].clientX - touches[0].clientX;
        var y = touches[1].clientY - touches[0].clientY;
        return Math.abs(Math.sqrt(x * x + y * y));
    };
    Grid.prototype.onResizeHandle = function (gridCell) {
        var _this = this;
        this.resizing = true;
        this.selection.from = undefined;
        this.selection.to = undefined;
        var resizeData;
        this.updateSelector();
        this.pageEl.classList.add('resizing');
        gridCell.removeEvents();
        gridCell.on(gridCell.RESIZING, function (data) {
            resizeData = data;
            _this.setSize(gridCell, data);
        });
        gridCell.on(gridCell.RESIZED, function () {
            console.log('resized', resizeData);
            if (!resizeData)
                return;
            // emit size
            _this.setPositions(gridCell, resizeData);
            if (resizeData.type === 'row') {
                _this.Events.emit(_this.Events.RESIZED, {
                    which: 'col',
                    index: gridCell.params.content.index.col,
                    size: resizeData.width
                });
            }
            else {
                // this.setPositions('row', gridCell.dataset.row, resizeData.offsetY);
                _this.Events.emit(_this.Events.RESIZED, {
                    which: 'row',
                    index: gridCell.params.content.index.row,
                    size: resizeData.height
                });
            }
            _this.applyState();
            _this.setFreezeSizes(true);
            _this.applyScale();
            _this.render();
        });
        console.log(gridCell);
    };
    Grid.prototype.onCtrlNavigate = function (which) {
        if (this.editing ||
            ['Home', 'End', 'ArrowLeft', 'ArrowRight', 'ArrowDown', 'ArrowUp', 'a'].indexOf(which) < 0 ||
            !this.ctrlDown ||
            !this.selection.from ||
            !this.selection.to ||
            this.disallowSelection) {
            return false;
        }
        var rowFrom = this.selection.from.dataset.row;
        var colFrom = this.selection.from.dataset.col;
        var rowTo = this.selection.to.dataset.row;
        var colTo = this.selection.to.dataset.col;
        var update = 'all';
        if (which === 'a') {
            rowFrom = 0;
            colFrom = 0;
            rowTo = this.selection.rowLast;
            colTo = this.content[0].length - 1;
            this.selection.type = 'all';
        }
        if (which === 'Home') {
            if (!this.shiftDown) {
                rowFrom = 0;
                colFrom = 0;
            }
            rowTo = 0;
            colTo = 0;
        }
        if (which === 'End') {
            if (!this.shiftDown) {
                rowFrom = this.selection.rowLast;
                colFrom = this.content[0].length - 1;
            }
            rowTo = this.selection.rowLast;
            colTo = this.content[0].length - 1;
            // update = 'end';
        }
        var x = 0;
        var y = 0;
        // look for next empty cell value
        var lookForEmptyValue = true;
        var refCell = this.selection.to.dataset.col > this.selection.from.dataset.col ? this.selection.to : this.selection.from;
        if (which === 'ArrowRight') {
            var refCell_1 = this.selection.to.dataset.col > this.selection.from.dataset.col ? this.selection.to : this.selection.from;
            for (var colIndex = refCell_1.dataset.col + 1; colIndex < this.content[0].length; colIndex++) {
                var cell_3 = this.content[refCell_1.dataset.row][colIndex];
                var hidden = this.state.hiddenColumns.indexOf(cell_3.index.col) > -1;
                if (hidden)
                    continue;
                if (colIndex === refCell_1.dataset.col + 1 && !cell_3.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell_3.value && lookForEmptyValue) {
                    x = colIndex - 1;
                    break;
                }
                if (cell_3.value && !lookForEmptyValue) {
                    x = colIndex;
                    break;
                }
            }
            if (!x) {
                x = this.content[0].length - 1;
            }
            colTo = x;
            if (!this.shiftDown) {
                colFrom = x;
            }
        }
        else if (which === 'ArrowLeft') {
            var refCell_2 = this.selection.to.dataset.col > this.selection.from.dataset.col ? this.selection.from : this.selection.to;
            for (var colIndex = refCell_2.dataset.col - 1; colIndex >= 0; colIndex--) {
                var cell_4 = this.content[refCell_2.dataset.row][colIndex];
                if (colIndex === refCell_2.dataset.col - 1 && !cell_4.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell_4.value && lookForEmptyValue) {
                    x = colIndex + 1;
                    break;
                }
                if (cell_4.value && !lookForEmptyValue) {
                    x = colIndex;
                    break;
                }
            }
            colTo = x;
            if (!this.shiftDown) {
                colFrom = x;
            }
        }
        else if (which === 'ArrowDown') {
            var refCell_3 = this.selection.to.dataset.row > this.selection.from.dataset.row ? this.selection.to : this.selection.from;
            for (var rowIndex = refCell_3.dataset.row + 1; rowIndex < this.content.length; rowIndex++) {
                var cell_5 = this.content[rowIndex][refCell_3.dataset.col];
                if (rowIndex === refCell_3.dataset.row + 1 && !cell_5.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell_5.value && lookForEmptyValue) {
                    y = rowIndex - 1;
                    break;
                }
                if (cell_5.value && !lookForEmptyValue) {
                    y = rowIndex;
                    break;
                }
            }
            if (!y) {
                y = this.selection.rowLast;
            }
            rowTo = y;
            if (!this.shiftDown) {
                rowFrom = y;
            }
        }
        else if (which === 'ArrowUp') {
            var refCell_4 = this.selection.to.dataset.row > this.selection.from.dataset.row ? this.selection.from : this.selection.to;
            for (var rowIndex = refCell_4.dataset.row - 1; rowIndex >= 0; rowIndex--) {
                var cell_6 = this.content[rowIndex][refCell_4.dataset.col];
                if (rowIndex === refCell_4.dataset.row - 1 && !cell_6.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell_6.value && lookForEmptyValue) {
                    y = rowIndex + 1;
                    break;
                }
                if (cell_6.value && !lookForEmptyValue) {
                    y = rowIndex;
                    break;
                }
            }
            rowTo = y;
            if (!this.shiftDown) {
                rowFrom = y;
            }
        }
        var cell = this.content[rowFrom][colFrom];
        this.selection.from = this.getGridCell(cell.index.row, cell.index.col);
        var cellTo = this.content[rowTo][colTo];
        this.selection.to = this.getGridCell(cellTo.index.row, cellTo.index.col);
        this.selection.keyboard = true;
        this.updateSelector(update);
        return true;
    };
    Grid.prototype.emitSelected = function (evt, type) {
        var from = this.selection.from;
        var to = this.selection.to;
        var count = 0;
        // get frozen cell instead
        // if (this.commonCells[from.row] && this.commonCells[from.row][from.col]) {
        //   from = this.commonCells[from.row][from.col];
        // } else if (this.columnCells[from.row] && this.columnCells[from.row][from.col]) {
        //   from = this.columnCells[from.row][from.col];
        // }
        // if (this.commonCells[to.row] && this.commonCells[to.row][to.col]) {
        //   from = this.commonCells[to.row][to.col];
        // } else if (this.columnCells[to.row] && this.columnCells[to.row][to.col]) {
        //   from = this.columnCells[to.row][to.col];
        // }
        if (this.selection.type === 'row') {
            count = to.row + 1 - from.row;
        }
        else if (this.selection.type === 'col') {
            count = to.col + 1 - from.col;
        }
        var reference = helpers.getCellsReference(this.selection.from.position, this.selection.to.position, type);
        // checkbox button type
        if (this.rowChecked && from.button && from.button.type === 'checkbox' && from === to) {
            from.toggleCheckbox();
            var event_1 = {
                cell: from.getDetails(),
                content: from.params.content,
                value: from.params.content.value,
                checked: from.checked,
            };
            this.Events.emit(this.Events.CELL_CHECKED, event_1);
            this.rowChecked = false;
            return;
        }
        this.Events.emitCellSelected({
            rightClick: this.rightClick,
            selection: {
                from: from.getDetails(),
                to: to.getDetails(),
                heading: this.selection.heading,
                inside: this.selection.inside,
                reference: reference,
                keyboard: this.selection.keyboard,
                type: this.selection.type
            },
            which: this.selection.type,
            count: count,
            event: evt,
            reference: reference,
            multiple: from !== to
        });
    };
    Grid.prototype.isInsideSelection = function (gridCell) {
        if (!this.selection.from || !this.selection.to)
            return false;
        if (gridCell instanceof GridHeadingCell_1.GridHeadingCell) {
            if (gridCell.type === 'row') {
                return (gridCell.dataset.col >= this.selection.from.dataset.col &&
                    gridCell.dataset.col <= this.selection.to.dataset.col);
            }
            else if (gridCell.type === 'col') {
                return (gridCell.dataset.row >= this.selection.from.dataset.row &&
                    gridCell.dataset.row <= this.selection.to.dataset.row);
            }
            return false;
        }
        return (gridCell.dataset.row >= this.selection.from.dataset.row &&
            gridCell.dataset.row <= this.selection.to.dataset.row &&
            gridCell.dataset.col >= this.selection.from.dataset.col &&
            gridCell.dataset.col <= this.selection.to.dataset.col);
    };
    Grid.prototype.setCellSelection = function (target) {
        if (target.dataset.type === 'corner') {
            this.selection.from = this.gridCells[this.selection.rowStart][0];
            this.selection.to = this.gridCells[this.selection.rowEnd][this.gridCells[0].length - 1];
            this.selection.heading = true;
            return null;
        }
        var gridCell = this.getGridCellByTarget(target);
        if (!gridCell)
            return null;
        var heading = gridCell instanceof GridHeadingCell_1.GridHeadingCell;
        var insideSelection = this.isInsideSelection(gridCell);
        if (insideSelection && this.rightClick) {
            this.selection.inside = true;
            this.selection.heading = heading;
            return gridCell;
        }
        if (gridCell instanceof GridHeadingCell_1.GridHeadingCell) {
            this.selection.heading = true;
            if (gridCell.type === 'row') {
                this.selection.from = this.gridCells[this.selection.rowStart][gridCell.col];
                this.selection.to = this.gridCells[this.selection.rowEnd][gridCell.col];
            }
            else {
                this.selection.from = this.gridCells[gridCell.row][0];
                this.selection.to = this.gridCells[gridCell.row][this.gridCells[0].length - 1];
            }
        }
        else {
            this.selection.from = gridCell;
            this.selection.to = gridCell;
        }
        return gridCell;
    };
    Grid.prototype.getGridCellByTarget = function (target) {
        if (!target)
            return null;
        var cell = this.getCell(target);
        if (!cell)
            return null;
        var row = parseFloat(cell.dataset.row || '-1');
        var col = parseFloat(cell.dataset.col || '-1');
        var r = parseFloat(cell.dataset.r || '-1');
        var c = parseFloat(cell.dataset.c || '-1');
        var type = cell.dataset.type;
        if (!this.gridCells[r] || !this.gridCells[r][c])
            return null;
        if (cell.classList.contains('grid-cell-heading-row')) {
            return this.gridRowHeadingCells[c];
        }
        if (cell.classList.contains('grid-cell-heading-col')) {
            return this.gridColumnHeadingCells[r];
        }
        return this.gridCells[r][c];
    };
    Grid.prototype.selectNextCell = function (direction, key) {
        if (key === void 0) { key = ''; }
        if (!this.selection.from || !this.selection.to)
            return;
        var which = this.shiftDown && key !== 'Enter' && key !== 'Tab' ? 'to' : 'from';
        var row = this.selection[which].dataset.row;
        var col = this.selection[which].dataset.col;
        var set = '';
        if (direction === 'pagedown') {
            set = 'pagedown';
            for (var rowIndex = row + 1; rowIndex < this.content.length; rowIndex++) {
                var cell_7 = this.content[rowIndex][col];
                var gridCell = this.gridCells[cell_7.index.row][cell_7.index.col];
                if (!this.rows[cell_7.index.row].hidden && !this.isFullyVisible(gridCell, 'y')) {
                    row = rowIndex;
                    break;
                }
            }
            if (row === this.selection[which].dataset.row) {
                row = this.selection.rowLast;
            }
        }
        if (direction === 'pageup') {
            set = 'pageup';
            for (var rowIndex = row - 1; rowIndex >= 0; rowIndex--) {
                var cell_8 = this.content[rowIndex][col];
                var gridCell = this.gridCells[cell_8.index.row][cell_8.index.col];
                if (!this.isFullyVisible(gridCell, 'y')) {
                    row = rowIndex;
                    break;
                }
            }
            if (row === this.selection[which].dataset.row) {
                row = 0;
            }
        }
        if (direction === 'home') {
            col = 0;
            set = 'col';
        }
        if (direction === 'end') {
            col = this.content[0].length - 1;
            set = 'col';
        }
        if (direction === 'up') {
            for (var rowIndex = row - 1; rowIndex >= 0; rowIndex--) {
                var cell_9 = this.content[rowIndex][col];
                if (this.rows[cell_9.index.row].hidden)
                    continue;
                row = rowIndex;
                break;
            }
            // row--;
            // if (row < 0) row = 0;
        }
        if (direction === 'down') {
            for (var rowIndex = row + 1; rowIndex < this.content.length; rowIndex++) {
                var cell_10 = this.content[rowIndex][col];
                if (this.rows[cell_10.index.row].hidden)
                    continue;
                row = rowIndex;
                break;
            }
        }
        if (direction === 'right') {
            for (var colIndex = col + 1; colIndex < this.content[0].length; colIndex++) {
                var cell_11 = this.content[row][colIndex];
                if (this.state.hiddenColumns.indexOf(cell_11.index.col) > -1)
                    continue;
                col = colIndex;
                break;
            }
            // col++;
            // if (col >= this.content[0].length) col = this.content[0].length - 1;
        }
        if (direction === 'left') {
            for (var colIndex = col - 1; colIndex >= 0; colIndex--) {
                var cell_12 = this.content[row][colIndex];
                if (this.state.hiddenColumns.indexOf(cell_12.index.col) > -1)
                    continue;
                col = colIndex;
                break;
            }
            // col--;
            // if (col < 0) col = 0;
        }
        var cell = this.content[row][col];
        this.selection[which] = this.gridCells[cell.index.row][cell.index.col];
        if (!this.shiftDown || key === 'Enter' || key === 'Tab') {
            this.selection.to = this.selection.from;
        }
        this.selection.keyboard = true;
        this.updateSelector(set);
        this.cancelCellEdit();
        // this.arrowKeyDown = false;
    };
    Grid.prototype.updateSelector = function (set, type) {
        var _this = this;
        if (set === void 0) { set = ''; }
        if (type === void 0) { type = ''; }
        if (!this.selection.from || !this.selection.to) {
            this.gridColumnHeadingCells.forEach(function (gridCell) {
                gridCell.selected(false);
            });
            this.gridRowHeadingCells.forEach(function (gridCell) {
                gridCell.selected(false);
            });
            this.pageEl.classList.remove('selector');
            return;
        }
        if (!this.disallowSelection)
            this.pageEl.classList.add('selector');
        else
            this.pageEl.classList.remove('selector');
        var flipRow = this.selection.to.dataset.row < this.selection.from.dataset.row;
        var flipCol = this.selection.to.dataset.col < this.selection.from.dataset.col;
        // console.log(flipRow, flipCol);
        this.selection.rowFrom = flipRow ? this.selection.to.row : this.selection.from.row;
        this.selection.rowTo = flipRow ? this.selection.from.row : this.selection.to.row;
        this.selection.colFrom = flipCol ? this.selection.to.col : this.selection.from.col;
        this.selection.colTo = flipCol ? this.selection.from.col : this.selection.to.col;
        var from = this.getGridCell(this.selection.rowFrom, this.selection.colFrom);
        var to = this.getGridCell(this.selection.rowTo, this.selection.colTo);
        var width = to.dataset.x + (flipCol ? from.dataset.width : to.dataset.width) - from.dataset.x;
        var height = to.dataset.y + to.dataset.height - from.dataset.y;
        var selectors = this.selectorWrapper.getElementsByClassName('grid-selector');
        for (var i = 0; i < selectors.length; i++) {
            var element = selectors[i];
            // element.style.setProperty('transform', `translate(${from.dataset.x}px, ${from.dataset.y}px)`);
            element.style.setProperty('left', from.dataset.x + "px");
            element.style.setProperty('top', from.dataset.y + "px");
            element.style.setProperty('width', width + "px");
            element.style.setProperty('height', height + "px");
        }
        // this.mainSelectorEl.classList.add('show');
        // this.rowsSelectorEl.style.setProperty('transform', `translate(${from.dataset.x}px, ${from.dataset.y}px)`);
        // this.rowsSelectorEl.style.setProperty('width', `${width}px`);
        // this.rowsSelectorEl.style.setProperty('height', `${height}px`);
        // this.rowsSelectorEl.classList.add('show');
        // this.columnsSelectorEl.style.setProperty('transform', `translate(${from.dataset.x}px, ${from.dataset.y}px)`);
        // this.columnsSelectorEl.style.setProperty('width', `${width}px`);
        // this.columnsSelectorEl.style.setProperty('height', `${height}px`);
        // this.columnsSelectorEl.classList.add('show');
        // this.commonSelectorEl.style.setProperty('transform', `translate(${from.dataset.x}px, ${from.dataset.y}px)`);
        // this.commonSelectorEl.style.setProperty('width', `${width}px`);
        // this.commonSelectorEl.style.setProperty('height', `${height}px`);
        // this.commonSelectorEl.classList.add('show');
        this.gridRowHeadingCells.forEach(function (gridCell) {
            if (gridCell.dataset.col >= _this.selection.colFrom && gridCell.dataset.col <= _this.selection.colTo) {
                gridCell.selected(true);
            }
            else {
                gridCell.selected(false);
            }
        });
        this.gridColumnHeadingCells.forEach(function (gridCell) {
            if (gridCell.dataset.row >= _this.selection.from.dataset.row &&
                gridCell.dataset.row <= _this.selection.to.dataset.row) {
                gridCell.selected(true);
            }
            else {
                gridCell.selected(false);
            }
        });
        var gridCell = this.selection.to;
        if (this.selection.heading) {
            console.log(type);
            if (type === 'row') {
                gridCell = this.gridCells[0][gridCell.col];
            }
            else {
                gridCell = this.gridCells[gridCell.row][0];
            }
        }
        // keep within view
        if (!this.scrollTimer &&
            !this.touch &&
            !set &&
            // !this.selection.heading &&
            !this.rightClick &&
            this.fit !== 'contain') {
            var threshold_1 = 20;
            this.scrollTimer = setTimeout(function () {
                if (gridCell) {
                    if (_this.fit !== 'width') {
                        if (gridCell.col >= _this.freezeCol &&
                            // !this.isFullyVisible(gridCell) &&
                            _this.scaleDimension(gridCell.dataset.x + gridCell.dataset.overflow - _this.freezeWidth) >
                                _this.scaleDimension(_this.mainEl.scrollLeft) + _this.mainElRect.width - threshold_1) {
                            // if freeze, cell could be hidden
                            if (!_this.isVisible(gridCell) && !_this.selection.selected) {
                                _this.mainEl.scrollLeft = gridCell.dataset.x - _this.freezeWidth - 1;
                            }
                            else {
                                _this.mainEl.scrollLeft += _this.scaleDimension(gridCell.dataset.overflow) + threshold_1;
                            }
                        }
                        else if (gridCell.col >= _this.freezeCol &&
                            _this.scaleDimension(gridCell.dataset.x) <
                                _this.scaleDimension(_this.mainEl.scrollLeft + _this.freezeWidth) + threshold_1) {
                            if (!_this.isVisible(gridCell) && !_this.selection.selected) {
                                _this.mainEl.scrollLeft = gridCell.dataset.x - _this.freezeWidth - 1;
                            }
                            else {
                                _this.mainEl.scrollLeft -= _this.scaleDimension(gridCell.dataset.overflow) + threshold_1;
                            }
                        }
                    }
                    if (_this.fit !== 'height' && type !== 'row') {
                        if (gridCell.row >= _this.freezeRow &&
                            _this.scaleDimension(gridCell.dataset.y + gridCell.dataset.height - _this.freezeHeight) >
                                _this.scaleDimension(_this.mainEl.scrollTop) + _this.mainElRect.height - threshold_1) {
                            if (!_this.isVisible(gridCell)) {
                                _this.mainEl.scrollTop = gridCell.dataset.y - _this.freezeHeight - 1;
                            }
                            else {
                                _this.mainEl.scrollTop += _this.scaleDimension(gridCell.dataset.height) + threshold_1;
                            }
                        }
                        else if (gridCell.row >= _this.freezeRow &&
                            _this.scaleDimension(gridCell.dataset.y) <
                                _this.scaleDimension(_this.mainEl.scrollTop + _this.freezeHeight) + threshold_1) {
                            if (!_this.isVisible(gridCell)) {
                                _this.mainEl.scrollTop = gridCell.dataset.y - _this.freezeHeight - 1;
                            }
                            else {
                                _this.mainEl.scrollTop -= _this.scaleDimension(gridCell.dataset.height) + threshold_1;
                            }
                        }
                    }
                }
                clearTimeout(_this.scrollTimer);
                _this.scrollTimer = null;
                _this.arrowKeyDown = false;
            }, 20);
        }
        else {
            if (set) {
                if (set == 'all' || set === 'col')
                    this.mainEl.scrollLeft = this.selection.from.dataset.x - this.freezeWidth - 1;
                if (set == 'all' || set === 'row')
                    // find min scrolltop
                    this.mainEl.scrollTop = this.selection.from.dataset.y - this.freezeHeight - 1;
                if (set === 'pagedown') {
                    this.mainEl.scrollTop += this.mainEl.clientHeight;
                }
                if (set === 'pageup') {
                    this.mainEl.scrollTop -= this.mainEl.clientHeight;
                }
                // if (set === 'rowEnd')
                //   this.mainEl.scrollTop =
                //     this.selection.from.dataset.y + this.selection.from.dataset.height - this.wrapperElRect.height;
            }
            this.arrowKeyDown = false;
        }
    };
    Grid.prototype.updateHistoryCells = function () {
        if (!this.tracking || !this.trackingData || this.trackingData.length < 2)
            return;
        for (var i = 0; i < this.trackingData.length; i++) {
            if (!i)
                continue;
            for (var rowIndex = 0; rowIndex < this.trackingData[i].content_diff.length; rowIndex++) {
                if (!this.trackingData[i].content_diff[rowIndex])
                    continue;
                for (var colIndex = 0; colIndex < this.trackingData[i].content_diff[rowIndex].length; colIndex++) {
                    if (!this.trackingData[i].content_diff[rowIndex][colIndex])
                        continue;
                    var cell = this.getGridCell(rowIndex, colIndex);
                    if (!cell)
                        continue;
                    cell.history(true, this.getTrackingUser(this.trackingData[i].modified_by.id));
                    if (this.columnCells[rowIndex] && this.columnCells[rowIndex][colIndex]) {
                        this.columnCells[rowIndex][colIndex].history(true, this.getTrackingUser(this.trackingData[i].modified_by.id));
                    }
                    if (this.commonCells[rowIndex] && this.commonCells[rowIndex][colIndex]) {
                        this.commonCells[rowIndex][colIndex].history(true, this.getTrackingUser(this.trackingData[i].modified_by.id));
                    }
                }
            }
        }
    };
    Grid.prototype.getTrackingUser = function (userId) {
        var index = this.trackingUsers.indexOf(userId);
        if (index > -1) {
            return index;
        }
        else {
            this.trackingUsers.push(userId);
            return this.trackingUsers.length - 1;
        }
    };
    Grid.prototype.dom = function () {
        var _this = this;
        this.pageEl.classList.add(this.contrast);
        this.pageEl.oncontextmenu = function () {
            return false;
        };
        this.pageElRect = this.pageEl.getBoundingClientRect();
        // scale
        this.scaleEl = document.createElement('div');
        this.scaleEl.className = 'grid-scale';
        this.pageEl.appendChild(this.scaleEl);
        // edit el
        this.editEl = document.createElement('div');
        this.editEl.classList.add('grid-edit');
        this.pageEl.appendChild(this.editEl);
        // box
        // this.scrollEl = document.createElement('div');
        // this.scrollEl.className = 'grid-scroll';
        // this.pageEl.appendChild(this.scrollEl);
        // this.boxEl = document.createElement('div');
        // this.boxEl.className = 'grid-box grid-box-scroll';
        // this.scrollEl.appendChild(this.boxEl);
        this.mergeEl = document.createElement('div');
        this.mergeEl.className = 'grid-merge';
        this.scaleEl.appendChild(this.mergeEl);
        // scroll box
        var gridBox = document.createElement('div');
        gridBox.className = 'grid-box grid-box-headings';
        // cell box
        var cellBox = document.createElement('div');
        cellBox.className = 'grid-cells';
        // headings
        this.headingsEl = document.createElement('div');
        this.headingsEl.classList.add('grid-headings');
        this.headingsColumnsEl = document.createElement('div');
        this.headingsColumnsEl.classList.add('grid-container', 'grid-columns');
        this.headingsColumnsCellsEl = cellBox.cloneNode();
        this.headingsColumnsEl.appendChild(gridBox.cloneNode());
        this.headingsColumnsEl.appendChild(this.headingsColumnsCellsEl);
        this.headingsEl.appendChild(this.headingsColumnsEl);
        this.headingsRowsEl = document.createElement('div');
        this.headingsRowsEl.classList.add('grid-container', 'grid-rows');
        this.headingsRowsCellsEl = cellBox.cloneNode();
        this.headingsRowsEl.appendChild(gridBox.cloneNode());
        this.headingsRowsEl.appendChild(this.headingsRowsCellsEl);
        this.headingsEl.appendChild(this.headingsRowsEl);
        this.headingsCommonEl = document.createElement('div');
        this.headingsCommonEl.classList.add('grid-container', 'grid-common');
        this.headingsCommonCellsEl = cellBox.cloneNode();
        // this.headingsCommonEl.appendChild(gridBox.cloneNode());
        this.headingsCommonEl.appendChild(this.headingsCommonCellsEl);
        var cornerCell = document.createElement('div');
        cornerCell.className = 'grid-cell-heading grid-cell-heading-corner';
        cornerCell.dataset.type = 'corner';
        this.headingsCommonEl.appendChild(cornerCell);
        this.headingsEl.appendChild(this.headingsCommonEl);
        this.scaleEl.appendChild(this.headingsEl);
        // container
        this.wrapperEl = document.createElement('div');
        this.wrapperEl.className = 'grid-main';
        this.scaleEl.appendChild(this.wrapperEl);
        // main scrolling box
        this.mainBoxEl = document.createElement('div'); // pageEl.getElementsByClassName('grid-box')[0]; // scroll size
        this.mainBoxEl.className = 'grid-box grid-box-main';
        this.mainEl = document.createElement('div'); // pageEl.getElementsByClassName('grid-main')[0]; // all cells
        this.mainEl.className = 'grid-container grid-main';
        this.mainCellsEl = document.createElement('div'); // mainEl.getElementsByClassName('grid-cells')[0]; // all cells
        this.mainCellsEl.className = 'grid-cells grid-cells-main';
        this.mainEl.appendChild(this.mainBoxEl);
        this.mainEl.appendChild(this.mainCellsEl);
        this.wrapperEl.appendChild(this.mainEl);
        // main selector
        // this.mainSelectorEl = document.createElement('div');
        // this.mainSelectorEl.className = 'grid-selector';
        // this.mainEl.appendChild(this.mainSelectorEl);
        this.columnsEl = document.createElement('div'); // pageEl.getElementsByClassName('grid-columns')[0]; // frozen columns
        this.columnsEl.className = 'grid-container grid-columns';
        this.columnsCellsEl = document.createElement('div'); // columnsEl.getElementsByClassName('grid-cells')[0]; // frozen columns
        this.columnsCellsEl.className = 'grid-cells';
        this.columnsEl.appendChild(this.columnsCellsEl);
        // this.columnsEl.appendChild(gridBox.cloneNode());
        this.wrapperEl.appendChild(this.columnsEl);
        // columns selector
        // this.columnsSelectorContainerEl = document.createElement('div');
        // this.columnsSelectorContainerEl.className = 'grid-selector-container';
        // this.columnsSelectorEl = document.createElement('div');
        // this.columnsSelectorEl.className = 'grid-selector';
        // this.columnsSelectorEl.dataset.which = 'col';
        // this.columnsSelectorContainerEl.appendChild(this.columnsSelectorEl);
        // this.columnsEl.appendChild(this.columnsSelectorContainerEl);
        this.rowsEl = document.createElement('div'); // pageEl.getElementsByClassName('grid-rows')[0]; // frozen rows
        this.rowsEl.className = 'grid-container grid-rows';
        this.rowsCellsEl = document.createElement('div'); // rowsEl.getElementsByClassName('grid-cells')[0]; // frozen rows
        this.rowsCellsEl.className = 'grid-cells';
        this.rowsEl.appendChild(this.rowsCellsEl);
        // this.rowsEl.appendChild(gridBox.cloneNode());
        this.wrapperEl.appendChild(this.rowsEl);
        // rows selector
        // this.rowsSelectorEl = document.createElement('div');
        // this.rowsSelectorEl.className = 'grid-selector';
        // this.rowsSelectorEl.dataset.which = 'row';
        // this.rowsEl.appendChild(this.rowsSelectorEl);
        this.commonEl = document.createElement('div'); // pageEl.getElementsByClassName('grid-common')[0]; // frozen
        this.commonEl.className = 'grid-container grid-common';
        this.commonCellsEl = document.createElement('div'); // commonEl.getElementsByClassName('grid-cells')[0]; // frozen
        this.commonCellsEl.className = 'grid-cells';
        this.commonEl.appendChild(this.commonCellsEl);
        this.wrapperEl.appendChild(this.commonEl);
        // columns selector
        // this.commonSelectorContainerEl = document.createElement('div');
        // this.commonSelectorContainerEl.className = 'grid-selector-container';
        // this.commonSelectorEl = document.createElement('div');
        // this.commonSelectorEl.className = 'grid-selector';
        // this.commonSelectorEl.dataset.which = 'common';
        // this.commonSelectorContainerEl.appendChild(this.commonSelectorEl);
        // this.commonEl.appendChild(this.commonSelectorContainerEl);
        // selectors
        var selector = document.createElement('div');
        selector.className = 'grid-selector';
        this.selectorWrapper = document.createElement('div');
        this.selectorWrapper.className = 'grid-selector-wrapper';
        ['Main', 'Columns', 'Rows', 'Common'].forEach(function (el) {
            _this["selector" + el + "Container"] = document.createElement('div');
            _this["selector" + el + "Container"].className = "grid-selector-container grid-selector-" + el.toLowerCase();
            _this["selector" + el + "ContainerPosition"] = document.createElement('div');
            _this["selector" + el + "ContainerPosition"].className = 'grid-selector-position';
            _this["selector" + el] = selector.cloneNode();
            _this["selector" + el + "ContainerPosition"].appendChild(_this["selector" + el]);
            var gridBox = document.createElement('div');
            gridBox.className = 'grid-box';
            _this["selector" + el + "Container"].appendChild(gridBox);
            _this["selector" + el + "Container"].appendChild(_this["selector" + el + "ContainerPosition"]);
            _this.selectorWrapper.appendChild(_this["selector" + el + "Container"]);
        });
        this.scaleEl.appendChild(this.selectorWrapper);
        // sort el
        this.sortWrapperEl = document.createElement('div');
        this.sortWrapperEl.classList.add('grid-sorting-wrapper');
        this.sortEl = document.createElement('div');
        this.sortEl.innerHTML = '<div></div>';
        this.sortEl.classList.add('grid-sorting');
        this.sortWrapperEl.appendChild(this.sortEl);
        this.scaleEl.appendChild(this.sortWrapperEl);
        // popper
        // this.popperEl = document.createElement('div');
        // this.popperEl.className = 'grid-popper';
        // this.scaleEl.appendChild(this.popperEl);
    };
    Grid.prototype.domHeadings = function () {
        if (!this.headings) {
            this.headingsColumnsCellsEl.innerText = '';
            this.headingsRowsCellsEl.innerText = '';
            //   this.headingsCommonCellsEl.innerText = '';
            return;
        }
        // this.headingsColumnCells = [];
        // this.headingsRowCells = [];
        // this.headingsCommonCells = [];
        // for (let rowIndex = 0; rowIndex < this.cells.length; rowIndex++) {
        //   for (let colIndex = 0; colIndex < this.cells[rowIndex].length; colIndex++) {
        //   }
        // }
    };
    Grid.prototype.isFullyVisible = function (cell, axis) {
        var rect = this.pageEl.getBoundingClientRect();
        var cellRect = cell.cell.getBoundingClientRect();
        if (!axis || axis === 'y') {
            if (cellRect.top + cellRect.height > rect.top + rect.height) {
                return false;
            }
            if (cellRect.top + cellRect.height < rect.top) {
                return false;
            }
        }
        if (!axis || axis === 'x') {
            if (cellRect.left + cellRect.width > rect.left + rect.width) {
                return false;
            }
            if (cellRect.left + cellRect.width < rect.left) {
                return false;
            }
        }
        return true;
    };
    Grid.prototype.isVisible = function (cell, axis) {
        // var rect = elm.getBoundingClientRect();
        var cellData = cell.dataset;
        if (!axis || axis === 'y') {
            if (this.scaleDimension(parseFloat(cellData.y) + parseFloat(cellData.height)) <
                this.scaleDimension(this.offsetY + this.freezeHeight)) {
                return false;
            }
            if (this.scaleDimension(cellData.y - this.offsetY) > this.pageElRect.height * 1.2) {
                return false;
            }
        }
        if (!axis || axis === 'x') {
            if (this.scaleDimension(parseFloat(cellData.x) + parseFloat(cellData.overflow)) <
                this.scaleDimension(this.offsetX + this.freezeWidth)) {
                return false;
            }
            if (this.scaleDimension(parseFloat(cellData.x) - this.offsetX) > this.pageElRect.width * 1.2) {
                return false;
            }
        }
        return true;
    };
    Grid.prototype.scaleDimension = function (n) {
        return n * this.scale;
    };
    Grid.prototype.isTarget = function (evtTarget, element) {
        var clickedEl = evtTarget;
        while (clickedEl && clickedEl !== element) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl === element;
    };
    Grid.prototype.getCell = function (evtTarget) {
        var clickedEl = evtTarget;
        while (clickedEl &&
            ['DIV', 'IMG'].indexOf(clickedEl.nodeName) > -1 &&
            clickedEl.className.indexOf('grid-cell') < 0) {
            clickedEl = clickedEl.parentNode;
        }
        if (clickedEl.classList === undefined ||
            clickedEl.nodeName !== 'DIV' ||
            clickedEl.className.indexOf('grid-cell') < 0)
            return null;
        return clickedEl;
    };
    Grid.prototype.isRightClick = function (evt) {
        var isRightMB = false;
        if ('which' in evt)
            // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
            isRightMB = evt.which === 3;
        else if ('button' in evt)
            // IE, Opera
            isRightMB = evt.button === 2;
        return isRightMB;
    };
    Grid.prototype.createListeners = function () {
        var _this = this;
        var scrollEl = this.mainEl;
        if (!this.touch) {
            // this.ps = new PerfectScrollbar(scrollEl, { handlers: ['click-rail', 'drag-thumb', 'wheel', 'touch'] });
            this.ps = new GridScrollbars_1.GridScrollbars(this.pageEl, this.mainEl);
            this.ps.on(this.ps.SCROLL_X, function () {
                _this.destroyPopper();
                // this.rowsEl.scrollLeft = scrollEl.scrollLeft * 1;
                _this.sortWrapperEl.style.setProperty('left', scrollEl.scrollLeft * -1 + "px");
                _this.rowsEl.style.setProperty('left', scrollEl.scrollLeft * -1 + "px");
                _this.selectorMainContainer.scrollLeft = scrollEl.scrollLeft * 1;
                _this.selectorRowsContainer.scrollLeft = scrollEl.scrollLeft * 1;
                _this.headingsRowsEl.scrollLeft = scrollEl.scrollLeft * 1;
                // this.mainEl.scrollLeft = (scrollEl.scrollLeft * 1);
                if (_this.rendering)
                    return;
                requestAnimationFrame(_this.render);
            });
            this.ps.on(this.ps.SCROLL_Y, function () {
                _this.destroyPopper();
                // this.columnsEl.scrollTop = (scrollEl.scrollTop * 1);
                // this.sortWrapperEl.style.setProperty('top', `${scrollEl.scrollTop * -1}px`);
                _this.columnsEl.style.setProperty('top', scrollEl.scrollTop * -1 + "px");
                _this.selectorMainContainer.scrollTop = scrollEl.scrollTop * 1;
                _this.selectorColumnsContainer.scrollTop = scrollEl.scrollTop * 1;
                _this.headingsColumnsEl.scrollTop = scrollEl.scrollTop * 1;
                // this.mainEl.scrollTop = (scrollEl.scrollTop * 1);
                if (_this.rendering)
                    return;
                requestAnimationFrame(_this.render);
                // this.render();
            });
            this.pageEl.appendChild(this.ps.scrollTrackX);
            this.pageEl.appendChild(this.ps.scrollTrackY);
        }
        else {
            scrollEl.addEventListener('scroll', function () {
                _this.destroyPopper();
                _this.sortWrapperEl.style.setProperty('left', scrollEl.scrollLeft * -1 + "px");
                _this.rowsEl.style.setProperty('left', scrollEl.scrollLeft * -1 + "px");
                _this.selectorMainContainer.scrollLeft = scrollEl.scrollLeft * 1;
                _this.selectorRowsContainer.scrollLeft = scrollEl.scrollLeft * 1;
                _this.headingsRowsEl.scrollLeft = scrollEl.scrollLeft * 1;
                _this.sortWrapperEl.style.setProperty('top', scrollEl.scrollTop * -1 + "px");
                _this.columnsEl.style.setProperty('top', scrollEl.scrollTop * -1 + "px");
                _this.selectorMainContainer.scrollTop = scrollEl.scrollTop * 1;
                _this.selectorColumnsContainer.scrollTop = scrollEl.scrollTop * 1;
                _this.headingsColumnsEl.scrollTop = scrollEl.scrollTop * 1;
                if (_this.rendering)
                    return;
                requestAnimationFrame(_this.render);
            });
        }
        window.addEventListener('resize', this.onResize);
        window.addEventListener('keydown', this.onKeydown);
        window.addEventListener('keyup', this.onKeyup);
        if (this.touch) {
            window.addEventListener('touchstart', this.onMouseDown, false);
            window.addEventListener('touchmove', this.onMouseMove, false);
            window.addEventListener('touchend', this.onMouseUp, false);
        }
        else {
            window.addEventListener('mousedown', this.onMouseDown);
            window.addEventListener('mousemove', this.onMouseMove);
            window.addEventListener('mouseup', this.onMouseUp);
            // window.oncontextmenu = (e) => {
            //   if (this.isFocus) {
            //     // this.preventContextMenu = false;
            //     // return false;
            //     return false;
            //   }
            // };
        }
    };
    Grid.prototype.destroyListeners = function () {
        window.removeEventListener('resize', this.onResize);
        window.removeEventListener('keydown', this.onKeydown);
        window.removeEventListener('keyup', this.onKeyup);
        if (this.touch) {
            window.removeEventListener('touchstart', this.onMouseDown, false);
            window.removeEventListener('touchend', this.onMouseUp, false);
        }
        else {
            window.removeEventListener('mousedown', this.onMouseDown);
            window.removeEventListener('mousemove', this.onMouseMove);
            window.removeEventListener('mouseup', this.onMouseUp);
        }
        this.Events.removeEvents();
    };
    Grid.prototype.applySorting = function () {
        var _this = this;
        // if (!this.state.sorting || !this.state.sorting.enabled) return content;
        // let key = Object.keys(this.state.sorting)[0];
        var filter = this.state.sorting;
        var data = this.freezeRow ? this.content.slice(this.freezeRow, this.content.length) : this.content.slice(0);
        var exp = filter.direction === 'desc' ? '>' : '<';
        var opp = filter.direction === 'desc' ? '<' : '>';
        data.sort(function (a, b) {
            if (!_this.state.sorting.enabled) {
                return a[0].index.row - b[0].index.row;
            }
            var col = filter.col;
            if (!a || !b || !a[col] || !b[col]) {
                return 1;
            }
            var aVal = filter.type === 'number' ? parseFloat(a[col].value) || 0 : "\"" + a[col].formatted_value + "\"";
            var bVal = filter.type === 'number' ? parseFloat(b[col].value) || 0 : "\"" + b[col].formatted_value + "\"";
            if (eval(aVal + " " + exp + " " + bVal)) {
                return -1;
            }
            if (eval(aVal + " " + opp + " " + bVal)) {
                return 1;
            }
            return 0;
        });
        this.content = this.freezeRow ? this.content.slice(0, this.freezeRow).concat(data) : data;
    };
    Grid.prototype.applyFilters = function () {
        this.rowsToHide = [];
        // let originalData = JSON.parse(JSON.stringify(content));
        if (!this.state.filters.length) {
            return;
        }
        // let data = [];
        var rowsToRemove = [];
        // let rowOffset = this.Grid.Settings.headings ? 1 : 0;
        for (var rowIndex = 0; rowIndex < this.originalContent.length; rowIndex++) {
            // let offset = this.Grid.Settings.headings ? -1 : 0;
            if (rowIndex < this.freezeRow) {
                // rowsToKeep.push(rowIndex);
                continue;
            }
            var _loop_1 = function (i) {
                var filter = this_1.state.filters[i];
                if (filter.enabled === false) {
                    return "continue";
                }
                var col = this_1.originalContent[rowIndex][filter.col];
                // if (!col || rowsToRemove.indexOf(rowIndex) > -1) continue;
                if (!col)
                    return "continue";
                var colValue = filter.type === 'number' ? parseFloat("" + col.value) : "" + col.formatted_value;
                var filterValue = filter.type === 'number' ? parseFloat(filter.value) : "" + filter.value;
                var validValue = false;
                switch (filter.type) {
                    case 'number':
                        validValue = eval(colValue + " " + filter.exp + " " + filterValue);
                        break;
                    case 'date':
                        var dates = filterValue.split(',');
                        var fromDate = new Date(dates[0]).toISOString();
                        var toDate = new Date(dates[1] || dates[0]).toISOString();
                        var dateString = colValue;
                        var date = new Date(dateString);
                        if (helpers.isNumber(colValue)) {
                            dateString = helpers.parseDateExcel(colValue);
                            date = new Date(dateString);
                        }
                        else if (filter.dateFormat) {
                            // eg: DD MMMM YYYY (01 January 2019)
                            date = moment_timezone_1.default(colValue, filter.dateFormat);
                        }
                        if (date.toString() === 'Invalid Date')
                            break;
                        if (filter.exp === 'between') {
                            validValue = date.toISOString() >= fromDate && date.toISOString() <= toDate;
                        }
                        else {
                            validValue = eval("'" + date.toISOString() + "' " + filter.exp + " '" + fromDate + "'");
                        }
                        break;
                    default:
                        filterValue.split(',').forEach(function (value) {
                            if (validValue) {
                                return;
                            }
                            switch (filter.exp) {
                                case 'contains':
                                    validValue = colValue.toLowerCase().indexOf(value.toLowerCase()) > -1;
                                    break;
                                case 'starts_with':
                                    validValue = colValue.toLowerCase().substring(0, value.length) === value.toLowerCase();
                                    break;
                                case '!=':
                                    validValue = colValue != value; // eval(`"${colValue}" == "${value}"`);
                                    break;
                                default:
                                    validValue = colValue == value; // eval(`"${colValue}" == "${value}"`);
                                    break;
                            }
                        });
                        break;
                }
                if (!validValue && rowsToRemove.indexOf(rowIndex) < 0) {
                    rowsToRemove.push(col.index.row);
                }
            };
            var this_1 = this;
            for (var i = 0; i < this.state.filters.length; i++) {
                _loop_1(i);
            }
        }
        // for (let i = rowsToRemove.length - 1; i >= 0; i--) content.splice(rowsToRemove[i], 1);
        this.rowsToHide = rowsToRemove;
        // return content;
        // return data;
    };
    return Grid;
}());
exports.Grid = Grid;
//# sourceMappingURL=Grid.js.map