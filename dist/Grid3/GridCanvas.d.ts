import { IGridCells } from './Grid';
export interface IGridCanvas {
    canvas: HTMLCanvasElement;
    canvasContext: CanvasRenderingContext2D;
    bufferCanvas: HTMLCanvasElement;
    bufferCanvasContext: CanvasRenderingContext2D;
    gridCells: IGridCells;
    gridHeight: number;
    gridWidth: number;
    render(options: IGridImageOptions): string;
}
export interface IGridImageOptions {
    width: number;
    height: number;
    gridlines?: boolean;
    offsetX?: number;
    offsetY?: number;
    scale?: number;
    fit?: string;
}
declare class GridCanvas implements IGridCanvas {
    container: HTMLDivElement;
    canvas: HTMLCanvasElement;
    canvasContext: CanvasRenderingContext2D;
    bufferCanvas: HTMLCanvasElement;
    bufferCanvasContext: CanvasRenderingContext2D;
    rectangles: any;
    offsetRectangles: any;
    texts: any;
    lines: any;
    hidden: any;
    gridCells: IGridCells;
    width: number;
    height: number;
    gridHeight: number;
    gridWidth: number;
    offsetX: number;
    offsetY: number;
    scale: number;
    gridlines: boolean;
    borders: any;
    helpers: any;
    constructor(container: HTMLDivElement);
    render(options: IGridImageOptions): string;
    private resetElements;
    private clearCanvas;
    private drawCanvas;
    private drawRect;
    private drawOffsetRect;
    private drawLines;
    private drawLine;
    private drawText;
    private findLines;
    private cleanValue;
    private getFontString;
    private translateCanvas;
    private resetCanvas;
}
export default GridCanvas;
