"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GridRow = /** @class */ (function () {
    function GridRow(params) {
        this.params = params;
        this.hidden = false;
        this.row = 0;
        var dataset = {
            y: params.y,
            row: params.rowIndex,
            zIndexRow: params.zIndexRow,
            height: params.height
        };
        // this.hidden = params.hidden;
        this.dataset = dataset;
        this.row = params.rowIndex;
        this.rowEl = document.createElement('div');
        this.rowEl.className = 'grid-row';
        this.rowEl.style.setProperty('transform', "translateY(" + params.y + "px)");
        this.rowEl.style.setProperty('z-index', "" + params.zIndexRow);
        this.rowEl.dataset.row = params.rowIndex.toString();
        // this.rowEl.dataset.height = params.height.toString();
    }
    GridRow.prototype.setHeight = function (size) {
        this.dataset.height = size;
    };
    GridRow.prototype.setOffet = function (offset) {
        this.rowEl.style.setProperty('transform', "translateY(" + (this.dataset.y + offset) + "px)");
    };
    GridRow.prototype.setPosition = function (offset, move) {
        this.dataset.y += offset;
        if (!move)
            return;
        this.rowEl.style.setProperty('transform', "translateY(" + this.dataset.y + "px)");
        // this.dataset.height += offset;
    };
    GridRow.prototype.setIndex = function (row, y, zIndex) {
        this.dataset.row = row;
        this.dataset.y = y;
        this.dataset.zIndexRow = zIndex;
        this.rowEl.style.setProperty('transform', "translateY(" + this.dataset.y + "px)");
        this.rowEl.style.setProperty('z-index', "" + zIndex);
        this.rowEl.dataset.row = row.toString();
    };
    GridRow.prototype.setHover = function (n) {
        if (n) {
            this.rowEl.classList.add('hover');
        }
        else {
            this.rowEl.classList.remove('hover');
        }
    };
    return GridRow;
}());
exports.GridRow = GridRow;
//# sourceMappingURL=GridRow.js.map