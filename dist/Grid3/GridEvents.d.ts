import Emitter, { IEmitter } from '../Emitter';
import { IPageContentCell } from '../Page/Content';
import { IGridSelection, IGridCells } from './Grid';
import { IGridCell, IGridCellDetails } from './GridCell';
export interface IGridEvents extends IEmitter {
    BLUR: string;
    NO_CELL: string;
    CELL_LINK_CLICKED: string;
    CELL_DOUBLE_CLICKED: string;
    CELL_CLICKED: string;
    CELL_VALUE: string;
    CELL_SELECTED: string;
    CELL_RESET: string;
    CELL_CHECKED: string;
    EDITING: string;
    RESIZED: string;
    DELETE: string;
    SELECTED_ALL: string;
    ERROR: string;
    emitCellSelected(data: IGridEventCellSelected): void;
}
declare class GridEvents extends Emitter implements IGridEvents {
    constructor();
    get CELL_CLICKED(): string;
    get BLUR(): string;
    get NO_CELL(): string;
    get CELL_HISTORY_CLICKED(): string;
    get CELL_LINK_CLICKED(): string;
    get CELL_DOUBLE_CLICKED(): string;
    get CELL_VALUE(): string;
    get CELL_SELECTED(): string;
    get CELL_RESET(): string;
    get CELL_CHECKED(): string;
    get EDITING(): string;
    get RESIZED(): string;
    get DELETE(): string;
    get SELECTED_ALL(): string;
    get ERROR(): string;
    emitCellSelected(data: IGridEventCellSelected): void;
}
export interface IGridEventEditing {
    content: IPageContentCell;
    cell: IGridCell;
    editing: boolean;
    dirty: boolean;
}
export interface IGridEventCellClicked {
    cell: IGridCell;
    content: IPageContentCell;
    event: MouseEvent;
    rightClick: boolean;
    historyClick: boolean;
    heading: boolean;
}
export interface IGridEventCellChecked {
    cell: IGridCellDetails;
    content?: IPageContentCell;
    checked: boolean;
    value: any;
}
export interface IGridEventCellSelection {
    from: IGridCellDetails;
    to: IGridCellDetails;
    heading: boolean;
    inside: boolean;
    reference: string;
    keyboard: boolean;
    type: string;
}
export interface IGridEventCellSelected {
    reference: string;
    selection: IGridEventCellSelection;
    rightClick: boolean;
    event: MouseEvent | KeyboardEvent | TouchEvent;
    which: string;
    count: number;
    multiple: boolean;
}
export interface IGridEventDelete {
    cells: IGridCells;
    selection: IGridSelection;
    event: KeyboardEvent;
}
export interface IGridEventCellValue {
    cell: IGridCell;
    content: IPageContentCell;
    value: string | number | boolean;
}
export interface IGridEventResized {
    index: number;
    which: string;
    size: number;
}
export interface IGridEventError {
    message: string;
}
export default GridEvents;
