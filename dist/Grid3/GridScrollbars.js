"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = __importDefault(require("../Emitter"));
var GridScrollbars = /** @class */ (function (_super) {
    __extends(GridScrollbars, _super);
    function GridScrollbars(pageEl, mainEl) {
        var _this = _super.call(this) || this;
        _this.pageEl = pageEl;
        _this.mainEl = mainEl;
        _this.scrollOffset = 20;
        _this.fit = 'scroll';
        _this.scale = 1;
        _this.scrolling = false;
        _this.axis = '';
        _this.offset = 0;
        _this.start = 0;
        _this.onScroll = function (evt) {
            _this.updateHandles();
            _this.emit(_this.SCROLL_X);
            _this.emit(_this.SCROLL_Y);
        };
        _this.onResize = function (evt) {
            // this.update();
        };
        _this.onMouseWheel = function (evt) {
            if (_this.fit === 'contain')
                return;
            var offsetX = evt.deltaX ? (Math.abs(evt.deltaX) >= 100 ? Math.round(Math.abs(evt.deltaX) / 100) : 1) : 0;
            var offsetY = evt.deltaY ? (Math.abs(evt.deltaY) >= 100 ? Math.round(Math.abs(evt.deltaY) / 100) : 1) : 0;
            var directionX = evt.deltaX > 0 ? 1 : -1;
            var directionY = evt.deltaY > 0 ? 1 : -1;
            // console.log(offsetX, offsetY);
            if (_this.scrollTrackY.classList.contains('disabled')) {
                if (_this.fit !== 'width')
                    _this.mainEl.scrollLeft += 10 * directionY;
            }
            else {
                if (_this.fit !== 'width')
                    _this.mainEl.scrollLeft += offsetX * _this.scrollOffset * directionX;
                if (_this.fit !== 'height')
                    _this.mainEl.scrollTop += offsetY * _this.scrollOffset * directionY * (1 / _this.scale);
            }
            if ((_this.mainEl.scrollLeft > 0 && _this.mainEl.scrollWidth - _this.mainEl.scrollLeft > _this.mainEl.clientWidth) ||
                (_this.mainEl.scrollTop > 0 && _this.mainEl.scrollHeight - _this.mainEl.scrollTop > _this.mainEl.clientHeight)) {
                evt.preventDefault();
            }
        };
        _this.onMouseDownTrack = function (evt) {
            // console.log('ow');
            var target = evt.target;
            var axis = target.classList.contains('grid-scrollbar-x') ? 'x' : 'y';
            var handle = axis === 'x' ? _this.scrollHandleX : _this.scrollHandleY;
            var rect = handle.getBoundingClientRect();
            if (axis === 'x') {
                var scale = _this.mainEl.scrollWidth / _this.mainEl.clientWidth;
                _this.mainEl.scrollLeft += handle.clientWidth * scale * (evt.x < rect.left ? -1 : 1);
            }
            else {
                var scale = _this.mainEl.scrollHeight / _this.mainEl.clientHeight;
                _this.mainEl.scrollTop += (handle.clientHeight * scale * (evt.y < rect.top ? -1 : 1) * 1) / _this.scale;
            }
        };
        _this.onMouseDown = function (evt) {
            if (!evt.target)
                return;
            var target = evt.target;
            _this.scrolling = true;
            _this.axis = target.classList.contains('grid-scrollbar-handle-x') ? 'x' : 'y';
            _this.start = _this.axis === 'x' ? evt.x : evt.y;
            _this.offset = _this.axis === 'x' ? _this.mainEl.scrollLeft : _this.mainEl.scrollTop;
            _this.pageEl.classList.add("scrolling");
            _this.pageEl.classList.add("scrolling-" + _this.axis);
            evt.stopPropagation();
        };
        _this.onMouseMove = function (evt) {
            if (!_this.scrolling)
                return;
            if (_this.axis === 'x') {
                var offset = ((evt.x - _this.start) * (_this.mainEl.scrollWidth / _this.pageEl.clientWidth));
                // let offset = ((((evt.x - this.start) * this.mainEl.scrollWidth) / this.mainEl.clientWidth) * 1) / this.scale;
                // offset *= this.mainEl.scrollWidth / this.mainEl.clientWidth;
                _this.mainEl.scrollLeft += offset;
                _this.emit(_this.SCROLL_X);
                _this.start = evt.x;
            }
            if (_this.axis === 'y') {
                var offset = ((evt.y - _this.start) * (_this.mainEl.scrollHeight / _this.pageEl.clientHeight));
                _this.mainEl.scrollTop += offset;
                _this.emit(_this.SCROLL_Y);
                _this.start = evt.y;
            }
        };
        _this.onMouseUp = function (evt) {
            _this.scrolling = false;
            _this.pageEl.classList.remove('scrolling');
            _this.pageEl.classList.remove('scrolling-x');
            _this.pageEl.classList.remove('scrolling-y');
        };
        _this.mainEl.style.setProperty('overflow-x', 'hidden');
        _this.mainEl.style.setProperty('overflow-y', 'hidden');
        _this.scrollTrackX = document.createElement('div');
        _this.scrollTrackX.classList.add('grid-scrollbar', 'grid-scrollbar-x');
        _this.scrollTrackX.addEventListener('mousedown', _this.onMouseDownTrack);
        _this.scrollHandleX = document.createElement('div');
        _this.scrollHandleX.classList.add('grid-scrollbar-handle', 'grid-scrollbar-handle-x');
        _this.scrollTrackX.appendChild(_this.scrollHandleX);
        _this.scrollHandleX.addEventListener('mousedown', _this.onMouseDown);
        _this.scrollTrackY = document.createElement('div');
        _this.scrollTrackY.classList.add('grid-scrollbar', 'grid-scrollbar-y');
        _this.scrollTrackY.addEventListener('mousedown', _this.onMouseDownTrack);
        _this.scrollHandleY = document.createElement('div');
        _this.scrollHandleY.classList.add('grid-scrollbar-handle', 'grid-scrollbar-handle-y');
        _this.scrollTrackY.appendChild(_this.scrollHandleY);
        _this.scrollHandleY.addEventListener('mousedown', _this.onMouseDown);
        _this.mainEl.addEventListener('scroll', _this.onScroll);
        // window.addEventListener('resize', this.onResize);
        _this.pageEl.addEventListener('wheel', _this.onMouseWheel);
        window.addEventListener('mousemove', _this.onMouseMove);
        window.addEventListener('mouseup', _this.onMouseUp);
        return _this;
        // this.update();
    }
    Object.defineProperty(GridScrollbars.prototype, "SCROLL", {
        get: function () {
            return 'scroll';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridScrollbars.prototype, "SCROLL_X", {
        get: function () {
            return 'scroll-x';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridScrollbars.prototype, "SCROLL_Y", {
        get: function () {
            return 'scroll-y';
        },
        enumerable: true,
        configurable: true
    });
    GridScrollbars.prototype.update = function (fit, scale) {
        if (scale === void 0) { scale = 1; }
        if (fit)
            this.fit = fit;
        this.scale = scale;
        if (this.mainEl.scrollWidth > this.mainEl.clientWidth && this.fit !== 'width' && this.fit !== 'contain') {
            this.scrollTrackX.classList.remove('disabled');
            var size = (this.pageEl.clientWidth * this.mainEl.clientWidth) / this.mainEl.scrollWidth;
            // console.log(this.pageEl.clientWidth, this.mainEl.clientWidth / this.mainEl.scrollWidth);
            this.scrollHandleX.style.setProperty('width', size + "px");
        }
        else {
            this.scrollTrackX.classList.add('disabled');
        }
        if (this.mainEl.scrollHeight > this.mainEl.clientHeight && this.fit !== 'height' && this.fit !== 'contain') {
            this.scrollTrackY.classList.remove('disabled');
            var size = (this.pageEl.clientHeight * this.mainEl.clientHeight) / this.mainEl.scrollHeight;
            // let size: number = (this.pageEl.clientHeight / this.mainEl.scrollHeight) * this.pageEl.clientHeight;
            this.scrollHandleY.style.setProperty('height', size + "px");
        }
        else {
            this.scrollTrackY.classList.add('disabled');
        }
    };
    GridScrollbars.prototype.destroy = function () {
        // window.removeEventListener('resize', this.onResize);
        this.scrollTrackX.removeEventListener('mousedown', this.onMouseDownTrack);
        this.scrollHandleX.removeEventListener('mousedown', this.onMouseDown);
        this.scrollTrackY.removeEventListener('mousedown', this.onMouseDownTrack);
        this.scrollHandleY.removeEventListener('mousedown', this.onMouseDown);
        this.pageEl.removeEventListener('wheel', this.onMouseWheel);
        window.removeEventListener('mousemove', this.onMouseMove);
        window.removeEventListener('mouseup', this.onMouseUp);
    };
    GridScrollbars.prototype.updateHandles = function () {
        var posX = (this.pageEl.clientWidth * this.mainEl.scrollLeft) / this.mainEl.scrollWidth;
        this.scrollHandleX.style.setProperty('left', posX + "px");
        var posY = (this.pageEl.clientHeight * this.mainEl.scrollTop) / this.mainEl.scrollHeight;
        this.scrollHandleY.style.setProperty('top', posY + "px");
    };
    return GridScrollbars;
}(Emitter_1.default));
exports.GridScrollbars = GridScrollbars;
//# sourceMappingURL=GridScrollbars.js.map