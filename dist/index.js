'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var version = require('../package.json').version;
exports.version = version;
var Config_1 = require("./Config");
var Api_1 = require("./Api");
var Storage_1 = require("./Storage");
var Classy_1 = __importDefault(require("./Classy"));
var Utils_1 = __importDefault(require("./Utils"));
var Helpers_1 = __importDefault(require("./Helpers"));
var Auth_1 = require("./Auth");
var Crypto_1 = require("./Crypto");
var Page_1 = require("./Page/Page");
exports.Page = Page_1.Page;
var Clipboard_1 = require("./Clipboard");
exports.Clipboard = Clipboard_1.Clipboard;
var Tracking_1 = __importDefault(require("./Tracking"));
var Range_1 = require("./Page/Range");
exports.PermissionRange = Range_1.PermissionRange;
var Pdf_1 = __importStar(require("./Pdf"));
exports.Pdf = Pdf_1.Pdf;
// import { IParser, Parser, IParserColumnDefinition, ParserColumnDefinition, IPageCellStyle } from './Page/Parser';
var Grid_1 = require("./Grid4/Grid");
exports.Grid = Grid_1.Grid;
var Functions_1 = require("./Functions");
exports.Functions = Functions_1.Functions;
var Keys_1 = __importDefault(require("./Keys"));
var Buttons_1 = require("./Actions/Buttons");
exports.Button = Buttons_1.Button;
var Charts_1 = require("./Actions/Charts");
exports.Chart = Charts_1.Chart;
var Tasks_1 = require("./Actions/Tasks");
exports.Task = Tasks_1.Task;
exports.TaskActions = Tasks_1.TaskActions;
exports.TaskAction = Tasks_1.TaskAction;
exports.ShareTaskAction = Tasks_1.ShareTaskAction;
exports.DataTaskAction = Tasks_1.DataTaskAction;
exports.NotificationTaskAction = Tasks_1.NotificationTaskAction;
exports.ComponentTaskAction = Tasks_1.ComponentTaskAction;
var Styles_1 = require("./Actions/Styles");
exports.Style = Styles_1.Style;
exports.StyleCondition = Styles_1.StyleCondition;
var Folder_1 = require("./Folder");
exports.Folder = Folder_1.Folder;
var Usage_1 = __importDefault(require("./Usage"));
var Billing_1 = __importDefault(require("./Billing"));
var Organization_1 = __importDefault(require("./Organization"));
var Schema_1 = require("./Page/Schema");
exports.PageSchema = Schema_1.PageSchema;
var ScriptLoader = function (url) {
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = url;
    var x = document.getElementsByTagName('head')[0];
    x.appendChild(s);
};
exports.ScriptLoader = ScriptLoader;
var config = new Config_1.Config();
exports.config = config;
var storage = new Storage_1.StorageService(config);
exports.storage = storage;
var api = new Api_1.Api(config, storage);
exports.api = api;
var crypto = new Crypto_1.Crypto();
exports.crypto = crypto;
var auth = new Auth_1.Auth(config, api, storage);
exports.auth = auth;
var helpers = new Helpers_1.default();
exports.helpers = helpers;
var utils = new Utils_1.default();
exports.utils = utils;
var tracking = new Tracking_1.default(storage);
exports.tracking = tracking;
var usage = new Usage_1.default(api, auth);
exports.usage = usage;
var organization = new Organization_1.default(api, auth);
exports.organization = organization;
var keys = new Keys_1.default(storage);
exports.keys = keys;
var billing = new Billing_1.default(config, api, storage);
exports.billing = billing;
new Page_1.PageWrap(api, auth, storage, keys, crypto, config);
new Pdf_1.default(storage, config);
new Folder_1.FolderWrap(api, auth);
var Create = /** @class */ (function () {
    function Create(settings) {
        this.settings = settings;
        this.version = version;
        this.config = config;
        this.config.set(settings);
        this.classy = new Classy_1.default();
        this.helpers = helpers;
        this.utils = utils;
        this.crypto = crypto;
        this.storage = new Storage_1.StorageService(this.config);
        this.api = new Api_1.Api(this.config, this.storage);
        this.auth = new Auth_1.Auth(this.config, this.api, this.storage);
        this.tracking = new Tracking_1.default(this.storage);
        this.usage = new Usage_1.default(this.api, this.auth);
        this.organization = new Organization_1.default(this.api, this.auth);
        this.helpers = helpers;
        this.keys = keys;
        this.billing = billing;
        // this.Parser = Parser;
        // this.ParserColumnDefintion = ParserColumnDefinition;
        this.Grid = Grid_1.Grid;
        new Page_1.PageWrap(this.api, this.auth, this.storage, this.keys, this.crypto, this.config);
        this.Page = Page_1.Page;
        this.PermissionRange = Range_1.PermissionRange;
        this.Clipboard = Clipboard_1.Clipboard;
        this.Pdf = Pdf_1.Pdf;
        this.Functions = Functions_1.Functions;
        this.Button = Buttons_1.Button;
        this.Chart = Charts_1.Chart;
        this.Task = Tasks_1.Task;
        this.TaskActions = Tasks_1.TaskActions;
        this.TaskAction = Tasks_1.TaskAction;
        this.DataTaskAction = Tasks_1.DataTaskAction;
        this.ShareTaskAction = Tasks_1.ShareTaskAction;
        this.NotificationTaskAction = Tasks_1.NotificationTaskAction;
        this.ComponentTaskAction = Tasks_1.ComponentTaskAction;
        this.Style = Styles_1.Style;
        this.StyleCondition = Styles_1.StyleCondition;
        new Folder_1.FolderWrap(api, auth);
        this.Folder = Folder_1.Folder;
        this.PageSchema = Schema_1.PageSchema;
    }
    return Create;
}());
exports.Create = Create;
var ipushpull = {
    version: version,
    config: config,
    storage: storage,
    api: api,
    crypto: crypto,
    auth: auth,
    helpers: helpers,
    utils: utils,
    tracking: tracking,
    usage: usage,
    organization: organization,
    keys: keys,
    billing: billing,
    Create: Create,
    Page: Page_1.Page,
    Grid: Grid_1.Grid,
    Pdf: Pdf_1.Pdf,
    Functions: Functions_1.Functions,
    Clipboard: Clipboard_1.Clipboard,
    PermissionRange: Range_1.PermissionRange,
    ScriptLoader: ScriptLoader,
    Button: Buttons_1.Button,
    Chart: Charts_1.Chart,
    Task: Tasks_1.Task,
    TaskActions: Tasks_1.TaskActions,
    TaskAction: Tasks_1.TaskAction,
    DataTaskAction: Tasks_1.DataTaskAction,
    ShareTaskAction: Tasks_1.ShareTaskAction,
    NotificationTaskAction: Tasks_1.NotificationTaskAction,
    ComponentTaskAction: Tasks_1.ComponentTaskAction,
    Style: Styles_1.Style,
    StyleCondition: Styles_1.StyleCondition,
    Folder: Folder_1.Folder,
    PageSchema: Schema_1.PageSchema
};
exports.default = ipushpull;
//# sourceMappingURL=index.js.map