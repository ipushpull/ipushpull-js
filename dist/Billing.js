"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Api_1 = require("./Api");
var bluebird_1 = __importDefault(require("bluebird"));
var Billing = /** @class */ (function () {
    function Billing(config, api, storage) {
        this.config = config;
        this.api = api;
        this.storage = storage;
        this.customerInfo = {
            id: '',
            description: '',
            email: ''
        };
        this.cardInfo = {
            number: '',
            cvc: '',
            exp_month: '',
            exp_year: ''
        };
        return;
    }
    Billing.prototype.setCustomerId = function (id) {
        this.customerInfo.id = id;
    };
    Billing.prototype.self = function () {
        return this.fetch(this.config.billing_url + "/self");
    };
    Billing.prototype.getCard = function () {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            _this.fetch(_this.config.billing_url + "/cards")
                .then(function (res) {
                if (res.data && res.data.length) {
                    _this.customerCard = res.data[0];
                    resolve(_this.customerCard);
                    return;
                }
                resolve(false);
            })
                .catch(reject);
        });
        return p;
    };
    Billing.prototype.createCard = function (data) {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            _this.fetch(_this.config.billing_url + "/cards", 'POST', data)
                .then(function (res) {
                if (res) {
                    _this.customerCard = res;
                    resolve(_this.customerCard);
                    return;
                }
                resolve(false);
                return;
            })
                .catch(reject);
        });
        return p;
    };
    Billing.prototype.getCustomerInfo = function () {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            return _this.fetch(_this.config.billing_url + "/customers")
                .then(function (res) {
                if (res && !res.deleted) {
                    _this.customerInfo = res;
                    resolve(_this.customerInfo);
                    return;
                }
                resolve(false);
            })
                .catch(reject);
        });
        return p;
    };
    Billing.prototype.updateCustomerInfo = function (data) {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            _this.fetch(_this.config.billing_url + "/customers", 'PUT', {
                email: data.email,
                description: data.description,
                metadata: data.metadata
            })
                .then(function (res) {
                if (res) {
                    resolve(res);
                }
                else {
                    resolve(false);
                }
            })
                .catch(reject);
        });
        return p;
    };
    Billing.prototype.createCustomer = function (data) {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            _this.fetch(_this.config.billing_url + "/customers", 'POST', data)
                .then(function (res) {
                _this.customerInfo = res;
                resolve(_this.customerInfo);
            })
                .catch(reject);
        });
        return p;
    };
    Billing.prototype.getCustomerSubscription = function () {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            _this.fetch(_this.config.billing_url + "/customers/subscription")
                .then(function (res) {
                resolve(res);
            })
                .catch(reject);
        });
        return p;
    };
    Billing.prototype.deleteCustomerSubscription = function (data) {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            _this.fetch(_this.config.billing_url + "/customers/subscription", 'DELETE', data)
                .then(function (res) {
                resolve(res);
            })
                .catch(reject);
        });
        return p;
    };
    Billing.prototype.getInvoices = function () {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            _this.fetch(_this.config.billing_url + "/invoices")
                .then(function (res) {
                if (res.data && res.data.length) {
                    _this.invoices = res.data;
                    resolve(_this.invoices);
                    return;
                }
                resolve(false);
            })
                .catch(reject);
        });
        return p;
    };
    Billing.prototype.getInvoice = function (id) {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            _this.fetch(_this.config.billing_url + "/invoices/" + id)
                .then(function (res) {
                resolve(res);
            })
                .catch(reject);
        });
        return p;
    };
    Billing.prototype.getUpcomingInvoices = function () {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            _this.fetch(_this.config.billing_url + "/invoices/upcoming")
                .then(function (res) {
                console.log('getUpcomingInvoices', res);
                if (res.lines && res.lines.data.length) {
                    _this.upcomingInvoices = res;
                    resolve(_this.upcomingInvoices);
                    return;
                }
                resolve(false);
            })
                .catch(function (err) {
                if (err.statusCode === 404) {
                    resolve(false);
                }
                else {
                    reject(err);
                }
            });
        });
        return p;
    };
    Billing.prototype.downloadInvoiceUrl = function (invoiceId) {
        return this.config.billing_url + "/invoices/download/" + invoiceId;
    };
    Billing.prototype.getPlan = function () {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            _this.fetch(_this.config.billing_url + "/plans")
                .then(function (res) {
                if (!res.deleted) {
                    _this.planInfo = res;
                    resolve(_this.planInfo);
                    return;
                }
                resolve(false);
            })
                .catch(function (err) {
                if (err.statusCode === 404) {
                    resolve(false);
                }
                else {
                    reject(err);
                }
            });
        });
        return p;
    };
    Billing.prototype.getUsage = function () {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            _this.fetch(_this.config.billing_url + "/usage")
                .then(function (res) {
                resolve(res.data);
            })
                .catch(reject);
        });
        return p;
    };
    Billing.prototype.createIntent = function (data) {
        var _this = this;
        var p = new bluebird_1.default(function (resolve, reject) {
            _this.fetch(_this.config.billing_url + "/intents/" + data.type, 'POST')
                .then(function (res) {
                resolve(res);
            })
                .catch(reject);
        });
        return p;
    };
    Billing.prototype.fetch = function (url, method, body, buffer) {
        if (method === void 0) { method = 'GET'; }
        var data = {
            method: method,
            headers: {
                'Content-Type': 'application/json',
                Authorization: "Bearer " + this.api.tokens.access_token
            }
        };
        var token = '';
        if (this.storage) {
            token = this.storage.persistent.get('access_token');
        }
        if (!token && this.api.tokens && this.api.tokens.access_token) {
            token = this.api.tokens.access_token;
        }
        if (token) {
            data.headers.Authorization = "Bearer " + token;
        }
        if (body) {
            data.body = JSON.stringify(body);
        }
        var error = 0;
        var p = new bluebird_1.default(function (resolve, reject) {
            fetch(url, data)
                .then(function (response) {
                if (response.status >= 300) {
                    error = response.status;
                    if (response.status >= 500) {
                        reject(response);
                        return;
                    }
                }
                return buffer ? response.arrayBuffer() : response.json();
            })
                .then(function (response) {
                if (error) {
                    reject(new Api_1.ApiError({
                        method: method,
                        data: null,
                        code: error,
                        statusCode: error,
                        httpCode: error,
                        error: response.message,
                        message: response.message
                    }));
                }
                else {
                    resolve(response);
                }
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return p;
    };
    return Billing;
}());
exports.default = Billing;
//# sourceMappingURL=Billing.js.map