import { IPageContent } from './Page/Content';
export interface IEncryptionKey {
    name: string;
    passphrase: string;
}
export interface ICryptoService {
    decryptContent: (key: any, data: string) => IPageContent | null;
    encryptContent: (key: IEncryptionKey, data: IPageContent) => string | null;
}
export declare class Crypto implements ICryptoService {
    static _instance(): Crypto;
    /**
     * Decrypt page content using encryption key and encrypted string
     *
     * @param key
     * @param data
     * @returns {any}
     */
    decryptContent(key: any, data: string): IPageContent | null;
    /**
     * Encrypt raw page content with supplied encryption key
     *
     * @param key
     * @param data
     * @returns {any}
     */
    encryptContent(key: IEncryptionKey, data: IPageContent): string | null;
    /**
     * Use forge library"s util to hash passphrase
     *
     * @param passphrase
     * @returns {any}
     */
    private hashPassphrase;
    private libCheck;
}
