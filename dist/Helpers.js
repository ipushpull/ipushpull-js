"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var uuid = __importStar(require("uuid"));
var Helpers = /** @class */ (function () {
    function Helpers() {
        this.letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return;
    }
    Helpers.prototype.getUuid = function () {
        return uuid.v4();
    };
    Helpers.prototype.getNextValueInArray = function (value, arr) {
        if (!arr || !arr.length) {
            return value;
        }
        var n = value;
        for (var i = 0; i < arr.length; i++) {
            if (n !== arr[i]) {
                continue;
            }
            if (arr[i + 1] === undefined) {
                n = arr[0];
            }
            else {
                n = arr[i + 1];
            }
            break;
        }
        if (n === value) {
            n = arr[0];
        }
        return n;
    };
    Helpers.prototype.isNumber = function (value) {
        return !/^[0-9\.\-]+$/.test(value) || isNaN(value) ? false : true;
    };
    Helpers.prototype.convertToNumber = function (value) {
        // if (!isNaN(value)) return value;
        value = "" + value;
        var regex = /[0-9\.\-]+/gm;
        var str = "" + value;
        var m;
        var matches = [];
        while ((m = regex.exec(str)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            // The result can be accessed through the `m`-variable.
            m.forEach(function (match, groupIndex) {
                matches.push(match);
            });
        }
        if (!matches.length)
            return null;
        var segments = value.split(matches[0]);
        var suffix = "";
        segments.forEach(function (segment, index) {
            if (segment === "")
                return;
            suffix = segment;
        });
        var number = parseFloat(matches[0]);
        switch (suffix) {
            case "k":
            case "K":
                number *= 1000;
                break;
            case "m":
            case "M":
            case "mm":
            case "MM":
                number *= 1000000;
                break;
            default:
                break;
        }
        return number;
    };
    Helpers.prototype.getRefIndex = function (str, obj) {
        str = str.toUpperCase();
        var regex = /^[a-zA-Z]+/gm;
        var m;
        // let col = -1;
        var count = 0;
        var letters = "";
        while ((m = regex.exec(str)) !== null) {
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            m.forEach(function (match) {
                letters = match;
            });
        }
        var i;
        var j;
        var col = 0;
        for (i = 0, j = letters.length - 1; i < letters.length; i += 1, j -= 1) {
            col += Math.pow(this.letters.length, j) * (this.letters.indexOf(letters[i]) + 1);
        }
        col--;
        var numbers = str.match(/\d/g);
        var row = numbers ? parseFloat(numbers.join("")) - 1 : -1;
        return obj ? { row: row, col: col } : [row, col];
    };
    Helpers.prototype.toColumnName = function (num) {
        var name = "";
        for (var a = 1, b = 26; (num -= a) >= 0; a = b, b *= 26) {
            name = String.fromCharCode((num % b) / a + 65) + name;
        }
        return name;
    };
    Helpers.prototype.cellRange = function (str, rows, cols) {
        var _a = str.split(":"), from = _a[0], to = _a[1];
        var fromRow = 0, fromCol = 0, toRow = 0, toCol = 0;
        var isFromNumber = this.isNumber(from);
        var fromCell = this.getRefIndex(from, true);
        if (to) {
            var isToNumber = this.isNumber(to);
            var toCell = this.getRefIndex(to, true);
            // 1:? - multiple rows
            if (isFromNumber) {
                fromRow = parseInt(from) - 1;
                fromCol = 0;
            }
            else {
                // A1:? - cell range
                // A:? - multiple columns
                fromCol = fromCell.col;
                fromRow = fromCell.row;
                if (fromCell.row < 0) {
                    fromRow = 0;
                }
            }
            // ?:2, ?:-1 - to end of row
            if (isToNumber) {
                toRow = parseInt(to) < 0 ? rows - 1 : parseInt(to) - 1;
                toCol = cols - 1;
                // ?:B, ?:B3
            }
            else {
                toRow = toCell.row < 0 ? rows - 1 : toCell.row;
                toCol = toCell.col < 0 ? cols - 1 : toCell.col;
            }
        }
        else {
            // 1 - single row
            if (isFromNumber) {
                fromRow = parseInt(from) - 1;
                toRow = fromRow;
                fromCol = 0;
                toCol = cols - 1;
            }
            else {
                // A1 - single cell
                // A - single column
                fromCol = fromCell.col;
                fromRow = fromCell.row;
                toCol = fromCol;
                if (fromCell.row < 0) {
                    fromRow = 0;
                    toRow = rows - 1;
                }
                else {
                    toRow = fromRow;
                }
            }
        }
        return {
            from: { row: fromRow, col: fromCol },
            to: { row: toRow, col: toCol },
        };
    };
    Helpers.prototype.validHex = function (hex) {
        return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(hex);
    };
    Helpers.prototype.rgbToHex = function (rgb) {
        rgb = rgb.replace("rgba(", "").replace("rgb(", "").replace(")", "");
        var parts = rgb.split(",");
        if (parts.length === 4) {
            if (parts[3].trim() === "0") {
                return "FFFFFF";
            }
        }
        return this.componentToHex(parseInt(parts[0], 10)) + this.componentToHex(parseInt(parts[1], 10)) + this.componentToHex(parseInt(parts[2], 10));
    };
    Helpers.prototype.componentToHex = function (c) {
        var hex = c.toString(16);
        return hex.length === 1 ? "0" + hex : hex;
    };
    Helpers.prototype.addEvent = function (element, eventName, func) {
        if (!element) {
            return;
        }
        element.addEventListener(eventName, func, false);
        if (eventName === "click") {
            element.addEventListener("touchstart", func, false);
        }
    };
    Helpers.prototype.capitalizeFirstLetter = function (str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    };
    /**
     * Remove event from element
     *
     * @param element
     * @param eventName
     * @param func
     * @returns {any}
     */
    Helpers.prototype.removeEvent = function (element, eventName, func) {
        element.removeEventListener(eventName, func, false);
        if (eventName === "click") {
            element.removeEventListener("touchstart", func, false);
        }
    };
    Helpers.prototype.isTouch = function () {
        return "ontouchstart" in document.documentElement;
    };
    Helpers.prototype.isTouchDevice = function () {
        return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
            /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)));
    };
    Helpers.prototype.clickEvent = function () {
        return this.isTouch() ? "touchstart" : "click";
    };
    Helpers.prototype.openWindow = function (link, target, params) {
        if (target === void 0) { target = "_blank"; }
        if (params === void 0) { params = {}; }
        var paramsStr = "";
        for (var key in params) {
            if (!params.hasOwnProperty(key)) {
                continue;
            }
            paramsStr += key + "=" + params[key] + ",";
        }
        paramsStr = paramsStr.substring(0, paramsStr.length - 1);
        return window.open(link, target, paramsStr);
    };
    Helpers.prototype.createSlug = function (str) {
        return str.split(" ").join("_");
    };
    Helpers.prototype.getScrollbarWidth = function (parent) {
        var outer = document.createElement("div");
        outer.style.visibility = "hidden";
        outer.style.width = "100px";
        outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps
        if (parent)
            parent.appendChild(outer);
        else
            document.body.appendChild(outer);
        var widthNoScroll = outer.offsetWidth;
        // force scrollbars
        outer.style.overflow = "scroll";
        // add innerdiv
        var inner = document.createElement("div");
        inner.style.width = "100%";
        outer.appendChild(inner);
        var widthWithScroll = inner.offsetWidth;
        // remove divs
        if (outer.parentNode) {
            outer.parentNode.removeChild(outer);
        }
        return widthNoScroll - widthWithScroll;
    };
    Helpers.prototype.parseExcelDate = function (excelTimestamp) {
        return this.parseDateExcel(excelTimestamp);
    };
    Helpers.prototype.parseDateExcel = function (excelTimestamp) {
        var secondsInDay = 24 * 60 * 60;
        var excelEpoch = new Date(1899, 11, 31);
        var excelEpochAsUnixTimestamp = excelEpoch.getTime();
        var missingLeapYearDay = secondsInDay * 1000;
        var delta = excelEpochAsUnixTimestamp - missingLeapYearDay;
        var excelTimestampAsUnixTimestamp = excelTimestamp * secondsInDay * 1000;
        var parsed = excelTimestampAsUnixTimestamp + delta;
        return isNaN(parsed) ? null : parsed;
    };
    Helpers.prototype.toExcelDate = function (timestamp) {
        var secondsInDay = 24 * 60 * 60;
        var excelEpoch = new Date(1899, 11, 31);
        var excelEpochAsUnixTimestamp = excelEpoch.getTime();
        var missingLeapYearDay = secondsInDay * 1000;
        var delta = excelEpochAsUnixTimestamp - missingLeapYearDay;
        // const excelTimestampAsUnixTimestamp = timestamp
        var parsed = (timestamp - delta) / secondsInDay / 1000;
        return isNaN(parsed) ? null : parsed;
    };
    Helpers.prototype.getContrastYIQ = function (hexcolor) {
        hexcolor = hexcolor.replace("#", "");
        var r = parseInt(hexcolor.substr(0, 2), 16);
        var g = parseInt(hexcolor.substr(2, 2), 16);
        var b = parseInt(hexcolor.substr(4, 2), 16);
        var yiq = (r * 299 + g * 587 + b * 114) / 1000;
        return yiq >= 128 ? "light" : "dark";
    };
    Helpers.prototype.convertTime = function (timestamp) {
        if (typeof timestamp === "string") {
            timestamp = new Date(timestamp);
        }
        var hours = timestamp.getHours();
        if (hours < 10) {
            hours = "0" + hours;
        }
        var minutes = timestamp.getMinutes();
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        var seconds = timestamp.getSeconds();
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        return hours + ":" + minutes + ":" + seconds;
    };
    Helpers.prototype.isValidEmail = function (str) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(str);
    };
    Helpers.prototype.serializeObject = function (obj) {
        var str = "";
        for (var key in obj) {
            if (str != "") {
                str += "&";
            }
            str += key + "=" + encodeURI(obj[key]);
        }
        return str;
    };
    Helpers.prototype.getCellsReference = function (from, to, headingSelected) {
        var colFrom = this.toColumnName(from.col + 1);
        var rowFrom = from.row + 1;
        var colTo = this.toColumnName(to.col + 1);
        var rowTo = to.row + 1;
        if (headingSelected === "all") {
            return "1:-1";
        }
        else if (headingSelected === "col") {
            if (colFrom === colTo)
                return colFrom;
            return colFrom + ":" + colTo;
        }
        else if (headingSelected === "row") {
            if (rowFrom === rowTo)
                return "" + rowFrom;
            return rowFrom + ":" + rowTo;
        }
        if (colFrom === colTo && rowFrom === rowTo) {
            return "" + colFrom + rowFrom;
        }
        if (!rowTo) {
            return "" + colFrom + rowFrom + ":" + colTo;
        }
        return "" + colFrom + rowFrom + ":" + colTo + rowTo;
    };
    Helpers.prototype.safeTitle = function (str, replaceWith) {
        if (replaceWith === void 0) { replaceWith = "_"; }
        return str.replace(/[^a-zA-Z0-9_\.\-]/g, replaceWith);
    };
    Helpers.prototype.safeXmlChars = function (str) {
        var expressions = [/<div(.*?)<\/div>/gm, /<table(.*?)<\/table>/gm, /<[\w](.*?)\/>/gm];
        var m;
        var ignore = [];
        expressions.forEach(function (regex) {
            while ((m = regex.exec(str)) !== null) {
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                ignore.push(m[0]);
            }
        });
        ignore.forEach(function (s, i) {
            str = str.replace(s, "[$" + i + "]");
        });
        str = str
            // .replace(/\<br \/\>/g, '[BR]')
            .replace(/&/g, "&amp;")
            .replace(/>/g, "&gt;")
            .replace(/</g, "&lt;")
            .replace(/'/g, "&apos;")
            .replace(/"/g, "&quot;");
        // .replace(/\[BR\]/g, '<br />');
        ignore.forEach(function (s, i) {
            str = str.replace("[$" + i + "]", s);
        });
        return str;
    };
    Helpers.prototype.isTarget = function (el, target) {
        var clickedEl = target;
        while (clickedEl && clickedEl !== el) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl === el;
    };
    Helpers.prototype.formatNumber = function (n, precision) {
        if (precision === void 0) { precision = 2; }
        var nStr = n.toFixed(precision);
        nStr += "";
        var x = nStr.split(".");
        var x1 = x[0];
        var x2 = x.length > 1 ? "." + x[1] : "";
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, "$1" + "," + "$2");
        }
        return x1 + x2;
    };
    Helpers.prototype.sentenceCase = function (str) {
        if (str === null || str === "")
            return str;
        else
            str = str.toString();
        return str.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    };
    Helpers.prototype.tableFromArray = function (arr) {
        if (arr === void 0) { arr = []; }
        var table = "<div class=\"excel-table\"><table>";
        // columns
        arr.forEach(function (row, rowIndex) {
            if (!rowIndex) {
                table += "<thead>";
            }
            else if (rowIndex === 1) {
                table += "<tbody>";
            }
            table += "<tr>";
            var t = rowIndex ? "td" : "th";
            row.forEach(function (value) {
                table += "<" + t + ">" + value + "</" + t + ">";
            });
            table += "</tr>";
            if (!rowIndex) {
                table += "</thead>";
            }
            else if (rowIndex === arr.length - 1) {
                table += "</tbody>";
            }
        });
        // values
        table += "</table></div>";
        return table;
    };
    Helpers.prototype.getURLParameter = function (name, url) {
        var reg = new RegExp("[?|&]" + name + "=" + "([^&;]+?)(&|#|;|$)").exec(url || location.search);
        if (!reg)
            return null;
        return decodeURIComponent(reg[1].replace(/\+/g, "%20")) || null;
    };
    Helpers.prototype.arrayDiff = function (a1, a2) {
        return a1.filter(function (i) {
            return a2.indexOf(i) < 0;
        });
    };
    Helpers.prototype.expandCellsReference = function (str, direction, rowLen, colLen) {
        var index = this.cellRange(str, rowLen, colLen);
        var reference = this.getCellsReference(index.from, index.from);
        var colName = this.toColumnName(index.to.col + 1);
        if (direction === "down") {
            reference += ":" + colName;
        }
        else {
            reference += ":" + (index.to.row + 1);
        }
        return reference;
    };
    Helpers.prototype.downloadFile = function (contentType, name, data) {
        var types = {
            json: "text/json",
            csv: "text/csv",
        };
        for (var k in types) {
            if (!types.hasOwnProperty(k)) {
                continue;
            }
            if (contentType === k) {
                contentType = types[k];
                break;
            }
        }
        var a = document.createElement("a");
        var blob = new Blob([data], { type: contentType });
        a.href = URL.createObjectURL(blob);
        a.download = name;
        var e = document.createEvent("MouseEvents");
        e.initMouseEvent("click", true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        a.dispatchEvent(e);
    };
    Helpers.prototype.factorsOf = function (num) {
        var half = Math.floor(num / 2), // Ensures a whole number <= num.
        i, j;
        var factors = [];
        factors.push(1);
        // Determine our increment value for the loop and starting point.
        num % 2 === 0 ? ((i = 2), (j = 1)) : ((i = 3), (j = 2));
        for (i; i <= half; i += j) {
            if (num % i === 0) {
                factors.push(i);
            }
        }
        factors.push(num); // Always include the original number.
        return factors;
    };
    Helpers.prototype.getSymphonyStyles = function () {
        return [
            "background",
            "background-attachment",
            "background-blend-mode",
            "background-clip",
            "background-color",
            "background-image",
            "background-position",
            "background-repeat",
            "background-size",
            "border",
            "border-bottom",
            "border-bottom-color",
            "border-bottom-left-radius",
            "border-bottom-right-radius",
            "border-bottom-style",
            "border-bottom-width",
            "border-collapse",
            "border-color",
            "border-image",
            "border-image-outset",
            "border-image-repeat",
            "border-image-slice",
            "border-image-source",
            "border-image-width",
            "border-left",
            "border-left-color",
            "border-left-style",
            "border-left-width",
            "border-radius",
            "border-right",
            "border-right-color",
            "border-right-style",
            "border-right-width",
            "border-spacing",
            "border-style",
            "border-top",
            "border-top-color",
            "border-top-left-radius",
            "border-top-right-radius",
            "border-top-style",
            "border-top-width",
            "border-width",
            "box-shadow",
            "box-sizing",
            "caption-side",
            "clear",
            "color",
            "content",
            "counter-increment",
            "counter-reset",
            "display",
            "empty-cells",
            "font",
            "font-family",
            "font-kerning",
            "font-size",
            "font-size-adjust",
            "font-stretch",
            "font-style",
            "font-variant",
            "font-weight",
            "height",
            "letter-spacing",
            "line-height",
            "list-style",
            "list-style-image",
            "list-style-position",
            "list-style-type",
            "margin",
            "margin-bottom",
            "margin-left",
            "margin-right",
            "margin-top",
            "max-height",
            "max-width",
            "min-height",
            "min-width",
            "opacity",
            "outline",
            "outline-color",
            "outline-offset",
            "outline-style",
            "outline-width",
            "overflow",
            "overflow-x",
            "overflow-y",
            "padding",
            "padding-bottom",
            "padding-left",
            "padding-right",
            "padding-top",
            "table-layout",
            "text-align",
            "text-align-last",
            "text-decoration",
            "text-decoration-color",
            "text-decoration-line",
            "text-decoration-style",
            "text-indent",
            "text-justify",
            "text-overflow",
            "text-shadow",
            "text-transform",
            "visibility",
            "white-space",
            "width",
            "word-break",
            "word-spacing",
            "word-wrap",
        ];
    };
    /**
     * Returns array of keys in an object
     *
     * @param obj
     * @returns {Array}
     */
    Helpers.prototype.getObjectKeys = function (obj) {
        var keys = [];
        for (var k in obj) {
            if (!obj.hasOwnProperty(k)) {
                continue;
            }
            keys.push(k);
        }
        return keys;
    };
    /**
     * Deep copy of object
     *
     * @todo Should be using angular clone..
     *
     * @param o
     * @returns {any}
     */
    Helpers.prototype.cloneObject = function (o) {
        var g = function () {
            var gdcc = "__getDeepCircularCopy__";
            this.getGdcc = function () {
                return gdcc;
            };
        };
        var constants = new g();
        if (o !== Object(o)) {
            return o; // primitive value
        }
        var set = constants.getGdcc() in o, cache = o[constants.getGdcc()], result;
        if (set && typeof cache == "function") {
            return cache();
        }
        // else
        o[constants.getGdcc()] = function () {
            return result;
        }; // overwrite
        if (o instanceof Array) {
            result = [];
            for (var i = 0; i < o.length; i++) {
                result[i] = this.cloneObject(o[i]);
            }
        }
        else {
            result = {};
            for (var prop in o) {
                if (!o.hasOwnProperty(prop)) {
                    continue;
                }
                if (prop != constants.getGdcc()) {
                    result[prop] = this.cloneObject(o[prop]);
                }
                else if (set) {
                    result[prop] = this.cloneObject(cache);
                }
            }
        }
        if (set) {
            o[constants.getGdcc()] = cache; // reset
        }
        else {
            delete o[constants.getGdcc()]; // unset again
        }
        return result;
    };
    Helpers.prototype.isDarkModeEnabled = function () {
        return window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches;
    };
    Helpers.prototype.parseURL = function (str) {
        var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        var valid = pattern.test(str);
        if (!valid)
            return false;
        var parser = document.createElement("a");
        parser.href = str;
        var url = {
            protocol: parser.protocol,
            hostname: parser.hostname,
            port: parser.port,
            pathname: parser.pathname,
            search: parser.search,
            hash: parser.hash,
            host: parser.host,
            url: str,
        };
        // parser = undefined;
        return url;
    };
    Helpers.prototype.arrayMove = function (arr, old_index, new_index) {
        if (new_index >= arr.length) {
            var k = new_index - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr; // for testing
    };
    Helpers.prototype.generatePassword = function (len) {
        var length = len ? len : 10;
        var string = "abcdefghijklmnopqrstuvwxyz"; //to upper
        var numeric = "0123456789";
        var punctuation = "!@#$%^&*()_+~`|}{[]:;?><,./-=";
        var password = "";
        var character = "";
        // let crunch = true;
        while (password.length < length) {
            var entity1 = Math.ceil(string.length * Math.random() * Math.random());
            var entity2 = Math.ceil(numeric.length * Math.random() * Math.random());
            var entity3 = Math.ceil(punctuation.length * Math.random() * Math.random());
            var hold = string.charAt(entity1);
            hold = password.length % 2 == 0 ? hold.toUpperCase() : hold;
            character += hold;
            character += numeric.charAt(entity2);
            character += punctuation.charAt(entity3);
            password = character;
        }
        password = password
            .split("")
            .sort(function () {
            return 0.5 - Math.random();
        })
            .join("");
        return password.substr(0, len);
    };
    Helpers.prototype.chunkArray = function (arr, n) {
        return Array.from(Array(Math.ceil(arr.length / n)), function (_, i) { return arr.slice(i * n, i * n + n); });
    };
    Helpers.prototype.getNested = function (obj, path) {
        return path.split(".").reduce(function (obj, level) { return obj && obj[level]; }, obj);
    };
    Helpers.prototype.safeSymphonyUrl = function (str) {
        return str.replace(/\//gi, "_").replace(/\+/g, "-").replace(/\=/g, "");
    };
    Helpers.prototype.toNumberSuffix = function (n) {
        if (Math.abs(n / 1000000) >= 1) {
            return n / 1000000 + "MM";
        }
        if (Math.abs(n / 1000) >= 1) {
            return n / 1000 + "k";
        }
        return "" + n;
    };
    Helpers.prototype.normalizeCellSelection = function (from, to) {
        var selection = {
            from: { row: 0, col: 0 },
            to: { row: 0, col: 0 },
            range: "A1",
        };
        // if (!this.cellSelected) return;
        selection.from.col = from.col > to.col ? to.col : from.col;
        selection.to.col = to.col < from.col ? from.col : to.col;
        selection.from.row = from.row > to.row ? to.row : from.row;
        selection.to.row = to.row < from.row ? from.row : to.row;
        selection.range = this.getCellsReference(selection.from, selection.to);
        return selection;
    };
    return Helpers;
}());
exports.default = Helpers;
//# sourceMappingURL=Helpers.js.map