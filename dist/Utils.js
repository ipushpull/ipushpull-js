"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var underscore_1 = __importDefault(require("underscore"));
var _1 = require(".");
var Utils = /** @class */ (function () {
    function Utils() {
        return;
    }
    Object.defineProperty(Utils, "DEFAULT_CELL_STYLE", {
        get: function () {
            return {
                "background-color": "FFFFFF",
                color: "000000",
                "font-family": "Calibri",
                "font-size": "11pt",
                "font-style": "normal",
                "font-weight": "normal",
                "text-wrap": "normal",
                "text-align": "left",
                "vertical-align": "bottom",
                width: "64px",
                height: "20px",
                "number-format": "General",
                lbs: "none",
                lbw: "none",
                lbc: "none",
                rbs: "none",
                rbw: "none",
                rbc: "none",
                tbs: "none",
                tbw: "none",
                tbc: "none",
                bbs: "none",
                bbw: "none",
                bbc: "none",
            };
        },
        enumerable: true,
        configurable: true
    });
    Utils.prototype.parseApiError = function (err, def) {
        if (def === void 0) { def = ""; }
        var msg = def;
        if (err.data) {
            var keys = Object.keys(err.data);
            if (keys.length) {
                if (underscore_1.default.isArray(err.data[keys[0]])) {
                    msg = err.data[keys[0]][0];
                }
                else if (typeof err.data[keys[0]] === "string") {
                    msg = err.data[keys[0]];
                }
                else {
                    msg = def;
                }
            }
            else {
                msg = def;
            }
        }
        else {
            msg = def;
        }
        return msg;
    };
    Utils.prototype.clonePageContent = function (content, removeStyles) {
        var copy = [];
        var deltaStyle = {};
        var borders = ["t", "r", "b", "l"];
        for (var i = 0; i < content.length; i++) {
            copy[i] = [];
            var _loop_1 = function (j) {
                var cell = __assign({}, content[i][j]);
                var style = {};
                var hasBorder = [];
                borders.forEach(function (k) {
                    if (cell.style[k + "bs"] != "none")
                        hasBorder.push(k);
                });
                // let rb =  (cell.style.rbs != 'none')
                // let lb =  (cell.style.lbs != 'none')
                for (var k in cell.style) {
                    var cont = false;
                    if (cell.style[k] === deltaStyle[k])
                        cont = true;
                    // borders.forEach(b => {
                    //   if (cont && k.indexOf(`${b}b`) === 0 && hasBorder.includes(b)) cont = false;
                    // })
                    // if (cont && k.indexOf('rb') === 0 && rb) cont = false;
                    // if (cont && k.indexOf('lb') === 0 && lb) cont = false;
                    if (cont)
                        continue;
                    style[k] = cell.style[k];
                }
                deltaStyle = __assign({}, cell.style);
                cell.style = removeStyles ? {} : style;
                if (cell.index)
                    delete cell.index;
                // if (cell.formatting) delete cell.formatting;
                copy[i].push(cell);
            };
            for (var j = 0; j < content[i].length; j++) {
                _loop_1(j);
            }
        }
        return copy;
    };
    Utils.prototype.comparePageContent = function (currentContent, newContent, ignore_styles) {
        if (ignore_styles === void 0) { ignore_styles = false; }
        if (!currentContent || !newContent) {
            return false;
        }
        var diffContent = [];
        var sizeDiff = false;
        // compare
        for (var i = 0; i < newContent.length; i++) {
            for (var k = 0; k < newContent[i].length; k++) {
                var diff = false;
                if (currentContent[i] && currentContent[i][k]) {
                    // check for formatted value changes
                    if (newContent[i][k].formatted_value !== currentContent[i][k].formatted_value) {
                        diff = true;
                    }
                    // check for value changes
                    if (newContent[i][k].value !== currentContent[i][k].value) {
                        diff = true;
                    }
                    // check for style changes
                    if (!diff && !ignore_styles) {
                        for (var attr in newContent[i][k].style) {
                            if (newContent[i][k].style[attr] !== currentContent[i][k].style[attr]) {
                                diff = true;
                                if (["width", "height"].indexOf(attr) > -1)
                                    sizeDiff = true;
                                break;
                            }
                        }
                    }
                }
                else {
                    diff = true;
                }
                if (diff) {
                    if (!diffContent[i]) {
                        diffContent[i] = [];
                    }
                    diffContent[i][k] = newContent[i][k];
                }
            }
        }
        if (diffContent.length || newContent.length < currentContent.length || newContent[0].length < currentContent[0].length) {
            var rowDiff = newContent.length - currentContent.length;
            var colDiff = newContent[0].length - currentContent[0].length;
            return {
                content: newContent,
                content_diff: diffContent,
                colDiff: colDiff,
                rowDiff: rowDiff,
                sizeDiff: sizeDiff,
            };
        }
        return false;
    };
    Utils.prototype.mergePageContent = function (_original, _deltas, cellStyle) {
        var current = [];
        // let style: IPageCellStyle = merge({}, _defaultStyles);
        var style = cellStyle;
        var colWidths = [];
        for (var rowIndex = 0; rowIndex < _original.length; rowIndex++) {
            var row = _original[rowIndex];
            current[rowIndex] = [];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                if (cellStyle instanceof Array && cellStyle.length === 2) {
                    if (!rowIndex && cellStyle[0][colIndex])
                        style = cellStyle[0][colIndex];
                    else if (cellStyle[1][colIndex])
                        style = cellStyle[1][colIndex];
                }
                var col = void 0;
                if (_deltas && _deltas[rowIndex] && _deltas[rowIndex][colIndex]) {
                    col = _deltas[rowIndex][colIndex];
                    // col.style = merge({}, style, col.style);
                    col.style = __assign({}, row[colIndex].style, col.style);
                    style = __assign({}, style, row[colIndex].style);
                }
                else {
                    col = __assign({}, row[colIndex]);
                    col.style = __assign({}, style, col.style);
                    delete col.dirty;
                    if (col.checked !== undefined)
                        delete col.checked;
                    style = __assign({}, col.style);
                }
                if (!rowIndex && col.style) {
                    colWidths[colIndex] = col.style.width;
                }
                if (col.style)
                    col.style.width = colWidths[colIndex];
                // col.formatting = {};
                col.index = {
                    row: rowIndex,
                    col: colIndex,
                };
                current[rowIndex].push(col);
            }
        }
        return current;
    };
    Utils.prototype.mergePageContent2 = function (original, deltas) {
        var content = JSON.parse(JSON.stringify(original));
        deltas.forEach(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach(function (col, colIndex) {
                if (!col)
                    return;
                var cell = _1.helpers.getNested(content, rowIndex + "." + colIndex);
                if (!cell)
                    return;
                content[rowIndex][colIndex] = col;
            });
        });
        return content;
    };
    Utils.prototype.getDefaultCellStyle = function () {
        return {
            "background-color": "FFFFFF",
            color: "000000",
            "font-family": "Arial",
            "font-size": "11pt",
            "font-style": "normal",
            "font-weight": "normal",
            height: "20px",
            "number-format": "",
            "text-align": "left",
            "text-wrap": "normal",
            "vertical-align": "middle",
            width: "64px",
            tbs: "none",
            rbs: "none",
            bbs: "none",
            lbs: "none",
            tbc: "000000",
            rbc: "000000",
            bbc: "000000",
            lbc: "000000",
            tbw: "none",
            rbw: "none",
            bbw: "none",
            lbw: "none",
        };
    };
    Utils.prototype.cellStyleMap = function () {
        return [
            {
                short: "bc",
                long: "background-color",
            },
            {
                short: "c",
                long: "color",
            },
            {
                short: "ff",
                long: "font-family",
            },
            {
                short: "fs",
                long: "font-size",
            },
            {
                short: "fw",
                long: "font-weight",
            },
            {
                short: "h",
                long: "height",
            },
            {
                short: "f",
                long: "number-format",
            },
            {
                short: "ta",
                long: "text-align",
                values: true,
            },
            {
                short: "tw",
                long: "text-wrap",
            },
            {
                short: "va",
                long: "vertical-align",
                values: true,
            },
            {
                short: "w",
                long: "width",
            },
            {
                short: "tbs",
                long: "tbs",
                values: true,
            },
            {
                short: "rbs",
                long: "rbs",
                values: true,
            },
            {
                short: "bbs",
                long: "bbs",
                values: true,
            },
            {
                short: "lbs",
                long: "lbs",
                values: true,
            },
            {
                short: "tbc",
                long: "tbc",
                values: true,
            },
            {
                short: "rbc",
                long: "rbc",
                values: true,
            },
            {
                short: "bbc",
                long: "bbc",
                values: true,
            },
            {
                short: "lbc",
                long: "lbc",
                values: true,
            },
            {
                short: "tbw",
                long: "tbw",
                values: true,
            },
            {
                short: "rbw",
                long: "rbw",
                values: true,
            },
            {
                short: "bbw",
                long: "bbw",
                values: true,
            },
            {
                short: "lbw",
                long: "lbw",
                values: true,
            },
        ];
    };
    Utils.prototype.cellStyleValueMap = function () {
        return [
            { short: "s", long: "solid" },
            { short: "d", long: "dot" },
            { short: "a", long: "dash" },
            { short: "ad", long: "dashdot" },
            { short: "add", long: "dashdotdot" },
            { short: "sad", long: "slantdashdot" },
            { short: "db", long: "double" },
            { short: "h", long: "hairline" },
            { short: "t", long: "thin" },
            { short: "k", long: "thick" },
            { short: "m", long: "medium" },
            { short: "n", long: "normal" },
            { short: "b", long: "bold" },
            { short: "i", long: "italic" },
            { short: "T", long: "top" },
            { short: "M", long: "middle" },
            { short: "B", long: "bottom" },
            { short: "C", long: "center" },
            { short: "L", long: "left" },
            { short: "R", long: "right" },
            { short: "J", long: "justify" },
        ];
    };
    Utils.prototype.transformStyle = function (style, l2s) {
        var _this = this;
        var newStyle = {};
        Object.keys(style).forEach(function (key) {
            var map = _this.cellStyleMap().find(function (m) {
                if (l2s)
                    return m.long == key;
                return m.short == key;
            });
            if (!map)
                return;
            var v = style[key];
            if (map.values) {
                var value = _this.cellStyleValueMap().find(function (m) {
                    if (l2s)
                        return m.long == style[key];
                    return m.short == style[key];
                });
                if (value)
                    v = value.long;
            }
            newStyle[l2s ? map.short : map.long] = v;
        });
        return newStyle;
    };
    Utils.prototype.transformContent = function (rawContent) {
        var content = [];
        var style = this.getDefaultCellStyle();
        rawContent.values.forEach(function (row, rowIndex) {
            content[rowIndex] = [];
            row.forEach(function (value, col) {
                // formatted value
                var formatted_value = _1.helpers.getNested(rawContent.formatted_values, rowIndex + "." + col);
                if (formatted_value === undefined || formatted_value === null)
                    formatted_value = value;
                // styles
                var index = _1.helpers.getNested(rawContent.cell_styles, rowIndex + "." + col);
                var uStyle = _1.helpers.getNested(rawContent.unique_styles, "" + index);
                if (uStyle) {
                    // style = { ...style, ...this.transformStyle(uStyle) };
                    style = __assign({}, style, uStyle);
                }
                content[rowIndex].push({
                    value: value,
                    formatted_value: formatted_value,
                    style: style,
                    index: {
                        row: rowIndex,
                        col: col,
                    },
                });
            });
        });
        // custom fields
        if (rawContent.custom_fields) {
            Object.keys(rawContent.custom_fields).forEach(function (key) {
                var values = rawContent.custom_fields[key];
                if (!(values instanceof Array))
                    return;
                values.forEach(function (row, rowIndex) {
                    if (row === null)
                        return;
                    row.forEach(function (col, colIndex) {
                        if (col === null)
                            return;
                        var cell = _1.helpers.getNested(content, rowIndex + "." + colIndex);
                        if (!cell)
                            return;
                        cell[key] = col;
                    });
                });
            });
        }
        // optional fields
        if (rawContent.optional_fields) {
            Object.keys(rawContent.optional_fields).forEach(function (key) {
                var values = rawContent.optional_fields[key];
                if (!(values instanceof Array))
                    return;
                values.forEach(function (value) {
                    var cell = _1.helpers.getNested(content, value.row + "." + value.col);
                    if (!cell)
                        return;
                    cell[key] = value;
                });
            });
        }
        // params
        if (rawContent.parameter_mapping && rawContent.parameter_mapping instanceof Array) {
            rawContent.parameter_mapping.forEach(function (map) {
                var cell = _1.helpers.getNested(content, map.row + "." + map.col);
                if (!cell)
                    return;
                cell.map = map;
            });
        }
        // let links = helpers.getNested(rawContent, 'optional_fields.link') || [];
        // links.forEach((link: IPageContent2OptionalFieldsLink) => {
        //   let cell: IPageContentCell = helpers.getNested(content, `${link.row}.${link.col}`);
        //   if (!cell) return;
        //   cell.link = link;
        // });
        return content;
    };
    return Utils;
}());
exports.default = Utils;
//# sourceMappingURL=Utils.js.map