export interface IEmitter {
    on: (name: string, callback: any) => void;
    off: (name: string, callback: any) => void;
    emit: (name: string, args?: any) => void;
    register: (callback: any) => void;
    unRegister: (callback: any) => void;
    removeEvent(): void;
    removeEvents(): void;
}
interface IListeners extends Array<any> {
    [index: number]: any;
}
declare class Emitter implements IEmitter {
    listeners: IListeners;
    onListeners: any;
    constructor();
    on: (name: string, callback: any) => void;
    off: (name: string, callback: any) => void;
    register: (callback: any) => void;
    unRegister(callback: any): void;
    emit(name: string, args?: any): void;
    removeEvent(): void;
    removeEvents(): void;
}
export default Emitter;
