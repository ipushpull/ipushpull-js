import { IPageColumnDefs } from "./Page";
import { IPageCellStyle, IPageContent } from "./Content";
export interface IPageSchemaFormatColumn {
    style: IPageCellStyle;
}
export interface IPageSchemaFormat {
    [key: string]: IPageSchemaFormatColumn;
}
export interface IPageSchemaRows {
    header_rows: IPageSchemaFormat[];
    data_rows: IPageSchemaFormat[];
    [key: string]: any;
}
export interface IPageSchema {
    columns: IPageColumnDefs[];
    formats: IPageSchemaRows;
    auto_insert_rows: boolean;
    header_rows: boolean;
    parse(data: any): void;
    toString(): string;
    exportContent(): IPageContent;
    importContent(content: IPageContent, columnKeys: number[], rows?: number, hiddenColumns?: number[]): void;
    [key: string]: any;
}
export declare class PageSchema implements IPageSchema {
    columns: IPageColumnDefs[];
    formats: IPageSchemaRows;
    auto_insert_rows: boolean;
    header_rows: boolean;
    private utils;
    private helpers;
    constructor(data?: any);
    parse(data: any): void;
    get(): {
        columns: IPageColumnDefs[];
        formats: IPageSchemaRows;
        auto_insert_rows: boolean;
        header_rows: boolean;
    };
    toString(): string;
    importContent(content: IPageContent, columnKeys?: number[], rows?: number, hiddenColumns?: number[]): void;
    exportContent(): IPageContent;
    getColumnFormat(name: string, header?: boolean, rowIndex?: number): IPageSchemaFormatColumn;
}
