import EventEmitter, { IEmitter } from '../Emitter';
import { IApiService } from '../Api';
import { IAuthService } from '../Auth';
import { IStorageService } from '../Storage';
import { IIPPConfig } from '../Config';
export interface IPageProvider extends IEmitter {
    start: () => void;
    stop: () => void;
    destroy: () => void;
    getData: () => void;
}
export declare class Providers {
    rest: ProviderREST;
    socket: IPageProvider;
    constructor(ippApi: IApiService, ippAuth: IAuthService, ippStorage: IStorageService, ippConf: IIPPConfig);
}
export declare class ProviderREST extends EventEmitter implements IPageProvider {
    private _pageId?;
    private _folderId?;
    private _uuid?;
    private _stopped;
    private _requestOngoing;
    private _timer;
    private _timeout;
    private _seqNo;
    private _data;
    private _error;
    seqNo: number;
    requestOngoing: boolean;
    now: number;
    constructor(_pageId?: number | undefined, _folderId?: number | undefined, _uuid?: string | undefined);
    /**
     * Start polling for page updates
     */
    start(): void;
    /**
     * Stop polling for page updates
     */
    stop(): void;
    /**
     * Stop polling for page updates and stop all events
     */
    destroy(): void;
    getData(): any;
    /**
     * Start the actual polling (loop)
     */
    private startPolling;
    /**
     * Load page data from service
     * @param ignoreSeqNo
     * @returns {Promise<IPage>}
     */
    private load;
    private onError;
    /**
     * Temporary solution to get the required subset of data from full page object
     *
     * @param data
     * @returns {{id: number, seq_no: number, content_modified_timestamp: Date, content: any, content_modified_by: any, push_interval: number, pull_interval: number, is_public: boolean, description: string, encrypted_content: string, encryption_key_used: number, encryption_type_used: number, special_page_type: number}}
     */
    private tempGetContentOb;
    /**
     * Temporary solution to get the required subset of data from full page object
     *
     * @param data
     * @returns {any}
     */
    private tempGetSettingsOb;
}
export declare class ProviderSocket extends EventEmitter implements IPageProvider {
    private _pageId?;
    private _folderId?;
    static readonly SOCKET_EVENT_PAGE_ERROR: string;
    static readonly SOCKET_EVENT_PAGE_CONTENT: string;
    static readonly SOCKET_EVENT_PAGE_PUSH: string;
    static readonly SOCKET_EVENT_PAGE_SETTINGS: string;
    static readonly SOCKET_EVENT_PAGE_DATA: string;
    static readonly SOCKET_EVENT_PAGE_USER_JOINED: string;
    static readonly SOCKET_EVENT_PAGE_USER_LEFT: string;
    private _stopped;
    private _socket;
    private _wsUrl;
    private _redirectCounter;
    private _redirectLimit;
    private _data;
    private _error;
    constructor(_pageId?: number | undefined, _folderId?: number | undefined);
    /**
     * Start listening for content updates. If socket not connected yet, it will initialize the socket connection
     */
    start(): void;
    /**
     * Stop receiving page content updates (still keeps receiving settings/meta updates and other events)
     * This will NOT disconnect the socket, merely it will just stop listening for updates - throws them away
     */
    stop(): void;
    /**
     * Remove listeners for all socket events, disconnects socket and destroys object
     * @param hard
     */
    destroy(hard?: boolean): void;
    getData(): any;
    /**
     * Initialize socket connection with all listeners
     */
    private init;
    /**
     * Connect socket to a server. If client doesnt support websockets, it tries to fall back to long polling
     * @returns {SocketIOClient.Socket}
     */
    private connect;
    /**
     * onConnect event action
     */
    private onConnect;
    private onReconnectingError;
    /**
     * onReconnectError event action
     * Try and brake the previous redirect. Go back to root
     */
    private onReconnectError;
    /**
     * onDisconnect event action
     */
    private onDisconnect;
    /**
     * onRedirect event action
     * @param msg
     */
    private onRedirect;
    /**
     * onPageContent eent action
     * @param data
     */
    private onPageContent;
    /**
     * onPageSettings event action
     * @param data
     */
    private onPageSettings;
    /**
     * onPageError event action
     * @param data
     */
    private onPageError;
    /**
     * onOAuthError event action
     * @param data
     */
    private onOAuthError;
    /**
     * onAuthRefresh event action
     */
    private onAuthRefresh;
    /**
     * Determines if client supports websockets
     * @returns {boolean}
     */
    private supportsWebSockets;
}
