import { IActionsButton } from "../Actions/Buttons";
import { IPageSchema } from "./Schema";
import { IGridSelection } from "../Grid4/Grid";
export interface IPageContentLink {
    external: boolean;
    address: string;
}
export interface IPageCellStyle {
    "background-color"?: string;
    color?: string;
    "font-family"?: string;
    "font-size"?: string;
    "font-style"?: string;
    "font-weight"?: string;
    height?: string;
    "number-format"?: string;
    "text-align"?: string;
    "text-wrap"?: string;
    width?: string;
    tbs?: string;
    rbs?: string;
    bbs?: string;
    lbs?: string;
    tbc?: string;
    rbc?: string;
    bbc?: string;
    lbc?: string;
    tbw?: string;
    rbw?: string;
    bbw?: string;
    lbw?: string;
    [key: string]: any;
}
export interface IPageCellStyle2 {
    bc?: string;
    c?: string;
    ff?: string;
    fs?: string;
    fy?: string;
    fw?: string;
    h?: string;
    f?: string;
    ta?: string;
    tw?: string;
    w?: string;
    tbs?: string;
    rbs?: string;
    bbs?: string;
    lbs?: string;
    tbc?: string;
    rbc?: string;
    bbc?: string;
    lbc?: string;
    tbw?: string;
    rbw?: string;
    bbw?: string;
    lbw?: string;
    [key: string]: any;
}
export interface IPageCellFormatting {
    background?: string;
    color?: string;
}
export interface IPageContentCellIndex {
    row: number;
    col: number;
}
export interface IPageContentCell {
    value: string | number;
    formatted_value?: string | number;
    column_name?: string;
    default_value?: string;
    pk?: boolean;
    index?: IPageContentCellIndex;
    link?: IPageContentLink;
    style: IPageCellStyle;
    originalStyle?: IPageCellStyle;
    dirty?: boolean;
    access?: string;
    button?: IActionsButton;
    checked?: boolean;
    map?: IPageContent2ParamMap;
    [key: string]: any;
}
export interface IPageContent2 {
    values: IPageContent2Values[];
    formatted_values?: IPageContent2Values[];
    unique_styles?: IPageCellStyle2[];
    cell_styles?: IPageContent2CellStyles;
    optional_fields?: IPageContent2Fields;
    custom_fields?: IPageContent2Fields;
    parameter_mapping?: any[];
}
export interface IPageContent2Values extends Array<any> {
    [index: number]: any[];
}
export interface IPageContent2CellStyles extends Array<any> {
    [index: number]: number[];
}
export interface IPageContent2Fields {
    link?: any;
    tag?: any;
    hash?: any;
    [key: string]: any[];
}
export interface IPageContent2OptionalFieldsLink {
    external: boolean;
    address: string;
    row: number;
    col: number;
}
export interface IPageContent2ParamMap {
    row: number;
    col: number;
    page: number;
    name: string;
    description?: string;
}
export interface IPageContent extends Array<any> {
    [index: number]: IPageContentCell[];
}
export interface IPageDeltaContentCol {
    col_index: number;
    cell_content: IPageContentCell;
}
export interface IPageDeltaContentRow {
    row_index: number;
    cols: IPageDeltaContentCol[];
}
export interface IPageDelta {
    new_rows: number[];
    new_cols: number[];
    content_delta: IPageDeltaContentRow[];
}
export interface IPageQuery {
    updates?: any[];
    deletes?: any[];
    error?: string;
}
export interface IPageQueryWhere {
    [index: number]: IPageQueryWhereColumn[];
}
export interface IPageQueryWhereColumn {
    [key: string]: string;
}
export interface IPageCellUpdate {
    row: number;
    col: number;
    cell: IPageContentCell;
}
export interface IPageContentSchema {
    default_value?: string;
    display_name?: string;
    name: string;
    pk: boolean;
}
export interface IPageContentProvider {
    structured: boolean;
    deltas: IPageContent;
    current: IPageContent;
    original: IPageContent;
    canDoDelta: boolean;
    dirty: boolean;
    rowLen: number;
    colLen: number;
    permissions: any;
    parameterMapping: any;
    version: number;
    schema: IPageContentSchema[];
    sync(): void;
    update: (rawContent: IPageContent, clean?: boolean, replace?: boolean, ignore_permissions?: boolean) => void;
    updateDelta(content: IPageContent): void;
    reset: (content?: IPageContent) => void;
    getCellByRef: (ref: string) => IPageContentCell;
    getCell: (rowIndex: number, columnIndex: number) => IPageContentCell;
    getCells: (fromCell: IPageContentCellIndex, toCell: IPageContentCellIndex, valueOnly?: boolean) => any;
    getCellsByRange: (str: string, valueOnly?: boolean) => any;
    getRowCells: (rowIndex: number) => any;
    getColCells: (colIndex: number) => any;
    updateCell: (rowIndex: number, columnIndex: number, data: IPageContentCell, delta?: boolean) => void;
    updateCells(from: IPageContentCellIndex, to: IPageContentCellIndex, data: IPageContentCell): void;
    addRow: (index: number, direction?: string) => void;
    addColumn: (index: number, direction?: string) => void;
    removeRow: (index: number, direction?: string) => void;
    removeColumn: (index: number) => void;
    setColSize(col: number, value: number): void;
    setRowSize(row: number, value: number): void;
    getCellDeltas: () => IPageDelta;
    getQuery: (full?: boolean) => IPageQuery;
    getDelta: (clean?: boolean) => IPageDelta;
    getFull: () => IPageContent | IPageContent2;
    getHtml(from?: IPageContentCellIndex, to?: IPageContentCellIndex): any;
    flattenStyles(styles: IPageCellStyle, only?: string[]): string;
    createSchema(columnKeys: number[]): IPageSchema;
    getParamMap(row: number, col: number): IPageContent2ParamMap | undefined;
    getParamMapIndex(row: number, col: number): number;
    updateParamMap(data: IPageContent2ParamMap): void;
    removeParamMap(data: IPageContent2ParamMap): void;
}
export declare class PageContent implements IPageContentProvider {
    structured: boolean;
    canDoDelta: boolean;
    dirty: boolean;
    permissions: any;
    version: number;
    parameterMapping: IPageContent2ParamMap[];
    schema: IPageContentSchema[];
    private _original;
    private _current;
    private _deltas;
    private _newRows;
    private _newCols;
    private _utils;
    private _helpers;
    private borders;
    private cellStyles;
    readonly deltas: IPageContent;
    readonly current: IPageContent;
    readonly original: IPageContent;
    constructor(rawContent: IPageContent);
    update(rawContent: any, clean?: boolean, replace?: boolean, ignore_permissions?: boolean): void;
    sync(): void;
    updateDelta(content: IPageContent): void;
    reset(content?: IPageContent): void;
    readonly rowLen: number;
    readonly colLen: number;
    getCellByRef(ref: string): IPageContentCell;
    getCell(rowIndex: number, columnIndex: number): IPageContentCell;
    getCells(fromCell: any, toCell: any, valueOnly?: boolean, noIndex?: boolean): IPageContent;
    getCellsByIndexes(indexes: IPageContentCellIndex[]): IPageContent;
    getCellsByRange(str: string, valueOnly?: boolean, noIndex?: boolean): any;
    getRowCells(rowIndex: number): IPageContentCell[];
    getColCells(columnIndex: number): IPageContentCell[];
    updateCellByRef(ref: string, data: IPageContentCell): void;
    updateCell(rowIndex: number, columnIndex: number, data: IPageContentCell, delta?: Boolean): IPageContentCell;
    updateCells(from: IPageContentCellIndex, to: IPageContentCellIndex, data: IPageContentCell): void;
    addRow(index: number, direction?: string): void;
    shiftColumns(insertIndex: number, gridSelection: IGridSelection): void;
    moveColumns(fromIndex: number, toIndex: number, newIndex: number): void;
    addColumn(index: number, direction?: string): void;
    removeRow(index: number): void;
    removeColumn(index: number): void;
    setRowSize(row: number, value: number): void;
    setColSize(col: number, value: number): void;
    getDelta(): IPageDelta;
    getCellDeltas(): IPageDelta;
    getQuery(full?: boolean): IPageQuery;
    getFull(): IPageContent | IPageContent2;
    getHtml(from?: IPageContentCellIndex, to?: IPageContentCellIndex): any;
    flattenStyles(styles: IPageCellStyle, only?: string[]): string;
    createSchema(columnKeys: number[]): IPageSchema;
    getParamMap(row: number, col: number): IPageContent2ParamMap | undefined;
    getParamMapIndex(row: number, col: number): number;
    updateParamMap(data: IPageContent2ParamMap): void;
    removeParamMap(data: IPageContent2ParamMap): void;
    private addDelta;
    private defaultContent;
}
