import EventEmitter, { IEmitter } from '../Emitter';
import { IApiService } from '../Api';
import Promise from 'bluebird';
export declare class RangesWrap {
    constructor(ippApi: IApiService);
}
export interface IPageRangeRights {
    ro: number[];
    no: number[];
    [key: string]: any;
}
export interface IPageRange {
    name: string;
    start: string;
    end: string;
    rights: IPageRangeRights;
    freeze: boolean;
    button: boolean;
    style: boolean;
    notification: boolean;
    access: boolean;
    list: boolean;
}
export interface IPageRangeItem {
    name: string;
    toObject: () => IPageRange;
}
export interface IPagePermissionRange extends IPageRangeItem {
    rowStart: number;
    rowEnd: number;
    colStart: number;
    colEnd: number;
    getCellsReference(): string;
    setCellsReference(ref: string, rows: number, cols: number): void;
    setPermission: (userId: number, permission?: string) => void;
    getPermission: (userId: number) => string;
    togglePermission(userId: number, currentPermission: string | null): string;
}
export declare class PermissionRange implements IPagePermissionRange {
    name: string;
    rowStart: number;
    rowEnd: number;
    colStart: number;
    colEnd: number;
    private _permissions;
    constructor(name: string, rowStart?: number, rowEnd?: number, colStart?: number, colEnd?: number, permissions?: IPageRangeRights);
    getCellsReference(): string;
    setCellsReference(ref: string, rows: number, cols: number): void;
    /**
     * Set permission for a user
     * @param userId
     * @param permission
     */
    setPermission(userId: number, permission?: string | null): void;
    togglePermission(userId: number, currentPermission: string | null): string;
    /**
     * Get permission for a user
     * @param userId
     * @returns {string}
     */
    getPermission(userId: number): string;
    /**
     * Serialize range to final service-accepted object
     * @returns {{name: string, start: string, end: string, rights: IPageRangeRights, freeze: boolean}}
     */
    toObject(): IPageRange;
}
export declare class PermissionRangeWrap {
    constructor();
}
export interface IPageRangesCollection extends IEmitter {
    TYPE_PERMISSION_RANGE: string;
    EVENT_UPDATED: string;
    ranges: (IPageRangeItem)[];
    setRanges: (ranges: IPageRangeItem[]) => this;
    addRange: (range: IPageRangeItem) => this;
    removeRange: (rangeName: string) => this;
    save: () => Promise<any>;
    parse: (pageAccessRights: string) => IPageRangeItem[];
}
export declare class Ranges extends EventEmitter implements IPageRangesCollection {
    /**
     * List of ranges in collection
     * @type {Array}
     * @private
     */
    private _ranges;
    private _folderId;
    private _pageId;
    readonly TYPE_PERMISSION_RANGE: string;
    readonly TYPE_FREEZING_RANGE: string;
    readonly EVENT_UPDATED: string;
    /**
     * Getter for list of ranges
     * @returns {IPageRangeItem[]}
     */
    readonly ranges: (IPageRangeItem)[];
    constructor(folderId: number, pageId: number, pageAccessRights?: string);
    /**
     * Rewrites current collection of ranges with given ranges
     * @param ranges
     * @returns {ipushpull.Ranges}
     */
    setRanges(ranges: (IPageRangeItem)[]): this;
    /**
     * Adds range to collection. Also prevents duplicate names
     * @param range
     * @returns {ipushpull.Ranges}
     */
    addRange(range: IPageRangeItem): this;
    /**
     * Removes range from collection
     *
     * @param rangeName
     * @returns {ipushpull.Ranges}
     */
    removeRange(rangeName: string): this;
    /**
     * Save ranges to a page
     * @returns {Promise<IRequestResult>}
     */
    save(): Promise<any>;
    /**
     * Parse range data loaded from service into range collection
     * @param pageAccessRights
     * @returns {IPageRangeItem[]}
     */
    parse(pageAccessRights: string): IPageRangeItem[];
}
