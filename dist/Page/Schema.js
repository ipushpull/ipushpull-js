"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Utils_1 = __importDefault(require("../Utils"));
var Helpers_1 = __importDefault(require("../Helpers"));
var PageSchema = /** @class */ (function () {
    function PageSchema(data) {
        this.columns = [];
        this.formats = {
            header_rows: [],
            data_rows: [],
        };
        this.auto_insert_rows = true;
        this.header_rows = true;
        this.utils = new Utils_1.default();
        this.helpers = new Helpers_1.default();
        this.parse(data || {});
    }
    PageSchema.prototype.parse = function (data) {
        var _this = this;
        ["columns", "formats", "auto_insert_rows", "header_rows"].forEach(function (key) {
            if (!data[key])
                return;
            if (key === "formats") {
                if (data["formats"].header_rows)
                    _this["formats"].header_rows =
                        data["formats"].header_rows;
                if (data["formats"].data_rows)
                    _this["formats"].data_rows = data["formats"].data_rows;
            }
            else {
                _this[key] = data[key];
            }
        });
    };
    PageSchema.prototype.get = function () {
        return {
            columns: this.columns,
            formats: this.formats,
            auto_insert_rows: this.auto_insert_rows,
            header_rows: this.header_rows,
        };
    };
    PageSchema.prototype.toString = function () {
        return JSON.stringify(this.get());
    };
    PageSchema.prototype.importContent = function (content, columnKeys, rows, hiddenColumns) {
        var _this = this;
        if (columnKeys === void 0) { columnKeys = []; }
        if (rows === void 0) { rows = 2; }
        if (hiddenColumns === void 0) { hiddenColumns = []; }
        // if (!columnKeys || !columnKeys.length)
        //   throw new Error("At least one column need to be a primary key");
        if (content.length < 2)
            throw new Error("At least two rows are required");
        // check if header row contain values
        var hasValue = true;
        var hasPk = false;
        var hasDuplicatePk = false;
        var pkNames = [];
        var columns = [];
        var headerRow = {};
        var dataRow = {};
        var dataRows = [];
        content.forEach(function (row, rowIndex) {
            if (rowIndex > rows)
                return;
            dataRow = {};
            row.forEach(function (cell, colIndex) {
                var dataCell = content[1][colIndex];
                var name = _this.helpers.safeTitle("" + dataCell.formatted_value);
                if (!name)
                    hasValue = false;
                if (!rowIndex) {
                    // if (!cell.value) hasValue = false;
                    var headerCell = content[0][colIndex];
                    var pk = columnKeys.includes(colIndex) || headerCell.pk || false;
                    if (pk)
                        hasPk = true;
                    var defaultCell = content[2] ? content[2][colIndex] : null;
                    if (pkNames.includes(name)) {
                        hasDuplicatePk = true;
                    }
                    pkNames.push(name);
                    columns.push({
                        pk: pk,
                        name: name,
                        display_name: "" + cell.formatted_value,
                        default_value: defaultCell ? defaultCell.formatted_value : "",
                        hidden: hiddenColumns.includes(colIndex)
                    });
                    headerRow[name] = {
                        style: cell.style,
                    };
                }
                // let dataCell = content[1][colIndex];
                dataRow[name] = {
                    style: cell.style,
                };
            });
            if (rowIndex)
                dataRows.push(dataRow);
        });
        if (!hasValue)
            throw new Error("Header row is missing column names");
        if (!hasPk)
            throw new Error("No primary keys found");
        if (hasDuplicatePk)
            throw new Error("Duplicate primary keys found");
        this.columns = columns;
        this.formats.header_rows = [headerRow];
        this.formats.data_rows = dataRows;
    };
    PageSchema.prototype.exportContent = function () {
        var _this = this;
        // let content: IPageContent = [];
        if (!this.columns.length)
            throw new Error("No columns found");
        var headerRow = [];
        var dataRow1 = [];
        var dataRow2 = [];
        var dataRow3 = [];
        var dataRow4 = [];
        this.columns.forEach(function (def, colIndex) {
            var headerFormat = _this.getColumnFormat(def.name, true);
            var style = headerFormat.style;
            if (!colIndex) {
                style = __assign({}, _this.utils.getDefaultCellStyle(), style);
            }
            headerRow.push({
                value: "" + def.display_name,
                formatted_value: "" + def.display_name,
                style: style,
                index: {
                    row: 0,
                    col: colIndex,
                },
            });
            // field name
            dataRow1.push({
                value: "" + def.name,
                formatted_value: "" + def.name,
                style: _this.getColumnFormat(def.name, false, 0).style,
                index: {
                    row: 1,
                    col: colIndex,
                },
            });
            // default column value
            dataRow2.push({
                value: "" + (def.default_value || ""),
                formatted_value: "" + (def.default_value || ""),
                style: __assign({}, _this.getColumnFormat(def.name, false, 1).style),
                index: {
                    row: 2,
                    col: colIndex,
                },
            });
            // primary key
            dataRow3.push({
                value: "",
                formatted_value: "",
                style: __assign({}, _this.utils.getDefaultCellStyle(), { "text-align": "center" }),
                index: {
                    row: 3,
                    col: colIndex,
                },
            });
            // hidden column
            dataRow4.push({
                value: "",
                formatted_value: "",
                style: __assign({}, _this.utils.getDefaultCellStyle(), { "text-align": "center" }),
                index: {
                    row: 4,
                    col: colIndex,
                },
            });
        });
        return this.utils.mergePageContent([
            headerRow,
            dataRow1,
            dataRow2,
            dataRow3,
            dataRow4,
        ]);
    };
    PageSchema.prototype.getColumnFormat = function (name, header, rowIndex) {
        if (rowIndex === void 0) { rowIndex = 0; }
        var key = header ? "header_rows" : "data_rows";
        var format = {
            style: {},
        };
        if (!this.formats[key].length ||
            !this.formats[key][rowIndex] ||
            !this.formats[key][rowIndex][name])
            return format;
        return this.formats[key][rowIndex][name];
    };
    return PageSchema;
}());
exports.PageSchema = PageSchema;
//# sourceMappingURL=Schema.js.map