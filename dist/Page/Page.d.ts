import EventEmitter, { IEmitter } from "../Emitter";
import { IPageContentProvider, IPageCellStyle } from "./Content";
import { IPageContent } from "./Content";
import { IPageRangesCollection } from "./Range";
import { IEncryptionKey, ICryptoService } from "../Crypto";
import { IApiService } from "../Api";
import { IAuthService } from "../Auth";
import { IStorageService } from "../Storage";
import { IIPPConfig } from "../Config";
import Promise from "bluebird";
import { IActions } from "../Actions/Actions";
import { IFunctions } from "../Functions";
import { IKeysService } from "../Keys";
export interface IPageColumns {
    index: number;
    column: string;
    value: string;
}
export interface IPageColumnDefs {
    name: string;
    pk?: boolean;
    hidden?: boolean;
    display_name?: string | number;
    default_value?: string | number;
    style?: IPageCellStyle;
    width?: number;
}
export interface IPageTypes {
    regular: number;
    pageAccessReport: number;
    domainUsageReport: number;
    globalUsageReport: number;
    pageUpdateReport: number;
    alert: number;
    pdf: number;
    liveUsage: number;
}
export interface IPageServiceContent {
    id: number;
    seq_no: number;
    content_modified_timestamp: Date;
    content: IPageContent;
    content_modified_by: any;
    push_interval: number;
    pull_interval: number;
    is_public: boolean;
    description: string;
    encrypted_content: string;
    encryption_key_used: string;
    encryption_type_used: number;
    special_page_type: number;
}
export interface IPageServiceMeta {
    by_name_url: string;
    id: number;
    name: string;
    description: string;
    url: string;
    uuid: string;
    access_rights: string;
    range_access: string;
    action_definitions: string;
    background_color: string;
    content: IPageContent;
    content_modified_by: any;
    content_modified_timestamp: Date;
    created_by: any;
    created_timestamp: Date;
    domain_id: number;
    domain_name: string;
    domain_url: string;
    encrypted_content: string;
    encryption_key_to_use: string;
    encryption_key_used: string;
    encryption_type_to_use: number;
    encryption_type_used: number;
    is_obscured_public: boolean;
    is_public: boolean;
    is_template: boolean;
    modified_by: any;
    modified_timestamp: Date;
    pull_interval: number;
    push_interval: number;
    record_history: boolean;
    seq_no: number;
    show_gridlines: boolean;
    special_page_type: number;
    ws_enabled: boolean;
    symphony_sid: string;
    page_schema: any;
    workflow: any;
}
export interface IPage extends IPageServiceMeta {
    [key: string]: any;
}
export interface IUserPageDomainCurrentUserAccess {
    default_page_id: number;
    default_page_url: string;
    domain_id: number;
    domain_url: string;
    is_active: boolean;
    is_administrator: boolean;
    is_default_domain: boolean;
    is_pending: boolean;
    page_count: number;
    user_id: number;
    user_url: string;
    access_level: string;
}
export interface IUserPageDomainAccess {
    alerts_enabled: boolean;
    by_name_url: string;
    current_user_domain_access: IUserPageDomainCurrentUserAccess;
    description: string;
    display_name: string;
    domain_type: number;
    encryption_enabled: boolean;
    id: number;
    is_page_access_mode_selectable: boolean;
    is_paying_customer: boolean;
    login_screen_background_color: "";
    logo_url: string;
    name: string;
    page_access_mode: number;
    page_access_url: string;
    url: string;
    default_symphony_sid: string;
}
export interface IUserPageAccess {
    by_name_url: string;
    content_by_name_url: string;
    content_url: string;
    domain: IUserPageDomainAccess;
    domain_id: number;
    domain_name: string;
    domain_url: string;
    encryption_to_use: number;
    encryption_key_to_use: string;
    id: number;
    is_administrator: boolean;
    is_public: boolean;
    is_users_default_page: boolean;
    name: string;
    pull_interval: number;
    push_interval: number;
    special_page_type: number;
    url: string;
    write_access: boolean;
    ws_enabled: boolean;
    connected_page: boolean;
}
export interface IPageTemplate {
    by_name_url: string;
    category: string;
    description: string;
    domain_id: number;
    domain_name: string;
    id: number;
    name: string;
    special_page_type: number;
    url: string;
    uuid: string;
}
export interface IPageCloneOptions {
    clone_ranges?: boolean;
}
export interface IPageCopyOptions {
    content?: boolean;
    access_rights?: boolean;
    settings?: boolean;
}
export interface IPageColumnStyles extends Array<any> {
    [index: number]: IPageCellStyle[];
}
export interface IPageMain extends IEmitter {
    TYPE_REGULAR: number;
    TYPE_ALERT: number;
    TYPE_PDF: number;
    TYPE_STRUCTURED: number;
    TYPE_PAGE_ACCESS_REPORT: number;
    TYPE_DOMAIN_USAGE_REPORT: number;
    TYPE_GLOBAL_USAGE_REPORT: number;
    TYPE_PAGE_UPDATE_REPORT: number;
    TYPE_LIVE_USAGE_REPORT: number;
    EVENT_READY: string;
    EVENT_NEW_CONTENT: string;
    EVENT_NEW_CONTENT_DELTA: string;
    EVENT_NEW_META: string;
    EVENT_RANGES_UPDATED: string;
    EVENT_COLUMN_DEFS_UPDATED: string;
    EVENT_ACCESS_UPDATED: string;
    EVENT_DECRYPTED: string;
    EVENT_ERROR: string;
    EVENT_RESET: string;
    ready: boolean;
    decrypted: boolean;
    updatesOn: boolean;
    types: IPageTypes;
    encryptionKeyPull: IEncryptionKey;
    encryptionKeyPush: IEncryptionKey;
    data: IPage;
    access: IUserPageAccess;
    Content: IPageContentProvider;
    Ranges: IPageRangesCollection;
    start: () => void;
    stop: () => void;
    push: (forceFull?: boolean) => Promise<any>;
    saveMeta: (data: any) => Promise<any>;
    destroy: () => void;
    reset: () => void;
    decrypt: (key: IEncryptionKey) => void;
    clone: (folderId: number, name: string, options?: IPageCloneOptions) => Promise<IPageMain>;
    copy: (folderId: number, name: string, options?: IPageCopyOptions) => Promise<IPageMain>;
    canEdit: () => boolean;
    canEditSettings(): boolean;
    getColumnNames(): IPageColumns[];
    getUniqueValuesForColumn(index: number, type: string, headersRows?: number): any[];
    reloadActions(): void;
    resetActions(): void;
    getColumnDefs(): IPageColumnDefs[];
    getColumnStyles(): IPageColumnStyles;
    getRow: (rowIndex: number) => any;
    insertRow: (data: any) => Promise<any>;
    updateRow: (rowIndex: number, data: any) => Promise<any>;
    removeRow: (rowIndex: number | number[]) => Promise<any>;
}
export interface IPageService {
    new (pageId: number | string, folderId: number | string, uuid?: string): IPageMain;
    create(folderId: number, name: string, type?: number, template?: IPageTemplate, organization_public?: boolean): Promise<IPageMain>;
}
export declare class PageWrap {
    constructor(ippApi: IApiService, ippAuth: IAuthService, ippStorage: IStorageService, ippKeys: IKeysService, ippCrypto: ICryptoService, ippConf: IIPPConfig);
}
export declare class Page extends EventEmitter {
    readonly TYPE_REGULAR: number;
    readonly TYPE_ALERT: number;
    readonly TYPE_NOTIFICATION: number;
    readonly TYPE_PDF: number;
    readonly TYPE_STRUCTURED: number;
    readonly TYPE_PAGE_ACCESS_REPORT: number;
    readonly TYPE_DOMAIN_USAGE_REPORT: number;
    readonly TYPE_GLOBAL_USAGE_REPORT: number;
    readonly TYPE_PAGE_UPDATE_REPORT: number;
    readonly TYPE_LIVE_USAGE_REPORT: number;
    readonly EVENT_READY: string;
    readonly EVENT_DECRYPTED: string;
    readonly EVENT_NEW_CONTENT: string;
    readonly EVENT_NEW_CONTENT_DELTA: string;
    readonly EVENT_EMPTY_UPDATE: string;
    readonly EVENT_NEW_META: string;
    readonly EVENT_RANGES_UPDATED: string;
    readonly EVENT_ACTIONS_UPDATED: string;
    readonly EVENT_COLUMN_DEFS_UPDATED: string;
    readonly EVENT_ACCESS_UPDATED: string;
    readonly EVENT_ERROR: string;
    readonly EVENT_RESET: string;
    /**
     * Indicates when page is ready (both content and settings/meta are loaded)
     * @type {boolean}
     */
    ready: boolean;
    /**
     * Indicates if page is decrypted.
     * @type {boolean}
     */
    decrypted: boolean;
    /**
     * Indicates if page updates are on - page is requesting/receiving new updates
     * @type {boolean}
     */
    updatesOn: boolean;
    /**
     * Mapping list of page types label - id
     * @type {IPageTypes}
     */
    types: IPageTypes;
    /**
     * Class for page range manipulation
     * @type {IPageRangesCollection}
     */
    Ranges: IPageRangesCollection;
    /**
     * Class for page actions manipulation
     * @type {IActions}
     */
    Actions: IActions;
    Functions: IFunctions;
    /**
     * Page content provider class
     * @type {IPageContentProvider}
     */
    Content: IPageContentProvider;
    /**
     * Indicates if client supports websockets
     * @type {boolean}
     * @private
     */
    private _supportsWS;
    private _wsDisabled;
    private _checkAccess;
    /**
     * Object that holds page data provider
     * @type {IPageProvider}
     */
    private _provider;
    /**
     * Object that holds the setInterval object for requesting page access data
     * @type {number}
     */
    private _accessInterval;
    /**
     * Holds page content and page meta together
     * @type {IPage}
     */
    private _data;
    /**
     * Holds page access values
     * @type {IUserPageAccess}
     */
    private _access;
    private _pageId;
    private _folderId;
    private _pageName;
    private _folderName;
    private _uuid;
    private _contentLoaded;
    private _metaLoaded;
    private _accessLoaded;
    private _hasAccess;
    private _error;
    private _destroyed;
    private _encryptionKeyPull;
    private _encryptionKeyPush;
    /**
     * Creates new page in the system
     *
     * @param folderId
     * @param name
     * @param type
     * @param template
     * @returns {Promise<IPageMain>}
     */
    static create(folderId: number, name: string, type?: number, template?: IPageTemplate, organization_public?: boolean): Promise<IPageMain>;
    /**
     * Starts new page object
     *
     * @param pageId
     * @param folderId
     */
    constructor(pageId: number | string, folderId: number | string, uuid?: string);
    /**
     * Setter for pull encryption key
     * @param key
     */
    encryptionKeyPull: IEncryptionKey;
    /**
     * Setter for push encryption key
     * @param key
     */
    encryptionKeyPush: IEncryptionKey;
    /**
     * Getter for page data
     * @returns {IPage}
     */
    readonly data: IPage;
    /**
     * Getter for page access
     * @returns {IUserPageAccess}
     */
    readonly access: IUserPageAccess;
    readonly error: boolean;
    /**
     * Start page updates
     */
    start(): void;
    /**
     * Stop page updates
     */
    stop(): void;
    /**
     * Push new data to a page. This method accepts either full page content or delta content update
     * @param forceFull
     * @returns {Promise<any>}
     */
    push(forceFull?: boolean): Promise<any>;
    /**
     * Save page settings/meta
     * @param data
     * @returns {Promise<any>}
     */
    saveMeta(data: any): Promise<any>;
    /**
     * Sets current page as folders default for current user
     * @returns {Promise<IRequestResult>}
     */
    setAsFoldersDefault(): Promise<any>;
    /**
     * Deletes current page
     * @returns {Promise<IRequestResult>}
     */
    del(): Promise<any>;
    /**
     * Check if page is encrypted and decrypt it if it is
     * @param key
     */
    decrypt(key?: IEncryptionKey): void;
    /**
     * Destroy page object
     */
    destroy(): void;
    reset(): void;
    /**
     * Clone current page. Clones page content and some settings, can specify more options via options param.
     * @param folderId
     * @param name
     * @param options
     * @deprecated
     * @returns {Promise<IPageMain>}
     */
    clone(folderId: number, name: string, options?: IPageCloneOptions): Promise<IPageMain>;
    /**
     * Clone current page. Clones page content and some settings, can specify more options via options param.
     * @param folderId
     * @param name
     * @param copyContent
     * @param data
     * @returns {Promise<IPageMain>}
     */
    copy(folderId: number, name: string, options?: IPageCopyOptions): Promise<IPageMain>;
    canEdit(): boolean;
    canEditSettings(): boolean;
    getColumnNames(): IPageColumns[];
    getUniqueValuesForColumn(colIndex: number, type?: string, headersRows?: number): any[];
    getColumnDefs(): IPageColumnDefs[];
    getColumnStyles(): IPageColumnStyles;
    resetActions(): void;
    reloadActions(): void;
    getRow(rowIndex: number): any;
    insertRow(data: any): Promise<any>;
    updateCell(rowIndex: number, colIndex: number, data: any): void;
    updateRow(rowQuery: any, update: any): Promise<any>;
    removeRow(rowQuery: any): Promise<any>;
    private isRowDataValid;
    private createProvider;
    /**
     * Actual bootstrap of the page, starts page updates, registeres provider, starts page access updates
     */
    private init;
    private checkPageAccess;
    /**
     * In case page is requested with name, get page ID from service
     * @param folderName
     * @param pageName
     * @returns {Promise<any>}
     */
    private getPageId;
    /**
     * Load page access
     * @returns {Promise<any>}
     */
    private getPageAccess;
    /**
     * Register listeners. THese are listeners on events emitted from page providers, which are processed and then re-emitted to public
     */
    private registerListeners;
    private onPageError;
    /**
     * Push full content update
     * @param content
     * @returns {Promise<any>}
     */
    private pushFull;
    /**
     * Push delta content update
     * @param data
     * @returns {Promise<any>}
     */
    private pushDelta;
    /**
     * Check if page is considered to be ready
     */
    private checkReady;
    /**
     * Temporary fix to update page types. This will take any page report types and adds 1000 to them. This way can do easier filtering.
     * @param pageType
     * @returns {number}
     */
    private updatePageType;
    /**
     * Encrypt page content with given key
     * @param key
     * @param content
     * @returns {string}
     */
    private encrypt;
    private applyMetaUpdate;
    private getCellPermissions;
}
