export interface IClassy {
    hasClass(ele: HTMLElement, cls: string): boolean;
    addClass(ele: HTMLElement, cls: string): any;
    removeClass(ele: HTMLElement, cls: string): any;
}
declare class Classy implements IClassy {
    constructor();
    hasClass(ele: HTMLElement, cls: string): boolean;
    /**
     * Adds class to an element
     *
     * @param ele
     * @param cls
     * @returns {boolean}
     */
    addClass(ele: HTMLElement, cls: string): any;
    /**
     * Removes class from element
     *
     * @param ele
     * @param cls
     * @returns {boolean}
     */
    removeClass(ele: HTMLElement, cls: string): any;
}
export default Classy;
