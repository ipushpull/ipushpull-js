"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Config = /** @class */ (function () {
    function Config() {
        this._config = {
            api_url: 'https://www.ipushpull.com/api',
            api_version: '2.0',
            ws_url: 'https://www.ipushpull.com',
            web_url: 'https://www.ipushpull.com',
            app_url: 'https://www.ipushpull.com/pages',
            embed_url: 'https://www.ipushpull.com/embed',
            auth_url: 'https://www.ipushpull.com/auth',
            docs_url: 'https://docs.ipushpull.com',
            billing_url: 'https://www.ipushpull.com/billing',
            chat_url: '',
            help_url: '',
            api_key: '',
            api_secret: '',
            transport: 'polling',
            storage_prefix: 'ipp',
            cookie: {
                oauth_access_token: 'access_token',
                ouath_refresh_token: 'refresh_token',
                uuid: 'uuid'
            },
            client_version: '',
            uuid: '',
            hsts: true,
            maximum_pasted_cells: 10000,
            settings: {}
        };
        this.pdfjs = {
            worker: 'https://s3-eu-west-1.amazonaws.com/assets.ipushpull.com/js/pdfjs-dist/build/pdf.worker.min.js',
            load: [
                'https://s3-eu-west-1.amazonaws.com/assets.ipushpull.com/js/pdfjs-dist/web/compatibility.js',
                'https://s3-eu-west-1.amazonaws.com/assets.ipushpull.com/js/pdfjs-dist/build/pdf.min.js'
            ]
        };
    }
    Config.prototype.set = function (config) {
        if (!config) {
            return;
        }
        for (var prop in config) {
            if (config.hasOwnProperty(prop)) {
                this._config[prop] = config[prop];
            }
        }
        if (config.api_url && !config.ws_url) {
            var parts = config.api_url.split('/');
            this._config.ws_url = parts[0] + '//' + parts[2];
        }
        if (config.api_url) {
            var parts = config.api_url.split('/');
            var apiIndex = parts.indexOf('api');
            if (apiIndex == parts.length - 2) {
                this._config.api_version = parts.pop();
                this._config.api_url = parts.join('/');
            }
        }
    };
    Object.defineProperty(Config.prototype, "api_url", {
        get: function () {
            return this._config.api_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "api_version", {
        get: function () {
            return this._config.api_version;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "ws_url", {
        get: function () {
            return this._config.ws_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "web_url", {
        get: function () {
            return this._config.web_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "app_url", {
        get: function () {
            return this._config.app_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "embed_url", {
        get: function () {
            return this._config.embed_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "docs_url", {
        get: function () {
            return this._config.docs_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "chat_url", {
        get: function () {
            return this._config.chat_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "help_url", {
        get: function () {
            return this._config.help_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "billing_url", {
        get: function () {
            return this._config.billing_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "auth_url", {
        get: function () {
            return this._config.auth_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "api_key", {
        get: function () {
            return this._config.api_key;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "api_secret", {
        get: function () {
            return this._config.api_secret;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "transport", {
        get: function () {
            return this._config.transport;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "storage_prefix", {
        get: function () {
            return this._config.storage_prefix;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "cookie", {
        get: function () {
            return this._config.cookie;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "uuid", {
        get: function () {
            return this._config.uuid;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "client_version", {
        get: function () {
            return this._config.client_version;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "hsts", {
        get: function () {
            return this._config.hsts;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "maximum_pasted_cells", {
        get: function () {
            return this._config.maximum_pasted_cells;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "settings", {
        get: function () {
            return this._config.settings;
        },
        enumerable: true,
        configurable: true
    });
    Config.prototype.$get = function () {
        return this._config;
    };
    return Config;
}());
exports.Config = Config;
// export default Config;
//# sourceMappingURL=Config.js.map