import Emitter, { IEmitter } from './Emitter';
import { IApiService } from './Api';
import { IAuthService } from './Auth';
import Promise from 'bluebird';
export declare class FolderWrap {
    constructor(ippApi: IApiService, ippAuth: IAuthService);
}
export declare enum IFolderDataUserAccessLevel {
    FA = "FA",
    PA = "PA",
    RW = "RW",
    RO = "RO"
}
export interface IFolderDataUserAccess {
    default_page_id: number;
    default_page_url: string;
    domain_id: number;
    domain_url: string;
    is_active: boolean;
    is_administrator: boolean;
    access_level: IFolderDataUserAccessLevel;
    is_pending: false;
    is_default_domain: false;
    page_count: number;
    user_id: number;
    user_url: string;
}
export interface IFolderDataModifiedBy {
    id: number;
    url: string;
    screen_name: string;
    first_name: string;
    last_name: string;
}
export interface IFolderDataCreatedBy {
    id: number;
    url: string;
    screen_name: string;
    first_name: string;
    last_name: string;
}
export interface IFolderData {
    id: number;
    url: string;
    by_name_url: string;
    name: string;
    display_name: string;
    description: string;
    encryption_enabled: boolean;
    logo_url: string;
    login_screen_background_color: string;
    page_access_url: string;
    access_groups_url: string;
    users_url: string;
    created_by: IFolderDataCreatedBy;
    created_timestamp: string;
    modified_by: IFolderDataModifiedBy;
    modified_timestamp: string;
    current_user_domain_access: IFolderDataUserAccess;
    page_access_mode: number;
    is_page_access_mode_selectable: boolean;
    domain_type: boolean;
    is_paying_customer: boolean;
    default_push_interval: number;
    default_pull_interval: number;
    email_updates: boolean;
    email_updates_new_page: boolean;
    default_ws_enabled: boolean;
    default_symphony_sid: string;
}
export interface IFolderHistory {
    created_timestamp: string;
    agent: string;
    action_type: string;
    action_type_display: string;
    object: any;
}
export interface IFolder extends IEmitter {
    PAGE_ACCESS_BASIC: number;
    PAGE_ACCESS_ENTERPRISE: number;
    hasAccess: boolean;
    id: number | string;
    data: IFolderData;
    history: IFolderHistory[];
    load(id: number): Promise<any>;
    getData(): Promise<any>;
    getHistory(limit?: number): Promise<any>;
    getUserAccess(pageId?: number): Promise<any>;
    getOrgUsers(): Promise<any>;
    getDomainInvitations(): Promise<any>;
    getDomainUsers(): Promise<any>;
    updateUserDomainAccess(users: IFolderUser[], level?: IFolderDataUserAccessLevel, active?: boolean): Promise<any>;
    removeUserDomainAccess(users: IFolderUser[]): Promise<any>;
    addUserDomainAccess(users: IFolderOrgUser[], isEnterprise: boolean): Promise<any>;
    cancelUserDomainAccess(users: IFolderUser[]): Promise<any>;
}
export interface IFolderOrgUser {
    id: number;
    email: string;
    text: string;
    first_name: string;
    is_active: boolean;
    is_organization_admin: boolean;
    job_title: string;
    last_login: string;
    last_name: string;
}
export interface IFolderUserAccess {
    user_id: number;
    page_id: number;
    access: '';
}
export interface IFolderUserUser {
    id: number;
    email: string;
    email_count?: number;
    invited_user?: any;
}
export interface IFolderUser {
    access_level: IFolderDataUserAccessLevel;
    default_page_id: number | null;
    default_page_url: number | null;
    domain_id: number;
    domain_url: string;
    is_active: boolean;
    is_administrator: boolean;
    is_default_domain: boolean;
    is_pending: boolean;
    user: IFolderUserUser;
    own_page: boolean;
    access: IFolderUserAccess;
    invitation?: IFolderInvitation;
    _administrator?: boolean;
}
export interface IFolderPage {
    id: number;
    is_public: boolean;
    name: string;
    own_page: boolean;
    special_page_type: number;
}
export interface IFolderInvitationUser {
    id: number;
    url: string;
    screen_name: string;
    first_name: string;
    last_name: string;
}
export interface IFolderInvitationDomainAccess {
    is_active: boolean;
    is_administrator: boolean;
    is_pending: boolean;
}
export interface IFolderInvitationCreatedBy {
    id: number;
    url: string;
    screen_name: string;
    first_name: string;
    last_name: string;
}
export interface IFolderInvitation {
    id: number;
    url: string;
    email: string;
    invited_user: IFolderInvitationUser;
    domain: number;
    domain_access: IFolderInvitationDomainAccess;
    type: string;
    type_display: string;
    status: string;
    status_display: string;
    email_count: number;
    created_timestamp: string;
    created_by: IFolderInvitationCreatedBy;
    completed_timestamp: string | null;
    completed_by: string | null;
    is_complete: boolean;
    customer: any;
}
export declare class Folder extends Emitter implements IFolder {
    id: number;
    hasAccess: boolean;
    data: IFolderData;
    history: IFolderHistory[];
    orgUsers: IFolderOrgUser[];
    users: IFolderUser[];
    currentUsers: IFolderUser[];
    suspendedUsers: IFolderUser[];
    pendingUsers: IFolderUser[];
    invitedUsers: IFolderInvitation[];
    pendingAccess: any[];
    readonly PAGE_ACCESS_BASIC: number;
    readonly PAGE_ACCESS_ENTERPRISE: number;
    constructor(id?: number);
    load(id: number, pageId?: number): Promise<any>;
    getPageAccess(): Promise<any>;
    getData(): Promise<any>;
    getHistory(limit?: number): Promise<any>;
    getUserAccess(pageId?: number): Promise<any>;
    getOrgUsers(): Promise<any>;
    getDomainInvitations(): Promise<any>;
    getDomainUsers(): Promise<any>;
    updateUserDomainAccess(users: IFolderUser[], level?: IFolderDataUserAccessLevel, active?: boolean): Promise<any>;
    removeUserDomainAccess(users: IFolderUser[]): Promise<any>;
    addUserDomainAccess(users: IFolderOrgUser[], isEnterprise: boolean): Promise<any>;
    cancelUserDomainAccess(users: IFolderUser[]): Promise<any>;
}
