"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IActionType;
(function (IActionType) {
    IActionType["VALUE"] = "value";
    IActionType["COPY"] = "copy";
    IActionType["NOTIFICATION"] = "notification";
    IActionType["SHARE"] = "share";
    // JAVASCRIPT = 'javascript',
    IActionType["COMPONENT"] = "component";
})(IActionType = exports.IActionType || (exports.IActionType = {}));
var INotificationType;
(function (INotificationType) {
    INotificationType["CARD"] = "card";
    INotificationType["TEXT"] = "text";
})(INotificationType = exports.INotificationType || (exports.INotificationType = {}));
var Task = /** @class */ (function () {
    function Task(data) {
        this.ref = "";
        this.range = "";
        this.tasks = [];
        this.parse(data || {});
    }
    Task.prototype.parse = function (data) {
        var _this = this;
        ["ref", "range", "tasks"].forEach(function (key) {
            if (data[key] === undefined)
                return;
            if (key === "tasks") {
                _this.tasks = [];
                data[key].forEach(function (taskSetData) {
                    _this.tasks.push(new TaskActions(taskSetData));
                });
            }
            else {
                _this[key] = data[key];
            }
        });
    };
    return Task;
}());
exports.Task = Task;
var TaskActions = /** @class */ (function () {
    function TaskActions(data) {
        this.name = "";
        this.condition = "";
        this.actions = [];
        this.parse(data || {});
    }
    TaskActions.prototype.parse = function (data) {
        var _this = this;
        ["name", "condition", "actions"].forEach(function (key) {
            if (data[key] === undefined)
                return;
            if (key === "actions") {
                _this.actions = [];
                data[key].forEach(function (actionData) {
                    var actionSet = new TaskAction();
                    actionSet.parse(actionData);
                    _this.actions.push(actionSet);
                });
            }
            else {
                _this[key] = data[key];
            }
        });
    };
    return TaskActions;
}());
exports.TaskActions = TaskActions;
var TaskAction = /** @class */ (function () {
    function TaskAction(data) {
        this.type = IActionType.VALUE;
        this.offset = "";
        this.cell = "";
        this.parse(data || {});
    }
    TaskAction.prototype.parse = function (data) {
        var _this = this;
        ["type", "offset", "cell"].forEach(function (key) {
            if (data[key] === undefined)
                return;
            _this[key] = data[key];
        });
        TaskAction.TYPES.forEach(function (key) {
            if (key === "share") {
                _this.share = new ShareTaskAction(data[key]);
            }
            else if (key === "notification") {
                _this.notification = new NotificationTaskAction(data[key]);
            }
            else if (key === "data") {
                _this.data = new DataTaskAction(data[key]);
            }
            else if (key === "component") {
                _this.component = new ComponentTaskAction(data[key]);
            }
            else {
                if (data[key] === undefined)
                    return;
                _this[key] = data[key];
            }
        });
    };
    TaskAction.TYPES = [
        "share",
        "notification",
        "data",
        "component"
    ];
    return TaskAction;
}());
exports.TaskAction = TaskAction;
var ShareTaskAction = /** @class */ (function () {
    function ShareTaskAction(data) {
        this.title = "";
        this.subTitle = "";
        this.blurb = "";
        this.icon = "";
        this.parse(data || {});
    }
    ShareTaskAction.prototype.parse = function (data) {
        var _this = this;
        ["title", "subTitle", "blurb", "icon"].forEach(function (key) {
            if (data[key] === undefined)
                return;
            _this[key] = data[key];
        });
    };
    return ShareTaskAction;
}());
exports.ShareTaskAction = ShareTaskAction;
var NotificationTaskAction = /** @class */ (function () {
    function NotificationTaskAction(data) {
        this.type = INotificationType.TEXT;
        this.message = "";
        this.body = "";
        this.conversation_id = ""; // DEPREACATED
        this.allow_custom_text = false;
        this.allow_selected_chatrooms = false; // DEPREACATED
        this.send_to = "custom";
        this.destinations = [];
        this.notification_destinations = [];
        this.parse(data || {});
    }
    NotificationTaskAction.prototype.parse = function (data) {
        var _this = this;
        [
            "type",
            "message",
            "body",
            "conversation_id",
            "allow_custom_text",
            "allow_selected_chatrooms",
            "send_to",
            "destinations",
            "notification_destinations"
        ].forEach(function (key) {
            if (data[key] === undefined)
                return;
            _this[key] = data[key];
        });
    };
    return NotificationTaskAction;
}());
exports.NotificationTaskAction = NotificationTaskAction;
var DataTaskAction = /** @class */ (function () {
    function DataTaskAction(data) {
        this.value = "";
        this.formatted_value = "";
        this.reset_value = "";
        this.reset_time = "";
        this.action = "update";
        this.parse(data || {});
    }
    DataTaskAction.prototype.parse = function (data) {
        var _this = this;
        ["value", "formatted_value", "reset_value", "reset_time", "action"].forEach(function (key) {
            if (data[key] === undefined)
                return;
            _this[key] = data[key];
        });
    };
    return DataTaskAction;
}());
exports.DataTaskAction = DataTaskAction;
var ComponentTaskAction = /** @class */ (function () {
    function ComponentTaskAction(data) {
        this.name = "";
        this.type = "News";
        this.data = "";
        this.parse(data || {});
    }
    ComponentTaskAction.prototype.parse = function (data) {
        var _this = this;
        ["name", "type", "data"].forEach(function (key) {
            if (data[key] === undefined)
                return;
            _this[key] = data[key];
        });
    };
    return ComponentTaskAction;
}());
exports.ComponentTaskAction = ComponentTaskAction;
//# sourceMappingURL=Tasks.js.map