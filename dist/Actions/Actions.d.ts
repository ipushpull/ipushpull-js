import { IFunctions } from "../Functions";
import { IPageContent, IPageContentCellIndex, IPageContentProvider } from "../Page/Content";
import { IActionsButton } from "./Buttons";
import { IActionsStyle } from "./Styles";
import { ITask, ITaskActionSetActionNotification, ITaskActionSetActionShare, ITaskActionSetActionShareComponent } from "./Tasks";
import Emitter, { IEmitter } from "../Emitter";
import { IApiService } from "../Api";
import Promise from "bluebird";
import { IGridCell } from "../Grid4/GridCell";
import { IActionsState } from "./States";
import { IActionsChart } from "./Charts";
export interface IActionsFilters {
    enabled: boolean;
}
export interface IActionsConfig {
    states_menu: string;
}
export interface IActions extends IEmitter {
    EVENT_UPDATED: string;
    buttons: IActionsButton[];
    styles: IActionsStyle[];
    tasks: ITask[];
    freeze: IPageContentCellIndex;
    states: IActionsState[];
    charts: IActionsChart[];
    filters: IActionsFilters;
    config: IActionsConfig;
    [key: string]: any;
    parse(str: string): void;
    toString(): string;
    save(): Promise<any>;
    run(gridCell: IGridCell, canEdit?: boolean): IActionsRun;
    sendNotification(messageNotification: ITaskActionSetActionNotification, options?: IActionsSendNotificationOptions): Promise<any>;
    validateButton(button: IActionsButton, buttonIndex: number): string[];
    addButton(button: IActionsButton): IActionsButton[];
    removeButton(name: string): void;
    removeStyle(name: string): void;
    removeTask(name: string): void;
    addChart(chart: IActionsChart): IActionsChart[];
    removeChart(name: string): void;
}
export interface IActionsRun {
    cellUpdates: IPageContent;
    notifications: ITaskActionSetActionNotification[];
    shares: ITaskActionSetActionShare[];
    styles: IActionsStyle[];
    values: string[];
    components: ITaskActionSetActionShareComponent[];
}
export interface IActionsSendNotificationOptions {
    icon?: string;
    type?: string;
    write_access?: boolean;
}
export declare class Actions extends Emitter implements IActions {
    api: IApiService;
    Content: IPageContentProvider;
    Functions: IFunctions;
    folderId: number;
    pageId: number;
    readonly EVENT_UPDATED: string;
    buttons: IActionsButton[];
    styles: IActionsStyle[];
    tasks: ITask[];
    states: IActionsState[];
    freeze: IPageContentCellIndex;
    charts: IActionsChart[];
    filters: IActionsFilters;
    config: IActionsConfig;
    constructor(api: IApiService, Content: IPageContentProvider, Functions: IFunctions, folderId: number, pageId: number);
    parse(str: string): void;
    toString(): string;
    save(): Promise<any>;
    run(gridCell: IGridCell, canEdit?: boolean): IActionsRun;
    private reset;
    /**
     *
     * @param messageNotification
     * @param options
     *
     */
    sendNotification(messageNotification: ITaskActionSetActionNotification, options?: IActionsSendNotificationOptions): Promise<any>;
    validateButton(button: IActionsButton, buttonIndex: number): string[];
    addButton(button: IActionsButton): IActionsButton[];
    addChart(chart: IActionsChart): IActionsChart[];
    addState(state: IActionsState): IActionsState[];
    addStyle(style: IActionsStyle): IActionsStyle[];
    addTask(task: ITask): ITask[];
    removeButton(name: string): void;
    removeState(name: string): void;
    removeStyle(name: string): void;
    removeTask(name: string): void;
    removeChart(name: string): void;
}
