export declare enum IActionsButtonType {
    SELECT = "select",
    CHECKBOX = "checkbox",
    ROTATE = "rotate",
    CHAT = "chat",
    BUTTON = "button",
    DATE = "date",
    EVENT = "event",
    FILTER = "filter"
}
export interface IActionsButton {
    name: string;
    range: string;
    defaultValue: string;
    type: IActionsButtonType;
    options: any[];
    users: IActionsButtonUser[];
    checkboxTrueValue: any;
    checkboxFalseValue: any;
    checkboxToggle: string;
    checkboxShowValue: boolean;
    componentPlugin: string;
    filterHideInput: boolean;
    parse(data: any): void;
    [key: string]: any;
}
export interface IActionsButtonUser {
    name: string;
    id: string;
}
export declare class Button implements IActionsButton {
    name: string;
    range: string;
    defaultValue: string;
    type: IActionsButtonType;
    options: string[];
    users: IActionsButtonUser[];
    checkboxTrueValue: any;
    checkboxFalseValue: any;
    checkboxToggle: string;
    checkboxShowValue: boolean;
    dateValueFormat: string;
    dateDisplayFormat: string;
    componentPlugin: string;
    filterHideInput: boolean;
    constructor(data?: any);
    parse(data: any): void;
}
