"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var Style = /** @class */ (function () {
    function Style(data) {
        this.ref = "";
        this.range = "";
        this.conditions = [];
        this.default_style = {
            color: "",
            background: "",
            "font-weight": "",
            "font-style": "",
        };
        this.default_columns = ["C"];
        this.parse(data || {});
    }
    Style.prototype.parse = function (data) {
        var _this = this;
        ["ref", "range", "conditions", "default_style", "default_columns"].forEach(function (key) {
            if (!data[key])
                return;
            if (key === "style") {
                _this.default_style = __assign({}, _this.default_style, data[key]);
            }
            else {
                _this[key] = data[key];
            }
        });
    };
    return Style;
}());
exports.Style = Style;
var StyleCondition = /** @class */ (function () {
    function StyleCondition(data) {
        this.exp = "";
        this.style = {
            color: "#000000",
            background: "#FFFFFF",
            "font-weight": "",
            "font-style": "",
        };
        this.columns = ["C"];
        this.parse(data || {});
    }
    StyleCondition.prototype.parse = function (data) {
        var _this = this;
        ["exp", "style", "columns"].forEach(function (key) {
            if (!data[key])
                return;
            if (key === "style") {
                _this.style = __assign({}, _this.style, data[key]);
            }
            else {
                _this[key] = data[key];
            }
        });
    };
    return StyleCondition;
}());
exports.StyleCondition = StyleCondition;
//# sourceMappingURL=Styles.js.map