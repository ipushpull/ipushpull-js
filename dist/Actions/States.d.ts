import { IGridState } from '../Grid4/Grid';
export interface IActionsState {
    name: string;
    state: IGridState;
    parse(data: any): void;
    [key: string]: any;
}
export declare class ActionState implements IActionsState {
    name: string;
    state: IGridState;
    constructor(data?: any);
    parse(data: any): void;
}
