export interface ITask {
    ref: string;
    range: string;
    tasks: ITaskActionSet[];
    parse(data: any): void;
    [key: string]: any;
}
export interface ITaskActionSet {
    condition: string;
    actions: ITaskActionSetAction[];
    [key: string]: any;
    parse(data: any): void;
}
export declare enum IActionType {
    VALUE = "value",
    COPY = "copy",
    NOTIFICATION = "notification",
    SHARE = "share",
    COMPONENT = "component"
}
export interface ITaskActionSetAction {
    type: IActionType;
    offset: string;
    cell: string;
    share: ITaskActionSetActionShare;
    notification: ITaskActionSetActionNotification;
    data: ITaskActionSetActionData;
    component: ITaskActionSetActionShareComponent;
    [key: string]: any;
    parse(data: any): void;
}
export interface ITaskActionSetActionShare {
    title: string;
    subTitle: string;
    blurb: string;
    icon: string;
    [key: string]: any;
    parse(data: any): void;
}
export interface ITaskActionSetActionShareComponent {
    name: string;
    type: string;
    data: string;
    [key: string]: any;
    parse(data: any): void;
}
export declare enum INotificationType {
    CARD = "card",
    TEXT = "text"
}
export interface ITaskActionSetActionNotification {
    type: INotificationType;
    message: string;
    body: string;
    conversation_id: string;
    allow_custom_text: boolean;
    allow_selected_chatrooms: boolean;
    send_to: string;
    table?: any;
    destinations?: any[];
    notification_destinations?: number[];
    [key: string]: any;
    parse(data: any): void;
}
export interface ITaskActionSetActionData {
    value: string;
    formatted_value: string;
    action: string;
    reset_value?: string;
    reset_time?: string;
    [key: string]: any;
    parse(data: any): void;
}
export interface ITaskActionSetActionJavascript {
    code: string;
}
export declare class Task implements ITask {
    ref: string;
    range: string;
    tasks: ITaskActionSet[];
    constructor(data?: any);
    parse(data: any): void;
}
export declare class TaskActions implements ITaskActionSet {
    name: string;
    condition: string;
    actions: ITaskActionSetAction[];
    constructor(data?: any);
    parse(data: any): void;
}
export declare class TaskAction implements ITaskActionSetAction {
    static TYPES: string[];
    type: IActionType;
    offset: string;
    cell: string;
    share: ITaskActionSetActionShare;
    notification: ITaskActionSetActionNotification;
    data: ITaskActionSetActionData;
    component: ITaskActionSetActionShareComponent;
    constructor(data?: any);
    parse(data: any): void;
}
export declare class ShareTaskAction implements ITaskActionSetActionShare {
    title: string;
    subTitle: string;
    blurb: string;
    icon: string;
    constructor(data?: any);
    parse(data: any): void;
}
export declare class NotificationTaskAction implements ITaskActionSetActionNotification {
    type: INotificationType;
    message: string;
    body: string;
    conversation_id: string;
    allow_custom_text: boolean;
    allow_selected_chatrooms: boolean;
    send_to: string;
    destinations: never[];
    notification_destinations: never[];
    constructor(data?: any);
    parse(data: any): void;
}
export declare class DataTaskAction implements ITaskActionSetActionData {
    value: string;
    formatted_value: string;
    reset_value?: string;
    reset_time?: string;
    action: string;
    constructor(data?: any);
    parse(data: any): void;
}
export declare class ComponentTaskAction implements ITaskActionSetActionShareComponent {
    name: string;
    type: string;
    data: string;
    constructor(data?: any);
    parse(data: any): void;
}
