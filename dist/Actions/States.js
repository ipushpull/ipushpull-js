"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ActionState = /** @class */ (function () {
    function ActionState(data) {
        this.name = '';
        this.parse(data || {});
    }
    ActionState.prototype.parse = function (data) {
        var _this = this;
        ['name', 'state'].forEach(function (key) {
            if (!data[key])
                return;
            _this[key] = data[key];
        });
    };
    return ActionState;
}());
exports.ActionState = ActionState;
//# sourceMappingURL=States.js.map