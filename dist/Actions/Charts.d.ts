export interface IActionsChartOptionsXAxis {
    categories: string[];
}
export interface IActionsChartOptionsPlotBar {
    horizontal: boolean;
}
export interface IActionsChartOptionsPlot {
    bar?: IActionsChartOptionsPlotBar;
}
export interface IActionsChartOptionsLegend {
    show?: boolean;
}
export interface IActionsChartOptions {
    xaxis: IActionsChartOptionsXAxis;
    plotOptions?: IActionsChartOptionsPlot;
    legend?: IActionsChartOptionsLegend;
}
export interface IActionsChart {
    name: string;
    type: string;
    options: IActionsChartOptions;
    range: string;
    series: string[];
    [key: string]: any;
}
export declare class Chart implements IActionsChart {
    name: string;
    type: string;
    range: string;
    options: IActionsChartOptions;
    series: any[];
    constructor(data?: any);
    parse(data: any): void;
}
