"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var deepmerge_1 = __importDefault(require("deepmerge"));
var Chart = /** @class */ (function () {
    //   public series = [];
    function Chart(data) {
        this.name = '';
        this.type = 'bar';
        this.range = '';
        this.options = {
            plotOptions: {
                bar: {
                    horizontal: false
                }
            },
            xaxis: {
                categories: []
            },
            legend: {
                show: false,
            }
        };
        this.series = [];
        this.parse(data || {});
    }
    Chart.prototype.parse = function (data) {
        var _this = this;
        ['name', 'type', 'options', 'range', 'series'].forEach(function (key) {
            if (!data[key])
                return;
            _this[key] = ['options'].includes(key) ? deepmerge_1.default(_this.options, data[key]) : data[key];
        });
    };
    return Chart;
}());
exports.Chart = Chart;
//# sourceMappingURL=Charts.js.map