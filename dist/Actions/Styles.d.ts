export interface IActionsStyle {
    ref: string;
    range: string;
    conditions: IActionsStyleCondition[];
    default_style: IActionsStyleConditionStyle;
    default_columns: any;
    [key: string]: any;
    parse(data: any): void;
}
export interface IActionsStyleCondition {
    exp: string;
    style: IActionsStyleConditionStyle;
    columns: any[];
    [key: string]: any;
    parse(data: any): void;
}
export interface IActionsStyleConditionStyle {
    color: string;
    background: string;
    "font-weight": string;
    "font-style": string;
    [key: string]: any;
}
export declare class Style implements IActionsStyle {
    ref: string;
    range: string;
    conditions: IActionsStyleCondition[];
    default_style: IActionsStyleConditionStyle;
    default_columns: any;
    constructor(data?: any);
    parse(data: any): void;
}
export declare class StyleCondition implements IActionsStyleCondition {
    exp: string;
    style: IActionsStyleConditionStyle;
    columns: any[];
    constructor(data?: any);
    parse(data: any): void;
}
