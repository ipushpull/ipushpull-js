"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IActionsButtonType;
(function (IActionsButtonType) {
    IActionsButtonType["SELECT"] = "select";
    IActionsButtonType["CHECKBOX"] = "checkbox";
    IActionsButtonType["ROTATE"] = "rotate";
    IActionsButtonType["CHAT"] = "chat";
    IActionsButtonType["BUTTON"] = "button";
    IActionsButtonType["DATE"] = "date";
    IActionsButtonType["EVENT"] = "event";
    IActionsButtonType["FILTER"] = "filter";
})(IActionsButtonType = exports.IActionsButtonType || (exports.IActionsButtonType = {}));
var Button = /** @class */ (function () {
    function Button(data) {
        this.name = "";
        this.range = "";
        this.defaultValue = "";
        this.type = IActionsButtonType.BUTTON;
        this.options = [];
        this.users = [];
        this.checkboxTrueValue = "";
        this.checkboxFalseValue = "";
        this.checkboxToggle = "";
        this.checkboxShowValue = false;
        this.dateValueFormat = "YYYY-MM-DD";
        this.dateDisplayFormat = "YYYY-MM-DD";
        this.componentPlugin = "";
        this.filterHideInput = false;
        this.parse(data || {});
    }
    Button.prototype.parse = function (data) {
        var _this = this;
        [
            "type",
            "name",
            "defaultValue",
            "range",
            "options",
            "users",
            "checkboxTrueValue",
            "checkboxFalseValue",
            "checkboxToggle",
            "checkboxShowValue",
            "dateValueFormat",
            "dateDisplayFormat",
            "componentPlugin",
            "filterHideInput"
        ].forEach(function (key) {
            if (!data[key])
                return;
            _this[key] = data[key];
        });
    };
    return Button;
}());
exports.Button = Button;
//# sourceMappingURL=Buttons.js.map