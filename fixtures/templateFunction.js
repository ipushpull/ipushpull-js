const merge = require("deepmerge");

Cypress.Commands.add("__KEY__", (command) => {
  if (!command.data) {
    command.data = {};
  }
  if (!command.status) {
    command.status = {};
  }
  cy.server();
  __SETTINGS__;
});
