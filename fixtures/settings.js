if (command.routes.includes("getSelfInfo")) {
  cy.fixture("GET/200_getSelfInfo.json")
    .then((data) => {
      data = merge(data, command.data["getSelfInfo"] || {});
    })
    .as("getSelfInfo");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/users/self/?*",
    response: "@getSelfInfo",
  });
}

if (command.routes.includes("getIntegrationChannels")) {
  cy.fixture("GET/200_getIntegrationChannels.json")
    .then((data) => {
      data = merge(data, command.data["getIntegrationChannels"] || {});
    })
    .as("getIntegrationChannels");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/self/integration_channels/?*",
    response: "@getIntegrationChannels",
  });
}

if (command.routes.includes("getIntegrationChannel")) {
  cy.fixture("GET/200_getIntegrationChannel.json")
    .then((data) => {
      data = merge(data, command.data["getIntegrationChannel"] || {});
    })
    .as("getIntegrationChannel");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/integration_channels/*/?*",
    response: "@getIntegrationChannel",
  });
}

if (command.routes.includes("getCounterparties")) {
  cy.fixture("GET/200_getCounterparties.json")
    .then((data) => {
      data = merge(data, command.data["getCounterparties"] || {});
    })
    .as("getCounterparties");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/self/counterparties/?*",
    response: "@getCounterparties",
  });
}

if (command.routes.includes("getCounterparty")) {
  cy.fixture("GET/200_getCounterparty.json")
    .then((data) => {
      data = merge(data, command.data["getCounterparty"] || {});
    })
    .as("getCounterparty");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/counterparty/*?*",
    response: "@getCounterparty",
  });
}

if (command.routes.includes("getDomains")) {
  cy.fixture("GET/200_getDomains.json")
    .then((data) => {
      data = merge(data, command.data["getDomains"] || {});
    })
    .as("getDomains");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domains/?*",
    response: "@getDomains",
  });
}

if (command.routes.includes("getDomain")) {
  cy.fixture("GET/200_getDomain.json")
    .then((data) => {
      data = merge(data, command.data["getDomain"] || {});
    })
    .as("getDomain");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domains/*/?*",
    response: "@getDomain",
  });
}

if (command.routes.includes("getDomainPages")) {
  cy.fixture("GET/200_getDomainPages.json")
    .then((data) => {
      data = merge(data, command.data["getDomainPages"] || {});
    })
    .as("getDomainPages");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domains/*/page_access/?*",
    response: "@getDomainPages",
  });
}

if (command.routes.includes("getDomainsAndPages")) {
  cy.fixture("GET/200_getDomainsAndPages.json")
    .then((data) => {
      data = merge(data, command.data["getDomainsAndPages"] || {});
    })
    .as("getDomainsAndPages");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domain_page_access/?*",
    response: "@getDomainsAndPages",
  });
}

if (command.routes.includes("getPage")) {
  cy.fixture("GET/200_getPage.json")
    .then((data) => {
      data = merge(data, command.data["getPage"] || {});
    })
    .as("getPage");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domains/id/*/page_content/id/*/?*",
    response: "@getPage",
  });
}

if (command.routes.includes("getPageByName")) {
  cy.fixture("GET/200_getPageByName.json")
    .then((data) => {
      data = merge(data, command.data["getPageByName"] || {});
    })
    .as("getPageByName");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domains/name/*/page_content/name/*/?*",
    response: "@getPageByName",
  });
}

if (command.routes.includes("getPageByUuid")) {
  cy.fixture("GET/200_getPageByUuid.json")
    .then((data) => {
      data = merge(data, command.data["getPageByUuid"] || {});
    })
    .as("getPageByUuid");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/internal/page_content/*/?*",
    response: "@getPageByUuid",
  });
}

if (command.routes.includes("getPageVerions")) {
  cy.fixture("GET/200_getPageVerions.json")
    .then((data) => {
      data = merge(data, command.data["getPageVerions"] || {});
    })
    .as("getPageVerions");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/page/id/*/versions/?*",
    response: "@getPageVerions",
  });
}

if (command.routes.includes("getPageVerion")) {
  cy.fixture("GET/200_getPageVerion.json")
    .then((data) => {
      data = merge(data, command.data["getPageVerion"] || {});
    })
    .as("getPageVerion");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/page/id/*/version/*/?*",
    response: "@getPageVerion",
  });
}

if (command.routes.includes("getPageAccess")) {
  cy.fixture("GET/200_getPageAccess.json")
    .then((data) => {
      data = merge(data, command.data["getPageAccess"] || {});
    })
    .as("getPageAccess");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domains/id/*/page_access/id/*/?*",
    response: "@getPageAccess",
  });
}

if (command.routes.includes("getPageWebhooks")) {
  cy.fixture("GET/200_getPageWebhooks.json")
    .then((data) => {
      data = merge(data, command.data["getPageWebhooks"] || {});
    })
    .as("getPageWebhooks");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/page/*/webhooks/?*",
    response: "@getPageWebhooks",
  });
}

if (command.routes.includes("getPageWebhook")) {
  cy.fixture("GET/200_getPageWebhook.json")
    .then((data) => {
      data = merge(data, command.data["getPageWebhook"] || {});
    })
    .as("getPageWebhook");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/webhook/*/?*",
    response: "@getPageWebhook",
  });
}

if (command.routes.includes("getPageById")) {
  cy.fixture("GET/200_getPageById.json")
    .then((data) => {
      data = merge(data, command.data["getPageById"] || {});
    })
    .as("getPageById");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domains/*/pages/*/?*",
    response: "@getPageById",
  });
}

if (command.routes.includes("getPageContent")) {
  cy.fixture("GET/200_getPageContent.json")
    .then((data) => {
      data = merge(data, command.data["getPageContent"] || {});
    })
    .as("getPageContent");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/page/*/query/?*",
    response: "@getPageContent",
  });
}

if (command.routes.includes("getUserMetaData")) {
  cy.fixture("GET/200_getUserMetaData.json")
    .then((data) => {
      data = merge(data, command.data["getUserMetaData"] || {});
    })
    .as("getUserMetaData");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/users/*/meta/?*",
    response: "@getUserMetaData",
  });
}

if (command.routes.includes("domainInvitations")) {
  cy.fixture("GET/200_domainInvitations.json")
    .then((data) => {
      data = merge(data, command.data["domainInvitations"] || {});
    })
    .as("domainInvitations");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domains/*/invitations/?is_complete=False",
    response: "@domainInvitations",
  });
}

if (command.routes.includes("userInvitations")) {
  cy.fixture("GET/200_userInvitations.json")
    .then((data) => {
      data = merge(data, command.data["userInvitations"] || {});
    })
    .as("userInvitations");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/users/self/invitations/?is_complete=False",
    response: "@userInvitations",
  });
}

if (command.routes.includes("domainAccessLog")) {
  cy.fixture("GET/200_domainAccessLog.json")
    .then((data) => {
      data = merge(data, command.data["domainAccessLog"] || {});
    })
    .as("domainAccessLog");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domain_access/*/events/?*",
    response: "@domainAccessLog",
  });
}

if (command.routes.includes("domainUsers")) {
  cy.fixture("GET/200_domainUsers.json")
    .then((data) => {
      data = merge(data, command.data["domainUsers"] || {});
    })
    .as("domainUsers");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domain_access/*/users/?*",
    response: "@domainUsers",
  });
}

if (command.routes.includes("getInvitation")) {
  cy.fixture("GET/200_getInvitation.json")
    .then((data) => {
      data = merge(data, command.data["getInvitation"] || {});
    })
    .as("getInvitation");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/users/invitations/*/?*",
    response: "@getInvitation",
  });
}

if (command.routes.includes("getDomainNotificationDestinations")) {
  cy.fixture("GET/200_getDomainNotificationDestinations.json")
    .then((data) => {
      data = merge(data, command.data["getDomainNotificationDestinations"] || {});
    })
    .as("getDomainNotificationDestinations");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/notification/user/self/destinations/?*",
    response: "@getDomainNotificationDestinations",
  });
}

if (command.routes.includes("getNotificationHistory")) {
  cy.fixture("GET/200_getNotificationHistory.json")
    .then((data) => {
      data = merge(data, command.data["getNotificationHistory"] || {});
    })
    .as("getNotificationHistory");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organization/self/notification/history/?*",
    response: "@getNotificationHistory",
  });
}

if (command.routes.includes("getNotificationDestinations")) {
  cy.fixture("GET/200_getNotificationDestinations.json")
    .then((data) => {
      data = merge(data, command.data["getNotificationDestinations"] || {});
    })
    .as("getNotificationDestinations");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/self/notification/destinations/?*",
    response: "@getNotificationDestinations",
  });
}

if (command.routes.includes("getNotificationDestination")) {
  cy.fixture("GET/200_getNotificationDestination.json")
    .then((data) => {
      data = merge(data, command.data["getNotificationDestination"] || {});
    })
    .as("getNotificationDestination");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/notification/destination/*/?*",
    response: "@getNotificationDestination",
  });
}

if (command.routes.includes("getUserSymphonyStreams")) {
  cy.fixture("GET/200_getUserSymphonyStreams.json")
    .then((data) => {
      data = merge(data, command.data["getUserSymphonyStreams"] || {});
    })
    .as("getUserSymphonyStreams");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/notification/user/self/symphony_streams/?*",
    response: "@getUserSymphonyStreams",
  });
}

if (command.routes.includes("getDomainAccessGroups")) {
  cy.fixture("GET/200_getDomainAccessGroups.json")
    .then((data) => {
      data = merge(data, command.data["getDomainAccessGroups"] || {});
    })
    .as("getDomainAccessGroups");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domains/*/access_groups/?*",
    response: "@getDomainAccessGroups",
  });
}

if (command.routes.includes("getDomainAccessGroup")) {
  cy.fixture("GET/200_getDomainAccessGroup.json")
    .then((data) => {
      data = merge(data, command.data["getDomainAccessGroup"] || {});
    })
    .as("getDomainAccessGroup");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domains/*/access_groups/*/?*",
    response: "@getDomainAccessGroup",
  });
}

if (command.routes.includes("getDomainPageAccess")) {
  cy.fixture("GET/200_getDomainPageAccess.json")
    .then((data) => {
      data = merge(data, command.data["getDomainPageAccess"] || {});
    })
    .as("getDomainPageAccess");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domain_page_access/*/?*",
    response: "@getDomainPageAccess",
  });
}

if (command.routes.includes("getDomainCustomers")) {
  cy.fixture("GET/200_getDomainCustomers.json")
    .then((data) => {
      data = merge(data, command.data["getDomainCustomers"] || {});
    })
    .as("getDomainCustomers");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domains/*/customers/?*",
    response: "@getDomainCustomers",
  });
}

if (command.routes.includes("getDomainUsage")) {
  cy.fixture("GET/200_getDomainUsage.json")
    .then((data) => {
      data = merge(data, command.data["getDomainUsage"] || {});
    })
    .as("getDomainUsage");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/domains/id/*/usage/?*",
    response: "@getDomainUsage",
  });
}

if (command.routes.includes("getTemplates")) {
  cy.fixture("GET/200_getTemplates.json")
    .then((data) => {
      data = merge(data, command.data["getTemplates"] || {});
    })
    .as("getTemplates");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/templates/?*",
    response: "@getTemplates",
  });
}

if (command.routes.includes("getApplicationPageList")) {
  cy.fixture("GET/200_getApplicationPageList.json")
    .then((data) => {
      data = merge(data, command.data["getApplicationPageList"] || {});
    })
    .as("getApplicationPageList");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/application_page_list/?*",
    response: "@getApplicationPageList",
  });
}

if (command.routes.includes("getOrganization")) {
  cy.fixture("GET/200_getOrganization.json")
    .then((data) => {
      data = merge(data, command.data["getOrganization"] || {});
    })
    .as("getOrganization");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/*/?*",
    response: "@getOrganization",
  });
}

if (command.routes.includes("getOrganizationMetaData")) {
  cy.fixture("GET/200_getOrganizationMetaData.json")
    .then((data) => {
      data = merge(data, command.data["getOrganizationMetaData"] || {});
    })
    .as("getOrganizationMetaData");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/*/meta/?*",
    response: "@getOrganizationMetaData",
  });
}

if (command.routes.includes("getOrganizationUsers")) {
  cy.fixture("GET/200_getOrganizationUsers.json")
    .then((data) => {
      data = merge(data, command.data["getOrganizationUsers"] || {});
    })
    .as("getOrganizationUsers");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/*/users/?*",
    response: "@getOrganizationUsers",
  });
}

if (command.routes.includes("getOrganizationLinkedUsers")) {
  cy.fixture("GET/200_getOrganizationLinkedUsers.json")
    .then((data) => {
      data = merge(data, command.data["getOrganizationLinkedUsers"] || {});
    })
    .as("getOrganizationLinkedUsers");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/*/users/linked/?*",
    response: "@getOrganizationLinkedUsers",
  });
}

if (command.routes.includes("getOrganizationUsage")) {
  cy.fixture("GET/200_getOrganizationUsage.json")
    .then((data) => {
      data = merge(data, command.data["getOrganizationUsage"] || {});
    })
    .as("getOrganizationUsage");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/self/usage/?*",
    response: "@getOrganizationUsage",
  });
}

if (command.routes.includes("getOrganizationUser")) {
  cy.fixture("GET/200_getOrganizationUser.json")
    .then((data) => {
      data = merge(data, command.data["getOrganizationUser"] || {});
    })
    .as("getOrganizationUser");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/*/users/*/?*",
    response: "@getOrganizationUser",
  });
}

if (command.routes.includes("getOrganizationDomains")) {
  cy.fixture("GET/200_getOrganizationDomains.json")
    .then((data) => {
      data = merge(data, command.data["getOrganizationDomains"] || {});
    })
    .as("getOrganizationDomains");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/*/domains/?*",
    response: "@getOrganizationDomains",
  });
}

if (command.routes.includes("getSsoStatus")) {
  cy.fixture("GET/200_getSsoStatus.json")
    .then((data) => {
      data = merge(data, command.data["getSsoStatus"] || {});
    })
    .as("getSsoStatus");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/sso/status/?*",
    response: "@getSsoStatus",
  });
}

if (command.routes.includes("getTokens")) {
  cy.fixture("GET/200_getTokens.json")
    .then((data) => {
      data = merge(data, command.data["getTokens"] || {});
    })
    .as("getTokens");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/credentials/client_app_access/token/self/?*",
    response: "@getTokens",
  });
}

if (command.routes.includes("getSchemas")) {
  cy.fixture("GET/200_getSchemas.json")
    .then((data) => {
      data = merge(data, command.data["getSchemas"] || {});
    })
    .as("getSchemas");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/self/page_schemas?*",
    response: "@getSchemas",
  });
}

if (command.routes.includes("getSchema")) {
  cy.fixture("GET/200_getSchema.json")
    .then((data) => {
      data = merge(data, command.data["getSchema"] || {});
    })
    .as("getSchema");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/page_schemas/*?*",
    response: "@getSchema",
  });
}

if (command.routes.includes("getUserDelegates")) {
  cy.fixture("GET/200_getUserDelegates.json")
    .then((data) => {
      data = merge(data, command.data["getUserDelegates"] || {});
    })
    .as("getUserDelegates");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/self/user_delegates?*",
    response: "@getUserDelegates",
  });
}

if (command.routes.includes("getUserDelegate")) {
  cy.fixture("GET/200_getUserDelegate.json")
    .then((data) => {
      data = merge(data, command.data["getUserDelegate"] || {});
    })
    .as("getUserDelegate");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/user_delegate/*?*",
    response: "@getUserDelegate",
  });
}

if (command.routes.includes("getWorkflowTypes")) {
  cy.fixture("GET/200_getWorkflowTypes.json")
    .then((data) => {
      data = merge(data, command.data["getWorkflowTypes"] || {});
    })
    .as("getWorkflowTypes");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/self/workflow_types/?*",
    response: "@getWorkflowTypes",
  });
}

if (command.routes.includes("getReportsClientAppUsageStats")) {
  cy.fixture("GET/200_getReportsClientAppUsageStats.json")
    .then((data) => {
      data = merge(data, command.data["getReportsClientAppUsageStats"] || {});
    })
    .as("getReportsClientAppUsageStats");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/self/reports/client_app_usage/?*",
    response: "@getReportsClientAppUsageStats",
  });
}

if (command.routes.includes("getReportsUsageStats")) {
  cy.fixture("GET/200_getReportsUsageStats.json")
    .then((data) => {
      data = merge(data, command.data["getReportsUsageStats"] || {});
    })
    .as("getReportsUsageStats");
  cy.route({
    method: "GET",
    status: 200,
    url: "**/organizations/all/reports/usage_stats/?*",
    response: "@getReportsUsageStats",
  });
}

if (command.routes.includes("createIntegrationChannel")) {
  cy.fixture("POST/200_createIntegrationChannel.json")
    .then((data) => {
      data = merge(data, command.data["createIntegrationChannel"] || {});
    })
    .as("createIntegrationChannel");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/organizations/self/integration_channels/?*",
    response: "@createIntegrationChannel",
  });
}

if (command.routes.includes("createCounterparty")) {
  cy.fixture("POST/200_createCounterparty.json")
    .then((data) => {
      data = merge(data, command.data["createCounterparty"] || {});
    })
    .as("createCounterparty");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/organizations/self/counterparties/?*",
    response: "@createCounterparty",
  });
}

if (command.routes.includes("workflowEvent")) {
  cy.fixture("POST/200_workflowEvent.json")
    .then((data) => {
      data = merge(data, command.data["workflowEvent"] || {});
    })
    .as("workflowEvent");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/workflows/types/*/events/types/*/?*",
    response: "@workflowEvent",
  });
}

if (command.routes.includes("createFolder")) {
  cy.fixture("POST/200_createFolder.json")
    .then((data) => {
      data = merge(data, command.data["createFolder"] || {});
    })
    .as("createFolder");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/domains/?*",
    response: "@createFolder",
  });
}

if (command.routes.includes("createDomain")) {
  cy.fixture("POST/200_createDomain.json")
    .then((data) => {
      data = merge(data, command.data["createDomain"] || {});
    })
    .as("createDomain");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/domains/?*",
    response: "@createDomain",
  });
}

if (command.routes.includes("createPageWebhook")) {
  cy.fixture("POST/200_createPageWebhook.json")
    .then((data) => {
      data = merge(data, command.data["createPageWebhook"] || {});
    })
    .as("createPageWebhook");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/page/*/webhooks/?*",
    response: "@createPageWebhook",
  });
}

if (command.routes.includes("createPage")) {
  cy.fixture("POST/200_createPage.json")
    .then((data) => {
      data = merge(data, command.data["createPage"] || {});
    })
    .as("createPage");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/domains/*/pages/?*",
    response: "@createPage",
  });
}

if (command.routes.includes("createPageNotification")) {
  cy.fixture("POST/201_createPageNotification.json")
    .then((data) => {
      data = merge(data, command.data["createPageNotification"] || {});
    })
    .as("createPageNotification");
  cy.route({
    method: "POST",
    status: 201,
    url: "**/page/*/notification/?*",
    response: "@createPageNotification",
  });
}

if (command.routes.includes("createPageContentNotification")) {
  cy.fixture("POST/201_createPageContentNotification.json")
    .then((data) => {
      data = merge(data, command.data["createPageContentNotification"] || {});
    })
    .as("createPageContentNotification");
  cy.route({
    method: "POST",
    status: 201,
    url: "**/page/*/content_notification/?*",
    response: "@createPageContentNotification",
  });
}

if (command.routes.includes("resetPassword")) {
  cy.fixture("POST/201_resetPassword.json")
    .then((data) => {
      data = merge(data, command.data["resetPassword"] || {});
    })
    .as("resetPassword");
  cy.route({
    method: "POST",
    status: 201,
    url: "**/password_reset/confirm/?*",
    response: "@resetPassword",
  });
}

if (command.routes.includes("inviteUsers")) {
  cy.fixture("POST/200_inviteUsers.json")
    .then((data) => {
      data = merge(data, command.data["inviteUsers"] || {});
    })
    .as("inviteUsers");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/domains/*/invitations/?*",
    response: "@inviteUsers",
  });
}

if (command.routes.includes("acceptInvitation")) {
  cy.fixture("POST/200_acceptInvitation.json")
    .then((data) => {
      data = merge(data, command.data["acceptInvitation"] || {});
    })
    .as("acceptInvitation");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/users/invitation/confirm/?*",
    response: "@acceptInvitation",
  });
}

if (command.routes.includes("createDomainNotificationDestination")) {
  cy.fixture("POST/200_createDomainNotificationDestination.json")
    .then((data) => {
      data = merge(data, command.data["createDomainNotificationDestination"] || {});
    })
    .as("createDomainNotificationDestination");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/notification/user/self/destinations/?*",
    response: "@createDomainNotificationDestination",
  });
}

if (command.routes.includes("createNotificationDestination")) {
  cy.fixture("POST/200_createNotificationDestination.json")
    .then((data) => {
      data = merge(data, command.data["createNotificationDestination"] || {});
    })
    .as("createNotificationDestination");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/organizations/self/notification/destinations/?*",
    response: "@createNotificationDestination",
  });
}

if (command.routes.includes("createNotification")) {
  cy.fixture("POST/201_createNotification.json")
    .then((data) => {
      data = merge(data, command.data["createNotification"] || {});
    })
    .as("createNotification");
  cy.route({
    method: "POST",
    status: 201,
    url: "**/organizations/self/notification/?*",
    response: "@createNotification",
  });
}

if (command.routes.includes("addDomainAccessGroup")) {
  cy.fixture("POST/200_addDomainAccessGroup.json")
    .then((data) => {
      data = merge(data, command.data["addDomainAccessGroup"] || {});
    })
    .as("addDomainAccessGroup");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/domains/*/access_groups/?*",
    response: "@addDomainAccessGroup",
  });
}

if (command.routes.includes("putDomainAgroupMembers")) {
  cy.fixture("POST/200_putDomainAgroupMembers.json")
    .then((data) => {
      data = merge(data, command.data["putDomainAgroupMembers"] || {});
    })
    .as("putDomainAgroupMembers");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/domains/*/access_groups/*/members/?*",
    response: "@putDomainAgroupMembers",
  });
}

if (command.routes.includes("putDomainAgroupPages")) {
  cy.fixture("POST/200_putDomainAgroupPages.json")
    .then((data) => {
      data = merge(data, command.data["putDomainAgroupPages"] || {});
    })
    .as("putDomainAgroupPages");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/domains/*/access_groups/*/pages/?*",
    response: "@putDomainAgroupPages",
  });
}

if (command.routes.includes("createOrganizationUser")) {
  cy.fixture("POST/200_createOrganizationUser.json")
    .then((data) => {
      data = merge(data, command.data["createOrganizationUser"] || {});
    })
    .as("createOrganizationUser");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/organizations/*/users/?*",
    response: "@createOrganizationUser",
  });
}

if (command.routes.includes("createSchema")) {
  cy.fixture("POST/200_createSchema.json")
    .then((data) => {
      data = merge(data, command.data["createSchema"] || {});
    })
    .as("createSchema");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/organizations/self/page_schemas/?*",
    response: "@createSchema",
  });
}

if (command.routes.includes("createUserDelegate")) {
  cy.fixture("POST/200_createUserDelegate.json")
    .then((data) => {
      data = merge(data, command.data["createUserDelegate"] || {});
    })
    .as("createUserDelegate");
  cy.route({
    method: "POST",
    status: 200,
    url: "**/organizations/self/user_delegates/?*",
    response: "@createUserDelegate",
  });
}

if (command.routes.includes("createIntegrationChannel")) {
  cy.fixture("POST/400_createIntegrationChannel.json")
    .then((data) => {
      data = merge(data, command.data["createIntegrationChannel"] || {});
    })
    .as("createIntegrationChannel");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/organizations/self/integration_channels/?*",
    response: "@createIntegrationChannel",
  });
}

if (command.routes.includes("createCounterparty")) {
  cy.fixture("POST/400_createCounterparty.json")
    .then((data) => {
      data = merge(data, command.data["createCounterparty"] || {});
    })
    .as("createCounterparty");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/organizations/self/counterparties/?*",
    response: "@createCounterparty",
  });
}

if (command.routes.includes("createFolder")) {
  cy.fixture("POST/400_createFolder.json")
    .then((data) => {
      data = merge(data, command.data["createFolder"] || {});
    })
    .as("createFolder");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/domains/?*",
    response: "@createFolder",
  });
}

if (command.routes.includes("createDomain")) {
  cy.fixture("POST/400_createDomain.json")
    .then((data) => {
      data = merge(data, command.data["createDomain"] || {});
    })
    .as("createDomain");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/domains/?*",
    response: "@createDomain",
  });
}

if (command.routes.includes("createPageWebhook")) {
  cy.fixture("POST/400_createPageWebhook.json")
    .then((data) => {
      data = merge(data, command.data["createPageWebhook"] || {});
    })
    .as("createPageWebhook");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/page/*/webhooks/?*",
    response: "@createPageWebhook",
  });
}

if (command.routes.includes("createPage")) {
  cy.fixture("POST/400_createPage.json")
    .then((data) => {
      data = merge(data, command.data["createPage"] || {});
    })
    .as("createPage");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/domains/*/pages/?*",
    response: "@createPage",
  });
}

if (command.routes.includes("createPageNotification")) {
  cy.fixture("POST/400_createPageNotification.json")
    .then((data) => {
      data = merge(data, command.data["createPageNotification"] || {});
    })
    .as("createPageNotification");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/page/*/notification/?*",
    response: "@createPageNotification",
  });
}

if (command.routes.includes("createPageContentNotification")) {
  cy.fixture("POST/400_createPageContentNotification.json")
    .then((data) => {
      data = merge(data, command.data["createPageContentNotification"] || {});
    })
    .as("createPageContentNotification");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/page/*/content_notification/?*",
    response: "@createPageContentNotification",
  });
}

if (command.routes.includes("resetPassword")) {
  cy.fixture("POST/400_resetPassword.json")
    .then((data) => {
      data = merge(data, command.data["resetPassword"] || {});
    })
    .as("resetPassword");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/password_reset/confirm/?*",
    response: "@resetPassword",
  });
}

if (command.routes.includes("inviteUsers")) {
  cy.fixture("POST/400_inviteUsers.json")
    .then((data) => {
      data = merge(data, command.data["inviteUsers"] || {});
    })
    .as("inviteUsers");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/domains/*/invitations/?*",
    response: "@inviteUsers",
  });
}

if (command.routes.includes("acceptInvitation")) {
  cy.fixture("POST/400_acceptInvitation.json")
    .then((data) => {
      data = merge(data, command.data["acceptInvitation"] || {});
    })
    .as("acceptInvitation");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/users/invitation/confirm/?*",
    response: "@acceptInvitation",
  });
}

if (command.routes.includes("createDomainNotificationDestination")) {
  cy.fixture("POST/400_createDomainNotificationDestination.json")
    .then((data) => {
      data = merge(data, command.data["createDomainNotificationDestination"] || {});
    })
    .as("createDomainNotificationDestination");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/notification/user/self/destinations/?*",
    response: "@createDomainNotificationDestination",
  });
}

if (command.routes.includes("createNotificationDestination")) {
  cy.fixture("POST/400_createNotificationDestination.json")
    .then((data) => {
      data = merge(data, command.data["createNotificationDestination"] || {});
    })
    .as("createNotificationDestination");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/organizations/self/notification/destinations/?*",
    response: "@createNotificationDestination",
  });
}

if (command.routes.includes("createNotification")) {
  cy.fixture("POST/400_createNotification.json")
    .then((data) => {
      data = merge(data, command.data["createNotification"] || {});
    })
    .as("createNotification");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/organizations/self/notification/?*",
    response: "@createNotification",
  });
}

if (command.routes.includes("addDomainAccessGroup")) {
  cy.fixture("POST/400_addDomainAccessGroup.json")
    .then((data) => {
      data = merge(data, command.data["addDomainAccessGroup"] || {});
    })
    .as("addDomainAccessGroup");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/domains/*/access_groups/?*",
    response: "@addDomainAccessGroup",
  });
}

if (command.routes.includes("putDomainAgroupMembers")) {
  cy.fixture("POST/400_putDomainAgroupMembers.json")
    .then((data) => {
      data = merge(data, command.data["putDomainAgroupMembers"] || {});
    })
    .as("putDomainAgroupMembers");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/domains/*/access_groups/*/members/?*",
    response: "@putDomainAgroupMembers",
  });
}

if (command.routes.includes("putDomainAgroupPages")) {
  cy.fixture("POST/400_putDomainAgroupPages.json")
    .then((data) => {
      data = merge(data, command.data["putDomainAgroupPages"] || {});
    })
    .as("putDomainAgroupPages");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/domains/*/access_groups/*/pages/?*",
    response: "@putDomainAgroupPages",
  });
}

if (command.routes.includes("createOrganizationUser")) {
  cy.fixture("POST/400_createOrganizationUser.json")
    .then((data) => {
      data = merge(data, command.data["createOrganizationUser"] || {});
    })
    .as("createOrganizationUser");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/organizations/*/users/?*",
    response: "@createOrganizationUser",
  });
}

if (command.routes.includes("createSchema")) {
  cy.fixture("POST/400_createSchema.json")
    .then((data) => {
      data = merge(data, command.data["createSchema"] || {});
    })
    .as("createSchema");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/organizations/self/page_schemas/?*",
    response: "@createSchema",
  });
}

if (command.routes.includes("createUserDelegate")) {
  cy.fixture("POST/400_createUserDelegate.json")
    .then((data) => {
      data = merge(data, command.data["createUserDelegate"] || {});
    })
    .as("createUserDelegate");
  cy.route({
    method: "POST",
    status: 400,
    url: "**/organizations/self/user_delegates/?*",
    response: "@createUserDelegate",
  });
}

if (command.routes.includes("saveIntegrationChannel")) {
  cy.fixture("PUT/200_saveIntegrationChannel.json")
    .then((data) => {
      data = merge(data, command.data["saveIntegrationChannel"] || {});
    })
    .as("saveIntegrationChannel");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/integration_channels/*/?*",
    response: "@saveIntegrationChannel",
  });
}

if (command.routes.includes("updateCounterparty")) {
  cy.fixture("PUT/200_updateCounterparty.json")
    .then((data) => {
      data = merge(data, command.data["updateCounterparty"] || {});
    })
    .as("updateCounterparty");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/counterparty/*?",
    response: "@updateCounterparty",
  });
}

if (command.routes.includes("updateDomain")) {
  cy.fixture("PUT/200_updateDomain.json")
    .then((data) => {
      data = merge(data, command.data["updateDomain"] || {});
    })
    .as("updateDomain");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/domains/*/?*",
    response: "@updateDomain",
  });
}

if (command.routes.includes("restorePageVerion")) {
  cy.fixture("PUT/200_restorePageVerion.json")
    .then((data) => {
      data = merge(data, command.data["restorePageVerion"] || {});
    })
    .as("restorePageVerion");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/page/id/*/version/*/restore/?*",
    response: "@restorePageVerion",
  });
}

if (command.routes.includes("savePageWebhook")) {
  cy.fixture("PUT/200_savePageWebhook.json")
    .then((data) => {
      data = merge(data, command.data["savePageWebhook"] || {});
    })
    .as("savePageWebhook");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/webhook/*/?*",
    response: "@savePageWebhook",
  });
}

if (command.routes.includes("queryPageContent")) {
  cy.fixture("PUT/200_queryPageContent.json")
    .then((data) => {
      data = merge(data, command.data["queryPageContent"] || {});
    })
    .as("queryPageContent");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/page/*/query/?*",
    response: "@queryPageContent",
  });
}

if (command.routes.includes("savePageContent")) {
  cy.fixture("PUT/200_savePageContent.json")
    .then((data) => {
      data = merge(data, command.data["savePageContent"] || {});
    })
    .as("savePageContent");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/domains/id/*/page_content/id/*/?*",
    response: "@savePageContent",
  });
}

if (command.routes.includes("savePageContentDelta")) {
  cy.fixture("PUT/200_savePageContentDelta.json")
    .then((data) => {
      data = merge(data, command.data["savePageContentDelta"] || {});
    })
    .as("savePageContentDelta");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/domains/id/*/page_content_delta/id/*/?*",
    response: "@savePageContentDelta",
  });
}

if (command.routes.includes("savePageSettings")) {
  cy.fixture("PUT/200_savePageSettings.json")
    .then((data) => {
      data = merge(data, command.data["savePageSettings"] || {});
    })
    .as("savePageSettings");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/domains/*/pages/*/?*",
    response: "@savePageSettings",
  });
}

if (command.routes.includes("saveUserInfo")) {
  cy.fixture("PUT/200_saveUserInfo.json")
    .then((data) => {
      data = merge(data, command.data["saveUserInfo"] || {});
    })
    .as("saveUserInfo");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/users/self/?*",
    response: "@saveUserInfo",
  });
}

if (command.routes.includes("saveUserMetaData")) {
  cy.fixture("PUT/200_saveUserMetaData.json")
    .then((data) => {
      data = merge(data, command.data["saveUserMetaData"] || {});
    })
    .as("saveUserMetaData");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/users/*/meta/?*",
    response: "@saveUserMetaData",
  });
}

if (command.routes.includes("changePassword")) {
  cy.fixture("PUT/200_changePassword.json")
    .then((data) => {
      data = merge(data, command.data["changePassword"] || {});
    })
    .as("changePassword");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/credentials/self/?*",
    response: "@changePassword",
  });
}

if (command.routes.includes("changeEmail")) {
  cy.fixture("PUT/200_changeEmail.json")
    .then((data) => {
      data = merge(data, command.data["changeEmail"] || {});
    })
    .as("changeEmail");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/credentials/self/?*",
    response: "@changeEmail",
  });
}

if (command.routes.includes("setDomainDefault")) {
  cy.fixture("PUT/200_setDomainDefault.json")
    .then((data) => {
      data = merge(data, command.data["setDomainDefault"] || {});
    })
    .as("setDomainDefault");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/domain_access/*/users/self/?*",
    response: "@setDomainDefault",
  });
}

if (command.routes.includes("updateDomainAccess")) {
  cy.fixture("PUT/200_updateDomainAccess.json")
    .then((data) => {
      data = merge(data, command.data["updateDomainAccess"] || {});
    })
    .as("updateDomainAccess");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/domain_access/*/users/?*",
    response: "@updateDomainAccess",
  });
}

if (command.routes.includes("saveNotificationDestination")) {
  cy.fixture("PUT/200_saveNotificationDestination.json")
    .then((data) => {
      data = merge(data, command.data["saveNotificationDestination"] || {});
    })
    .as("saveNotificationDestination");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/notification/destination/*/?*",
    response: "@saveNotificationDestination",
  });
}

if (command.routes.includes("updateDomainAgroup")) {
  cy.fixture("PUT/200_updateDomainAgroup.json")
    .then((data) => {
      data = merge(data, command.data["updateDomainAgroup"] || {});
    })
    .as("updateDomainAgroup");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/domains/*/access_groups/*/?*",
    response: "@updateDomainAgroup",
  });
}

if (command.routes.includes("saveDomainPageAccess")) {
  cy.fixture("PUT/200_saveDomainPageAccess.json")
    .then((data) => {
      data = merge(data, command.data["saveDomainPageAccess"] || {});
    })
    .as("saveDomainPageAccess");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/domain_page_access/*/basic/?*",
    response: "@saveDomainPageAccess",
  });
}

if (command.routes.includes("saveOrganizationMetaData")) {
  cy.fixture("PUT/200_saveOrganizationMetaData.json")
    .then((data) => {
      data = merge(data, command.data["saveOrganizationMetaData"] || {});
    })
    .as("saveOrganizationMetaData");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/organizations/*/meta/?*",
    response: "@saveOrganizationMetaData",
  });
}

if (command.routes.includes("saveOrganizationUser")) {
  cy.fixture("PUT/200_saveOrganizationUser.json")
    .then((data) => {
      data = merge(data, command.data["saveOrganizationUser"] || {});
    })
    .as("saveOrganizationUser");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/organizations/*/users/*/?*",
    response: "@saveOrganizationUser",
  });
}

if (command.routes.includes("updateSchema")) {
  cy.fixture("PUT/200_updateSchema.json")
    .then((data) => {
      data = merge(data, command.data["updateSchema"] || {});
    })
    .as("updateSchema");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/page_schemas/*?",
    response: "@updateSchema",
  });
}

if (command.routes.includes("updateUserDelegate")) {
  cy.fixture("PUT/200_updateUserDelegate.json")
    .then((data) => {
      data = merge(data, command.data["updateUserDelegate"] || {});
    })
    .as("updateUserDelegate");
  cy.route({
    method: "PUT",
    status: 200,
    url: "**/user_delegate/*?",
    response: "@updateUserDelegate",
  });
}

if (command.routes.includes("saveIntegrationChannel")) {
  cy.fixture("PUT/400_saveIntegrationChannel.json")
    .then((data) => {
      data = merge(data, command.data["saveIntegrationChannel"] || {});
    })
    .as("saveIntegrationChannel");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/integration_channels/*/?*",
    response: "@saveIntegrationChannel",
  });
}

if (command.routes.includes("updateCounterparty")) {
  cy.fixture("PUT/400_updateCounterparty.json")
    .then((data) => {
      data = merge(data, command.data["updateCounterparty"] || {});
    })
    .as("updateCounterparty");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/counterparty/*?",
    response: "@updateCounterparty",
  });
}

if (command.routes.includes("updateDomain")) {
  cy.fixture("PUT/400_updateDomain.json")
    .then((data) => {
      data = merge(data, command.data["updateDomain"] || {});
    })
    .as("updateDomain");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/domains/*/?*",
    response: "@updateDomain",
  });
}

if (command.routes.includes("restorePageVerion")) {
  cy.fixture("PUT/400_restorePageVerion.json")
    .then((data) => {
      data = merge(data, command.data["restorePageVerion"] || {});
    })
    .as("restorePageVerion");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/page/id/*/version/*/restore/?*",
    response: "@restorePageVerion",
  });
}

if (command.routes.includes("savePageWebhook")) {
  cy.fixture("PUT/400_savePageWebhook.json")
    .then((data) => {
      data = merge(data, command.data["savePageWebhook"] || {});
    })
    .as("savePageWebhook");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/webhook/*/?*",
    response: "@savePageWebhook",
  });
}

if (command.routes.includes("queryPageContent")) {
  cy.fixture("PUT/400_queryPageContent.json")
    .then((data) => {
      data = merge(data, command.data["queryPageContent"] || {});
    })
    .as("queryPageContent");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/page/*/query/?*",
    response: "@queryPageContent",
  });
}

if (command.routes.includes("savePageContent")) {
  cy.fixture("PUT/400_savePageContent.json")
    .then((data) => {
      data = merge(data, command.data["savePageContent"] || {});
    })
    .as("savePageContent");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/domains/id/*/page_content/id/*/?*",
    response: "@savePageContent",
  });
}

if (command.routes.includes("savePageContentDelta")) {
  cy.fixture("PUT/400_savePageContentDelta.json")
    .then((data) => {
      data = merge(data, command.data["savePageContentDelta"] || {});
    })
    .as("savePageContentDelta");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/domains/id/*/page_content_delta/id/*/?*",
    response: "@savePageContentDelta",
  });
}

if (command.routes.includes("savePageSettings")) {
  cy.fixture("PUT/400_savePageSettings.json")
    .then((data) => {
      data = merge(data, command.data["savePageSettings"] || {});
    })
    .as("savePageSettings");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/domains/*/pages/*/?*",
    response: "@savePageSettings",
  });
}

if (command.routes.includes("saveUserInfo")) {
  cy.fixture("PUT/400_saveUserInfo.json")
    .then((data) => {
      data = merge(data, command.data["saveUserInfo"] || {});
    })
    .as("saveUserInfo");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/users/self/?*",
    response: "@saveUserInfo",
  });
}

if (command.routes.includes("saveUserMetaData")) {
  cy.fixture("PUT/400_saveUserMetaData.json")
    .then((data) => {
      data = merge(data, command.data["saveUserMetaData"] || {});
    })
    .as("saveUserMetaData");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/users/*/meta/?*",
    response: "@saveUserMetaData",
  });
}

if (command.routes.includes("changePassword")) {
  cy.fixture("PUT/400_changePassword.json")
    .then((data) => {
      data = merge(data, command.data["changePassword"] || {});
    })
    .as("changePassword");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/credentials/self/?*",
    response: "@changePassword",
  });
}

if (command.routes.includes("changeEmail")) {
  cy.fixture("PUT/400_changeEmail.json")
    .then((data) => {
      data = merge(data, command.data["changeEmail"] || {});
    })
    .as("changeEmail");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/credentials/self/?*",
    response: "@changeEmail",
  });
}

if (command.routes.includes("setDomainDefault")) {
  cy.fixture("PUT/400_setDomainDefault.json")
    .then((data) => {
      data = merge(data, command.data["setDomainDefault"] || {});
    })
    .as("setDomainDefault");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/domain_access/*/users/self/?*",
    response: "@setDomainDefault",
  });
}

if (command.routes.includes("updateDomainAccess")) {
  cy.fixture("PUT/400_updateDomainAccess.json")
    .then((data) => {
      data = merge(data, command.data["updateDomainAccess"] || {});
    })
    .as("updateDomainAccess");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/domain_access/*/users/?*",
    response: "@updateDomainAccess",
  });
}

if (command.routes.includes("saveNotificationDestination")) {
  cy.fixture("PUT/400_saveNotificationDestination.json")
    .then((data) => {
      data = merge(data, command.data["saveNotificationDestination"] || {});
    })
    .as("saveNotificationDestination");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/notification/destination/*/?*",
    response: "@saveNotificationDestination",
  });
}

if (command.routes.includes("updateDomainAgroup")) {
  cy.fixture("PUT/400_updateDomainAgroup.json")
    .then((data) => {
      data = merge(data, command.data["updateDomainAgroup"] || {});
    })
    .as("updateDomainAgroup");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/domains/*/access_groups/*/?*",
    response: "@updateDomainAgroup",
  });
}

if (command.routes.includes("saveDomainPageAccess")) {
  cy.fixture("PUT/400_saveDomainPageAccess.json")
    .then((data) => {
      data = merge(data, command.data["saveDomainPageAccess"] || {});
    })
    .as("saveDomainPageAccess");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/domain_page_access/*/basic/?*",
    response: "@saveDomainPageAccess",
  });
}

if (command.routes.includes("saveOrganizationMetaData")) {
  cy.fixture("PUT/400_saveOrganizationMetaData.json")
    .then((data) => {
      data = merge(data, command.data["saveOrganizationMetaData"] || {});
    })
    .as("saveOrganizationMetaData");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/organizations/*/meta/?*",
    response: "@saveOrganizationMetaData",
  });
}

if (command.routes.includes("saveOrganizationUser")) {
  cy.fixture("PUT/400_saveOrganizationUser.json")
    .then((data) => {
      data = merge(data, command.data["saveOrganizationUser"] || {});
    })
    .as("saveOrganizationUser");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/organizations/*/users/*/?*",
    response: "@saveOrganizationUser",
  });
}

if (command.routes.includes("updateSchema")) {
  cy.fixture("PUT/400_updateSchema.json")
    .then((data) => {
      data = merge(data, command.data["updateSchema"] || {});
    })
    .as("updateSchema");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/page_schemas/*?",
    response: "@updateSchema",
  });
}

if (command.routes.includes("updateUserDelegate")) {
  cy.fixture("PUT/400_updateUserDelegate.json")
    .then((data) => {
      data = merge(data, command.data["updateUserDelegate"] || {});
    })
    .as("updateUserDelegate");
  cy.route({
    method: "PUT",
    status: 400,
    url: "**/user_delegate/*?",
    response: "@updateUserDelegate",
  });
}
