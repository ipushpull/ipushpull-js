if (command.routes.includes("__KEY__")) {
  cy.fixture("__METHOD__/__STATUS_____KEY__.json")
    .then((data) => {
      data = merge(data, command.data["__KEY__"] || {});
      return data;
    })
    .as("fixture___KEY__");
  cy.route({
    method: "__METHOD__",
    status: command.status["__KEY__"] || __STATUS__,
    url: "__URL__",
    response: "@fixture___KEY__",
  }).as("__KEY__");
}
