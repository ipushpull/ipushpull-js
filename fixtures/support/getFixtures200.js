const merge = require("deepmerge");

Cypress.Commands.add("getFixtures200", (command) => {
  if (!command.data) {
    command.data = {};
  }
  if (!command.status) {
    command.status = {};
  }
  cy.server();
  if (command.routes.includes("getSelfInfo")) {
  cy.fixture("GET/200_getSelfInfo.json")
    .then((data) => {
      data = merge(data, command.data["getSelfInfo"] || {});
      return data;
    })
    .as("fixture_getSelfInfo");
  cy.route({
    method: "GET",
    status: command.status["getSelfInfo"] || 200,
    url: "**/users/self/?*",
    response: "@fixture_getSelfInfo",
  }).as("getSelfInfo");
}

if (command.routes.includes("getIntegrationChannels")) {
  cy.fixture("GET/200_getIntegrationChannels.json")
    .then((data) => {
      data = merge(data, command.data["getIntegrationChannels"] || {});
      return data;
    })
    .as("fixture_getIntegrationChannels");
  cy.route({
    method: "GET",
    status: command.status["getIntegrationChannels"] || 200,
    url: "**/organizations/self/integration_channels/?*",
    response: "@fixture_getIntegrationChannels",
  }).as("getIntegrationChannels");
}

if (command.routes.includes("getIntegrationChannel")) {
  cy.fixture("GET/200_getIntegrationChannel.json")
    .then((data) => {
      data = merge(data, command.data["getIntegrationChannel"] || {});
      return data;
    })
    .as("fixture_getIntegrationChannel");
  cy.route({
    method: "GET",
    status: command.status["getIntegrationChannel"] || 200,
    url: "**/integration_channels/*/?*",
    response: "@fixture_getIntegrationChannel",
  }).as("getIntegrationChannel");
}

if (command.routes.includes("getCounterparties")) {
  cy.fixture("GET/200_getCounterparties.json")
    .then((data) => {
      data = merge(data, command.data["getCounterparties"] || {});
      return data;
    })
    .as("fixture_getCounterparties");
  cy.route({
    method: "GET",
    status: command.status["getCounterparties"] || 200,
    url: "**/organizations/self/counterparties/?*",
    response: "@fixture_getCounterparties",
  }).as("getCounterparties");
}

if (command.routes.includes("getCounterparty")) {
  cy.fixture("GET/200_getCounterparty.json")
    .then((data) => {
      data = merge(data, command.data["getCounterparty"] || {});
      return data;
    })
    .as("fixture_getCounterparty");
  cy.route({
    method: "GET",
    status: command.status["getCounterparty"] || 200,
    url: "**/counterparty/*?*",
    response: "@fixture_getCounterparty",
  }).as("getCounterparty");
}

if (command.routes.includes("getDomains")) {
  cy.fixture("GET/200_getDomains.json")
    .then((data) => {
      data = merge(data, command.data["getDomains"] || {});
      return data;
    })
    .as("fixture_getDomains");
  cy.route({
    method: "GET",
    status: command.status["getDomains"] || 200,
    url: "**/domains/?*",
    response: "@fixture_getDomains",
  }).as("getDomains");
}

if (command.routes.includes("getDomain")) {
  cy.fixture("GET/200_getDomain.json")
    .then((data) => {
      data = merge(data, command.data["getDomain"] || {});
      return data;
    })
    .as("fixture_getDomain");
  cy.route({
    method: "GET",
    status: command.status["getDomain"] || 200,
    url: "**/domains/*/?*",
    response: "@fixture_getDomain",
  }).as("getDomain");
}

if (command.routes.includes("getDomainPages")) {
  cy.fixture("GET/200_getDomainPages.json")
    .then((data) => {
      data = merge(data, command.data["getDomainPages"] || {});
      return data;
    })
    .as("fixture_getDomainPages");
  cy.route({
    method: "GET",
    status: command.status["getDomainPages"] || 200,
    url: "**/domains/*/page_access/?*",
    response: "@fixture_getDomainPages",
  }).as("getDomainPages");
}

if (command.routes.includes("getDomainsAndPages")) {
  cy.fixture("GET/200_getDomainsAndPages.json")
    .then((data) => {
      data = merge(data, command.data["getDomainsAndPages"] || {});
      return data;
    })
    .as("fixture_getDomainsAndPages");
  cy.route({
    method: "GET",
    status: command.status["getDomainsAndPages"] || 200,
    url: "**/domain_page_access/?*",
    response: "@fixture_getDomainsAndPages",
  }).as("getDomainsAndPages");
}

if (command.routes.includes("getPage")) {
  cy.fixture("GET/200_getPage.json")
    .then((data) => {
      data = merge(data, command.data["getPage"] || {});
      return data;
    })
    .as("fixture_getPage");
  cy.route({
    method: "GET",
    status: command.status["getPage"] || 200,
    url: "**/domains/id/*/page_content/id/*/?*",
    response: "@fixture_getPage",
  }).as("getPage");
}

if (command.routes.includes("getPageByName")) {
  cy.fixture("GET/200_getPageByName.json")
    .then((data) => {
      data = merge(data, command.data["getPageByName"] || {});
      return data;
    })
    .as("fixture_getPageByName");
  cy.route({
    method: "GET",
    status: command.status["getPageByName"] || 200,
    url: "**/domains/name/*/page_content/name/*/?*",
    response: "@fixture_getPageByName",
  }).as("getPageByName");
}

if (command.routes.includes("getPageByUuid")) {
  cy.fixture("GET/200_getPageByUuid.json")
    .then((data) => {
      data = merge(data, command.data["getPageByUuid"] || {});
      return data;
    })
    .as("fixture_getPageByUuid");
  cy.route({
    method: "GET",
    status: command.status["getPageByUuid"] || 200,
    url: "**/internal/page_content/*/?*",
    response: "@fixture_getPageByUuid",
  }).as("getPageByUuid");
}

if (command.routes.includes("getPageVerions")) {
  cy.fixture("GET/200_getPageVerions.json")
    .then((data) => {
      data = merge(data, command.data["getPageVerions"] || {});
      return data;
    })
    .as("fixture_getPageVerions");
  cy.route({
    method: "GET",
    status: command.status["getPageVerions"] || 200,
    url: "**/page/id/*/versions/?*",
    response: "@fixture_getPageVerions",
  }).as("getPageVerions");
}

if (command.routes.includes("getPageVerion")) {
  cy.fixture("GET/200_getPageVerion.json")
    .then((data) => {
      data = merge(data, command.data["getPageVerion"] || {});
      return data;
    })
    .as("fixture_getPageVerion");
  cy.route({
    method: "GET",
    status: command.status["getPageVerion"] || 200,
    url: "**/page/id/*/version/*/?*",
    response: "@fixture_getPageVerion",
  }).as("getPageVerion");
}

if (command.routes.includes("getPageAccess")) {
  cy.fixture("GET/200_getPageAccess.json")
    .then((data) => {
      data = merge(data, command.data["getPageAccess"] || {});
      return data;
    })
    .as("fixture_getPageAccess");
  cy.route({
    method: "GET",
    status: command.status["getPageAccess"] || 200,
    url: "**/domains/id/*/page_access/id/*/?*",
    response: "@fixture_getPageAccess",
  }).as("getPageAccess");
}

if (command.routes.includes("getPageWebhooks")) {
  cy.fixture("GET/200_getPageWebhooks.json")
    .then((data) => {
      data = merge(data, command.data["getPageWebhooks"] || {});
      return data;
    })
    .as("fixture_getPageWebhooks");
  cy.route({
    method: "GET",
    status: command.status["getPageWebhooks"] || 200,
    url: "**/page/*/webhooks/?*",
    response: "@fixture_getPageWebhooks",
  }).as("getPageWebhooks");
}

if (command.routes.includes("getPageWebhook")) {
  cy.fixture("GET/200_getPageWebhook.json")
    .then((data) => {
      data = merge(data, command.data["getPageWebhook"] || {});
      return data;
    })
    .as("fixture_getPageWebhook");
  cy.route({
    method: "GET",
    status: command.status["getPageWebhook"] || 200,
    url: "**/webhook/*/?*",
    response: "@fixture_getPageWebhook",
  }).as("getPageWebhook");
}

if (command.routes.includes("getPageById")) {
  cy.fixture("GET/200_getPageById.json")
    .then((data) => {
      data = merge(data, command.data["getPageById"] || {});
      return data;
    })
    .as("fixture_getPageById");
  cy.route({
    method: "GET",
    status: command.status["getPageById"] || 200,
    url: "**/domains/*/pages/*/?*",
    response: "@fixture_getPageById",
  }).as("getPageById");
}

if (command.routes.includes("getPageContent")) {
  cy.fixture("GET/200_getPageContent.json")
    .then((data) => {
      data = merge(data, command.data["getPageContent"] || {});
      return data;
    })
    .as("fixture_getPageContent");
  cy.route({
    method: "GET",
    status: command.status["getPageContent"] || 200,
    url: "**/page/*/query/?*",
    response: "@fixture_getPageContent",
  }).as("getPageContent");
}

if (command.routes.includes("getUserMetaData")) {
  cy.fixture("GET/200_getUserMetaData.json")
    .then((data) => {
      data = merge(data, command.data["getUserMetaData"] || {});
      return data;
    })
    .as("fixture_getUserMetaData");
  cy.route({
    method: "GET",
    status: command.status["getUserMetaData"] || 200,
    url: "**/users/*/meta/?*",
    response: "@fixture_getUserMetaData",
  }).as("getUserMetaData");
}

if (command.routes.includes("domainInvitations")) {
  cy.fixture("GET/200_domainInvitations.json")
    .then((data) => {
      data = merge(data, command.data["domainInvitations"] || {});
      return data;
    })
    .as("fixture_domainInvitations");
  cy.route({
    method: "GET",
    status: command.status["domainInvitations"] || 200,
    url: "**/domains/*/invitations/?is_complete=False",
    response: "@fixture_domainInvitations",
  }).as("domainInvitations");
}

if (command.routes.includes("userInvitations")) {
  cy.fixture("GET/200_userInvitations.json")
    .then((data) => {
      data = merge(data, command.data["userInvitations"] || {});
      return data;
    })
    .as("fixture_userInvitations");
  cy.route({
    method: "GET",
    status: command.status["userInvitations"] || 200,
    url: "**/users/self/invitations/?is_complete=False",
    response: "@fixture_userInvitations",
  }).as("userInvitations");
}

if (command.routes.includes("domainAccessLog")) {
  cy.fixture("GET/200_domainAccessLog.json")
    .then((data) => {
      data = merge(data, command.data["domainAccessLog"] || {});
      return data;
    })
    .as("fixture_domainAccessLog");
  cy.route({
    method: "GET",
    status: command.status["domainAccessLog"] || 200,
    url: "**/domain_access/*/events/?*",
    response: "@fixture_domainAccessLog",
  }).as("domainAccessLog");
}

if (command.routes.includes("domainUsers")) {
  cy.fixture("GET/200_domainUsers.json")
    .then((data) => {
      data = merge(data, command.data["domainUsers"] || {});
      return data;
    })
    .as("fixture_domainUsers");
  cy.route({
    method: "GET",
    status: command.status["domainUsers"] || 200,
    url: "**/domain_access/*/users/?*",
    response: "@fixture_domainUsers",
  }).as("domainUsers");
}

if (command.routes.includes("getInvitation")) {
  cy.fixture("GET/200_getInvitation.json")
    .then((data) => {
      data = merge(data, command.data["getInvitation"] || {});
      return data;
    })
    .as("fixture_getInvitation");
  cy.route({
    method: "GET",
    status: command.status["getInvitation"] || 200,
    url: "**/users/invitations/*/?*",
    response: "@fixture_getInvitation",
  }).as("getInvitation");
}

if (command.routes.includes("getDomainNotificationDestinations")) {
  cy.fixture("GET/200_getDomainNotificationDestinations.json")
    .then((data) => {
      data = merge(data, command.data["getDomainNotificationDestinations"] || {});
      return data;
    })
    .as("fixture_getDomainNotificationDestinations");
  cy.route({
    method: "GET",
    status: command.status["getDomainNotificationDestinations"] || 200,
    url: "**/notification/user/self/destinations/?*",
    response: "@fixture_getDomainNotificationDestinations",
  }).as("getDomainNotificationDestinations");
}

if (command.routes.includes("getNotificationHistory")) {
  cy.fixture("GET/200_getNotificationHistory.json")
    .then((data) => {
      data = merge(data, command.data["getNotificationHistory"] || {});
      return data;
    })
    .as("fixture_getNotificationHistory");
  cy.route({
    method: "GET",
    status: command.status["getNotificationHistory"] || 200,
    url: "**/organization/self/notification/history/?*",
    response: "@fixture_getNotificationHistory",
  }).as("getNotificationHistory");
}

if (command.routes.includes("getNotificationDestinations")) {
  cy.fixture("GET/200_getNotificationDestinations.json")
    .then((data) => {
      data = merge(data, command.data["getNotificationDestinations"] || {});
      return data;
    })
    .as("fixture_getNotificationDestinations");
  cy.route({
    method: "GET",
    status: command.status["getNotificationDestinations"] || 200,
    url: "**/organizations/self/notification/destinations/?*",
    response: "@fixture_getNotificationDestinations",
  }).as("getNotificationDestinations");
}

if (command.routes.includes("getNotificationDestination")) {
  cy.fixture("GET/200_getNotificationDestination.json")
    .then((data) => {
      data = merge(data, command.data["getNotificationDestination"] || {});
      return data;
    })
    .as("fixture_getNotificationDestination");
  cy.route({
    method: "GET",
    status: command.status["getNotificationDestination"] || 200,
    url: "**/notification/destination/*/?*",
    response: "@fixture_getNotificationDestination",
  }).as("getNotificationDestination");
}

if (command.routes.includes("getUserSymphonyStreams")) {
  cy.fixture("GET/200_getUserSymphonyStreams.json")
    .then((data) => {
      data = merge(data, command.data["getUserSymphonyStreams"] || {});
      return data;
    })
    .as("fixture_getUserSymphonyStreams");
  cy.route({
    method: "GET",
    status: command.status["getUserSymphonyStreams"] || 200,
    url: "**/notification/user/self/symphony_streams/?*",
    response: "@fixture_getUserSymphonyStreams",
  }).as("getUserSymphonyStreams");
}

if (command.routes.includes("getDomainAccessGroups")) {
  cy.fixture("GET/200_getDomainAccessGroups.json")
    .then((data) => {
      data = merge(data, command.data["getDomainAccessGroups"] || {});
      return data;
    })
    .as("fixture_getDomainAccessGroups");
  cy.route({
    method: "GET",
    status: command.status["getDomainAccessGroups"] || 200,
    url: "**/domains/*/access_groups/?*",
    response: "@fixture_getDomainAccessGroups",
  }).as("getDomainAccessGroups");
}

if (command.routes.includes("getDomainAccessGroup")) {
  cy.fixture("GET/200_getDomainAccessGroup.json")
    .then((data) => {
      data = merge(data, command.data["getDomainAccessGroup"] || {});
      return data;
    })
    .as("fixture_getDomainAccessGroup");
  cy.route({
    method: "GET",
    status: command.status["getDomainAccessGroup"] || 200,
    url: "**/domains/*/access_groups/*/?*",
    response: "@fixture_getDomainAccessGroup",
  }).as("getDomainAccessGroup");
}

if (command.routes.includes("getDomainPageAccess")) {
  cy.fixture("GET/200_getDomainPageAccess.json")
    .then((data) => {
      data = merge(data, command.data["getDomainPageAccess"] || {});
      return data;
    })
    .as("fixture_getDomainPageAccess");
  cy.route({
    method: "GET",
    status: command.status["getDomainPageAccess"] || 200,
    url: "**/domain_page_access/*/?*",
    response: "@fixture_getDomainPageAccess",
  }).as("getDomainPageAccess");
}

if (command.routes.includes("getDomainCustomers")) {
  cy.fixture("GET/200_getDomainCustomers.json")
    .then((data) => {
      data = merge(data, command.data["getDomainCustomers"] || {});
      return data;
    })
    .as("fixture_getDomainCustomers");
  cy.route({
    method: "GET",
    status: command.status["getDomainCustomers"] || 200,
    url: "**/domains/*/customers/?*",
    response: "@fixture_getDomainCustomers",
  }).as("getDomainCustomers");
}

if (command.routes.includes("getDomainUsage")) {
  cy.fixture("GET/200_getDomainUsage.json")
    .then((data) => {
      data = merge(data, command.data["getDomainUsage"] || {});
      return data;
    })
    .as("fixture_getDomainUsage");
  cy.route({
    method: "GET",
    status: command.status["getDomainUsage"] || 200,
    url: "**/domains/id/*/usage/?*",
    response: "@fixture_getDomainUsage",
  }).as("getDomainUsage");
}

if (command.routes.includes("getTemplates")) {
  cy.fixture("GET/200_getTemplates.json")
    .then((data) => {
      data = merge(data, command.data["getTemplates"] || {});
      return data;
    })
    .as("fixture_getTemplates");
  cy.route({
    method: "GET",
    status: command.status["getTemplates"] || 200,
    url: "**/templates/?*",
    response: "@fixture_getTemplates",
  }).as("getTemplates");
}

if (command.routes.includes("getApplicationPageList")) {
  cy.fixture("GET/200_getApplicationPageList.json")
    .then((data) => {
      data = merge(data, command.data["getApplicationPageList"] || {});
      return data;
    })
    .as("fixture_getApplicationPageList");
  cy.route({
    method: "GET",
    status: command.status["getApplicationPageList"] || 200,
    url: "**/application_page_list/?*",
    response: "@fixture_getApplicationPageList",
  }).as("getApplicationPageList");
}

if (command.routes.includes("getOrganization")) {
  cy.fixture("GET/200_getOrganization.json")
    .then((data) => {
      data = merge(data, command.data["getOrganization"] || {});
      return data;
    })
    .as("fixture_getOrganization");
  cy.route({
    method: "GET",
    status: command.status["getOrganization"] || 200,
    url: "**/organizations/*/?*",
    response: "@fixture_getOrganization",
  }).as("getOrganization");
}

if (command.routes.includes("getOrganizationMetaData")) {
  cy.fixture("GET/200_getOrganizationMetaData.json")
    .then((data) => {
      data = merge(data, command.data["getOrganizationMetaData"] || {});
      return data;
    })
    .as("fixture_getOrganizationMetaData");
  cy.route({
    method: "GET",
    status: command.status["getOrganizationMetaData"] || 200,
    url: "**/organizations/*/meta/?*",
    response: "@fixture_getOrganizationMetaData",
  }).as("getOrganizationMetaData");
}

if (command.routes.includes("getOrganizationUsers")) {
  cy.fixture("GET/200_getOrganizationUsers.json")
    .then((data) => {
      data = merge(data, command.data["getOrganizationUsers"] || {});
      return data;
    })
    .as("fixture_getOrganizationUsers");
  cy.route({
    method: "GET",
    status: command.status["getOrganizationUsers"] || 200,
    url: "**/organizations/*/users/?*",
    response: "@fixture_getOrganizationUsers",
  }).as("getOrganizationUsers");
}

if (command.routes.includes("getOrganizationLinkedUsers")) {
  cy.fixture("GET/200_getOrganizationLinkedUsers.json")
    .then((data) => {
      data = merge(data, command.data["getOrganizationLinkedUsers"] || {});
      return data;
    })
    .as("fixture_getOrganizationLinkedUsers");
  cy.route({
    method: "GET",
    status: command.status["getOrganizationLinkedUsers"] || 200,
    url: "**/organizations/*/users/linked/?*",
    response: "@fixture_getOrganizationLinkedUsers",
  }).as("getOrganizationLinkedUsers");
}

if (command.routes.includes("getOrganizationUsage")) {
  cy.fixture("GET/200_getOrganizationUsage.json")
    .then((data) => {
      data = merge(data, command.data["getOrganizationUsage"] || {});
      return data;
    })
    .as("fixture_getOrganizationUsage");
  cy.route({
    method: "GET",
    status: command.status["getOrganizationUsage"] || 200,
    url: "**/organizations/self/usage/?*",
    response: "@fixture_getOrganizationUsage",
  }).as("getOrganizationUsage");
}

if (command.routes.includes("getOrganizationUser")) {
  cy.fixture("GET/200_getOrganizationUser.json")
    .then((data) => {
      data = merge(data, command.data["getOrganizationUser"] || {});
      return data;
    })
    .as("fixture_getOrganizationUser");
  cy.route({
    method: "GET",
    status: command.status["getOrganizationUser"] || 200,
    url: "**/organizations/*/users/*/?*",
    response: "@fixture_getOrganizationUser",
  }).as("getOrganizationUser");
}

if (command.routes.includes("getOrganizationDomains")) {
  cy.fixture("GET/200_getOrganizationDomains.json")
    .then((data) => {
      data = merge(data, command.data["getOrganizationDomains"] || {});
      return data;
    })
    .as("fixture_getOrganizationDomains");
  cy.route({
    method: "GET",
    status: command.status["getOrganizationDomains"] || 200,
    url: "**/organizations/*/domains/?*",
    response: "@fixture_getOrganizationDomains",
  }).as("getOrganizationDomains");
}

if (command.routes.includes("getSsoStatus")) {
  cy.fixture("GET/200_getSsoStatus.json")
    .then((data) => {
      data = merge(data, command.data["getSsoStatus"] || {});
      return data;
    })
    .as("fixture_getSsoStatus");
  cy.route({
    method: "GET",
    status: command.status["getSsoStatus"] || 200,
    url: "**/sso/status/?*",
    response: "@fixture_getSsoStatus",
  }).as("getSsoStatus");
}

if (command.routes.includes("downloadPage")) {
  cy.fixture("GET/200_downloadPage.json")
    .then((data) => {
      data = merge(data, command.data["downloadPage"] || {});
      return data;
    })
    .as("fixture_downloadPage");
  cy.route({
    method: "GET",
    status: command.status["downloadPage"] || 200,
    url: "**https://test.ipushpull.com/api/page/*/download/?*",
    response: "@fixture_downloadPage",
  }).as("downloadPage");
}

if (command.routes.includes("getTokens")) {
  cy.fixture("GET/200_getTokens.json")
    .then((data) => {
      data = merge(data, command.data["getTokens"] || {});
      return data;
    })
    .as("fixture_getTokens");
  cy.route({
    method: "GET",
    status: command.status["getTokens"] || 200,
    url: "**/credentials/client_app_access/token/self/?*",
    response: "@fixture_getTokens",
  }).as("getTokens");
}

if (command.routes.includes("getSchemas")) {
  cy.fixture("GET/200_getSchemas.json")
    .then((data) => {
      data = merge(data, command.data["getSchemas"] || {});
      return data;
    })
    .as("fixture_getSchemas");
  cy.route({
    method: "GET",
    status: command.status["getSchemas"] || 200,
    url: "**/organizations/self/page_schemas?*",
    response: "@fixture_getSchemas",
  }).as("getSchemas");
}

if (command.routes.includes("getSchema")) {
  cy.fixture("GET/200_getSchema.json")
    .then((data) => {
      data = merge(data, command.data["getSchema"] || {});
      return data;
    })
    .as("fixture_getSchema");
  cy.route({
    method: "GET",
    status: command.status["getSchema"] || 200,
    url: "**/page_schemas/*?*",
    response: "@fixture_getSchema",
  }).as("getSchema");
}

if (command.routes.includes("getUserDelegates")) {
  cy.fixture("GET/200_getUserDelegates.json")
    .then((data) => {
      data = merge(data, command.data["getUserDelegates"] || {});
      return data;
    })
    .as("fixture_getUserDelegates");
  cy.route({
    method: "GET",
    status: command.status["getUserDelegates"] || 200,
    url: "**/organizations/self/user_delegates?*",
    response: "@fixture_getUserDelegates",
  }).as("getUserDelegates");
}

if (command.routes.includes("getUserDelegate")) {
  cy.fixture("GET/200_getUserDelegate.json")
    .then((data) => {
      data = merge(data, command.data["getUserDelegate"] || {});
      return data;
    })
    .as("fixture_getUserDelegate");
  cy.route({
    method: "GET",
    status: command.status["getUserDelegate"] || 200,
    url: "**/user_delegate/*?*",
    response: "@fixture_getUserDelegate",
  }).as("getUserDelegate");
}

if (command.routes.includes("getConnectedHistory")) {
  cy.fixture("GET/200_getConnectedHistory.json")
    .then((data) => {
      data = merge(data, command.data["getConnectedHistory"] || {});
      return data;
    })
    .as("fixture_getConnectedHistory");
  cy.route({
    method: "GET",
    status: command.status["getConnectedHistory"] || 200,
    url: "**/connected_page/message_history/page_id/*/?*",
    response: "@fixture_getConnectedHistory",
  }).as("getConnectedHistory");
}

if (command.routes.includes("getWorkflowTypes")) {
  cy.fixture("GET/200_getWorkflowTypes.json")
    .then((data) => {
      data = merge(data, command.data["getWorkflowTypes"] || {});
      return data;
    })
    .as("fixture_getWorkflowTypes");
  cy.route({
    method: "GET",
    status: command.status["getWorkflowTypes"] || 200,
    url: "**/organizations/self/workflow_types/?*",
    response: "@fixture_getWorkflowTypes",
  }).as("getWorkflowTypes");
}

if (command.routes.includes("getReportsClientAppUsageStats")) {
  cy.fixture("GET/200_getReportsClientAppUsageStats.json")
    .then((data) => {
      data = merge(data, command.data["getReportsClientAppUsageStats"] || {});
      return data;
    })
    .as("fixture_getReportsClientAppUsageStats");
  cy.route({
    method: "GET",
    status: command.status["getReportsClientAppUsageStats"] || 200,
    url: "**/organizations/self/reports/client_app_usage/?*",
    response: "@fixture_getReportsClientAppUsageStats",
  }).as("getReportsClientAppUsageStats");
}

if (command.routes.includes("getReportsUsageStats")) {
  cy.fixture("GET/200_getReportsUsageStats.json")
    .then((data) => {
      data = merge(data, command.data["getReportsUsageStats"] || {});
      return data;
    })
    .as("fixture_getReportsUsageStats");
  cy.route({
    method: "GET",
    status: command.status["getReportsUsageStats"] || 200,
    url: "**/organizations/all/reports/usage_stats/?*",
    response: "@fixture_getReportsUsageStats",
  }).as("getReportsUsageStats");
}
;
});
