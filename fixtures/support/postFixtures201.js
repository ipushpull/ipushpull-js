const merge = require("deepmerge");

Cypress.Commands.add("postFixtures201", (command) => {
  if (!command.data) {
    command.data = {};
  }
  if (!command.status) {
    command.status = {};
  }
  cy.server();
  if (command.routes.includes("createPageNotification")) {
  cy.fixture("POST/201_createPageNotification.json")
    .then((data) => {
      data = merge(data, command.data["createPageNotification"] || {});
      return data;
    })
    .as("fixture_createPageNotification");
  cy.route({
    method: "POST",
    status: command.status["createPageNotification"] || 201,
    url: "**/page/*/notification/?*",
    response: "@fixture_createPageNotification",
  }).as("createPageNotification");
}

if (command.routes.includes("createPageContentNotification")) {
  cy.fixture("POST/201_createPageContentNotification.json")
    .then((data) => {
      data = merge(data, command.data["createPageContentNotification"] || {});
      return data;
    })
    .as("fixture_createPageContentNotification");
  cy.route({
    method: "POST",
    status: command.status["createPageContentNotification"] || 201,
    url: "**/page/*/content_notification/?*",
    response: "@fixture_createPageContentNotification",
  }).as("createPageContentNotification");
}

if (command.routes.includes("resetPassword")) {
  cy.fixture("POST/201_resetPassword.json")
    .then((data) => {
      data = merge(data, command.data["resetPassword"] || {});
      return data;
    })
    .as("fixture_resetPassword");
  cy.route({
    method: "POST",
    status: command.status["resetPassword"] || 201,
    url: "**/password_reset/confirm/?*",
    response: "@fixture_resetPassword",
  }).as("resetPassword");
}

if (command.routes.includes("createNotification")) {
  cy.fixture("POST/201_createNotification.json")
    .then((data) => {
      data = merge(data, command.data["createNotification"] || {});
      return data;
    })
    .as("fixture_createNotification");
  cy.route({
    method: "POST",
    status: command.status["createNotification"] || 201,
    url: "**/organizations/self/notification/?*",
    response: "@fixture_createNotification",
  }).as("createNotification");
}
;
});
