const merge = require("deepmerge");

Cypress.Commands.add("postFixtures400", (command) => {
  if (!command.data) {
    command.data = {};
  }
  if (!command.status) {
    command.status = {};
  }
  cy.server();
  if (command.routes.includes("createIntegrationChannel")) {
  cy.fixture("POST/400_createIntegrationChannel.json")
    .then((data) => {
      data = merge(data, command.data["createIntegrationChannel"] || {});
      return data;
    })
    .as("fixture_createIntegrationChannel");
  cy.route({
    method: "POST",
    status: command.status["createIntegrationChannel"] || 400,
    url: "**/organizations/self/integration_channels/?*",
    response: "@fixture_createIntegrationChannel",
  }).as("createIntegrationChannel");
}

if (command.routes.includes("createCounterparty")) {
  cy.fixture("POST/400_createCounterparty.json")
    .then((data) => {
      data = merge(data, command.data["createCounterparty"] || {});
      return data;
    })
    .as("fixture_createCounterparty");
  cy.route({
    method: "POST",
    status: command.status["createCounterparty"] || 400,
    url: "**/organizations/self/counterparties/?*",
    response: "@fixture_createCounterparty",
  }).as("createCounterparty");
}

if (command.routes.includes("createFolder")) {
  cy.fixture("POST/400_createFolder.json")
    .then((data) => {
      data = merge(data, command.data["createFolder"] || {});
      return data;
    })
    .as("fixture_createFolder");
  cy.route({
    method: "POST",
    status: command.status["createFolder"] || 400,
    url: "**/domains/?*",
    response: "@fixture_createFolder",
  }).as("createFolder");
}

if (command.routes.includes("createDomain")) {
  cy.fixture("POST/400_createDomain.json")
    .then((data) => {
      data = merge(data, command.data["createDomain"] || {});
      return data;
    })
    .as("fixture_createDomain");
  cy.route({
    method: "POST",
    status: command.status["createDomain"] || 400,
    url: "**/domains/?*",
    response: "@fixture_createDomain",
  }).as("createDomain");
}

if (command.routes.includes("createPageWebhook")) {
  cy.fixture("POST/400_createPageWebhook.json")
    .then((data) => {
      data = merge(data, command.data["createPageWebhook"] || {});
      return data;
    })
    .as("fixture_createPageWebhook");
  cy.route({
    method: "POST",
    status: command.status["createPageWebhook"] || 400,
    url: "**/page/*/webhooks/?*",
    response: "@fixture_createPageWebhook",
  }).as("createPageWebhook");
}

if (command.routes.includes("createPage")) {
  cy.fixture("POST/400_createPage.json")
    .then((data) => {
      data = merge(data, command.data["createPage"] || {});
      return data;
    })
    .as("fixture_createPage");
  cy.route({
    method: "POST",
    status: command.status["createPage"] || 400,
    url: "**/domains/*/pages/?*",
    response: "@fixture_createPage",
  }).as("createPage");
}

if (command.routes.includes("createPageNotification")) {
  cy.fixture("POST/400_createPageNotification.json")
    .then((data) => {
      data = merge(data, command.data["createPageNotification"] || {});
      return data;
    })
    .as("fixture_createPageNotification");
  cy.route({
    method: "POST",
    status: command.status["createPageNotification"] || 400,
    url: "**/page/*/notification/?*",
    response: "@fixture_createPageNotification",
  }).as("createPageNotification");
}

if (command.routes.includes("createPageContentNotification")) {
  cy.fixture("POST/400_createPageContentNotification.json")
    .then((data) => {
      data = merge(data, command.data["createPageContentNotification"] || {});
      return data;
    })
    .as("fixture_createPageContentNotification");
  cy.route({
    method: "POST",
    status: command.status["createPageContentNotification"] || 400,
    url: "**/page/*/content_notification/?*",
    response: "@fixture_createPageContentNotification",
  }).as("createPageContentNotification");
}

if (command.routes.includes("resetPassword")) {
  cy.fixture("POST/400_resetPassword.json")
    .then((data) => {
      data = merge(data, command.data["resetPassword"] || {});
      return data;
    })
    .as("fixture_resetPassword");
  cy.route({
    method: "POST",
    status: command.status["resetPassword"] || 400,
    url: "**/password_reset/confirm/?*",
    response: "@fixture_resetPassword",
  }).as("resetPassword");
}

if (command.routes.includes("inviteUsers")) {
  cy.fixture("POST/400_inviteUsers.json")
    .then((data) => {
      data = merge(data, command.data["inviteUsers"] || {});
      return data;
    })
    .as("fixture_inviteUsers");
  cy.route({
    method: "POST",
    status: command.status["inviteUsers"] || 400,
    url: "**/domains/*/invitations/?*",
    response: "@fixture_inviteUsers",
  }).as("inviteUsers");
}

if (command.routes.includes("acceptInvitation")) {
  cy.fixture("POST/400_acceptInvitation.json")
    .then((data) => {
      data = merge(data, command.data["acceptInvitation"] || {});
      return data;
    })
    .as("fixture_acceptInvitation");
  cy.route({
    method: "POST",
    status: command.status["acceptInvitation"] || 400,
    url: "**/users/invitation/confirm/?*",
    response: "@fixture_acceptInvitation",
  }).as("acceptInvitation");
}

if (command.routes.includes("createDomainNotificationDestination")) {
  cy.fixture("POST/400_createDomainNotificationDestination.json")
    .then((data) => {
      data = merge(data, command.data["createDomainNotificationDestination"] || {});
      return data;
    })
    .as("fixture_createDomainNotificationDestination");
  cy.route({
    method: "POST",
    status: command.status["createDomainNotificationDestination"] || 400,
    url: "**/notification/user/self/destinations/?*",
    response: "@fixture_createDomainNotificationDestination",
  }).as("createDomainNotificationDestination");
}

if (command.routes.includes("createNotificationDestination")) {
  cy.fixture("POST/400_createNotificationDestination.json")
    .then((data) => {
      data = merge(data, command.data["createNotificationDestination"] || {});
      return data;
    })
    .as("fixture_createNotificationDestination");
  cy.route({
    method: "POST",
    status: command.status["createNotificationDestination"] || 400,
    url: "**/organizations/self/notification/destinations/?*",
    response: "@fixture_createNotificationDestination",
  }).as("createNotificationDestination");
}

if (command.routes.includes("createNotification")) {
  cy.fixture("POST/400_createNotification.json")
    .then((data) => {
      data = merge(data, command.data["createNotification"] || {});
      return data;
    })
    .as("fixture_createNotification");
  cy.route({
    method: "POST",
    status: command.status["createNotification"] || 400,
    url: "**/organizations/self/notification/?*",
    response: "@fixture_createNotification",
  }).as("createNotification");
}

if (command.routes.includes("addDomainAccessGroup")) {
  cy.fixture("POST/400_addDomainAccessGroup.json")
    .then((data) => {
      data = merge(data, command.data["addDomainAccessGroup"] || {});
      return data;
    })
    .as("fixture_addDomainAccessGroup");
  cy.route({
    method: "POST",
    status: command.status["addDomainAccessGroup"] || 400,
    url: "**/domains/*/access_groups/?*",
    response: "@fixture_addDomainAccessGroup",
  }).as("addDomainAccessGroup");
}

if (command.routes.includes("putDomainAgroupMembers")) {
  cy.fixture("POST/400_putDomainAgroupMembers.json")
    .then((data) => {
      data = merge(data, command.data["putDomainAgroupMembers"] || {});
      return data;
    })
    .as("fixture_putDomainAgroupMembers");
  cy.route({
    method: "POST",
    status: command.status["putDomainAgroupMembers"] || 400,
    url: "**/domains/*/access_groups/*/members/?*",
    response: "@fixture_putDomainAgroupMembers",
  }).as("putDomainAgroupMembers");
}

if (command.routes.includes("putDomainAgroupPages")) {
  cy.fixture("POST/400_putDomainAgroupPages.json")
    .then((data) => {
      data = merge(data, command.data["putDomainAgroupPages"] || {});
      return data;
    })
    .as("fixture_putDomainAgroupPages");
  cy.route({
    method: "POST",
    status: command.status["putDomainAgroupPages"] || 400,
    url: "**/domains/*/access_groups/*/pages/?*",
    response: "@fixture_putDomainAgroupPages",
  }).as("putDomainAgroupPages");
}

if (command.routes.includes("createOrganizationUser")) {
  cy.fixture("POST/400_createOrganizationUser.json")
    .then((data) => {
      data = merge(data, command.data["createOrganizationUser"] || {});
      return data;
    })
    .as("fixture_createOrganizationUser");
  cy.route({
    method: "POST",
    status: command.status["createOrganizationUser"] || 400,
    url: "**/organizations/*/users/?*",
    response: "@fixture_createOrganizationUser",
  }).as("createOrganizationUser");
}

if (command.routes.includes("createSchema")) {
  cy.fixture("POST/400_createSchema.json")
    .then((data) => {
      data = merge(data, command.data["createSchema"] || {});
      return data;
    })
    .as("fixture_createSchema");
  cy.route({
    method: "POST",
    status: command.status["createSchema"] || 400,
    url: "**/organizations/self/page_schemas/?*",
    response: "@fixture_createSchema",
  }).as("createSchema");
}

if (command.routes.includes("createUserDelegate")) {
  cy.fixture("POST/400_createUserDelegate.json")
    .then((data) => {
      data = merge(data, command.data["createUserDelegate"] || {});
      return data;
    })
    .as("fixture_createUserDelegate");
  cy.route({
    method: "POST",
    status: command.status["createUserDelegate"] || 400,
    url: "**/organizations/self/user_delegates/?*",
    response: "@fixture_createUserDelegate",
  }).as("createUserDelegate");
}
;
});
