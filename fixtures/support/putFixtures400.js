const merge = require("deepmerge");

Cypress.Commands.add("putFixtures400", (command) => {
  if (!command.data) {
    command.data = {};
  }
  if (!command.status) {
    command.status = {};
  }
  cy.server();
  if (command.routes.includes("saveIntegrationChannel")) {
  cy.fixture("PUT/400_saveIntegrationChannel.json")
    .then((data) => {
      data = merge(data, command.data["saveIntegrationChannel"] || {});
      return data;
    })
    .as("fixture_saveIntegrationChannel");
  cy.route({
    method: "PUT",
    status: command.status["saveIntegrationChannel"] || 400,
    url: "**/integration_channels/*/?*",
    response: "@fixture_saveIntegrationChannel",
  }).as("saveIntegrationChannel");
}

if (command.routes.includes("updateCounterparty")) {
  cy.fixture("PUT/400_updateCounterparty.json")
    .then((data) => {
      data = merge(data, command.data["updateCounterparty"] || {});
      return data;
    })
    .as("fixture_updateCounterparty");
  cy.route({
    method: "PUT",
    status: command.status["updateCounterparty"] || 400,
    url: "**/counterparty/*?",
    response: "@fixture_updateCounterparty",
  }).as("updateCounterparty");
}

if (command.routes.includes("updateDomain")) {
  cy.fixture("PUT/400_updateDomain.json")
    .then((data) => {
      data = merge(data, command.data["updateDomain"] || {});
      return data;
    })
    .as("fixture_updateDomain");
  cy.route({
    method: "PUT",
    status: command.status["updateDomain"] || 400,
    url: "**/domains/*/?*",
    response: "@fixture_updateDomain",
  }).as("updateDomain");
}

if (command.routes.includes("restorePageVerion")) {
  cy.fixture("PUT/400_restorePageVerion.json")
    .then((data) => {
      data = merge(data, command.data["restorePageVerion"] || {});
      return data;
    })
    .as("fixture_restorePageVerion");
  cy.route({
    method: "PUT",
    status: command.status["restorePageVerion"] || 400,
    url: "**/page/id/*/version/*/restore/?*",
    response: "@fixture_restorePageVerion",
  }).as("restorePageVerion");
}

if (command.routes.includes("savePageWebhook")) {
  cy.fixture("PUT/400_savePageWebhook.json")
    .then((data) => {
      data = merge(data, command.data["savePageWebhook"] || {});
      return data;
    })
    .as("fixture_savePageWebhook");
  cy.route({
    method: "PUT",
    status: command.status["savePageWebhook"] || 400,
    url: "**/webhook/*/?*",
    response: "@fixture_savePageWebhook",
  }).as("savePageWebhook");
}

if (command.routes.includes("queryPageContent")) {
  cy.fixture("PUT/400_queryPageContent.json")
    .then((data) => {
      data = merge(data, command.data["queryPageContent"] || {});
      return data;
    })
    .as("fixture_queryPageContent");
  cy.route({
    method: "PUT",
    status: command.status["queryPageContent"] || 400,
    url: "**/page/*/query/?*",
    response: "@fixture_queryPageContent",
  }).as("queryPageContent");
}

if (command.routes.includes("savePageContent")) {
  cy.fixture("PUT/400_savePageContent.json")
    .then((data) => {
      data = merge(data, command.data["savePageContent"] || {});
      return data;
    })
    .as("fixture_savePageContent");
  cy.route({
    method: "PUT",
    status: command.status["savePageContent"] || 400,
    url: "**/domains/id/*/page_content/id/*/?*",
    response: "@fixture_savePageContent",
  }).as("savePageContent");
}

if (command.routes.includes("savePageContentDelta")) {
  cy.fixture("PUT/400_savePageContentDelta.json")
    .then((data) => {
      data = merge(data, command.data["savePageContentDelta"] || {});
      return data;
    })
    .as("fixture_savePageContentDelta");
  cy.route({
    method: "PUT",
    status: command.status["savePageContentDelta"] || 400,
    url: "**/domains/id/*/page_content_delta/id/*/?*",
    response: "@fixture_savePageContentDelta",
  }).as("savePageContentDelta");
}

if (command.routes.includes("savePageSettings")) {
  cy.fixture("PUT/400_savePageSettings.json")
    .then((data) => {
      data = merge(data, command.data["savePageSettings"] || {});
      return data;
    })
    .as("fixture_savePageSettings");
  cy.route({
    method: "PUT",
    status: command.status["savePageSettings"] || 400,
    url: "**/domains/*/pages/*/?*",
    response: "@fixture_savePageSettings",
  }).as("savePageSettings");
}

if (command.routes.includes("saveUserInfo")) {
  cy.fixture("PUT/400_saveUserInfo.json")
    .then((data) => {
      data = merge(data, command.data["saveUserInfo"] || {});
      return data;
    })
    .as("fixture_saveUserInfo");
  cy.route({
    method: "PUT",
    status: command.status["saveUserInfo"] || 400,
    url: "**/users/self/?*",
    response: "@fixture_saveUserInfo",
  }).as("saveUserInfo");
}

if (command.routes.includes("saveUserMetaData")) {
  cy.fixture("PUT/400_saveUserMetaData.json")
    .then((data) => {
      data = merge(data, command.data["saveUserMetaData"] || {});
      return data;
    })
    .as("fixture_saveUserMetaData");
  cy.route({
    method: "PUT",
    status: command.status["saveUserMetaData"] || 400,
    url: "**/users/*/meta/?*",
    response: "@fixture_saveUserMetaData",
  }).as("saveUserMetaData");
}

if (command.routes.includes("changePassword")) {
  cy.fixture("PUT/400_changePassword.json")
    .then((data) => {
      data = merge(data, command.data["changePassword"] || {});
      return data;
    })
    .as("fixture_changePassword");
  cy.route({
    method: "PUT",
    status: command.status["changePassword"] || 400,
    url: "**/credentials/self/?*",
    response: "@fixture_changePassword",
  }).as("changePassword");
}

if (command.routes.includes("changeEmail")) {
  cy.fixture("PUT/400_changeEmail.json")
    .then((data) => {
      data = merge(data, command.data["changeEmail"] || {});
      return data;
    })
    .as("fixture_changeEmail");
  cy.route({
    method: "PUT",
    status: command.status["changeEmail"] || 400,
    url: "**/credentials/self/?*",
    response: "@fixture_changeEmail",
  }).as("changeEmail");
}

if (command.routes.includes("setDomainDefault")) {
  cy.fixture("PUT/400_setDomainDefault.json")
    .then((data) => {
      data = merge(data, command.data["setDomainDefault"] || {});
      return data;
    })
    .as("fixture_setDomainDefault");
  cy.route({
    method: "PUT",
    status: command.status["setDomainDefault"] || 400,
    url: "**/domain_access/*/users/self/?*",
    response: "@fixture_setDomainDefault",
  }).as("setDomainDefault");
}

if (command.routes.includes("updateDomainAccess")) {
  cy.fixture("PUT/400_updateDomainAccess.json")
    .then((data) => {
      data = merge(data, command.data["updateDomainAccess"] || {});
      return data;
    })
    .as("fixture_updateDomainAccess");
  cy.route({
    method: "PUT",
    status: command.status["updateDomainAccess"] || 400,
    url: "**/domain_access/*/users/?*",
    response: "@fixture_updateDomainAccess",
  }).as("updateDomainAccess");
}

if (command.routes.includes("saveNotificationDestination")) {
  cy.fixture("PUT/400_saveNotificationDestination.json")
    .then((data) => {
      data = merge(data, command.data["saveNotificationDestination"] || {});
      return data;
    })
    .as("fixture_saveNotificationDestination");
  cy.route({
    method: "PUT",
    status: command.status["saveNotificationDestination"] || 400,
    url: "**/notification/destination/*/?*",
    response: "@fixture_saveNotificationDestination",
  }).as("saveNotificationDestination");
}

if (command.routes.includes("updateDomainAgroup")) {
  cy.fixture("PUT/400_updateDomainAgroup.json")
    .then((data) => {
      data = merge(data, command.data["updateDomainAgroup"] || {});
      return data;
    })
    .as("fixture_updateDomainAgroup");
  cy.route({
    method: "PUT",
    status: command.status["updateDomainAgroup"] || 400,
    url: "**/domains/*/access_groups/*/?*",
    response: "@fixture_updateDomainAgroup",
  }).as("updateDomainAgroup");
}

if (command.routes.includes("saveDomainPageAccess")) {
  cy.fixture("PUT/400_saveDomainPageAccess.json")
    .then((data) => {
      data = merge(data, command.data["saveDomainPageAccess"] || {});
      return data;
    })
    .as("fixture_saveDomainPageAccess");
  cy.route({
    method: "PUT",
    status: command.status["saveDomainPageAccess"] || 400,
    url: "**/domain_page_access/*/basic/?*",
    response: "@fixture_saveDomainPageAccess",
  }).as("saveDomainPageAccess");
}

if (command.routes.includes("saveOrganizationMetaData")) {
  cy.fixture("PUT/400_saveOrganizationMetaData.json")
    .then((data) => {
      data = merge(data, command.data["saveOrganizationMetaData"] || {});
      return data;
    })
    .as("fixture_saveOrganizationMetaData");
  cy.route({
    method: "PUT",
    status: command.status["saveOrganizationMetaData"] || 400,
    url: "**/organizations/*/meta/?*",
    response: "@fixture_saveOrganizationMetaData",
  }).as("saveOrganizationMetaData");
}

if (command.routes.includes("saveOrganizationUser")) {
  cy.fixture("PUT/400_saveOrganizationUser.json")
    .then((data) => {
      data = merge(data, command.data["saveOrganizationUser"] || {});
      return data;
    })
    .as("fixture_saveOrganizationUser");
  cy.route({
    method: "PUT",
    status: command.status["saveOrganizationUser"] || 400,
    url: "**/organizations/*/users/*/?*",
    response: "@fixture_saveOrganizationUser",
  }).as("saveOrganizationUser");
}

if (command.routes.includes("updateSchema")) {
  cy.fixture("PUT/400_updateSchema.json")
    .then((data) => {
      data = merge(data, command.data["updateSchema"] || {});
      return data;
    })
    .as("fixture_updateSchema");
  cy.route({
    method: "PUT",
    status: command.status["updateSchema"] || 400,
    url: "**/page_schemas/*?",
    response: "@fixture_updateSchema",
  }).as("updateSchema");
}

if (command.routes.includes("updateUserDelegate")) {
  cy.fixture("PUT/400_updateUserDelegate.json")
    .then((data) => {
      data = merge(data, command.data["updateUserDelegate"] || {});
      return data;
    })
    .as("fixture_updateUserDelegate");
  cy.route({
    method: "PUT",
    status: command.status["updateUserDelegate"] || 400,
    url: "**/user_delegate/*?",
    response: "@fixture_updateUserDelegate",
  }).as("updateUserDelegate");
}
;
});
