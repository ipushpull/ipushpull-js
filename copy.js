var ncp = require("ncp").ncp;

const paths = [
  {
    source: "./build",
    dest: "../ipushpull-components/node_modules/ipushpull-js/build",
  },
  {
    source: "./dist",
    dest: "../ipushpull-components/node_modules/ipushpull-js/dist",
  },
  {
    source: "./build",
    dest: "../ipp-auth/node_modules/ipushpull-js/build",
  },
  {
    source: "./dist",
    dest: "../ipp-auth/node_modules/ipushpull-js/dist",
  },
  {
    source: "./dist",
    dest: "../ipp-reports2/node_modules/ipushpull-js/dist",
  },
];

paths.forEach((path) => {
  ncp(path.source, path.dest, function (err) {
    if (err) {
      return console.error(err);
    }
    console.log("done!");
  });
});
