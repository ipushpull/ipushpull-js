# ipushpull javascript api

## Install

`npm install ipushpull-js`

## Usage

```
import ipushpull from 'ipushpull-js';
import { IPageService } from 'ipushpull-js/dist/Page/Page';

ipushpull.config.set({
    api_url: "https://test.ipushpull.com/api",
    api_version: "2.0",
    ws_url: "https://test.ipushpull.com",
    web_url: "https://test.ipushpull.com",
    docs_url: "https://docs.ipushpull.com",
    storage_prefix: "ipp_local",
    api_key: "???",
    api_secret: "???",
    transport: "polling",
    hsts: false, // strict cors policy
});

let page: IPageService = new ipushpull.Page([page id|name], [folder id|name]);

page.on(page.EVENT_READY, () => {
    console.log("EVENT_READY");
    console.log("page data", page.data);
    console.log("page content", page.Content.current);
});

page.on(page.EVENT_NEW_CONTENT, (data) => {
    console.log("EVENT_NEW_CONTENT", data);
    console.log("EVENT_NEW_CONTENT", data.diff);
});
```

## Demo

See ```./build/index.html```

## Development

```
webpack --watch --config webpack.dev.js
```

## Build

```
tsc
npm run prod
```

## Documentation 

```
# Install the global CLI
npm install --global typedoc

#Execute typedoc on your project
typedoc --out docs ./src
```