const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  name: 'ipushpull',
  mode: 'development',
  devtool: 'source-map',
  output: {
    filename: 'ipushpull.js',
  }
});
