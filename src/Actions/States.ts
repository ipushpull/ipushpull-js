import { IGridState } from '../Grid4/Grid';

export interface IActionsState {
  name: string;
  state: IGridState;
  parse(data: any): void;
  [key: string]: any;
}

export class ActionState implements IActionsState {
  public name: string = '';
  public state!: IGridState;
  constructor(data?: any) {
    this.parse(data || {});
  }
  public parse(data: any): void {
    ['name', 'state'].forEach(key => {
      if (!data[key]) return;
      (<IActionsState>this)[key] = data[key];
    });
  }
}
